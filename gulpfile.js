var elixir = require('laravel-elixir');
var gulp = require('gulp');
var gulpBowerFiles = require('main-bower-files');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.extend('bower', function(mix) {
	return gulp.src(gulpBowerFiles()).pipe(gulp.dest('public/libs'));
});


//----------------------------------------------------------------------- Application
elixir.extend('application', function(mix) {
	mix.styles([
		'libs/foundation.css',
		'libs/foundation-apps.css',
		'css/app.css',
  		'../node_modules/guillotine/css/jquery.guillotine.css',
		'components/sweetalert/dist/sweetalert.css'
	], 'public/output/all_app.css', 'public/')
		.scripts([

			'libs/angular.js',
			'libs/foundation-apps.js',
			'components/foundation-apps/dist/js/foundation-apps-templates.js',
			'libs/angular-animate.js',
			'libs/angular-resource.js',
			'libs/angular-route.js',
			'libs/angular-ui-router.js',
			'components/angular-ui-tree/dist/angular-ui-tree.min.js',
			'libs/ng-file-upload.js',
			'libs/fastclick.js',
			'libs/TweenMax.js',
			'application/app.js',
			'application/services.js',
			'application/sectionController.js',
			'application/bannerController.js',
			'application/controllers.js',
			'components/jquery/dist/jquery.js',
			'components/foundation/js/foundation.min.js',
			'../node_modules/guillotine/js/jquery.guillotine.js',
			'components/angular-foundation/mm-foundation.min.js',
			'components/angular-foundation/mm-foundation-tpls.min.js',
			'components/sweetalert/dist/sweetalert.min.js'
		], 'public/output/all_app.js', 'public/');
});


//----------------------------------------------------------------------- Website
elixir.extend('website', function(mix) {
    mix.styles([
        'libs/foundation.css',
        'css/site.css'
    ], 'public/output/all.css', 'public')
        .scripts([
            'libs/jquery.js',
            'libs/foundation.js',
            'application/site.js'
        ], 'public/output/all.js', 'public');
});
elixir.extend('utilitarios', function(mix) {
    mix.styles([
        'components/swiper/dist/css/swiper.min.css',
       'components/sweetalert/dist/sweetalert.css',
        'components/medium-editor/dist/css/medium-editor.css',
        'components/medium-editor/dist/css/themes/default.css',
        'public/output/bootstrap-notify.css'


    ], 'public/output/utils.css', 'public/')
    .scripts([
            'components/sweetalert/dist/sweetalert.min.js',
            'components/swiper/dist/js/swiper.min.js',
            'components/swiper/dist/js/swiper.jquery.min.js',
            'components/swiper/dist/js/swiper.jquery.umd.min.js',
            'components/Sortable/Sortable.min.js',
            'components/medium-editor/dist/js/medium-editor.js',
            'components/lite-uploader/liteuploader.js',
            'public/output/bootstrap-notify.js'


        ], 'public/output/utils.js', 'public');
});
elixir(function(mix) {
	mix.less(['app.less']);
	mix.bower(mix);
	mix.application(mix);
	mix.utilitarios(mix);
});