<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class OrdersUsers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('page_page', function(Blueprint $table){
			$table->smallInteger('weight')->default(0);
		});

		Schema::table('multimedia', function(Blueprint $table){
			$table->smallInteger('weight')->default(0);
		});

		Schema::table('magazines', function(Blueprint $table){
			$table->smallInteger('weight')->default(0);
		});

		Schema::table('users', function(Blueprint $table){
			$table->string('last_name')->nullable();
			$table->string('country')->nullable();
			$table->smallInteger('country_knowledge')->default(0);
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
