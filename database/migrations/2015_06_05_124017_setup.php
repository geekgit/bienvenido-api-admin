<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class Setup extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contents', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('code');
			$table->string('lang')->default('es');
			$table->text('content')->nullable();
			$table->integer('is_html')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});

		Schema::create('multimedia', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('description')->nullable();
			$table->string('code');
			$table->string('source');
			$table->string('type')->default('image');
			$table->timestamps();
			$table->softDeletes();
		});
		Schema::create('contenteables', function(Blueprint $table)
		{
			$table->integer('content_id')->unsigned()->nullable();
			$table->foreign('content_id')->references('id')->on('contents')->onDelete('CASCADE');
			$table->integer('contenteable_id')->unsigned()->nullable();
			$table->string('contenteable_type');
		});

		Schema::create('multimediables', function(Blueprint $table)
		{
			$table->integer('multimedia_id')->unsigned()->nullable();
			$table->foreign('multimedia_id')->references('id')->on('multimedia')->onDelete('CASCADE');
			$table->integer('multimediable_id')->unsigned()->nullable();
			$table->string('multimediable_type');
		});

		// --------- En content and multimedia

		Schema::create('categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->nullable();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('CASCADE');
			$table->string('name');
			$table->string('code');
			$table->text('description')->nullable();
			$table->timestamps();
		});

		Schema::create('pages', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->nullable();
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('CASCADE');
			$table->string('code');
			$table->string('name');
			$table->integer('weight')->default(0);
            $table->integer('parent_id');
            $table->string('postalcode');
			$table->string('metatags')->default('');
			$table->text('description')->nullable();
			$table->string('type')->default('');
			$table->timestamps();
		});

		Schema::create('page_page', function(Blueprint $table)
		{
			$table->integer('page_id')->unsigned()->nullable();
			$table->foreign('page_id')->references('id')->on('pages')->onDelete('CASCADE');
			$table->integer('child_id')->unsigned()->nullable();
			$table->foreign('child_id')->references('id')->on('pages')->onDelete('CASCADE');
		});

		Schema::create('comments', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('page_id')->unsigned()->nullable();
			$table->foreign('page_id')->references('id')->on('pages')->onDelete('CASCADE');
			$table->integer('user_id')->unsigned()->nullable();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('CASCADE');
			$table->string('title');
			$table->text('comment');
			$table->timestamps();
		});

		Schema::create('banners', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->timestamps();
		});

		Schema::create('banner_page', function(Blueprint $table)
		{
			$table->integer('page_id')->unsigned()->nullable();
			$table->foreign('page_id')->references('id')->on('pages')->onDelete('CASCADE');
			$table->integer('banner_id')->unsigned()->nullable();
			$table->foreign('banner_id')->references('id')->on('banners')->onDelete('CASCADE');
		});

		Schema::create('galleries', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->timestamps();
		});

		Schema::create('magazines', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		Schema::drop('magazines');
		Schema::drop('comments');
		Schema::drop('galleries');
		Schema::drop('banner_page');
		Schema::drop('banners');
		Schema::drop('pages');
		Schema::drop('page_page');
		Schema::drop('contenteables');
		Schema::drop('multimedia');
		Schema::drop('multimediables');
		Schema::drop('categories');
		Schema::drop('contents');
	}

}
