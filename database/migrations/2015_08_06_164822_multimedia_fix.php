<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultimediaFix extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('multimedia', function(Blueprint $table){
			$table->dropColumn(['offsetx', 'offsety']);
		});

		Schema::table('multimedia', function(Blueprint $table){
			$table->string('offsetx')->nullable(); // almacenaremos un array en formato json para multiples resoluciones
			$table->string('offsety')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
