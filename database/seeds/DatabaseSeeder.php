<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Page;
use App\Category;
use App\Content;
use App\Multimedia;


class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();


		DB::table('users')->delete();

		$rAdmin = \App\Role::create(['name' => 'admin', 'display_name' => 'Administrador', 'description' => 'Administrador de sistema']);
		\App\Role::create(['name' => 'editor', 'display_name' => 'Editor', 'description' => 'Creación de contenidos']);
		\App\Role::create(['name' => 'user', 'display_name' => 'Usuario', 'description' => 'Usuario del sistema']);

		$admin = \App\User::create([
			'name' => 'Gerson',
			'email' => 'gerson@geekadvice.pe',
			'password' => \Illuminate\Support\Facades\Hash::make('123456')
		]);

		$admin->attachRole($rAdmin);

		//Páginas iniciales
		$amazonia = Page::create(['name' => 'Amazonia', 'code' => 'amazonia', 'type' => 'region' ]);
		$playa = Page::create(['name' => 'Playas', 'code' => 'playas', 'type' => 'region' ]);
		$motana = Page::create(['name' => 'Montañas', 'code' => 'montanas', 'type' => 'region' ]);



		$home = Page::create(['name' => 'Home', 'code' => 'home',]);
		$peru = Page::create([
			'name' => 'Sobre Perú',
			'code' => 'sobre-peru',
			'description' => 'Perú está ubicado en la parte occidental de América del Sur. Su territorio limita con Ecuador, Colombia, Brasil, Bolivia y Chile. Qui debis miligniatis deles que debisin ctinciamus anis que ommolest magnisquid moluptatum di simus voluptatat quam vitibus nobis quos aut est acepuda con cum qui reptate nditaquos nus'
		]);

		$home_aventura = Page::create(['name' => 'Home Aventura', 'code' => 'home-aventura',]);
		$home_naturalesa = Page::create(['name' => 'Home Naturaleza', 'code' => 'home-naturaleza',]);
		$home_cultura = Page::create(['name' => 'Home Cultura', 'code' => 'home-cultura',]);
		$home_descanso = Page::create(['name' => 'Home Descanso', 'code' => 'home-descanso',]);
/*
		//End creación usuarios y roles
		$aventura	= Category::create(['id'=>1 ,'name' => 'Aventura', 'code'=>'aventura', 'description' => 'Descripcion de aventura']);
		$naturaleza	= Category::create(['id'=>2 ,'name' => 'Naturaleza', 'code'=>'naturaleza', 'description' => 'Descripcion de naturaleza']);
		$cultura	= Category::create(['id'=>3 ,'name' => 'Cultura', 'code'=>'cultura', 'description' => 'Descripcion de cultura']);
		$descanzo	= Category::create(['id'=>4 ,'name' => 'Descanso', 'code'=>'descanso', 'description' => 'Descripcion de descanso']);

		// Subcategorias de Aventura
		$a0 = Category::create(['category_id'=>1 ,'name' => 'Trekking', 'code'=>'trakking', 'description' => '<p style="color: #df5555;  font-size: 15px;">¿Por qué escoger Perú como destino cuando en el papel existen alternativas que podrían presentarse más atractivas y  mejor promocionadas?</p>
								<br><p>El Perú posee 2,500 Km de costa bañada por el más activo de todos los océanos: el Pacífico, y por ello sus oleajes son constantes y consistentes. Debido a una estratégica ubicación en el Hemisferio Sur, se posiciona lo suficientemente lejos de la Antártida como para recibir ordenadamente las marejadas generadas por las tormentas invernales (abril a octubre). Y, adicionalmente, al estar lo suficientemente cerca del Hemisferio Norte, también recepciona, aunque más estacionalmente, las bravezas generadas en el ártico (noviembre a marzo). En resumen, un magneto de olas durante todos los días del año en cantidades y calidades envidiables, sin necesidad de padecer huracanes, tifones u otras calamidades atmosféricas.
								</p><br><p>Con respecto a variedad y calidad, la irregularidad del litoral y del zócalo continental, hacen posible  encontrar rompientes variadas y ajustables a distintos oleajes, vientos y mareas. Nuestro país es reconocido mundialmente por poseer olas de recorridos infinitos (Chicama), de tamaño descomunal (Pico Alto), de perfección divina (Cabo Blanco) o de diversión extrema (Cerro Azul), por citar sólo algunos ejemplos. </p>
								<br><p>Aún cuando la corriente de Humboldt recorre el mar peruano enfriándolo, la cercanía a la línea Ecuatorial propicia un clima templado que contrarresta las bajas temperaturas, posibilitando surfear sin riesgo de sufrir un cuadro de Hipotermia y recurriendo únicamente a un traje integral de 3mm, durante los días más fríos del invierno. El resto del año, la densidad y longitud del traje de Neopreno pueden ir variando conforme se suceden las estaciones, hasta llegar a prescindir de wetsuit alguno, al arribo del verano.</p>
								<br><p>No han sido reportados ataques en nuestras aguas de tiburones, venenosas serpientes marinas o mortales medusas y por consiguiente, los límites entre diversión y peligro los dictan la habilidad del surfista y las características de cada playa. Sin embargo, y tratándose del océano más rico del planeta, es habitual compartir las sesiones de surf con delfines, lobos marinos o alguna tímida tortuga en tránsito al Ecuador. </p>
								<br><p>Gracias a la continuidad de las bravezas y a la cantidad de lugares aptos para el surf, los talentosos tablistas locales se encuentran acostumbrados a correr en playas de altos estándares de calidad, y por ello, sobran opciones con olas increíbles y escasa gente alrededor hasta en la mismísima ciudad capital.</p>
								<br><p>Fuera de Lima la historia es aún mejor, pues son comunes soleadas tardes de domingo en alguna rompiente famosa de izquierdas kilométricas, donde tienes que alzar la vista para encontrar quien te acompañe durante tu regreso hacia la enésima ola del día.</p>']);
		$a1 = Category::create(['category_id'=>1 ,'name' => 'Surf', 'code'=>'surf', 'description' => 'Descripcion de Canotaje']);
		$a2 = Category::create(['category_id'=>1 ,'name' => 'Canotaje', 'code'=>'canotaje', 'description' => 'Descripcion de Canotaje']);
		$a3 = Category::create(['category_id'=>1 ,'name' => 'Ciclismo', 'code'=>'ciclismo', 'description' => 'Descripcion de Andinismo']);
		$a4 = Category::create(['category_id'=>1 ,'name' => 'Parapente', 'code'=>'parapente', 'description' => 'Descripcion de Andinismo']);
		$a5 = Category::create(['category_id'=>1 ,'name' => 'Kayak', 'code'=>'Kayak', 'description' => 'Descripcion de escalada en roca']);


		// Iconos para Categoria Aventura
		$a0->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOTREKKING" style="font-size: 140px;top: -23px;left: 24px;"></i>']));
		$a1->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOTREKKING" style="font-size: 140px;top: -23px;left: 24px;"></i>']));
		$a2->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOCANOTAJE" style="font-size: 140px;top: -18px;left: 33px;"></i>']));
		$a3->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-CICLISMO" style="font-size: 80px;top: 28px;left: 39px;transform: scaleX(-1);"></i>']));
		$a4->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOPARAPENTE" style="font-size: 140px;top: -23px;left: 24px;"></i>']));
		$a5->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-KAYAK" style="font-size: 70px;top: 26px;left: 41px;"></i>']));


		// Subcategorias de descanzo
		$d0 = Category::create(['category_id'=>4 ,'name' => 'Termalismo', 'code'=>'termalismo', 'description' => '<p style="color: #df5555;  font-size: 15px;">¿Por qué escoger Perú como destino cuando en el papel existen alternativas que podrían presentarse más atractivas y  mejor promocionadas?</p>
								<br><p>El Perú posee 2,500 Km de costa bañada por el más activo de todos los océanos: el Pacífico, y por ello sus oleajes son constantes y consistentes. Debido a una estratégica ubicación en el Hemisferio Sur, se posiciona lo suficientemente lejos de la Antártida como para recibir ordenadamente las marejadas generadas por las tormentas invernales (abril a octubre). Y, adicionalmente, al estar lo suficientemente cerca del Hemisferio Norte, también recepciona, aunque más estacionalmente, las bravezas generadas en el ártico (noviembre a marzo). En resumen, un magneto de olas durante todos los días del año en cantidades y calidades envidiables, sin necesidad de padecer huracanes, tifones u otras calamidades atmosféricas.
								</p><br><p>Con respecto a variedad y calidad, la irregularidad del litoral y del zócalo continental, hacen posible  encontrar rompientes variadas y ajustables a distintos oleajes, vientos y mareas. Nuestro país es reconocido mundialmente por poseer olas de recorridos infinitos (Chicama), de tamaño descomunal (Pico Alto), de perfección divina (Cabo Blanco) o de diversión extrema (Cerro Azul), por citar sólo algunos ejemplos. </p>
								<br><p>Aún cuando la corriente de Humboldt recorre el mar peruano enfriándolo, la cercanía a la línea Ecuatorial propicia un clima templado que contrarresta las bajas temperaturas, posibilitando surfear sin riesgo de sufrir un cuadro de Hipotermia y recurriendo únicamente a un traje integral de 3mm, durante los días más fríos del invierno. El resto del año, la densidad y longitud del traje de Neopreno pueden ir variando conforme se suceden las estaciones, hasta llegar a prescindir de wetsuit alguno, al arribo del verano.</p>
								<br><p>No han sido reportados ataques en nuestras aguas de tiburones, venenosas serpientes marinas o mortales medusas y por consiguiente, los límites entre diversión y peligro los dictan la habilidad del surfista y las características de cada playa. Sin embargo, y tratándose del océano más rico del planeta, es habitual compartir las sesiones de surf con delfines, lobos marinos o alguna tímida tortuga en tránsito al Ecuador. </p>
								<br><p>Gracias a la continuidad de las bravezas y a la cantidad de lugares aptos para el surf, los talentosos tablistas locales se encuentran acostumbrados a correr en playas de altos estándares de calidad, y por ello, sobran opciones con olas increíbles y escasa gente alrededor hasta en la mismísima ciudad capital.</p>
								<br><p>Fuera de Lima la historia es aún mejor, pues son comunes soleadas tardes de domingo en alguna rompiente famosa de izquierdas kilométricas, donde tienes que alzar la vista para encontrar quien te acompañe durante tu regreso hacia la enésima ola del día.</p>']);
		$d1 = Category::create(['category_id'=>4 ,'name' => 'Resorts', 'code'=>'resorts', 'description' => 'Descripcion de Canotaje']);
		$d2 = Category::create(['category_id'=>4 ,'name' => 'Yoga y meditacion', 'code'=>'yoga-y-meditacion', 'description' => 'Descripcion de Canotaje']);
		$d3 = Category::create(['category_id'=>4 ,'name' => 'Cruceros', 'code'=>'cruceros', 'description' => 'Descripcion de Andinismo']);
		$d4 = Category::create(['category_id'=>4 ,'name' => 'Casa de tiempo', 'code'=>'casa-de-tiempo', 'description' => 'Descripcion de Andinismo']);

		// Iconos para Categoria Descanzo
		$d0->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOTREKKING" style="font-size: 140px;top: -23px;left: 24px;"></i>']));
		$d1->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOTREKKING" style="font-size: 140px;top: -23px;left: 24px;"></i>']));
		$d2->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOCANOTAJE" style="font-size: 140px;top: -18px;left: 33px;"></i>']));
		$d3->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-CICLISMO" style="font-size: 80px;top: 28px;left: 39px;transform: scaleX(-1);"></i>']));
		$d4->contents()->save(\App\Content::create(['name' => 'Trekking', 'code' => 'icon', 'content' => '<i class="icon-ICONOPARAPENTE" style="font-size: 140px;top: -23px;left: 24px;"></i>']));

		Category::create(['category_id'=>2 ,'name' => 'Parques y Reservas Naturales', 'code'=>'aventura', 'description' => 'Descripcion de escalada en roca']);
		Category::create(['category_id'=>2 ,'name' => 'Bird Watching', 'code'=>'bird-watching', 'description' => 'Descripcion de escalada en roca']);
		Category::create(['category_id'=>2 ,'name' => 'Orquidias', 'code'=>'orquidias', 'description' => 'Descripcion de escalada en roca']);
		Category::create(['category_id'=>3 ,'name' => 'Arqueología', 'code'=>'arqueologia', 'description' => 'Descripcion de escalada en roca']);
		Category::create(['category_id'=>3 ,'name' => 'Muses y Galerías', 'code'=>'musesogalerías', 'description' => 'Descripcion de escalada en roca']);
		Category::create(['category_id'=>3 ,'name' => 'Arquitectura', 'code'=>'arquitectura', 'description' => 'Descripcion de escalada en roca']);
		Category::create(['category_id'=>3 ,'name' => 'Música y Danza', 'code'=>'músicadanza', 'description' => 'Descripcion de escalada en roca']);
		Category::create(['category_id'=>3 ,'name' => 'Teatro', 'code'=>'teatro', 'description' => 'Descripcion de escalada en roca']);

		//End creación categorías



		$default_description = '<p>La amazonía.</p><p>Qui debis miligniatis deles que debisin ctinciamus anis que ommolest magnisquid moluptatum di simus voluptatat quam vitibus nobis quos aut est acepuda con cum qui reptate nditaquos nus.</p>';

		$amazonas = $home->childs()->save( Page::create(['type' => 'destino', 'name' => 'Amazonas', 'code' => 'amazonas', 'description' => $default_description]) );
		$ancash = $home->childs()->save(  Page::create(['type' => 'destino', 'name' => 'Ancash', 'code' => 'anchas', 'description' => $default_description]) );
		$apurimac = Page::create(['type' => 'destino', 'name' => 'Apurimac', 'code' => 'apurimac', 'description' => $default_description]);
		$arequipa = Page::create(['type' => 'destino', 'name' => 'Arequipa', 'code' => 'arequipa', 'description' => $default_description]);
		$ayacucho = Page::create(['type' => 'destino', 'name' => 'Ayacucho', 'code' => 'ayacucho',  'description' => $default_description]);
		$cajamarca = Page::create(['type' => 'destino', 'name' => 'Cajamarca', 'code' => 'cajamarca', 'description' => $default_description]);
		$cusco = Page::create(['type' => 'destino', 'name' => 'Cusco', 'code' => 'cusco', 'description' => $default_description]);
		$huancavelica = Page::create(['type' => 'destino', 'name' => 'Huancavelíca', 'code' => 'huancavelica', 'description' => $default_description]);


		$content1 = $home->childs()->save( Page::create(['type' => 'hecho_en', 'name' => 'qhapaq ñan', 'code' => 'qhapaq', 'description' => $default_description]) );
		$content2 = $home->childs()->save(  Page::create(['type' => 'hecho_en', 'name' => 'artesanía', 'code' => 'artesania', 'description' => $default_description]) );
		$content3 = $home->childs()->save( Page::create(['type' => 'hecho_en', 'name' => 'gastronomía', 'code' => 'gastronomia', 'description' => $default_description]) );
		$content4 = $home->childs()->save(  Page::create(['type' => 'hecho_en', 'name' => 'el pisco', 'code' => 'pisco', 'description' => $default_description]) );

		$content1->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-qhpaq.jpg', 'type' => 'image']));
		$content2->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-artesania.jpg', 'type' => 'image']));
		$content3->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-gastronomia.jpg', 'type' => 'image']));
		$content4->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-pisco.jpg', 'type' => 'image']));

		// Sub Destinos para Arequipa
		$sarequipa = $arequipa->childs()->save(Page::create(['type' => 'subdestino', 'name' => 'Arequipa', 'code'=>'arequipa', 'description' => '']));
		$scamana = $arequipa->childs()->save(Page::create(['type' => 'subdestino', 'name' => 'Camaná', 'code'=>'camana', 'description' => '']));
		$smollendo = $arequipa->childs()->save(Page::create(['type' => 'subdestino', 'name' => 'Mollendo', 'code'=>'mollendo', 'description' => '']));
		$schivay = $arequipa->childs()->save(Page::create(['type' => 'subdestino', 'name' => 'Chivay', 'code'=>'chivay', 'description' => 'desc']));

		$content11 = $arequipa->childs()->save( Page::create(['type' => 'hecho_en', 'name' => 'qhapaq ñan', 'code' => 'qhapaq', 'description' => $default_description]) );
		$content12 = $arequipa->childs()->save(  Page::create(['type' => 'hecho_en', 'name' => 'artesanía', 'code' => 'artesania', 'description' => $default_description]) );
		$content13 = $arequipa->childs()->save( Page::create(['type' => 'hecho_en', 'name' => 'gastronomía', 'code' => 'gastronomia', 'description' => $default_description]) );
		$content14 = $arequipa->childs()->save(  Page::create(['type' => 'hecho_en', 'name' => 'el pisco', 'code' => 'pisco', 'description' => $default_description]) );

		$content11->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-qhpaq.jpg', 'type' => 'image']));
		$content12->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-artesania.jpg', 'type' => 'image']));
		$content13->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-gastronomia.jpg', 'type' => 'image']));
		$content14->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'hperu-pisco.jpg', 'type' => 'image']));


		// Lugares para subdestino arequipa
		$pl1 = $sarequipa->childs()->save( Page::create(['type' => 'sitio', 'name' => 'Ciudad y centro histórico', 'code'=>'ciudad-centro-historico', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$pl2 = $sarequipa->childs()->save( Page::create(['type' => 'sitio', 'name' => 'Valle del Colca', 'code'=>'valle-del-colca', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$pl3 = $sarequipa->childs()->save( Page::create(['type' => 'sitio', 'name' => 'Costa Arequipa', 'code'=>'costa-del-arequipa', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$pl4 = $sarequipa->childs()->save( Page::create(['type' => 'sitio', 'name' => 'Toro Muerto', 'code'=>'toro-muerto', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$pl5 = $sarequipa->childs()->save( Page::create(['type' => 'sitio', 'name' => 'Ciudad 2 y centro historico', 'code'=>'ciudad-2-y-centro-historico', 'description' => 'Ciudad y centro historico', 'category_id'=>1]) );
		$pl6 = $sarequipa->childs()->save( Page::create(['type' => 'sitio', 'name' => 'Valled 2 de la ciudad', 'code'=>'valled-2-de-la-ciudad', 'description' => 'Esta es el gran valle de la ciudad', 'category_id'=>1]) );
		
		$plcomer1 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comer', 'name' => 'Comer centro histórico', 'code'=>'comer-centro-historico', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomer2 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comer', 'name' => 'Comer Valle del Colca', 'code'=>'comer-valle-del-colca', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomer3 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comer', 'name' => 'Comer Arequipa', 'code'=>'comer-del-arequipa', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomer3 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comer', 'name' => 'Comer Toro Muerto', 'code'=>'comer-toro-muerto', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomer5 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comer', 'name' => 'Comer 2 y centro historico', 'code'=>'comer-2-y-centro-historico', 'description' => 'Ciudad y centro historico', 'category_id'=>1]) );
		$plcomer6 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comer', 'name' => 'Comer 2 de la ciudad', 'code'=>'comer-2-de-la-ciudad', 'description' => 'Esta es el gran valle de la ciudad', 'category_id'=>1]) );
		

		$plcomprar1 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar centro histórico', 'code'=>'comprar-centro-historico', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomprar2 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar Valle del Colca', 'code'=>'comprar-valle-del-colca', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomprar3 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar Arequipa', 'code'=>'comprar-del-arequipa', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomprar3 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar Toro Muerto', 'code'=>'comprar-toro-muerto', 'description' => 'Representa la más grande e importante fiesta cultural y músical del Perú', 'category_id'=>1]) );
		$plcomprar5 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar 2 y centro historico', 'code'=>'comprar-2-y-centro-historico', 'description' => 'Ciudad y centro historico', 'category_id'=>1]) );
		$plcomprar6 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar 2 de la ciudad', 'code'=>'comprar-2-de-la-ciudad', 'description' => 'Esta es el gran valle de la ciudad', 'category_id'=>1]) );
		$plcomprar7 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar 2 y centro historico', 'code'=>'comprar-2-y-centro-historico', 'description' => 'Ciudad y centro historico', 'category_id'=>1]) );
		$plcomprar8 = $sarequipa->childs()->save( Page::create(['type' => 'sitio-comprar', 'name' => 'Comprar 2 de la ciudad', 'code'=>'comprar-2-de-la-ciudad', 'description' => 'Esta es el gran valle de la ciudad', 'category_id'=>1]) );


		$pl1->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'plazarmas.jpg', 'type' => 'image']));
		$pl2->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'santacatalina.jpg', 'type' => 'image']));
		$pl3->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'santacatalina.jpg', 'type' => 'image']));

		$pl1->multimedia()->save(\App\Multimedia::create(['name' => 'Titulo de prueba', 'description' => 'Esta es la descripcion', 'code' => 'img', 'source' => 'plazarmas.jpg', 'type' => 'image']));

		$sarequipa->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'arequipa-progreso.jpg', 'type' => 'image']));
		$scamana->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'vallecolca.jpg', 'type' => 'image']));
		$smollendo->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'costarequipa.jpg', 'type' => 'image']));
		$schivay->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'toromuerto-perfil3.jpg', 'type' => 'image']));

		// Conteidos Detallados para Peru

		$home->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));




		$fiesta1 = Page::create(['type' => 'fiesta', 'name' => 'Señor de los milagros','code' => 'senor-de-los-milagros','description' => 'Perú está ubicado en la parte occidental de América del Sur. Su territorio limita con Ecuador, Colombia, Brasil, Bolivia y Chile. Qui debis miligniatis deles que debisin ctinciamus anis que ommolest magnisquid moluptatum di simus voluptatat quam vitibus nobis quos aut est acepuda con cum qui reptate nditaquos nus']);
		$fiesta2 = Page::create(['type' => 'fiesta', 'name' => 'Inty Raymi','code' => 'senor-de-los-milagros','description' => 'Perú está ubicado en la parte occidental de América del Sur. Su territorio limita con Ecuador, Colombia, Brasil, Bolivia y Chile. Qui debis miligniatis deles que debisin ctinciamus anis que ommolest magnisquid moluptatum di simus voluptatat quam vitibus nobis quos aut est acepuda con cum qui reptate nditaquos nus']);
		$fiesta3 = Page::create(['type' => 'fiesta', 'name' => 'Virgen de la candelaria','code' => 'senor-de-los-milagros','description' => 'Perú está ubicado en la parte occidental de América del Sur. Su territorio limita con Ecuador, Colombia, Brasil, Bolivia y Chile. Qui debis miligniatis deles que debisin ctinciamus anis que ommolest magnisquid moluptatum di simus voluptatat quam vitibus nobis quos aut est acepuda con cum qui reptate nditaquos nus']);

		$fiesta1->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'toromuerto-perfil3.jpg', 'type' => 'image']));
		$fiesta2->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'toromuerto-perfil3.jpg', 'type' => 'image']));


		$peru->contents()->save(\App\Content::create(['name' => 'Ubicación', 'code' => 'ubicacion', 'content' => '<p style="font-weight: bold;">Perú está ubicado en la parte occidental de América del Sur. Su territorio limita con Ecuador, Colombia, Brasil, Bolivia y Chile.</p>
						<p>Qui debis miligniatis deles que debisin ctinciamus anis que ommolest magnisquid moluptatum di simus voluptatat quam vitibus nobis quos aut est acepuda con cum qui reptate nditaquos nus.</p>']));
		$peru->contents()->save(\App\Content::create(['name' => 'Idioma', 'code' => 'idioma', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'Clima', 'code' => 'clima', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'Documentación Necesaria', 'code' => 'documentacion-necesaria', 'content' => 'Los residentes de la Comunidad andina pueden acceder a Peru con el DNI o el pasaporte indistintamente, el único requisito es que éste se encuentre en vigor.VISADOS']));
		$peru->contents()->save(\App\Content::create(['name' => 'Moneda', 'code' => 'moneda', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'Salud y vacunas', 'code' => 'salud-y-vacunas', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'comunicaciones', 'code' => 'comunicaciones', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'aeropuertos', 'code' => 'aeropuertos', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'hora oficial', 'code' => 'hora-oficial', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'Electricidad', 'code' => 'electricidad', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'Horario Comercial', 'code' => 'horario-comercial', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'Temporada alta', 'code' => 'temporada-alta', 'content' => 'Sobre el perú']));
		$peru->contents()->save(\App\Content::create(['name' => 'Religion', 'code' => 'religion', 'content' => '123143 m2']));
		$peru->contents()->save(\App\Content::create(['name' => 'Gobierno', 'code' => 'gobierno', 'content' => '123143 m2']));

		$home->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'cover_default.jpg', 'type' => 'image']));

		// Multomedia Sliders para peru
		$home->multimedia()->save(Multimedia::create(['name'=>'<p>Lago Titicaca, <span class="text-uppercase">puno, perú</span></p>','code'=>'slider1','source'=>'first-slider.jpg'] ));
		$home->multimedia()->save(Multimedia::create(['name'=>'<p>Camino Inca, <span class="text-uppercase">cusco, perú</span></p>','code'=>'slider1','source'=>'second-slide.jpg'] ));
		$home->multimedia()->save(Multimedia::create(['name'=>'<p>Lago Titicaca, <span class="text-uppercase">puno, perú</span></p>','code'=>'slider1','source'=>'first-slider.jpg'] ));
		$home->multimedia()->save(Multimedia::create(['name'=>'<p>Camino Inca, <span class="text-uppercase">cusco, perú</span></p>','code'=>'slider1','source'=>'second-slide.jpg'] ));

		// Contenidos para Index
		$home->contents()->save(Content::create(['name' => 'welcome', 'code' => 'welcome-index', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$home->contents()->save(Content::create(['name' => 'tittle1', 'code' => 'tittle1-index', 'content' => 'LOS IMPERDIBLES']));
		$home->contents()->save(Content::create(['name' => 'text1', 'code' => 'text1-index', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));

		// Multimedia para cada cover de la subcategoria de aventura
		$a1->multimedia()->save(\App\Multimedia::create(['name'=>'<p>Lago Titicaca, <span class="text-uppercase">puno, perú</span></p>','code'=>'cover','source'=>'av-canotaje.jpg']) );
		$a2->multimedia()->save(\App\Multimedia::create(['name'=>'<p>Lago Titicaca, <span class="text-uppercase">puno, perú</span></p>','code'=>'cover','source'=>'av-surf.jpg']) );
		$a3->multimedia()->save(\App\Multimedia::create(['name'=>'<p>Lago Titicaca, <span class="text-uppercase">puno, perú</span></p>','code'=>'cover','source'=>'av-parapente.jpg']) );
		$a4->multimedia()->save(\App\Multimedia::create(['name'=>'<p>Lago Titicaca, <span class="text-uppercase">puno, perú</span></p>','code'=>'cover','source'=>'av-ciclismo.jpg']) );

		// Multimedia portada para Region
		$amazonas->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'imp-cordillera.jpg', 'type' => 'image']));
		$arequipa->multimedia()->save(\App\Multimedia::create(['name' => 'Portada', 'code' => 'cover', 'source' => 'imp-paracas.jpg', 'type' => 'image']));

		// Contenidos para destinos
		$amazonas->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$amazonas->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));
		$amazonas->contents()->save(\App\Content::create(['name' => 'text2', 'code' => 'text2', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$amazonas->contents()->save(\App\Content::create(['name' => 'text3', 'code' => 'text3', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$amazonas->contents()->save(\App\Content::create(['name' => 'text4', 'code' => 'text4', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$amazonas->contents()->save(\App\Content::create(['name' => 'text5', 'code' => 'text5', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));

		$amazonas->contents()->save(\App\Content::create(['name' => 'welcome-aventura', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$amazonas->contents()->save(\App\Content::create(['name' => 'text1 asd asda sda sda sdas d', 'code' => 'text1-aventura', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));
		$amazonas->contents()->save(\App\Content::create(['name' => 'text2 asdaa sdas dasdasd', 'code' => 'text2-aventura', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));

		$ancash->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$ancash->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));
		$ancash->contents()->save(\App\Content::create(['name' => 'text2', 'code' => 'text2', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$ancash->contents()->save(\App\Content::create(['name' => 'text3', 'code' => 'text3', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$ancash->contents()->save(\App\Content::create(['name' => 'text4', 'code' => 'text4', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$ancash->contents()->save(\App\Content::create(['name' => 'text5', 'code' => 'text5', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));

		$apurimac->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$apurimac->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));
		$apurimac->contents()->save(\App\Content::create(['name' => 'text2', 'code' => 'text2', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$apurimac->contents()->save(\App\Content::create(['name' => 'text3', 'code' => 'text3', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$apurimac->contents()->save(\App\Content::create(['name' => 'text4', 'code' => 'text4', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$apurimac->contents()->save(\App\Content::create(['name' => 'text5', 'code' => 'text5', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));

		$arequipa->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$arequipa->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));
		$arequipa->contents()->save(\App\Content::create(['name' => 'text2', 'code' => 'text2', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$arequipa->contents()->save(\App\Content::create(['name' => 'text3', 'code' => 'text3', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$arequipa->contents()->save(\App\Content::create(['name' => 'text4', 'code' => 'text4', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));
		$arequipa->contents()->save(\App\Content::create(['name' => 'text5', 'code' => 'text5', 'content' => 'Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.']));





		$aventura->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$aventura->contents()->save(\App\Content::create(['name' => 'tittle1', 'code' => 'tittle1', 'content' => 'LOS IMPERDIBLES']));
		$aventura->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));
	
		$naturaleza->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$naturaleza->contents()->save(\App\Content::create(['name' => 'tittle1', 'code' => 'tittle1', 'content' => 'LOS IMPERDIBLES']));
		$naturaleza->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));

		$cultura->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$cultura->contents()->save(\App\Content::create(['name' => 'tittle1', 'code' => 'tittle1', 'content' => 'LOS IMPERDIBLES']));
		$cultura->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));

		$descanzo->contents()->save(\App\Content::create(['name' => 'welcome', 'code' => 'welcome', 'content' => 'Bienvenida es un portal de Peru expertos en turismo en el Perú. Nuestros innumerables viajes nos dan la experiencia necesaria para poder guiarlos a que encuentren la información más completa<br>y la mejor opción en la elección de su viaje.']));
		$descanzo->contents()->save(\App\Content::create(['name' => 'tittle1', 'code' => 'tittle1', 'content' => 'LOS IMPERDIBLES']));
		$descanzo->contents()->save(\App\Content::create(['name' => 'text1', 'code' => 'text1', 'content' => '<p style="color: #555555;">La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p><br><p>La selva amazónica del Perú es una de las zonas con mayor diversidad biológica del planeta. Es tan grande la variedad de especies que se estima que la mayor parte de ellas sigue sin ser descubierta y menos estudiadas adecuadamente. El Perú es el segundo país, tras Colombia (que incluyen el territorio extracontinental de las islas caribeñas: Providencia y San Andrés),1 en lo que respecta a cantidad de especies de aves en el mundo2 y el tercero en cuanto a mamíferos, de los que 44% y 63% respectivamente habita en la Amazonia peruana.</p>']));
*/
	}

}
