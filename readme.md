## Laravel Seeder

Proyecto base para trabajar proyectos php

## Version

Current version: v 0.1

## Changelog

V0.1
- Activo bower, gulp, elixir, .env, .gitignore
- Entrust compatible con laravel 5
- Usa vistas y controladores de auth que vienen en laravel por defecto

## TODO

- Pendiente de proba: https://github.com/barryvdh/laravel-debugbar
- OAUTH server "lucadegasperi/oauth2-server-laravel": "4.1.*@dev"

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
