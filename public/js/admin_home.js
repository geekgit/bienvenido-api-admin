var uploadCrop;
var imagecrop = {};

function initAdmin()
{
	$("#formNewDestiny").submit(function(event){
        var data_page={name: $("#destinytext").val(), type: 'destino',code:$("#destinytext").val()};
        createNewPage(event,data_page)});
	$("#formEditPage").submit(savePageForm);

	$("#uploadimagecroppie").change(function ()
	{
		saveImage(this, imagecrop.typepage, imagecrop.codeimg, imagecrop.urlsave, imagecrop.codepage,imagecrop.idimage);
	});
	var editor = new MediumEditor('.editable', {
		toolbar: {
			static:true,
			hideOnClick:false,
			sticky:true,
			buttons: ['bold', 'italic', 'underline', 'list-extension', 'h2', 'h3', 'anchor', 'justifyCenter', 'removeFormat']
		}
	});
    $("[contenteditable]").blur(function ()
    {
        var pid=page_id;
        if ( $(this).is('[page-id]') ){
            pid=$(this).attr('page-id');
        }
        var code=$(this).attr('code');
        var type=$(this).attr('type');
        var text=$(this).html();
        editcontentv2( code, text, type,pid);
    });
}
function showMessage(message,type)//type:success,warning,info
{
	$('.top-right').notify({
		message: { text: message },
		type:type
	}).show();
}
function createNewPage(evt,data)
{
    var url= base_url + 'api/v1/pages/save';
    var data_page=data;
    $.post(url, data_page, function (_data){
        console.log(_data);
        if (_data.status.code == 200)
        {
            swal('Cambios guardados', 'Para poder ver los cambios necesitas actualizar la página', 'success');
        } else {
            swal('¡Error!', 'No se pudo completar la solicitud: ' + _data.status.description, 'warning');
        }
    },"json").fail(function(_error){
        swal('¡Error!', 'No pudimos conectarnos al servidor', 'error');
        console.log(_error);
    });
    evt.preventDefault();
}
function cropImage(typepage, idimage, codepage, path, url, codeimg, crop_width, crop_height, urlsave,maxheight)
{

	var maxheights=1400;

	if(codeimg=='cover' || codeimg=='cover1' || codeimg=='cover2' || codeimg=='cover3' || codeimg=='cover4' || codeimg=='icon')
	{
		maxheights=550;
	}
	if(codeimg=='slider1')
	{
		$("#uploadimagecroppie").css('visibility','hidden');
	}
	else{
		$("#uploadimagecroppie").css('visibility','visible');
	}
	if(maxheight==1400)
	{
		maxheights=1400;
	}
	imagecrop = { idimage: idimage, codepage: codepage, codeimg: codeimg, url: url, typepage: typepage, urlsave: urlsave,maxheight:maxheights};

	$('#myModalcropper').modal('show');
	if (uploadCrop != undefined)
	{
		uploadCrop.croppie('destroy');
	}
	uploadCrop = $('#upload-demo').croppie({
		enableExif: true,
		viewport: {
			width: crop_width,
			height: crop_height,
			type: 'square'
		},
		boundary: {
			width: 860,
			height: 400
		}
	});

	setTimeout(function ()
	{
		uploadCrop.croppie('bind', {
			url: path
		});
	}, 500);
}
function saveCropImage()
{
	var file;
	$('#myPleaseWait').modal('show');
   var results= uploadCrop.croppie('get');
		var data = new FormData();
		data.append('image_id', imagecrop.idimage);
		data.append('max-height',imagecrop.maxheight);
		data.append('x1',results.points[0]);
		data.append('y1',results.points[1]);
		data.append('x2',results.points[2]);
		data.append('y2',results.points[3]);
		$.ajax({
			url: imagecrop.url,
			type: "post",
			processData: false,
			contentType: false,
			data: data,
			success: function (data)
			{
				$('#myPleaseWait').modal('hide');
				location.reload();
			}
		}, "json");

	uploadCrop.croppie('destroy');

}
var init=0;
function saveImage(input, page_type, type_image, url, codepage,idimage)
{
	if (input.files && input.files[0])
	{


		$('#myPleaseWait').modal('show');
		$(input).liteUploader({
			script: url,
			ref:'file',
			params:{code: codepage,type:page_type,codechild:type_image}
		}).on("lu:success", function (e, response) {
			$('#myPleaseWait').modal('hide');
			var navegador = window.URL || window.webkitURL;
		   var  objeto_url = navegador.createObjectURL(input.files[0]);
			if(typeof(uploadCrop) != "undefined")
			{
				uploadCrop.croppie('bind', {
					url:objeto_url

				});
			}



		}).on("lu:progress", function (e, percentage) {
			$('#progress .progress-bar').css(
				'width',
				percentage + '%'
			);
			console.log(percentage);
		});
		$(input).data("liteUploader").startUpload();

	  /*  formData:
   */



	}

}
function moverminiatura(input, type)
{
	var index = parseInt($(input).parent().parent().attr('data-slick-index'));
	var pant = $(input).parent().parent().parent();
	var indexx;
	var length;
	var at = 'div';
	if ($(pant).find(" > div").length > $(pant).find(" > li").length)
	{
		length = $(pant).find(" > div").length;
	} else
	{
		length = $(pant).find(" > li").length;
		at = 'li';
	}

	indexx = index + 1;
	if (type == 'adelante' && indexx < length)
	{

		var indesig = $(pant).find(at + "[data-slick-index='" + indexx + "']");
		$(pant).find(at + "[data-slick-index='" + index + "']").insertAfter(indesig);
		$(pant).find(at + "[data-slick-index='" + index + "']").attr('data-slick-index', indexx);
		$(indesig).attr('data-slick-index', index);
	}
	else if (type == 'atras' && index > 0)
	{
		indexx = index - 1;
		var indant = $(pant).find(at + "[data-slick-index='" + indexx + "']");
		$(pant).find(at + "[data-slick-index='" + index + "']").insertBefore(indant);
		$(pant).find(at + "[data-slick-index='" + index + "']").attr('data-slick-index', indexx);
		$(indant).attr('data-slick-index', index);
	}
}
function sortabledrag(id, sortable)
{

	var el = document.getElementById(id);
	sortable = Sortable.create(el);
	sortable = new Sortable(el, {
		group: "name",  // or { name: "...", pull: [true, false, clone], put: [true, false, array] }
		sort: true,  // sorting inside list
		delay: 0, // time in milliseconds to define when the sorting should start
		disabled: false, // Disables the sortable if set to true.
		store: null,  // @see Store
		animation: 150,  // ms, animation speed moving items when sorting, `0` — without animation
		handle: ".my-handle",  // Drag handle selector within list items
		filter: ".ignore-elements",  // Selectors that do not lead to dragging (String or Function)
		draggable: ".slick-active",  // Specifies which items inside the element should be sortable
		ghostClass: "ghost",  // Class name for the drop placeholder
		chosenClass: "chosen",  // Class name for the chosen item
		dataIdAttr: 'data-id',

		forceFallback: false,  // ignore the HTML5 DnD behaviour and force the fallback to kick in
		fallbackClass: "sortable-fallback", // Class name for the cloned DOM Element when using forceFallback
		fallbackOnBody: false,  // Appends the cloned DOM Element into the Document's Body

		scroll: true, // or HTMLElement
		scrollSensitivity: 30, // px, how near the mouse must be to an edge to start scrolling.
		scrollSpeed: 10, // px

		setData: function (dataTransfer, dragEl)
		{
			dataTransfer.setData('Text', dragEl.textContent);
		},

		// dragging started
		onStart: function (/**Event*/evt)
		{
			evt.oldIndex;  // element index within parent
		},

		// dragging ended
		onEnd: function (/**Event*/evt)
		{
			evt.oldIndex;  // element's old index within parent
			evt.newIndex;  // element's new index within parent
		},

		// Element is dropped into the list from another list
		onAdd: function (/**Event*/evt)
		{
			var itemEl = evt.item;  // dragged HTMLElement
			evt.from;  // previous list
			// + indexes from onEnd
		},

		// Changed sorting within list
		onUpdate: function (/**Event*/evt)
		{
			var itemEl = evt.item;  // dragged HTMLElement
			// + indexes from onEnd
		},

		// Called by any change to the list (add / update / remove)
		onSort: function (/**Event*/evt)
		{
			// same properties as onUpdate
		},

		// Element is removed from the list into another list
		onRemove: function (/**Event*/evt)
		{
			// same properties as onUpdate
		},

		// Attempt to drag a filtered element
		onFilter: function (/**Event*/evt)
		{
			var itemEl = evt.item;  // HTMLElement receiving the `mousedown|tapstart` event.
		},

		// Event when you move an item in the list or between lists
		onMove: function (/**Event*/evt)
		{
			// Example: http://jsbin.com/tuyafe/1/edit?js,output
			evt.dragged; // dragged HTMLElement
			evt.draggedRect; // TextRectangle {left, top, right и bottom}
			evt.related; // HTMLElement on which have guided
			evt.relatedRect; // TextRectangle
			// return false; — for cancel
		}
	});
}
function editcontent(codedestiny, code, content, type, page, url)
{
	$.ajax({
		url: url,
		type: "post",
		data: {codedestiny: codedestiny, code: code, content: content, type: type, page: page},
		success: function (data)
		{
			showMessage('se guardaron los cambios correctamente', 'success');
		}
	}, "json");
}
function editcontentv2(code, content, type,page_id)
{
    var id=page_id;
    var url= base_url + 'api/v1/contents/save';
    var data_page={code: code,  text: content, type: type, page_id: id};
    $.post(url, data_page, function (_data){
        console.log(_data);
        if (_data.status.code == 200)
        {
            showMessage('se guardaron los cambios correctamente', 'success');
        } else {
            showMessage('No se pudo completar la solicitud: ' + _data.status.description, 'warning');
        }
    },"json").fail(function(_error){
        showMessage('No pudimos conectarnos al servidor', 'warning');
        console.log(_error);
    });
}

function saveOrderLIst(lista, url)
{
	$('#myPleaseWait').modal('show');
	$.ajax({
		url: url,
		type: "post",
		data: {list: lista},
		success: function (data)
		{
			$('#myPleaseWait').modal('hide');
			showMessage('se guardaron los cambios correctamente', 'success');
		}
	}, "json");
}
function eliminarpage(id, url)
{
	swal({
			title: "¿Está seguro de borrar este destino?",
			text: "Si borras este destino se eliminará todo el contenido que este relacionado con el",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Sí, Eliminar",
			cancelButtonText: "Cancelar",
			closeOnConfirm: false
		},
		function ()
		{
			$.ajax({
				url: url,
				type: "post",
				data: {id: id},
				success: function (data)
				{
					window.location.reload();
				}
			}, "json");
		});

}
function eliminarrelationpage(id, url, code_father)
{
    swal({
            title: "¿Está seguro de borrar este sitio",
            text: "Si borras este sitio solo se eliminara la relacion que hay entre la sección y el sitio",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Sí, Eliminar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false
        },
        function ()
        {
            $.ajax({
                url: url,
                type: "post",
                data: {child_id: id, page_code: code_father},
                success: function (data)
                {
                    window.location.reload();
                }
            }, "json");
        });

}
function savePageForm(evt)
{
	var url = $(this).attr('action');
	var data = $(this).serializeArray();

	$.post(url, data, function (_data){
		console.log(_data);
		if (_data.status.code == 200)
		{
			swal('Cambios guardados', 'Para poder ver los cambios necesitas actualizar la página', 'success');
		} else {
			swal('¡Error!', 'No se pudo completar la solicitud: ' + _data.status.description, 'warning');
		}
	},"json").fail(function(_error){
		swal('¡Error!', 'No pudimos conectarnos al servidor', 'error');
		console.log(_error);
	});

	evt.preventDefault();
}
function savemappoint(pageid, url, lat, lon, zoom)
{
	$.ajax({
		url: url,
		type: "post",
		data: {pageId: pageid, lat: lat, lng: lon, zoom: zoom},
		success: function (data)
		{
			showMessage('se guardaron los cambios correctamente', 'success');
			//window.location.href=data['code'];
		}
	}, "json");
}
function saveMapPosition(pageid, url, op)
{
	var lat, lng, zoom;
	if (op == 1)
	{
		var cnt = $('.pws_tab_active').parent().attr('data-slick-index');
		lat = map[cnt].center.lat();
		lng = map[cnt].center.lng();
		zoom = map[cnt].getZoom();
	}
	else
	{
		lat = map.center.lat();
		lng = map.center.lng();
		zoom = map.getZoom();
	}
	$.ajax({
		url: url,
		type: "post",
		data: {pageId: pageid, lat: lat, lng: lng, zoom: zoom},
		success: function (data)
		{
			showMessage('se guardaron los cambios correctamente', 'success');
			//window.location.href=data['code'];
		}
	}, "json");
}


function deletesliderimg(id, url)
{
	$.ajax({
		url: url,
		type: "post",
		data: {id: id},
		success: function (data)
		{
			showMessage('se guardaron los cambios correctamente', 'success');
			window.location.reload();
		}
	}, "json");
}
function openpopuporder(iddrag, idpage)
{
	var id = idpage;
	var dragList = $("#draglistdynamic");
	dragList.removeAttr('title');
	dragList.empty();
	$("#" + iddrag + " > li").each(function (index)
	{
		dragList.append('<li title="' + $(this).attr("data-id") + '" ><i class="icon-cursor-move"></i> ' + $(this).find(".title").text() + ' </li>');
	});
	dragList.attr('title', idpage);
	$('#myModaldrag').modal('show');
}


function saveIconPage(input) {
	codedestiny = $(btn).siblings('input').val();
	var i = $(btn).siblings('a').children('i');
	var text = $(input).attr('class');
	$(i).attr('class', text);
	$('#myModalicons').modal('hide');
	editcontent(codedestiny, 'icon', text, 'content', 'category', '<?php echo url("contenteditable")?>');
}


$(document).ready(initAdmin);
