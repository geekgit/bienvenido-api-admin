$(document).ready(initPage);

function initHome()
{
	new Swiper('.slider-region', {
		loop: true,
		pagination: '.swiper-pagination',
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev'
	});
	/*	$('.slider-egion').slick({
	 infinite: true,
	 dots: true,
	 slidesToShow: 1,
	 slidesToScroll: 1,
	 fade: true,
	 prevArrow: ("<img src='" + base_url + "img/regionright.png' style='  position: absolute;top: calc(50% - 26px);left: 21px;cursor:pointer;'>"),
	 nextArrow: ("<img src='" + base_url + "img/regionleft.png' style='  position: absolute;top: calc(50% - 26px);right: 21px;cursor:pointer;'>")
	 });*/
}

function showLoginForm()
{
	// TweenLite.to()
	$('#login-form').slideDown();
	$('#login-usuario').focus();
}

function hideLoginForm()
{
	$('#login-form').slideUp();

}

function initDestinies()
{
	var btnShowMore = $("#imgdestino");
	btnShowMore.click(function (evt)
	{
		if ($(this).attr('class') == 'vermenos')
		{
			$('#resenia-region').slideUp();
			//$('html, body').animate({scrollTop: $("#favorite-star").offset().top}, 1000);
			$(this).removeClass('vermenos');
			$(this).addClass('vermas');
			$(this).html('VER MÁS <i class="icon-arrow-down"></i>');
		}
		else
		{
			$('#resenia-region').slideDown();
			//$('html, body').animate({scrollTop: $("#favorite-star").offset().top}, 1000);
			$(this).html('OCULTAR <i class="icon-arrow-up"></i>');
			$(this).removeClass('vermas');
			$(this).addClass('vermenos');
		}
		evt.preventDefault();
	});
}

function events()
{
	$('.account-create').click(openPopup);
	$('.overlay').click(closePopup);
}

function openPopup()
{
	$('.white-popup, .overlay').addClass('visible');
}
function closePopup()
{
	$('.white-popup, .overlay').removeClass('visible');
}

function initPage()
{

	$('[data-toggle="tooltip"]').tooltip();
	events();
	initDestinies();
	/*$('.slideslick').slick({
	 infinite: false,
	 slidesToShow: 4,
	 slidesToScroll: 4,
	 draggable: true,
	 prevArrow: ("<img src='" + base_url + "img/slideleft.png' style='  position: absolute;top: calc(50% - 35px);left: -10px;cursor:pointer;'>"),
	 nextArrow: ("<img src='" + base_url + "img/slideright.png' style='  position: absolute;top: calc(50% - 35px);right: -10px;cursor:pointer;'>")
	 });*/


	/*$('.slidevertical').slick({
	 infinite: false,
	 dots: true,
	 slidesToShow: 1,
	 slidesToScroll: 1,
	 vertical: true,
	 dots: false,
	 prevArrow: ("<img src='" + base_url + "img/btn-down.png' style='  position: absolute;bottom:-34px;left: 180px;cursor:pointer;'>"),
	 nextArrow: ("<img src='" + base_url + "img/btn-up.png' style='  position: absolute;top: -38px;right: 180px;cursor:pointer;'>")
	 });*/

	/*if ($('.profile-tabs').length)
	{
		$('.profile-tabs .tabs').pwstabs({
			// scale / slideleft / slideright / slidetop / slidedown / none
			effect: 'scale',
			defaultTab: 1,
			containerWidth: '655px',
			tabsPosition: 'horizontal',
			horizontalPosition: 'top',
			verticalPosition: 'left',
			responsive: false,
			// Themes available: default: '' / pws_theme_violet / pws_theme_green / pws_theme_yellow
			// pws_theme_gold / pws_theme_orange / pws_theme_red / pws_theme_purple / pws_theme_grey
			theme: ''
		});
	}*/

	/*if ($('.circle-tabs').length)
	{
		$('.circle-tabs .tabs').pwstabs({
			// scale / slideleft / slideright / slidetop / slidedown / none
			effect: 'scale',
			defaultTab: 1,
			containerWidth: '940px',
			tabsPosition: 'horizontal',
			horizontalPosition: 'top',
			verticalPosition: 'left',
			responsive: false,
			// Themes available: default: '' / pws_theme_violet / pws_theme_green / pws_theme_yellow
			// pws_theme_gold / pws_theme_orange / pws_theme_red / pws_theme_purple / pws_theme_grey
			theme: ''
		});
	}*/



	if ($('.ver-todos').length)
	{
		var $ver = $('.ver-todos');
		var $content = $ver.prev();
		var $miniaturas = $content.children('a');

		if ($miniaturas.length > 6)
		{
			$miniaturas.slice(6, $miniaturas.length).wrapAll('<div class="wrapMiniaturas"/>');
			$ver.click(function ()
			{
				$('.wrapMiniaturas').slideToggle(500, function ()
				{
					$ver.find('p').text(function ()
					{
						return $('.wrapMiniaturas').is(":visible") ? "Ver menos" : "Ver todos";
					});
					$ver.find('img').removeClass().addClass(function ()
					{
						return $('.wrapMiniaturas').is(":visible") ? "vermenos" : "vermas";
					});
				});
			});
		} else
		{
			$ver.css({'display': 'none'});
		}
	}


	if ($('.comments-display').length)
	{
		$('.comments-pagination').jPages({
			containerID: 'comments-content',
			animation: 'fadeIn',
			perPage: 3,
			previous: "← Anterior",
			next: "Siguiente →"
		});
	}
	//if ($('#alternar-aventurahome, #aventhomebu').toggle) {
	$('#alternar-aventurahome, #aventhomebu').toggle(
		function (e)
		{
			$('#panelocultoaventurahome').slideDown();
			$('#alternar-aventurahome').text('Ver menos');
			e.preventDefault();
			$('#aventhomebu').removeClass('vermas');
			$('#aventhomebu').addClass('vermenos');

		},
		function (e)
		{
			$('#panelocultoaventurahome').slideUp();
			$('#alternar-aventurahome').text('Ver todos');
			$('#aventhomebu').removeClass('vermenos');
			$('#aventhomebu').addClass('vermas');

		}
	);
	//};
	if ($('#alterfiesre, #fesrebu').toggle)
	{
		$('#alterfiesre', '#fesrebu').toggle(
			function (e)
			{
				$('#panelofiesre').slideDown();
				$('#alterfiesre').text('Ver menos');
				e.preventDefault();
				$('#fesrebu').removeClass('vermas');
				$('#fesrebu').addClass('vermenos');

			},
			function (e)
			{
				$('#panelofiesre').slideUp();
				$('#alterfiesre').text('Ver todos');
				e.preventDefault();
				$('#fesrebu').removeClass('vermenos');
				$('#fesrebu').addClass('vermas');

			}
		);
	}
	;
	if ($('#alterhecho').toggle)
	{
		$('#alterhecho').toggle(
			function (e)
			{
				$('#panelhecho').slideDown();
				$(this).text('Ver menos');
				e.preventDefault();
				$('#hechobu').removeClass('vermas');
				$('#hechobu').addClass('vermenos');

			},
			function (e)
			{
				$('#panelhecho').slideUp();
				$(this).text('Ver todos');
				e.preventDefault();
				$('#hechobu').removeClass('vermenos');
				$('#hechobu').addClass('vermas');

			}
		);
	}
	;
	if ($('#alterexperto').toggle)
	{
		$('#alterexperto').toggle(
			function (e)
			{
				$('#panelexperto').slideDown();
				$(this).text('Leer menos');
				e.preventDefault();
				$('#expertobu').removeClass('vermas');
				$('#expertobu').addClass('vermenos');

			},
			function (e)
			{
				$('#panelexperto').slideUp();
				$(this).text('Leer más');
				e.preventDefault();
				$('#expertobu').removeClass('vermenos');
				$('#expertobu').addClass('vermas');

			}
		);
	}

	if ($('#alterfiestas').toggle)
	{
		$('#alterfiestas').toggle(
			function (e)
			{
				$('#panelfiesta').slideDown();
				$(this).text('Ver menos');
				e.preventDefault();
				$('#fiestabu').removeClass('vermas');
				$('#fiestabu').addClass('vermenos');

			},
			function (e)
			{
				$('#panelfiesta').slideUp();
				$(this).text('Ver todos');
				e.preventDefault();
				$('#fiestabu').removeClass('vermenos');
				$('#fiestabu').addClass('vermas');

			}
		);
	}
	;
	if ($('#alternar1').toggle)
	{
		$('#alternar1').toggle(
			function (e)
			{
				$('#paneloculto1').slideDown();

				e.preventDefault();
			},

			function (e)
			{
				$('#paneloculto1').slideUp();

				e.preventDefault();
			}
		);
	}
	;
	if ($('#alternar2').toggle)
	{
		$('#alternar2').toggle(
			function (e)
			{
				$('#paneloculto2').slideDown();

				e.preventDefault();
			},

			function (e)
			{
				$('#paneloculto2').slideUp();

				e.preventDefault();
			}
		);
	}
	;
	if ($('#alternar3').toggle)
	{
		$('#alternar3').toggle(
			function (e)
			{
				$('#paneloculto3').slideDown();

				e.preventDefault();
			},

			function (e)
			{
				$('#paneloculto3').slideUp();

				e.preventDefault();
			}
		);
	}
	;
	if ($('#alternar4').toggle)
	{
		$('#alternar4').toggle(
			function (e)
			{
				$('#paneloculto4').slideDown();

				e.preventDefault();
			},

			function (e)
			{
				$('#paneloculto4').slideUp();

				e.preventDefault();
			}
		);
	}
	;
	if ($('#alternar5').toggle)
	{
		$('#alternar5').toggle(
			function (e)
			{
				$('#paneloculto5').slideDown();

				e.preventDefault();
			},

			function (e)
			{
				$('#paneloculto5').slideUp();

				e.preventDefault();
			}
		);
	}
	;
	if ($('#alternar6').toggle)
	{
		$('#alternar6').toggle(
			function (e)
			{
				$('#paneloculto6').slideDown();

				e.preventDefault();
			},

			function (e)
			{
				$('#paneloculto6').slideUp();

				e.preventDefault();
			}
		);
	}
	;
	if ($('#alternar7').toggle)
	{
		$('#alternar7').toggle(
			function (e)
			{
				$('#paneloculto7').slideDown();

				e.preventDefault();
			},

			function (e)
			{
				$('#paneloculto7').slideUp();

				e.preventDefault();
			}
		);
	}
	;

}

//-- End init

function favorito(page_id, input, url)
{
	$.ajax({
		url: url,
		type: "post",
		data: {page_id: page_id},
		success: function (data)
		{
			if (data == 0 || data == 2)
			{
				$(input).removeClass("added");
			}
			else
			{
				$(input).addClass("added");
			}

		}
	}, "json");
}


