(function() {
    'use strict';

    angular
        .module('bienvenidaAdmin')
        .controller('RegionAgregarController', RegionAgregarController);

    RegionAgregarController.$inject = ['Restangular'];

		function RegionAgregarController(Restangular){

			var vm = this;

			var RegionURL = Restangular.one('region');
			// Restangular.all('websiteText').customGET("data",content);
			vm.form={};
			// console.log(vm.form);

			vm.register = function() {
				console.log(vm.form);
				RegionURL.customPOST(vm.form,'post',{},{}).then(function(data){
					console.log(data);
				});
			}

    }
})();
