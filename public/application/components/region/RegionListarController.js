(function() {
    'use strict';

    angular
        .module('bienvenidaAdmin')
        .controller('RegionListarController', RegionListarController);

    RegionListarController.$inject =
			[	'$state',
				'Restangular'];

    function RegionListarController(
			$state,
			Restangular) {

        var vm = this;

				var ListRegions = Restangular.all('region').getList();

				vm.regions = ListRegions.$object;

				vm.goToRegion = function(region,event){
					$state.go('region.editar',{region:region});
				};
    }
})();
