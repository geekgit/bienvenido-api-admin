(function() {
    'use strict';

    angular
        .module('bienvenidaAdmin')
        .controller('RegionController', RegionController);

    RegionController.$inject =
    [ 'Restangular',
      '$stateParams',
      '$filter',
      '$state'];

		function RegionController(
      Restangular,
      $stateParams,
      $filter,
      $state){

			var vm = this;

      var name = $stateParams.region;
      console.log(name);

      var ListRegions = Restangular.all('region').getList();

      vm.textoIntro = {};

      if ($state.is('region.editar')) {
        console.log('Prueba');
        ListRegions.then(function(response){
          var result = $filter('filter')(response,function(select){
            return select.name === name;
          })[0];

          //vm.idRegion = result.id;

          var ActualRegion = Restangular.one('region',result.id);

          ActualRegion.get().then(function(response) {

            vm.textoIntro.description = response.description;

            vm.submitDescription = function(){
              ActualRegion.customPOST(vm.textoIntro,'edit',{},{}).then(function(data){
                console.log(data);
              });
            }
          });

        });
      }

    }
})();
