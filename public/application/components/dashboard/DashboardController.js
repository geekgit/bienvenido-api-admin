(function() {
    'use strict';

    angular
        .module('bienvenidaAdmin')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = ['$mdSidenav','$state','Restangular'];

		function DashboardController($mdSidenav,$state,Restangular){

			var vm = this;

			/*vm.openLeftMenu = function() {
    		$mdSidenav('left').toggle();
  		};*/

      var ListRegions = Restangular.all('region').getList();

      /*ListRegionsURL.then(function(response){
        console.log(response);
      });*/

      vm.regions = ListRegions.$object;

      vm.opcionRegionElegida = '';

      vm.changeSelect= function(){

        if (vm.opcionRegionElegida == 'Agregar') {
          $state.go('region.agregar');
        }else if (vm.opcionRegionElegida == 'Listar') {
          $state.go('region.listar');
        }else {
          $state.go('region.editar',{region:vm.opcionRegionElegida});
        }
      }

    }
})();
