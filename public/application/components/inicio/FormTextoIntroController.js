(function() {
    'use strict';

    angular
        .module('bienvenidaAdmin')
        .controller('FormTextoIntroController', FormTextoIntroController);

    FormTextoIntroController.$inject = ['Restangular'];

    function FormTextoIntroController(Restangular) {
        var vm = this;

				var WebsiteText = Restangular.all('websiteText');
				// Restangular.all('websiteText').customGET("data",content);
        vm.form={};
				// console.log(vm.form);

				vm.register=function() {
					console.log(vm.form);
					WebsiteText.post(vm.form).then(function(data){
						console.log(data);
					});
				}

    }
})();
