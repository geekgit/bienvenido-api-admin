'user strict';

var GeekApp = angular.module('GeekApp', ['ngRoute', 'ngResource', 'ui.tree', 'foundation', 'ngFileUpload','mm.foundation','ui.tinymce']);

GeekApp.config(function($routeProvider){
	$routeProvider
		.when('/', { templateUrl:appConfig.views + '/dashboard'})
		.when('/pages/:contentID?', { templateUrl:appConfig.views + '/pages'})
		.when('/contents/:contentID?', { templateUrl:appConfig.views + '/content'})
		.when('/security/:userID?',{ templateUrl:appConfig.views + '/security'})
		.when('/banners/:bannerID?',{ templateUrl:appConfig.views + '/banners'})
		.when('/imagesfb/:bannerID?',{ templateUrl:appConfig.views + '/imagesfb'})
		.when('/config',{ templateUrl:appConfig.views + '/config'})
		.otherwise({ redirectTo: '/' });
});


window.onresize = function() { checkScreenSize(); };

function checkScreenSize()
{
	if(window.innerWidth < 500)
	{
		console.log(window.innerWidth);
		document.getElementsByTagName('body')[0].className = "mobile";
	}else{
		document.getElementsByTagName('body')[0].className = "";
	}
}



GeekApp.directive('sbLoad', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            var fn = $parse(attrs.sbLoad);
            elem.on('load', function (event) {
                scope.$apply(function() {
                    fn(scope, { $event: event });
                });
            });
        }
    };
}]);