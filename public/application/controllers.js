GeekApp.controller('MenuController', function ($scope, $location) {
	$scope.sections = [
		{label: 'Inicio', icon: 'icon-grid', href:'#/'},
		{label: 'Páginas', icon: 'icon-puzzle', href:'#pages'},
		{label: 'Destinos', icon: 'icon-directions', href:'#contents'},
		{label: 'Banners', icon: 'icon-basket-loaded', href:'#banners'},
		{label: 'Imagenes', icon: 'icon-images', href:'#imagesfb'},
		{label: 'Seguridad', icon: 'icon-users', href:'#security'},
		{label: 'Ajustes', icon: 'icon-settings', href:'#config'}
	];

	$scope.isSelected = function(_section)
	{
		var clase = "";

		if($location.path().indexOf(_section.href.substring(1)) > 0)
			clase = 'selected';

		return clase;
	};
});

GeekApp.controller('HomeController', function ($scope) {
	$scope.users = [
		{'name': 'Admin', 'email': 'gerson@geekadvice.pe', items:[1,4,3,3]},
		{'name': 'Gerson', 'email': 'gerson@geekadvice.pe', items:[1,4,3,3]},
		{'name': 'Roberto Gonzales', 'email': 'gerson@geekadvice.pe', items:[1,4,3,3]},
		{'name': 'Pedro Martinez', 'email': 'gerson@geekadvice.pe', items:[1,4,3,3]},
		{'name': 'Otro nombre', 'email': 'gerson@geekadvice.pe', items:[1,4,3,3]}
	];
});


GeekApp.controller('SecurityController', function ($scope, $routeParams) {
	$scope.users = [
		{'name': 'Admin', 'email': 'gerson@geekadvice.pe', rol:'admin'},
		{'name': 'Gerson', 'email': 'gerson@geekadvice.pe', rol:'editor'},
		{'name': 'Roberto Gonzales', 'email': 'gerson@geekadvice.pe', rol:'cliente'},
		{'name': 'Pedro Martinez', 'email': 'gerson@geekadvice.pe', rol:'cliente'},
		{'name': 'Otro nombre', 'email': 'gerson@geekadvice.pe', rol:'usuario'}
	];
	$scope.userID = $routeParams.userID;
});

GeekApp.controller('BannersController', function ($scope, $routeParams) {
	$scope.users = [
		{'name': 'La Ocopa banner Arequipa', 'email': 'landscape80*800', rol:'admin'},
		{'name': 'La Ocopa banner Arequipa', 'email': 'landscape80*800', rol:'admin'},
		{'name': 'La Ocopa banner Arequipa', 'email': 'landscape80*800', rol:'admin'},
		{'name': 'La Ocopa banner Arequipa', 'email': 'landscape80*800', rol:'admin'}
	];
	$scope.bannerID = $routeParams.bannerID;
});
