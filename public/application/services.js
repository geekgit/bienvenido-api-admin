'use strict';

/* Services */

var APIResources = function($resource)
{
	return {
		routes:
		{
			//CONTENIDOS
			saveContent:appConfig.api + 'contents/save',
			deleteContent:appConfig.api + 'contents/delete',
			savemap:appConfig.api + 'contents/savemap',

			//MULTIMEDIA
			saveMultimedia:appConfig.api + 'multimedia/save',
			deleteMultimedia:appConfig.api + 'multimedia/delete',
			saveDragImage:appConfig.api + 'multimedia/drag',

			//BANNERS
			saveBanner:appConfig.api + 'banner/save',
			findBanner:appConfig.api + 'banner/find',
			deletePageBanner:appConfig.api + 'banner/deletepage',
			savePageBanner:appConfig.api + 'banner/savepage',

			//REGIONES
			getPages:appConfig.api + 'regions/select',

			//PAGINAS
			savePage:appConfig.api + 'pages/save',
			deletePage:appConfig.api + 'pages/delete',
			addChild:appConfig.api + 'pages/attach',
			deletelist:appConfig.api + 'pages/delete-list',

			//FACEBOOK
			saveimagefb:appConfig.api + 'facebook/imageurl'
		},
		users : $resource(appConfig.api + '/users'),
		companies : $resource(appConfig.api + '/companies'),
		devices : $resource(appConfig.api + '/devices'),
		places : $resource(appConfig.api + '/places'),
		segments : $resource(appConfig.api + '/devices/segment'),
		imagesfb : $resource(appConfig.api + 'facebook/images'),
		banners : $resource(appConfig.api + 'banner/getall'),
		pages : $resource(appConfig.api + 'pages/select/:pageId', {pageId:'@id'})
	};
};

var SystemFactory = function()
{
	return {

	};
};

GeekApp.factory('APIResources', APIResources);
GeekApp.factory('SystemFactory', SystemFactory);