GeekApp.controller('bannerController', function ($scope, $http, APIResources, Upload,$timeout)
{
    $scope.bannerSelect=false;
    $scope.banners = APIResources.banners.get();
    $scope.pagesAvaliable = APIResources.pages.get();
    $scope.editBanner ={};
    $scope.bannercreate=true;
    $scope.banneredit=false;

    $scope.editBanner5=function(banner)
    {
        $scope.bannerSelect=true;
        $scope.editBanner=banner;
       // $('#file').val(_banner.multimedia[0].source);

        if($scope.editBanner.multimedia[0].source==null)
        {
            $scope.bannercreate=true;
            $scope.banneredit=false;
        }
        else
        {  $scope.bannercreate=false;
            $scope.banneredit=true;

        }
    };

    $scope.selectBanner=function()
    {
        /*$scope.bannercreate=true;
        $scope.banneredit=false;*/
    };

    $scope.uploadBanner = function (files) {

        if ($scope.editBanner!=null) {
            Upload.upload({
                url: APIResources.routes.saveBanner,
                fields: {description:$scope.editBanner.description,name: $scope.editBanner.name, id: $scope.editBanner.id},
                file: files
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            }).success(function (data, status, headers, config) {
                alert('Datos Guardados');
                //$scope.banners = APIResources.banners.get();
                $scope.bannerEdit = null;
                $scope.files = null;
                $scope.bannercreate=true;
                $scope.banneredit=false;
            });
        }
    };

    $scope.addPage=function()
    {
        if($scope.selectedPage!=null && !isNaN($scope.selectedPage))
        {
            $http.post(APIResources.routes.savePageBanner,
                {
                    page_id:$scope.selectedPage,
                    banner_id: $scope.editBanner.id
                }).success(function(_data){
                    alert('Cambios guardados');
                    $scope.findBanner();
                }).error(function(_error){
                    console.log(_error);
                    alert('Ocurrio un error');
                });
        }
        else {
            alert('Debe seleccionar una pagina.');
        }
    };

    $scope.findBanner=function()
    {
        $http.post(APIResources.routes.findBanner,
            {
                id:$scope.editBanner.id
            }).success(function(data){
                $scope.editBanner = data.data[0];
            }).error(function(_error){
                console.log(_error);
            });
    };

    $scope.deletePage=function(_pageId)
    {
        $http.post(APIResources.routes.deletePageBanner,
            {
                page_id:_pageId,
                banner_id: $scope.editBanner.id
            })
            .success(function(data){
                alert('Pagina eliminada');
                $scope.findBanner();
            })
            .error(function(error)
            {
                console.log(error);
                alert('Ocurrio un error');
            });
    }
});