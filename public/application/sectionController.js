GeekApp.controller('SectionsController', function ($scope, $http, APIResources, Upload,$timeout)
{
    /*$scope.options = {
        accept: function(sourceNode, destNodes, destIndex) {
            /*var data = sourceNode.$modelValue;
            var destType = destNodes.$element.attr('data-type');
            return (data.type == destType); // only accept the same type*/
       /* },
        dragStop: function(event) {
            console.log(event);

        }
    };*/
    $scope.tinymceOptions = {
        onChange: function(e) {
            // put logic here for keypress and cut/paste changes
        },
        inline: false,
        //   plugins : 'advlist autolink link image lists charmap print preview',
        plugins :'table image',
        skin: 'lightgray',
        theme : 'modern',
        menu : { // this is the complete default configuration
            table  : {title : 'Table' , items : 'inserttable tableprops deletetable | cell row column'},
            tools  : {title : 'Tools' , items : 'spellchecker code'}
        }
    };
    $scope.description='Nuevo';
    $scope.pages = null;
    $scope.pagesAvaliable = APIResources.pages.get();
    $scope.contentEdit = '';
    $scope.multimediaEdit = null;
    $scope.pageEdit = false;
    $scope.type = null;
    $scope.selectedPage = null;
    $scope.myImage='';
    $scope.pagetype='';
    $scope.myCroppedImage='';
    $scope.showmap=false;
    $scope.optionPage=false;
    $scope.preview=null;
    $scope.levelpage=null;
    $scope.newpage=null;
    $scope.drag={x1:0,x2:0,y1:0,y2:0};
    $scope.formResize={size:'1600x900'};
    $scope.formDrag={type:'Slider'};
    $scope.latitud=0;
    $scope.longitud=0;
    $scope.zoom=4;
    var markers = [];

    var map;
    $scope.selects = {};
    $scope.selects.lang="es";
    $scope.lan="es";
	var handleFileSelect=function(evt) {
		var file=evt.currentTarget.files[0];
		var reader = new FileReader();
		reader.onload = function (evt) {
			$scope.$apply(function($scope){
				$scope.myImage=evt.target.result;
			});
		};
		reader.readAsDataURL(file);
	};
	angular.element(document.querySelector('#fileInput')).on('change',handleFileSelect);

    function placeMarkerAndPanTo(latLng, map) {

        var marker = new google.maps.Marker({
            position: latLng,
            map: map
        });
        markers.push(marker);
        $scope.latitud=marker.position.lat();
        $scope.longitud=marker.position.lng();
        $scope.zoom=map.getZoom();
        $('#lat').val(marker.position.lat());
        $('#log').val(marker.position.lng());
    }
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }

    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }

    }

    function clearMarkers() {
        setMapOnAll(null);
    }

    $scope.actualizar=function(){
        if($scope.pageEdit.data.ubications.length>0)
        {
            $scope.latitud=$scope.pageEdit.data.ubications[0].content;
            $scope.longitud=$scope.pageEdit.data.ubications[1].content;

        }
        else
        {
            $scope.latitud=0;
            $scope.longitud=0;
        }
        if($scope.pageEdit.data.ubications.length>2){
            $scope.zoom=$scope.pageEdit.data.ubications[2].content;
        }
        else{
            $scope.zoom=4;
        }
       var myLatlng = new google.maps.LatLng($scope.latitud,$scope.longitud);
        var mapOptions = {
            zoom:  parseInt($scope.zoom),
            center: myLatlng
        };
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        // Place a draggable marker on the map
        var marker = new google.maps.Marker({
         position: myLatlng,
         map: map,
         title:""
         });
        map.addListener('click', function(e) {
            deleteMarkers();
            placeMarkerAndPanTo(e.latLng, map);
        });

        $('#lat').val($scope.latitud);
        $('#log').val($scope.longitud);
        if(angular.isDefined(map))
        {
            var myIframe = $('#map-canvas iframe:first');
            myIframe.attr('src',myIframe.attr('src')+'');

        }
        markers.push(marker);

    };
    $scope.deleteList=function(childid,pageid){
        $http.post(APIResources.routes.deletelist,{child_id:childid,page_id:pageid}).success(function(data){
            swal({title: "",   text: "Se elimino el elemento con éxito",   timer: 1000,   showConfirmButton: false, type: "success" });
            $scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
        }).error(function(_error){
            console.log(_error);
            swal({title: "",   text: "Ocurrio al intentar eliminar el registro, vuelva a intentarlo nuevamente!", type: "warning" });
        });
    };
    $scope.savepointmap=function(){
        $scope.zoom=map.getZoom();
        $http.post(APIResources.routes.savemap,{pageId:$scope.pageEdit.data.id,lat: $scope.latitud,lng:$scope.longitud,zoom:$scope.zoom}).success(function(_data){
            swal({title: "",   text: "Se guardaron los cambios con éxito",   timer: 1000,   showConfirmButton: false, type: "success" });
            $scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
        }).error(function(_error){
            console.log(_error);
            swal({title: "",   text: "Ocurrio al intentar guardar los registros, vuelva a intentarlo nuevamente!", type: "warning" });
        });
    };

	$scope.setDefaults = function(_type)
	{
        $scope.pagetype=_type;
		$scope.levelpage = _type;
		$scope.pages = APIResources.pages.get({pageId:_type});
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = 'http://maps.googleapis.com/maps/api/js?v=3.exp' +
        '&signed_in=true&callback=initialize';

        $scope.showmap=false;
	};
    $scope.saveDrag=function()
    {
        var picture = $('#thepicture');
        $scope.formResize.guillotine = picture.guillotine('getData');
        $http.post(APIResources.routes.saveDragImage,{'size':$scope.formResize.size,'multimedia_id':$scope.formResize.image.id,'guillotine':$scope.formResize.guillotine}).success(function(_data){
            swal({title: "",   text: "Se guardaron los cambios con éxito",   timer: 1000,   showConfirmButton: false, type: "success" });
        }).error(function(_error){
            console.log(_error);
            alert('Ocurrio un error');
            swal({title: "",   text: "Ocurrio al intentar guardar los registros, vuelva a intentarlo nuevamente!", type: "warning" });
        });
    };

    $scope.$watch('files', function () {
        $scope.upload($scope.files);
    });

    $scope.selector=function(c)
    {
        $scope.formResize.image= c;
        //$scope.formDrag.type= c.type;
        /*$("img").load(function(){
            $scope.dragType();
        });*/
        /*var picture = $('#thepicture');
        picture.guillotine('remove');
        picture.guillotine({width: 300, height: 300});
        $scope.formResize.guillotine = picture.guillotine('getData');*/

    };

    $scope.zoomIn=function()
    {
        var picture = $('#thepicture');
        picture.guillotine('zoomIn');
    };

    $scope.zoomOut=function()
    {
        var picture = $('#thepicture');
        picture.guillotine('zoomOut');
    };

    $scope.dragType=function()
    {
        if($scope.formDrag.type=='Slider')
        {
            var _width=$scope.formResize.size;
            $scope.array = _width.split('x');
            var picture2 = $('#thepicture');
            picture2.guillotine('remove');
            //picture2.guillotine({width: $scope.formResize.image.weight, height: $scope.formResize.image.height ,init: { x: $scope.formResize.image.offsetx, y: $scope.formResize.image.offsety}});
            picture2.guillotine({width: 300, height: 100 /*,init: { x: $scope.formResize.image.offsetx, y: $scope.formResize.image.offsety}*/});
            picture2.guillotine('fit');
            $("#theparent").css("width", "100%");
            $("#theparent").css("height", "500px");
            $scope.formResize.guillotine = picture2.guillotine('getData');
        }
        if($scope.formDrag.type=='Thumbnail'){
            var picture3 = $('#thepicture');
            picture3.guillotine('remove');
            picture3.guillotine({width: 300, height: 300 ,init: { x: $scope.formResize.image.offsetx, y: $scope.formResize.image.offsety}});
            //picture3.guillotine({ width: $scope.formResize.image.width, height: $scope.formResize.image.height, init: { x: $scope.formResize.image.offsetx, y: $scope.formResize.image.offsety}});
            $("#theparent").css("width", "300");
            $("#theparent").css("height", "300");
            $scope.formResize.guillotine = picture3.guillotine('getData');
        }

    };


	$scope.addChild = function(_id)
	{
		//console.log($scope.selectedPage);
		$http.post(APIResources.routes.addChild, {child_id:_id, page_id:$scope.pageEdit.data.id}).success(function(_data){
            swal({title: "",   text: "Se guardaron los cambios con éxito",   timer: 1000,   showConfirmButton: false, type: "success" });
			$scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
			$scope.contentEdit = null;
		});
	};

	$scope.addPage = function(_page, _type)
	{
        if($scope.newpage==null)
        {
            (_type=='')?$scope.optionPage=false:$scope.optionPage=true;
            if(!_type) _type = '';
            var page = {name: 'Nuevo', type:_type, childs:[]};
            $scope.pageEdit = {data:page};
            $scope.contentEdit = null;
            $scope.selectedPage = {id:undefined};
            if(_page)
            {
                page.page_id = _page.id;
                _page.childs.push(page);
            }else{
                $scope.pages.data.push(page);
            }
            $scope.newpage=page;
            console.log($scope.pages.data);
        }
        else{
            swal("", "Debe de guardar los cambios antes de seguir agregando más paginas.", "info");
        }
	};

	$scope.editPage = function(_page)
	{
		if(_page.id)
		{
			$scope.selectedPage = _page;
			$scope.pageEdit = APIResources.pages.get({pageId:_page.id});
			$scope.contentEdit = null;
			$scope.multimediaEdit = null;
		}
        else{

            $scope.pageEdit.data = $scope.newpage;
        }
	};
    $scope.previzualizar=function(code,type)
    {
        if(type=='destino')
        {
            window.open('destino/'+code);
        }
        else if(type=='subdestino')
        {
            window.open('sub-destino/'+code);
        }
        else if(type=='region')
        {
            window.open('region/'+code);
        }
        else if(type=='sitio')
        {
            window.open('parallax/'+code);
        }

    };

	$scope.isSelectedPage = function(_page)
	{

		//console.log(_page.id);
		if($scope.selectedPage)
		{
			//console.log($scope.selectedPage.id);
			if($scope.selectedPage.id == _page.id)
				return 'node-selected';
		}
	};

	//Content
	$scope.editContent = function(_content)
	{
		$scope.contentEdit = _content;

        tinyMCE.activeEditor.setContent($scope.contentEdit.content);
        $('#description').val($scope.description);
	};
	$scope.saveContent = function()
	{
		$scope.contentEdit.page_id = $scope.pageEdit.data.id;
        $scope.contentEdit.content=tinyMCE.activeEditor.getContent();
		$http.post(APIResources.routes.saveContent, $scope.contentEdit).success(function(_data){
            swal({title: "",   text: "Se guardaron los cambios con éxito",   timer: 1000,   showConfirmButton: false, type: "success" });
			$scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
			$scope.contentEdit = null;
            tinyMCE.activeEditor.setContent('');
		});
	};
	$scope.deleteContent = function(_id)
	{
		//$scope.contentEdit.page_id = $scope.pageEdit.data.id;
		$http.post(APIResources.routes.deleteContent, {id:_id}).success(function(_data){
            swal({title: "",   text: "Se elimino el registro con éxito",   timer: 1000,   showConfirmButton: false, type: "success" });
			$scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
			$scope.contentEdit = {};
		});
	};

	$scope.createContent = function()
	{
		$scope.contentEdit = {name:'Nuevo', content:'Nuevo',lang:'es'};
        $scope.description='Nuevo';
	};

	$scope.editMultimedia = function(_content)
	{
		$scope.multimediaEdit = _content;
    if($scope.multimediaEdit.source==null)
    {
        $scope.selectcreate=true;
        $scope.selectedit=false;
    }
    else
    {  $scope.selectcreate=false;
        $scope.selectedit=true;

    }
	};

    $scope.editBanner = function(_content)
    {
        $scope.bannerEdit = _content;
        if($scope.bannerEdit.multimedia[0].source==null)
        {
            $scope.bannercreate=true;
            $scope.banneredit=false;
        }
        else
        {  $scope.bannercreate=false;
            $scope.banneredit=true;

        }
    };
	$scope.createMultimedia = function()
	{
		$scope.multimediaEdit = {name:'Nuevo', content:'Nuevo', code:''};
        $scope.selectcreate=true;
        $scope.selectedit=false;
	};

    $scope.createBanner = function()
    {
        $scope.bannerEdit = {name:'Nuevo', content:'Nuevo', code:''};
        $scope.bannercreate=true;
        $scope.banneredit=false;
    };

    $scope.selectimage=function()
    {
        $scope.selectcreate=true;
        $scope.selectedit=false;
    };

    $scope.selectBanner=function()
    {
        $scope.bannercreate=true;
        $scope.banneredit=false;
    };
	$scope.upload = function (files) {
		//console.log($scope.pageEdit.data.id);
		//if (files) {
   if ($scope.multimediaEdit!=null) {
				Upload.upload({
					url: APIResources.routes.saveMultimedia,
					fields: {description:$scope.multimediaEdit.description,name: $scope.multimediaEdit.name, code:$scope.multimediaEdit.code, id: $scope.multimediaEdit.id, page_id: $scope.pageEdit.data.id},
					file: files
				}).progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
				}).success(function (data, status, headers, config) {
					//console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                    swal({title: "",   text: "Se guardaron los cambios con éxito",   timer: 1000,   showConfirmButton: false, type: "success" });
					$scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
					$scope.multimediaEdit = null;
					$scope.files = null;
				});
		}
	};


    $scope.uploadBanner = function (files) {
        if ($scope.bannerEdit!=null) {
            Upload.upload({
                url: APIResources.routes.saveBanner,
                fields: {description:$scope.bannerEdit.description,name: $scope.bannerEdit.name, id: $scope.bannerEdit.id,page_id: $scope.pageEdit.data.id},
                file: files
            }).progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
            }).success(function (data, status, headers, config) {
                //console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
                swal({title: "",   text: "Los datos se guardaron con éxito!",   timer: 1000,   showConfirmButton: false, type: "success" });
                $scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
                $scope.bannerEdit = null;
                $scope.files = null;
            });
        }
    };

    $scope.previewImage=function(_path)
    {
        $scope.preview=_path;
    };

	$scope.deleteMultimedia = function(_id)
	{
		$http.post(APIResources.routes.deleteMultimedia, {id:_id}).success(function(_data){
            swal({title: "",   text: "Se elimino el registro con éxito!",   timer: 1000,   showConfirmButton: false, type: "success" });
			$scope.pageEdit = APIResources.pages.get({pageId:$scope.pageEdit.data.id});
			$scope.multimediaEdit = null;
		});
	};

	$scope.saveSectionDetail = function(_type)
	{
		console.log(_type);
		$http.post(APIResources.routes.savePage, $scope.pageEdit.data).success(function(_data){
			//gu$scope.pageEdit.data.id = _data.data.id;
            console.log(_data);
            console.log($scope.pageEdit.data);
            swal({title: "",   text: "Cambios guardados con éxito!",   timer: 1000,   showConfirmButton: false, type: "success" });
            $scope.contentEdit = null;
            $scope.pages = APIResources.pages.get({pageId:$scope.pagetype});
            $scope.newpage=null;
		}).error(function(_error){
			console.log(_error);
            swal({title: "",   text: "Ocurrio al intentar guardar los registros, vuelva a intentarlo nuevamente!", type: "warning" });
		});
        $scope.pagesAvaliable = APIResources.pages.get();
	};

	$scope.deleteSection = function(_id)
	{
        if(($scope.pageEdit.data.id)!=null &&($scope.pageEdit.data.id)!=undefined&& !isNaN(($scope.pageEdit.data.id)) )
        {
            if (confirm('Esta seguro que desea eliminar este elemento?')) {
                $http.post(APIResources.routes.deletePage, {id:$scope.pageEdit.data.id}).success(function(_data){
                    $scope.pages = APIResources.pages.get({pageId:$scope.pagetype});
                    $scope.newpage=null;
                    /*var index = -1;
                     var comArr = eval($scope.pages.data);
                     for (var i = 0; i < comArr.length; i++) {
                     if (comArr[i].id === $scope.pageEdit.data.id) {
                     index = i;
                     break;
                     }
                     }
                     if (index === -1) {
                     }
                     $scope.pages.data.splice(index, 1);*/
                    $scope.pageEdit = null;
                    swal({title: "",   text: "Contenido eliminado con éxito!",   timer: 1000,   showConfirmButton: false, type: "success" });
                });
            }
        }
        else{
            $scope.newpage=null;
            swal({title: "",   text: "Contenido eliminado con éxito!",   timer: 1000,   showConfirmButton: false, type: "success" });
            $scope.pageEdit = null;
            $scope.pages = APIResources.pages.get({pageId:'pages'});
        }

	};
});