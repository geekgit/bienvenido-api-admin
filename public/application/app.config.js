(function() {
    'use strict';

    angular
			.module('bienvenidaAdmin')
			.config([
        '$locationProvider',
        'RestangularProvider',
        function($locationProvider,RestangularProvider){
				  $locationProvider
            .html5Mode(true)
            .hashPrefix('!');
          RestangularProvider
            .setBaseUrl('https://apis.formaxlabs.com/');
			  }]);
})();
