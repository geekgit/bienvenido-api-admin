GeekApp.controller('FacebookController',function ($scope, $http, APIResources, Upload,$timeout){

    $scope.images = APIResources.imagesfb.get();
    $scope.urlfb=null;
    $scope.preview=null;

    $scope.saveURL=function()
    {
        if($scope.urlfb!=null && $scope.urlfb!='')
        {
            $http.post(APIResources.routes.saveimagefb,{'url':$scope.urlfb})
                .success(function(_data){
                    if(_data.status.code=='200')
                    {
                        swal({title: "",   text: "La imagen se guardo con éxito!",   timer: 1000,   showConfirmButton: false, type: "success" });
                    }
                }).error(function(_error){
                    console.log(_error);
                    swal({title: "",   text: "Ocurrio al intentar guardar la URL, vuelva a intentarlo nuevamente!", type: "warning" });
                });
        }
        else{
            swal({title: "",   text: "Debe ingresar una URL", type: "info" });
        }

    };

    $scope.previewImage=function(_path)
    {
        $scope.preview=_path;
    };
});