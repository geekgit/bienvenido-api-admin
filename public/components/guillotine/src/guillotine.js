
(function(){

    if (!window.smw) {
        window.smw = {};
    }


    //utilities
    var hasClass = function(node,clss) {
        return new RegExp('\\b'+clss+'\\b').test(node.className);
    };

    var getClassThatStartsWith = function(node,starts) {
        var m = new RegExp('\\b'+starts+'([a-zA-Z0-9]+)\\b').test(node.className);
        if (m !== null && m.length > 0) {
            return m[1];
        }
        return null;
    };

    /**
     * Copy all items from src to dst, overwriting or skipping any existing items
     * Calls itself recursively when an items in src and dest with same key are both objects.
     *
     * @param {object} dest
     * @param {object} src
     * @param {boolean} overwrite
     */
    var extend = function(dest, src, overwrite) {
        for (var k in src) {
            if (src.hasOwnProperty(k)) {

                if (src[k] !== null && typeof src[k] === 'object' &&
                        dest[k] !== null && typeof dest[k] === 'object' &&
                        !Array.isArray(src[k]) && !Array.isArray(dest[k])) {
                    extend(dest[k], src[k], overwrite);
                } else if (overwrite) {
                    dest[k] = src[k];
                } else if (!dest.hasOwnProperty(k)) {
                    dest[k] = src[k];
                }
            }
        }
        return dest;
    };


    /**
     * Get height minus bottom padding, i.e the height left for children
     * we leave out top padding since we compare to childrens offsetTop.
     */
    var getContainerBottom = function(node){
        var styles = window.getComputedStyle(node);
        return node.clientHeight - parseFloat(styles.paddingBottom);
    };

    var getPaddingBottom = function(node) {
        var styles = window.getComputedStyle(node);
        return parseFloat(styles.paddingBottom);
    };




    /**
     * Determine and return function to cut supplied node into pieces
     * until it fits. Looks at possible classes first, then nodeName.
     *
     * @param {DOMNode} node
     * @param {object} options
     * @return {object} The weapon object
     */
    var determineWeaponOfChoice = function(node, options) {
        var weaponName = getClassThatStartsWith(node, options.weaponPrefix);
        if (!weaponName) {
            var nodeName = node.nodeName.toLowerCase();
            if (options.weaponForElement.hasOwnProperty(nodeName)) {
                weaponName = options.weaponForElement[nodeName];
            }
        }
        return options.weapons[weaponName] || options.weapons[options.defaultWeapon];
    };

    /**
     * Recursively scans the node for pagebreak elements. Removes all nodes that appear after the node
     * the first page break while keeping the correct tree structure
     *
     * @param {Element} node
     * @param {object} options
     * @returns {DocumentFragment} Fragment containing all children that are after the pagebreak. null if no pagebreak was found
     */
    var chopPageBreaks = function(node, options) {
        var children = node.children;

        var pagebreakTriggered = false;
        var toRemove = [];
        var leftovers = null;
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            if (pagebreakTriggered) {
                toRemove.push(child);
            } else if (options.pagebreakElements.indexOf(child.nodeName.toLowerCase()) !== -1) {
                // child is a pagebreak element
                if (i > 0) {
                    // Ignore the page break if it is the first element
                    pagebreakTriggered = true;
                }
                node.removeChild(child);
                i--;
            } else if (options.pagebreakClass && hasClass(child, options.pagebreakClass)) {
                // The element itself is a pagebreak element, we brake after the tag with the class
                pagebreakTriggered = true;
            } else if (child.childElementCount) {
                // Dive down to next childtree
                var fragment = chopPageBreaks(child, options);
                if (fragment) {
                    var clone = child.cloneNode(false);
                    clone.appendChild(fragment);

                    leftovers = document.createDocumentFragment();
                    leftovers.appendChild(clone);
                    pagebreakTriggered = true;
                }
            }
        }

        if (pagebreakTriggered) {
            if (!leftovers) {
                leftovers = document.createDocumentFragment();
            }

            if (toRemove.length) {
                for (var i = 0; i < toRemove.length; i++) {
                    leftovers.appendChild(toRemove[i]);
                }
            }
        }

        return leftovers;
    };


    var chopchop = function(node, context, options) {
        var doesItOverflow = context.doesItOverflow;

        //calculate height available for children
        //or use override
        var containerBottom = context.containerBottom || getContainerBottom(node);
        var initiallyOverflows = doesItOverflow();

        var overflowedContent = null;

        
        var children = node.children;
        var toRemove = [];
        var removeRest = false;
        var lastChild, weapon, rest;

        // Start with filtering out all children that is completely outside the container / page.
        // or if a page break is triggered
        for (var i = 0; i < children.length; i++) {
            var child = children[i];
            if (removeRest) {
                toRemove.push(child);
            } else if (initiallyOverflows && child.offsetTop > containerBottom) {
                toRemove.push(child);
                removeRest = true;
            } else if (options.pagebreakElements.indexOf(child.nodeName.toLowerCase()) !== -1) {
                // child is a pagebreak element
                if (i > 0) {
                    // Ignore the page break if it is the first element
                    removeRest = true;
                }
                node.removeChild(child);
                i--;
            } else if (options.pagebreakClass && hasClass(child, options.pagebreakClass)) {
                // The element itself is a pagebreak element, we brake after the tag with the class
                removeRest = true;
            }
        }

        // Lets remove all the elements after pagebreak or that we know are outside
        if (toRemove.length) {
            overflowedContent = document.createDocumentFragment();
            for (i = 0; i < toRemove.length; i++) {
                overflowedContent.appendChild(toRemove[i]);
            }
        }

        // Now lets see if we have an earlier page break nested somewhere.
        // We skip the last element because we are unsure if we are going to recurse it.
        // It might still overflow and we will handle pagebreak for that element later in that case
        toRemove = [];
        var pagebreakFound = false;
        for (i = 0; i < children.length; i++) {
            child = children[i];
            if (pagebreakFound) {
                toRemove.push(child);
            } else if (i < children.length - 1){
                rest = chopPageBreaks(child, options);
                if (rest) {
                    pagebreakFound = true;
                }
            }
        }
        
        if (pagebreakFound) {
            // Got a page break before page overflowed
            for (i = 0; i < toRemove.length; i++) {
                rest.appendChild(toRemove[i]);
            }
            overflowedContent = insertFirst(rest, overflowedContent);

        } else if (node.childElementCount > 0) {
            // No page break found

            if (initiallyOverflows && doesItOverflow()) {
                lastChild = node.lastElementChild;
                var indivisable = hasClass(lastChild, options.indivisableClass) || options.indivisable.indexOf(lastChild.nodeName.toLowerCase()) !== -1;

                if (!indivisable) {
                    // node overflows and is can be chopped!
                    weapon = determineWeaponOfChoice(lastChild, options);
                    if (weapon.attack) {
                        var rest = weapon.attack(lastChild, extend({
                            containerBottom: containerBottom
                        }, context), options, weapon);

                        if (rest) {
                            overflowedContent = insertFirst(rest, overflowedContent);
                        }
                    } else {
                        indivisable = true;
                    }
                }

                if (indivisable) {
                    overflowedContent = insertFirst(lastChild, overflowedContent);
                }

                if (node.childElementCount > 0) {
                    // Keep running post attack until elements stop disappearing
                    do {
                        lastChild = node.lastElementChild;
                        weapon = determineWeaponOfChoice(lastChild, options);
                        if (weapon.lastElementPostAttack) {
                            var rest = weapon.lastElementPostAttack(lastChild, extend({
                                containerBottom: containerBottom
                            }, context), options, weapon);

                            if (rest) {
                                overflowedContent = insertFirst(rest, overflowedContent);
                            }
                        }

                    } while (lastChild && lastChild !== node.lastElementChild);
                }
            } else {
                // Last element did not overflow so lets see if there is a pagebreak in it
                var rest = chopPageBreaks(node, options);
                if (rest) {
                    overflowedContent = insertFirst(rest, overflowedContent);
                }
            }
        }

        return overflowedContent;
    };

    /**
     * Inserts an element or fragment as first child in another element or fragment.
     * If the dest does not exist, it is created as a document fragment.
     *
     * @param {Node|DocumentFragment} src - The node to insert
     * @param {Node|DocumentFragment} [dest] - destination node. Auto-created if falsy
     * @returns {Node|DocumentFragment} The updated or created destination node
     */
    var insertFirst = function(src, dest) {
        if (dest) {
            dest.insertBefore(src, dest.firstChild);
        } else {
            dest = document.createDocumentFragment();
            dest.appendChild(src);
        }
        return dest;
    };

    /**
     * Wraps the chopchop method by inserting the leftovers (overflowed DOM) into a clone of 'node'
     *
     * @param {Node} node Node that needs to be cut
     * @param {Object} context
     * @param {Object} options
     * @returns {Node} A shallow clone of 'node' with the 'rest' that didn't fit.
     */
    var cutBlock = function(node, context, options, weaponOptions){
        // Special case:
        // If the block has no childNodes at all, we cant do anything except moving it to the fragment
        if (node.children.length === 0) {
            node.parentNode.removeChild(node);
            return node;
        }

        //cutting a block is... well kinda exactly what guillotine does.
        //so let's recurse!
        // For each element we recurse into, subtract the element bottom padding from container height
        var fragment = chopchop(node,{
            doesItOverflow: context.doesItOverflow,
            containerBottom: context.containerBottom - getPaddingBottom(node),
            scanPageBreaks: true
        }, options);

        var clone = node.cloneNode(false);
        // This while loop is for browsers that does not support cloneNode(deep=false), which basically is only old old firefox versions
        // Anyway, it does not hurt performance at all so lets just leave it
        while (clone.firstChild) {
            clone.removeChild(clone.firstChild);
        }
        clone.appendChild(fragment);
        return clone;
    };

    /**
     * Cuts <ol> and <ul> Fixing numbering, widows and orphans.
     *
     * @param {Element} node
     * @param {Object} context
     * @param {Object} options
     * @returns {Element}
     */
    var cutList = function(node, context, options, weaponOptions) {
        var leftovers = cutBlock(node, context, options);

        if (leftovers && node && leftovers !== node) {
            if (leftovers.childElementCount === 1 && !weaponOptions.allowWidows) {
                // We have a widow!
                if (node.childElementCount >= 1) {
                    var lastChild = node.lastElementChild;
                    node.removeChild(lastChild);
                    leftovers.insertBefore(lastChild, leftovers.firstChild);
                }
            }

            if (node.childElementCount === 1 && !weaponOptions.allowOrphans) {
                // We have an orphan lets move it to leftovers
                var orphan = node.lastElementChild;
                node.removeChild(orphan);
                leftovers.insertBefore(orphan, leftovers.firstChild);
            }

            if (node.nodeName === 'OL') {
                var computedStyle = window.getComputedStyle(node);
                var counterReset = computedStyle['counter-reset'].trim();
                var prevStart;
                if (counterReset) {
                    var parts = counterReset.split(' ');

                    if (parts.length < 2) {
                        parts.push(0);
                        prevStart = 0;
                    } else {
                        prevStart = parseInt(parts[1]);
                        if (isNaN(prevStart)) {
                            prevStart = 0;
                            parts.splice(1, 0, 0);
                        }
                    }

                    parts[1] = node.children.length + prevStart;
                    leftovers.style.counterReset = parts.join(' ');

                }
                prevStart = parseInt(node.getAttribute('start')) || 1;
                leftovers.setAttribute('start', node.children.length + prevStart);
            }
        }
        return leftovers;
    };


    /**
     *  Cut text of a node so that it fits, returns what's left
     *  inside a clone of the original node
     *
     *  @param {DOMNode} node
     *  @param {number} containerBottom to check for overflow
     *  @return {DOMNode}
     */
    var cutText = function(node, context, options, weaponOptions) {
        var computedStyle = window.getComputedStyle(node);
        var containerBottom = context.containerBottom;// - parseFloat(computedStyle.marginBottom);

        var text  = node.innerHTML;
        var visibleHeight = containerBottom - node.offsetTop;
        var computedLineHeight = computedStyle.lineHeight;
        var fontSize = parseFloat(computedStyle.fontSize);
        var paddingTop = parseFloat(computedStyle.paddingTop);
        var paddingBottom = parseFloat(computedStyle.paddingBottom);

        var lineHeight;
        if (computedLineHeight === "normal") {
            lineHeight = weaponOptions.fallbackLineHeight * fontSize;
        } else {
            lineHeight = parseFloat(computedLineHeight);
        }

        var minPossibleHeight;
        if (weaponOptions.allowOrphans) {
            minPossibleHeight = lineHeight;
        } else {
            minPossibleHeight = lineHeight * weaponOptions.minLinesOrIsOrphan;
        }
        minPossibleHeight += paddingTop + paddingBottom;

        // Exit quickly if we cannot fit minPossibleHeight of content
        if (minPossibleHeight >= visibleHeight) {
            var clone = node.cloneNode(true);
            if (weaponOptions.removeNodeIfEmpty) {
                node.parentNode.removeChild(node);
            } else {
                while (node.firstChild) {
                    node.removeChild(node.firstChild);
                }
            }
            return clone;
        }


        var parts;
        if (weaponOptions.boundary === 'sentence') {
            // Breaking text into sentences is quite troublesome
            // More info about the problem can be found here:
            // https://en.wikipedia.org/wiki/Sentence_boundary_disambiguation
            // Good place to test: https://regex101.com/#javascript
            parts = text.match(/((?:.|\n)+?(?:[.?!]+\S*\s*|$))/g);
        } else /* word */ {
            parts = text.match(/\S+\s*/g);
        }

        // Account for widows
        if (!weaponOptions.allowWidows) {
            var overflowingHeight = node.offsetHeight - visibleHeight;
            var minHeightToPreventWidows = (weaponOptions.minLinesOrIsWidow - 1) * lineHeight;
            if (overflowingHeight < minHeightToPreventWidows) {
                // We will have widows, lets reduce visible height to
                visibleHeight -= minHeightToPreventWidows - overflowingHeight;
            }
        }

        //do a best guesstimate based on what percentage of node is visible
        var splitPoint = Math.floor(parts.length * (visibleHeight / node.clientHeight));
        node.innerHTML = joinText(parts, 0, splitPoint, false);


        //we ignore bottom margin of the element we're cutting
        if (node.offsetHeight > visibleHeight) {
            //overflowed
            do {
                splitPoint--;
                node.innerHTML = joinText(parts, 0, splitPoint, weaponOptions.preventLastWordWidowWhenCutting);
            } while (node.offsetHeight > visibleHeight && splitPoint > 0);

        } else {
            //not enough
            var overflows;
            do {
                splitPoint++;
                node.innerHTML = joinText(parts, 0, splitPoint, false);
                overflows = node.offsetHeight > visibleHeight;
            } while (!overflows && splitPoint < parts.length);

            if (overflows) {
                splitPoint--;
                node.innerHTML = joinText(parts, 0, splitPoint, weaponOptions.preventLastWordWidowWhenCutting);
            }
        }

        var remainingHeight = visibleHeight - node.offsetHeight;
        var clone;

        if (weaponOptions.boundary === 'sentence' && splitPoint > 0 && remainingHeight >= lineHeight * (weaponOptions.maxEmptyLinesMidParagraph + 1)) {
            // We have to much space available here. Lets cut on a per word basis.
            splitPoint++;
            node.innerHTML = joinText(parts, 0, splitPoint, false);
            clone = cutText(node, context, options, extend({
                boundary: 'word',
                removeNodeIfEmpty: false
            }, weaponOptions));
            clone.innerHTML += joinText(parts, splitPoint, void 0, false);
        }

        if (!weaponOptions.allowOrphans && node.clientHeight - paddingTop - paddingBottom < lineHeight * weaponOptions.minLinesOrIsOrphan) {
            // After cutting, we have an orphan or no text at all
            // There is to little space to fit more text on this page so force everything to next instead.
            splitPoint = 0;
        }

        if (splitPoint === 0) {
            if (weaponOptions.boundary === 'sentence' && remainingHeight > weaponOptions.maxEmptyLines * lineHeight) {
                // We are leaving to many empty lines here.
                splitPoint++;
                node.innerHTML = joinText(parts, 0, splitPoint, false);
                clone = cutText(node, context, options, extend({
                    boundary: 'word',
                    removeNodeIfEmpty: false,
                    allowWidows: true // There are no widows at this point
                }, weaponOptions));
                clone.innerHTML += joinText(parts, splitPoint, void 0, false);
            } else {
                // Nothing to keep in this node so lets just remove it
                if (weaponOptions.removeNodeIfEmpty) {
                    node.parentNode.removeChild(node);
                }
            }
        }

        if (!clone) {
            clone = node.cloneNode(false);
            //prepare what's left
            clone.innerHTML = joinText(parts, splitPoint, void 0, false);
        }

        return clone;
    };


    var joinText = function(parts, start, length, preventLastWordWidow) {
        var text = parts.slice(start, length).join('');
        if (preventLastWordWidow) {
            text = text.replace(/(\S+)\s*(\S+)(\s*)$/, function(match, word1, word2, end) {
                return word1 + '\u00A0' + word2 + end;
            });
        }
        return text;
    };

    /**
     * Post attack function for text nodes.
     *
     * @param {Element} node
     * @param {object} context
     * @param {object} options
     * @param {object} weaponOptions
     * @returns {Node} Content that should be moved to next page.
     */
    var cutTextPostAttack = function(node, context, options, weaponOptions) {
        if (weaponOptions.allowOrphans) {
            return null;
        }

        if (/[:;,]\s*$/.test(node.textContent)) {
            var computedStyle = window.getComputedStyle(node);
            var computedLineHeight = computedStyle.lineHeight;
            var fontSize = parseFloat(computedStyle.fontSize);
            var paddingTop = parseFloat(computedStyle.paddingTop);
            var paddingBottom = parseFloat(computedStyle.paddingBottom);

            var lineHeight;
            if (computedLineHeight === "normal") {
                lineHeight = weaponOptions.fallbackLineHeight * fontSize;
            } else {
                lineHeight = parseFloat(computedLineHeight);
            }

            var contentHeight = node.clientHeight - paddingTop - paddingBottom;

            if (contentHeight <= weaponOptions.maxLinesForListPrequelOrphan * lineHeight) {
                return node;
            }
        }

        return null;
    };


    /**
     * Always move headings to next page if they are the last element.
     */
    var cutHeadingPostAttack = function(node, context, options, weaponOptions) {
        return node;
    };



    var defaultOptions = {
        /**
         * Elements that should be considered indivisable. When guillotine encounters them, it wont try to cut them.
         * If such element overflows it will simply be moved to leftovers.
         */
        indivisable: ['img','figure','h1','h2','h3','h4','li','tr'],

        /**
         * Elements with this class will be considered indivisable
         */
        indivisableClass: 'indivisable',

        /**
         * Content after any of these elements will be removed
         */
        pagebreakElements: ['hr'],

        /**
         * Content after an element with this class will be removed
         */
        pagebreakClass: null,

        /**
         * Classes can be used to enable a special weapon for an element that needs to be cut.
         * Any class beginning with weaponPrefix will be considered a possible for the element.
         *
         * Example: class="weapon-super-cutter" will map to the weapon superCutter.
         */
        weaponPrefix: 'weapon-',

        /**
         * The default weapon for elements when no specialized weapon is found.
         */
        defaultWeapon: 'blockCutter',

        /**
         * Specialized weapons to use for certain elements
         */
        weaponForElement: {
            p: 'textCutter',
            ol: 'listCutter',
            ul: 'listCutter',
            h1: 'headingCutter',
            h2: 'headingCutter',
            h3: 'headingCutter',
            h4: 'headingCutter'
        },

        /**
         * Defines all weapons and their settings
         */
        weapons: {

            /**
             * Cuts nodes containing text.
             */
            textCutter: {

                /**
                 * Sets how the text is preferred to be cut.
                 * - sentence: The cutter will prefer to cut after a complete sentence.
                 *             Fallbacks to cut on a per "word" basis.
                 *             See other settings for controlling when this should happen.
                 * - word:     The cutter fits as many words as possible on each page while still obeying orphan/widow rules
                 */
                boundary: 'sentence', // Can be either "word" or "sentence"

                /**
                 * Enables/disables orphan handling.
                 * See https://en.wikipedia.org/wiki/Widows_and_orphans for details
                 *
                 * If a cut generates an orphan, the content will be moved to the next page instead.
                 */
                allowOrphans: false,

                /**
                 * Enabled/disabled widow handling
                 * If a cut generates a widow, content from the previous page will be moved to next page.
                 */
                allowWidows: false,

                /**
                 * If the result of the cut is to move all content to next page,
                 * this options decided whether to leave the node empty or if it should be removed
                 */
                removeNodeIfEmpty: true,

                /**
                 * Injects a non breaking space between the last two words at the point where the content is cut.
                 * Note that this does not inject non-breaking spaces at the end of the text. This should be done prior
                 * to calling guillotine to get a consistent behavior as guilloutine only processes items that overflow.
                 */
                preventLastWordWidowWhenCutting: true,

                /**
                 * When using sentence boundary, lots of white-space can be generated at the end of a page if the first sentence which does not fit.
                 * This settings limits amount of emptiness on the page by falling back to cutting on "word" basis if
                 * there are more than maxEmptyLines at the end of the page. This rule is only applied to the first sentence of the paragraph
                 */
                maxEmptyLines: 4,

                /**
                 * Like maxEmptyLines, but the amount of empty lines that is allowed mid paragraph when cutting on a per sentence basis.
                 * If there are more empty lines than this setting, content will be cut on a per "word" basis instead to fill out the empty lines.
                 */
                maxEmptyLinesMidParagraph: 1,

                /**
                 * The minimim number of lines at the end of a page to not consider this an orphan.
                 */
                minLinesOrIsOrphan: 2,

                /**
                 * The minimum number of lines at the beginning of a new page to not consider this a widow.
                 */
                minLinesOrIsWidow: 2,

                /**
                 * The following setting handles this situation
                 *
                 * This is a list prequel:
                 * - 1
                 * - 2
                 * - 3
                 *
                 * ... and we happened to cut after "This is a list prequel:"
                 *
                 * Always move the list prequel to next page if it is on equal or fewer lines than maxLinesForListPrequelOrphan.
                 * the text is considered a "prequel" if it ends with any of these characters :;,
                 */
                maxLinesForListPrequelOrphan: 3,

                /**
                 * Guillotine will always try to read the line height from the nodes computed styles. If
                 * no line-height has been defined in the css, it will in most cases default to 'normal' which is font/browser/os specific
                 * According to MDN, the value is usually around 1.2.
                 * Try to always set a line-height using CSS when using guillotine to prevent using this fallback.
                 */
                fallbackLineHeight: 1.2,

                /**
                 * Function that performs the magic
                 */
                attack: cutText,
                lastElementPostAttack: cutTextPostAttack
            },

            /**
             * Cuts ul and ol lists while keeping numbering correct and handling orphans/widows
             */
            listCutter: {
                /**
                 * Enables/Disabled orphan list items
                 */
                allowOrphans: false,
                /**
                 * Enables/Disabled widow list items
                 */
                allowWidows: false,

                /**
                 * Function that performs the magic
                 */
                attack: cutList
            },

            /**
             * Cuts whole blocks by removing child elements
             */
            blockCutter: {
                attack: cutBlock
            },

            /**
             * Post weapon that ensures headings are not the last element on a page
             */
            headingCutter: {
                lastElementPostAttack: cutHeadingPostAttack
            }
        }
    };

    /**
     * Cut overflowing content from DOMNode
     * Returns what's left
     * @param {DOMNode} node
     * @param {Object} options
     * @return cloned DOMNode or DocumentFragment or None if there is nothing to cut
     */
    window.smw.guillotine = function(node, options) {
        var finalOptions = extend({}, defaultOptions, true);
        if (options) {
            finalOptions = extend(finalOptions, options, true);
        }

        var leftovers = chopchop(node, {
            doesItOverflow: function(){
                var lastChild = node.lastElementChild;
                // Due to rounding problems since values are rounded to integers,
                // we allow up to 1px overflowing to prevent false positives when content exactly fits.
                return lastChild && lastChild.offsetTop + lastChild.offsetHeight - 1 > getContainerBottom(node);
            }
        }, finalOptions);

        // We should have at least one node in the root element
        if (node.childElementCount === 0 && leftovers) {
            // IE and safari dont support all cool methods on document fragment so lets move it to a div.
            var div = document.createElement('div');
            div.appendChild(leftovers);

            if (div.firstChild) {
                node.appendChild(div.firstChild);
            }

            while (div.firstChild) {
                leftovers.appendChild(div.firstChild);
            }
        }

        return leftovers;
    };

})();

