guillotine
==========

Cut cut cut that overflowing DOM away! Great for flowing content over several fix-sized containers.

### Basic usage:

Put all content into a single container so that it overflows.

```javascript
var node = document.getElementById('#page')
var fragment = smw.guillotine(node)

// 'node' no longer has overflowing content, its content now fits!
// 'fragment' contains the content that overflowed.
// We can now inject 'fragment' into a new node and repeat until guillotine returns null.
```

```smw.guillotine``` is a function that takes a DOM node that is oveflowing with
content. It will remove whatever is overflowing and return that as a document fragment.

*guillotine* traverses down into the DOM to find the node that lay partially outside 
of the containing element, ie. the one that is triggering a overflow. Anything that lays totally 
outside is removed. It then tries to "cut" that node. Basically, if it's a list it will check 
what list items fit and just return the rest.

guillotine can be setup to cut elements differently.
For example, there is a built-in specialized cutter for ul and ol elements
to prevent orphans/widows and to keep the numbering correct.

Paragraphs (<p> elements) are also treated specially.
*guillotine* will split the text on a per sentence or word basis while still handling widows and orphans!

If some things should not be cut though, just add the class 'indivisable' and that node will not be
cut. See below for defaults options on which node types are indivisable.

### Limitations
Guilloutine is optimized for working with small to moderately sized content.
Content that generates up to 20 pages should be fine.

Passing it a complete book that generates hundreds of pages is possible but slow as the browser have to
layout the whole thing for every page it generates as you push the leftover fragment into next node.
A new version might come someday with a better API/implementation for this.
See accelerando.html for an example of how large content kills guillotine speed.
This can be prevented by passing smaller chunks at the time, for example one chapter at the time.

Text cutting does not support inline spans and things like that as the content is considered as text.
Next major version of guilloutine will probably work at text node level instead to allow for inline elements in texts.

Always specifiy a line-height for paragraphs for more exact text cutting.

Body text should always appear in <p> tags for now. Or things might get ugly...

### Options

*guilliotine* takes an optional ```options``` object.
These are the defaults

```javascript
    var defaultOptions = {
        /**
         * Elements that should be considered indivisable. When guillotine encounters them, it wont try to cut them.
         * If such element overflows it will simply be moved to leftovers.
         */
        indivisable: ['img','figure','h1','h2','h3','h4','li','tr'],

        /**
         * Elements with this class will be considered indivisable
         */
        indivisableClass: 'indivisable',

        /**
         * Content after any of these elements will be removed
         */
        pagebreakElements: ['hr'],

        /**
         * Content after an element with this class will be removed
         */
        pagebreakClass: 'pagebreak',

        /**
         * Classes can be used to enable a special weapon for an element that needs to be cut.
         * Any class beginning with weaponPrefix will be considered a possible for the element.
         *
         * Example: class="weapon-super-cutter" will map to the weapon superCutter.
         */
        weaponPrefix: 'weapon-',

        /**
         * The default weapon for elements when no specialized weapon is found.
         */
        defaultWeapon: 'blockCutter',

        /**
         * Specialized weapons to use for certain elements
         */
        weaponForElement: {
            p: 'textCutter',
            ol: 'listCutter',
            ul: 'listCutter',
            h1: 'headingCutter',
            h2: 'headingCutter',
            h3: 'headingCutter',
            h4: 'headingCutter'
        },

        /**
         * Defines all weapons and their settings
         */
        weapons: {

            /**
             * Cuts nodes containing text.
             */
            textCutter: {

                /**
                 * Sets how the text is preferred to be cut.
                 * - sentence: The cutter will prefer to cut after a complete sentence.
                 *             Fallbacks to cut on a per "word" basis.
                 *             See other settings for controlling when this should happen.
                 * - word:     The cutter fits as many words as possible on each page while still obeying orphan/widow rules
                 */
                boundary: 'sentence', // Can be either "word" or "sentence"

                /**
                 * Enables/disables orphan handling.
                 * See https://en.wikipedia.org/wiki/Widows_and_orphans for details
                 *
                 * If a cut generates an orphan, the content will be moved to the next page instead.
                 */
                allowOrphans: false,

                /**
                 * Enabled/disabled widow handling
                 * If a cut generates a widow, content from the previous page will be moved to next page.
                 */
                allowWidows: false,

                /**
                 * If the result of the cut is to move all content to next page,
                 * this options decided whether to leave the node empty or if it should be removed
                 */
                removeNodeIfEmpty: true,

                /**
                 * Injects a non breaking space between the last two words at the point where the content is cut.
                 * Note that this does not inject non-breaking spaces at the end of the text. This should be done prior
                 * to calling guillotine to get a consistent behavior as guilloutine only processes items that overflow.
                 */
                preventLastWordWidowWhenCutting: true,

                /**
                 * When using sentence boundary, lots of white-space can be generated at the end of a page if the first sentence which does not fit.
                 * This settings limits amount of emptiness on the page by falling back to cutting on "word" basis if
                 * there are more than maxEmptyLines at the end of the page. This rule is only applied to the first sentence of the paragraph
                 */
                maxEmptyLines: 4,

                /**
                 * Like maxEmptyLines, but the amount of empty lines that is allowed mid paragraph when cutting on a per sentence basis.
                 * If there are more empty lines than this setting, content will be cut on a per "word" basis instead to fill out the empty lines.
                 */
                maxEmptyLinesMidParagraph: 1,

                /**
                 * The minimim number of lines at the end of a page to not consider this an orphan.
                 */
                minLinesOrIsOrphan: 2,

                /**
                 * The minimum number of lines at the beginning of a new page to not consider this a widow.
                 */
                minLinesOrIsWidow: 2,

                /**
                 * The following setting handles this situation
                 *
                 * This is a list prequel:
                 * - 1
                 * - 2
                 * - 3
                 *
                 * ... and we happened to cut after "This is a list prequel:"
                 *
                 * Always move the list prequel to next page if it is on equal or fewer lines than maxLinesForListPrequelOrphan.
                 * the text is considered a "prequel" if it ends with any of these characters :;,
                 */
                maxLinesForListPrequelOrphan: 3,

                /**
                 * Guillotine will always try to read the line height from the nodes computed styles. If
                 * no line-height has been defined in the css, it will in most cases default to 'normal' which is font/browser/os specific
                 * According to MDN, the value is usually around 1.2.
                 * Try to always set a line-height using CSS when using guillotine to prevent using this fallback.
                 */
                fallbackLineHeight: 1.2,

                /**
                 * Function that performs the magic
                 */
                attack: cutText,
                lastElementPostAttack: cutTextPostAttack
            },

            /**
             * Cuts ul and ol lists while keeping numbering correct and handling orphans/widows
             */
            listCutter: {
                /**
                 * Enables/Disabled orphan list items
                 */
                allowOrphans: false,
                /**
                 * Enables/Disabled widow list items
                 */
                allowWidows: false,

                /**
                 * Function that performs the magic
                 */
                attack: cutList
            },

            /**
             * Cuts whole blocks by removing child elements
             */
            blockCutter: {
                attack: cutBlock
            },

            /**
             * Post weapon that ensures headings are not the last element on a page
             */
            headingCutter: {
                lastElementPostAttack: cutHeadingPostAttack
            }
        }
    };


smw.guilliotine(node, {
    indivisableClass: 'foobar',
    weapons: {
        textCutter: {
            maxEmptyLinesMidParagraph: 0
        }
    }
});


### Installation
```bash
$ bower install guillotine
```

### Examples
See ```examples``` folder for two examples of paging up a larger text into smaller pages
with *guillotine*.


Created for MyWo - Share My World (sharemyworld.se),
by David Jensen and Andreas Duchén
MIT licensed.
