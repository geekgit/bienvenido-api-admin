@extends('app')

@section('content')
	<style>
		body {
			background: none;
		}
	</style>
	<div class="content">
		<div class="tab-content">
			<div id="api" class="tab-pane active">
				<h2>Información sobre la API</h2>

				<p><span>Versión de API: </span> v 1.0<br/></p>

				<hr>

				<h4><?php echo URL::to('api/v1/countries'); ?></h4>

				<p>Permite registrar un nuevo país o editar un país ya existente.</p>
				<div class="column medium-4">
					<h4>Propiedades</h4>
					<ul>
						<li><span class="text-success">id</span> [int]</li>
						<li><span class="text-error">name</span> [string]</li>
						<li><span class="text-error">description</span> [string]</li>
						<li><span class="text-success">visits</span> [integer]</li>
						<li><span class="text-success">coin</span> [string]</li>
						<li><span class="text-success">extension</span> [string]</li>
						<li><span class="text-success">image</span> [file] - archivo de imagen</li>
					</ul>
				</div>
				<div class="column medium-4">
					<h4>/save</h4>
				</div>
				<div class="column medium-4">
					<h4>/select</h4>
				</div>
				<div class="column medium-4">
					<h4>/delete</h4>
				</div>


				<hr>

				<h4><span class="badge badge-info">Region/Save</span> :: <?php echo URL::to('api/v1/regions/save'); ?>
				</h4>

				<p>Permite registrar una nueva región o editar una región ya existente.</p>
				<ul>
					<li><span class="text-success">id</span> [int] - Valor en blanco se crea un nuevo registro, si se
						envía el id se edita el registro
					</li>
					<li><span class="text-error">name</span> [string]</li>
					<li><span class="text-error">description</span> [string]</li>
					<li><span class="text-success">visits</span> [integer]</li>
					<li><span class="text-success">coin</span> [string]</li>
					<li><span class="text-success">extension</span> [string]</li>
					<li><span class="text-success">image</span> [file] - archivo de imagen</li>
				</ul>

				<hr>
				<h4><span class="badge badge-info">Users</span> :: <?php echo URL::to('api/v1/users'); ?></h4>

				<p>Permite buscar o autenticar la existencia de un usuario</p>
				<ul>
					<li><span class="text-success">id</span> [int] - Devuelve el usuario con el id</li>
					<li><span class="text-error">username</span> [string] - Debe ser usado con [password] para poder
						verificar al usuario
					</li>
					<li><span class="text-error">password</span> [string] - De ser usado con [username] devolverá solo
						el usuario solicitado o un error en el caso de que no se encuentre o que ambos datos no
						coincidan
					</li>
					<li><span class="text-success">search</span> [string] - Si se envia este parametro cancela los
						anteriores devolviendo un listado de usuarios que coincidan con el termino de busqueda.
					</li>
				</ul>
			</div>
		</div>
	</div>

@endsection