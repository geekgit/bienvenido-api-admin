<!DOCTYPE html>
<html lang="en" ng-app="GeekApp">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{ env('APP_NAME', 'GeekAdvice') }} :. {{ $title or 'Bienvenido' }}</title>
	<link href="{{ asset('output/all_app.css') }}" rel="stylesheet">
	<link href="{{ asset('icons.css') }}" rel="stylesheet">
    	<!-- Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500|Open+Sans:300|Roboto+Condensed' rel='stylesheet' type='text/css'>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"   type="text/javascript"></script>


	<![endif]-->
	    <style>
	       html, body {
                    height: 100%;
                    margin: 0;
                    padding: 0;
                  }

        </style>
</head>
<body>
<div id="sampleModal" class="reveal-modal xlarge" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog"></div>
	@yield('content')

	<script>
		var appConfig = {
			views:"<?php echo URL::to('admin'); ?>",
			api:"<?php echo URL::to('api/v1'); ?>/",
			root:"<?php echo URL::to(''); ?>"
		}
	</script>
	@yield('scripts')
</body>
<script src="{{ asset('output/all_app.js') }}"></script>
    <script type="text/javascript" src="<?php echo URL::to('components/tinymce-dist/tinymce.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo URL::to('components/angular-ui-tinymce/src/tinymce.js'); ?>"></script>

    <script type="text/javascript" >
        tinyMCE.init({
                    mode : "textareas",
                    theme : "advanced",
                    plugins : "pagebreak,style,layer,table,save,advhr,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
                    editor_selector :"mceEditor",
                    skin : "o2k7",
                    skin_variant : "silver"
                 });
        </script>
<script>
    $(document).foundation();
</script>

<script>



function initialize() {

  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(-34.397, 150.644)
  };

  var map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

     $("#tabmap").click(function() {
       $( "#btnmap" ).trigger( "click" );
     });
}

function loadScript() {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src = 'http://maps.googleapis.com/maps/api/js?v=3.exp' +
      '&signed_in=true&callback=initialize';
  document.body.appendChild(script);
}

window.onload = loadScript;

    </script>

</html>
