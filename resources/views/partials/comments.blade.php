<section class="comentarios">
	<div class="container">
		<div class="block-comments">
			<div class="text-center">
				<h3 class="title">CUENTA SOBRE EL PERÚ</h3>
				<p>{{ $contents["cuenta"]->content or 'cuenta:contenido' }}</p>
			</div>

			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<form method="POST" action="{{ url('/api/v1/comment/save') }}">
						<input name="id" type="hidden" value="{{ $page_partial->id }}">
						<p><input class="form-control" name="title" type="text" placeholder="Título"></p>
						<p><textarea class="form-control" name="comment" placeholder="Comentario"></textarea></p>
						<p class="text-center">
							@if(Auth::check())
								<button type="submit">Enviar comentario</button>
							@else
								Por favor <a href="#registro" onclick="showLoginForm()">inicia sesión</a> o <a
										href="#registro" class="account-create">crea una cuenta</a> para poder comentar.
							@endif
						</p>
					</form>
					<hr>
				</div>
			</div>

			@if($comments)

				<h3 class="text-center">COMENTARIOS</h3>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<div class="comments-display">
							<div id='comments-content'>
								@foreach($comments as $comment)
									<div class="block-comment">
										<div class="comment-avatar" style="background-image:url('{{ App\Http\Middleware\WebSiteUtils::getAvatar($comment->user_id)  }}');"></div>
										<div class="comment-text">
											<div class="arrow-left"></div>
											<h4>{{ $comment->title }}<br>
												<small>
													{{ $comment->user->name }} -
													{{ LocalizedCarbon::instance($comment->created_at)->diffForHumans() }} en <a href="{{url('destino')}}">Arequipa</a>
												</small>
											</h4>
											<p>{{ $comment->comment }}</p>
										</div>
									</div>
								@endforeach
							</div>
							<div class="holder comments-pagination"></div>
						</div>
					</div>
				</div>
			@endif
		</div>
		<hr>
	</div>
</section>