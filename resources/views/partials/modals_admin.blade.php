@if(isset($page))
<div id="editPageModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar información de la página</h4>
			</div>
			<div class="modal-body">
				<form method="post" action="{{ url('api/v1/pages/save') }}" id="formEditPage">
					<input type="hidden" name="id" value="{{ $page->id }}">
					<input type="hidden" name="parent_id" value="{{ $page->parent_id }}">
					<div class="form-group">
						<label for="nombred">Título</label>
						<input type="text" class="form-control" id="nombred" name="name" placeholder="Título" value="{{ $page->name }}">
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="code">Código / Slug:</label>
								<input type="text" class="form-control" id="nombred" name="code" placeholder="Código" value="{{ $page->code }}">
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="type">Tipo</label>
								<select class="form-control" id="type" name="type">
									@foreach(\App\Http\Middleware\WebSiteUtils::getPageTypes() as $pt)
									<option value="{{ $pt }}" {{ ($page->type==$pt)?'selected':'' }}>{{ $pt }}</option>
									@endforeach
								</select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="metatags">Metatag / SEO</label>
						<input type="text" class="form-control" id="metatags" name="metatags" placeholder="Título" value="{{ $page->metatags }}">
					</div>
					<div class="form-group">
						<label for="description">Descripción:
							<small class="text-danger">Se usará para mostrar en metatag description</small>
						</label>
						<textarea class="form-control" name="description" rows="3" id="description">{{ $page->description }}</textarea>
					</div>
					<div class="form-group">
						<label for="postalcode">Clima:
							<small class="text-danger">Si es un subdestino se usará mostrar el clima</small>
						</label>
						<input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="Código" value="{{ $page->postalcode }}">
					</div>
					<button class="button-admin"><i class="icon-circle-check"></i> Guardar cambios</button>
					<a href="{{ url('api/v1/pages/delete/' . $page->id) }}"><i class="icon-trash-o"></i> Eliminar página</a>
				</form>
			</div>
		</div>
	</div>
</div>
@endif

<div id="myModaldrag" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar Posiciones</h4>
			</div>
			<div class="modal-body">
				<p class="info-admin">Arrastra los elementos hacia arriba o abajo</p>
				<ul id="draglistdynamic" class="list-drag"></ul>
				<p class="text-center">
					<button id="saveOrderDragList" class="button-admin">Guardar orden</button>
				</p>
			</div>
		</div>
	</div>
</div>

<div id="myModalcropper" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar imagen:
					<button class="button-admin" onclick="saveCropImage()">Cortar y guardar</button>
					|
					<input type='file' id="uploadimagecroppie"/>
				</h4>
			</div>
			<div class="modal-body">
				<div id="upload-demo"></div>
			</div>
		</div>
	</div>
</div>

<div class='notifications top-right'></div>
<div id="myModalicons" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h2>Editar aventura</h2>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<ul class="list-icons list-inline">
					@foreach(\App\Http\Middleware\WebSiteUtils::getIcons() as $icon)
						<li>
							<button onclick="saveiconpage(this)"><i class="{{$icon}}"></i></button>
						</li>
					@endforeach
				</ul>
			</div>
		</div>
	</div>
</div>

<div class="modal fade bs-example-modal-sm" id="myPleaseWait" tabindex="-1"
	 role="dialog" aria-hidden="true" data-backdrop="static">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<i class="icon-times"></i> Espere un momento...
			</div>
			<div class="modal-body">
				<div id="progress" class="progress">
					<div class="progress-bar progress-bar-info progress-bar-striped active" style="width: 100%;"></div>
				</div>
			</div>
		</div>
	</div>
</div>