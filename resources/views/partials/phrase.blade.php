<section class="testimonios">

	@if($phrase)
	<div class="container">
			<div class="profile">
				<div style="background-image: url('{{ asset($phrase['page']->getImageCropImg('cover')) }}');"></div>
			</div>
			<p class="con-tes" id="frasetext2"
					{{ $isAdmin?'page-id='.$phrase['page']->id.' code=text2 type=content contenteditable':'' }}>{!! $phrase['contents']["text2"]->content  or '[text2] frase' !!}</p>
			<p class="text-uppercase name-tes">
				<span id="frasename1" {{ $isAdmin?'page-id='.$phrase['page']->id.' code=text1 type=name contenteditable':'' }}>{{ $phrase['contents']["text1"]->name  or '[text1] - nombre' }} </span>
			</p>
			<p class="text-uppercase des-tes"
			   id="frasetext1" {{ $isAdmin?'page-id='.$phrase['page']->id.' code=text1 type=content contenteditable':'' }}>{!! $phrase['contents']["text1"]->content  or '[text1] - información de la persona' !!}</p>

	</div>
	@endif
	@if($isAdmin)
	<div class="text-center">
		@if(!$phrase)
			<p>Crear nueva frase</p>
			<input id="frasedestacadatext" type="text" placeholder="Nombre de frase">
			<button class="button-admin" id="frasedestacadanew">Crear</button>
		@else
			@if($isAdmin && isset($phrase))
            	<button class="button-admin"
            			onclick="cropImage('frasedestacada','{{ $phrase['page']->getImageCropId('cover')  }}','{{ $phrase['page']->code }}','{{url($phrase['page']->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
            		<i class="icon-crop"></i> Editar imagen
            	</button>
            	<br> @endif
		@endif
	</div>
	@endif

</section>