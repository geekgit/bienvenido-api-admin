	<section class="publicidad">
		<div class="content" style="border-top: dotted 1px #ababab;border-bottom: dotted 1px #ababab;">
			@if( isset($page_partial->banners[$number]))
				<div id="{{$page_partial->banners[$number]->id}}"
					 style="background: url('{{url($page_partial->banners[$number]->multimedia[0]->source_crop)}}') no-repeat center; background-size:cover; width:100%;height:212px;  margin: 13px 0;"></div>
			@else
				@if($isAdmin)
					<div style="; background-size:cover; width:100%;height:116px;  margin: 13px 0;background-position-y: 7%;">
						BANNER
					</div>
				@endif
			@endif
			@if($isAdmin)
				<button class="button-admin"
						onclick="cropImage('{{$page_partial->type}}','{{ isset($page_partial->banners[$number])?$page_partial->banners[$number]->multimedia[0]->id:0  }}','{{ $page_partial->code }}','{{ isset($page_partial->banners[$number])?url($page_partial->banners[$number]->multimedia[0]->source) :  url('img/default.jpg') }}','{{ url('save-image-crop') }}','banner',600,200,'{{ url('savebanner') }}',1400)">
					<i class="icon-crop"></i> Editar imagen
				</button>
			@endif
		</div>
	</section>