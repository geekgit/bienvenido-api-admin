<section class="life-destiny">
	<div class="container">
		<h2 class="title-content">
			<span id="textname-life-destiny" {{ $isAdmin?' code='.$code_text.' type=name contenteditable':'' }}>{{$contents_partial[$code_text]->name or 'life-destiny:nombre' }}</span>
		</h2>
		<p class="editable" id="textcontent-life-destiny" {{ $isAdmin?' code='.$code_text.' type=content contenteditable':'' }}>{!! $contents_partial[$code_text]->content or 'life-destiny:contenido' !!}</p>

		<div class="row">
			<div class="col-md-6">
				{{-- <iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2Fbienvenidaperu%2Fphotos%2Fa.960047194048200.1073741826.960047147381538%2F1129786967074221%2F%3Ftype%3D3&width=500"
						width="100%" height="420" style="border:none;overflow:hidden" scrolling="no" frameborder="0"
						allowTransparency="true"></iframe> --}}
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" width="420" height="315" src="https://www.youtube.com/embed/YLOlsQQguaY" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-md-6">
				<ul id="playfeed" class="list-live-peru"></ul>
			</div>
		</div>
	</div>
</section>
