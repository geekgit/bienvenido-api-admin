<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($itinerario->id);?>
<?php
function convertArrayContents($array)
{
	$sortArray = [];
	for ($i=0; $i < count($array); $i++) {
		$sortArray[$array[$i]->code] = $array[$i];
	}
	return $sortArray;
}
?>
@extends('website/appsite')

@section('content')

	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($multimedia['front']))
					|
					<button class="button-admin"
							onclick="cropImage('itinerario','{{ $multimedia['front']->id }}','{{ $itinerario->code }}','{{ url($multimedia['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif
		<div class="swiper-container" style="background: url('{{ URL::to('/') }}/{{ $multimedia['front']->source_crop or ''}}')no-repeat center;background-size: cover;background-position: center center;">

@include('partials.social_buttons')
			<div class="slider-caption">
				<h3 class="slogan" id="fraseitinerario" {{ $isAdmin?'contenteditable':'' }}> {{ $content["frase"]->content  or 'itinerario-contenidos:contenido' }} </h3>
				<h1 class="name">{{ $itinerario->name or  'page:name' }}</h1>
			</div>

			<button onclick="favorito('{{$itinerario->id}}', this, '{{ url('save-favorite') }}')"
					class="favorito {{ ($favorite == 1)?'added':'' }}">
				<i class="icon-star"></i>
			</button>
		</div>
	</section>
	<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="editable" id="welcomeitinerario" {{ $isAdmin?'contenteditable':'' }}> {!! $content["welcome"]->content or '[welcome]-destino-contenidos:contenido' !!} </div>
				</div>
			</div>
		</div>
	</section>
	<section class="escala">
		<div class="content">
		<!--	<p>Home > Arequipa <span>> Arequipa 24 horas</span></p>-->
		</div>
	</section>
	<section class="bread-crumb">
		<ol class="breadcrumb container">
			<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
			<li href="destino/{{$itinerario->parent()->code}}">{{$itinerario->parent()->name}}</li>
			<li class="active">{{$itinerario->name}}</li>
		</ol>
	</section>
	<section class="timeline">
			@if($isAdmin)
				<div class="block-admin">
					<input id="daytext" type="text" class="input-small" placeholder="DIA">
					<button class="button-admin" id="daynew">Crear día</button>
				</div>
                <div class="block-admin">
					<select id="selectday">
						<option value=""><-- Días disponibles --></option>
						@foreach($dias as $dia)
							<option value="{{ $dia->code  }}">{{ $dia->name . '-' . $dia->type  }}</option>
						@endforeach
					</select>
					<button id="selectdaynew" class="button-admin"> Agregar</button>
				</div>
			@endif
    @foreach($dias as $dia)
		<img src='{{ URL::to('/') }}/{{ $dia->getImageCropImg('cover') }}' style="margin-bottom: -5px;">
          @if($isAdmin)
		    <button class="button-admin"
		    		onclick="cropImage('itinerario_day','{{ $dia->getImageCropId('cover') }}','{{ $dia->code }}','{{ asset($dia->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
		    	<i class="icon-crop"></i> Editar imagen
		    </button>
          @endif
		<div class="content">
		<?php $b=0; ?>
		@for($a=0;$a<count($dia->activitys);$a++)
		        <?php $activitycontents = convertArrayContents($dia->activitys[$b]->contents()->get());?>
			@if($a%2==0)
			    <div class="art">
                <div class="line"></div>
                	<div class="circle"></div>
                		<div class="informationbox">
                			<div class="picture">
                				<img src=" {{url($dia->activitys[$b]->getImageCropImg('cover'))}}  ">
                				@if($isAdmin)
							        <button class="button-admin"
							        		onclick="cropImage('activity_day','{{ $dia->activitys[$b]->getImageCropId('cover') }}','{{ $dia->activitys[$b]->code }}','{{ asset($dia->activitys[$b]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
							        	<i class="icon-crop"></i> Editar imagen
							        </button>
                				@endif
                			</div>
                			<div class="desc">
                			    <div style="padding-right: 19px;" name="{{ $isAdmin?$dia->activitys[$b]->code:'' }}">
                					<p class="text-uppercase text1name"  {{ $isAdmin?'contenteditable':'' }}>{{$activitycontents['text1-activity']->name or 'actividad[text1-activity]-nombre'}}</p>
                					<p class="text1content" {{ $isAdmin?'contenteditable':'' }}>{{$activitycontents['text1-activity']->content or 'actividad[text1-activity]-contenido'}}</p><p><a target="_blank" href="{{$activitycontents['url-activity']->content or 'actividad[url-activity]-contenido'}}">Ver mas.</a></p>@if($isAdmin)<p class="urltext" {{ $isAdmin?'contenteditable':'' }}>{{$activitycontents['url-activity']->content or 'aqui URL'}}</p>@endif
                				</div>
                			</div>
                		</div>
                </div>
			@else
            <div class="art">
				<div class="line"></div>
				<div class="circle"></div>
				<div class="informationbox espejo">
                	<div class="picture">
                		<img src=" {{url($dia->activitys[$b]->getImageCropImg('cover'))}}  ">
                		@if($isAdmin)
					        <button class="button-admin"
					        		onclick="cropImage('activity_day','{{ $dia->activitys[$b]->getImageCropId('cover') }}','{{ $dia->activitys[$b]->code }}','{{ asset($dia->activitys[$b]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
					        	<i class="icon-crop"></i> Editar imagen
					        </button>
                		@endif
                	</div>
                	<div class="desc">
                	    <div style="padding-right: 19px;" name="{{ $isAdmin?$dia->activitys[$b]->code:'' }}">
                			<p class="text-uppercase text1name"  {{ $isAdmin?'contenteditable':'' }}>{{$activitycontents['text1-activity']->name or 'actividad[text1-activity]-nombre'}}</p>
                			<p class="text1content" {{ $isAdmin?'contenteditable':'' }}>{{$activitycontents['text1-activity']->content or 'actividad[text1-activity]-contenido'}}</p><p><a target="_blank" href="{{$activitycontents['url-activity']->content or 'actividad[url-activity]-contenido'}}">Ver mas.</a></p>@if($isAdmin)<p class="urltext" {{ $isAdmin?'contenteditable':'' }}>{{$activitycontents['url-activity']->content or 'aqui URL'}}</p>@endif
                		</div>
                	</div>
				</div>
			</div>
			@endif
			<?php $b++; ?>

		@endfor

		</div>
		        @endforeach
	</section>
	<section class="testimonios">
		<div class="container">
			@if(count($frasedestacada) > 0)
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="profile pull-left"><div style="background-image: url('{{ asset($frasedestacada[0]->getImageCropImg('cover')) }}');"></div></div>
						<div class="text-left cont-tes">
							<p class="name-tes" id="frasename1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=name contenteditable':'' }}><span>{!! $frasecontent[0]["text1"]->name  or '[text1]-Persona:nombre' !!}</span></p>
							<p class="des-tes" id="frasetext1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=content contenteditable':'' }}>{!! $frasecontent[0]["text1"]->content  or '[text1]-Bio:contenido' !!}</p>
							<p id="frasetext2" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text2 type=content contenteditable':'' }}>{!! $frasecontent[0]["text2"]->content  or '[text2]-Frase:contenido' !!}</p>
							@if($isAdmin && isset($frasedestacada[0]))
                                 <button class="button-admin"
                                 		onclick="cropImage('frasedestacada','{{ $frasedestacada[0]->getImageCropId('cover')  }}','{{ $frasedestacada[0]->code }}','{{url($frasedestacada[0]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
                                 	<i class="icon-crop"></i> Editar imagen
                                 </button>
                             @endif
						</div>
					</div>
				</div>
			@else
				@if($isAdmin)
					<div class="block-admin text-center">
						<input id="frasedestacadatext" type="hidden" value="testimonio-{{ $itinerario->code }}">
						<h4>No hay un testimonio en esta página</h4>
						<p class="info-admin">Haz clic en el botón para crear uno</p>
						<button id="frasedestacadanew" class="button-admin"><i class="icon-plus"></i> Crear testimonio</button>
					</div>
				@endif
			@endif
		</div>
	</section>
	<section class="planea">
		<div class="content">
			<p class="text-uppercase" style="text-align:center;  padding-bottom: 40px;"><span style="color:white;font-size:25px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">planea tu viaje</span></p>
			<?php for($a=0;$a<count($itinerarios);$a++){ ?>
			<?php if($a==0){ ?>
			<div class="planea-left">
				<div class="imgplaneabig" style="background: url('<?php echo url($itinerarios[$a]->getCover() ) ?>')no-repeat center;background-size: cover;"></div>
				<img src="<?php echo url($itinerarios[$a]->getIcon()) ?>" style="width: 40px" class="duracion">
				<a href="<?php echo url('itinerario/'.$destinycode.'/'.$itinerarios[$a]->code) ?>"><img src="<?php echo url('img/btnitinerario.jpg' ) ?>" class="botoni"></a>
				<div class="desc">
					<p class="text-uppercase"><?php echo $itinerarios[$a]->name ?></p>
				</div>
			</div>

			<div class="planea-right">
			   <?php } if($a==1){ ?>
				<div class="small-box">
					<img src="<?php echo url($itinerarios[$a]->getIcon()) ?>" style="width: 30px">
				<a href="<?php echo url('itinerario/'.$destinycode.'/'.$itinerarios[$a]->code) ?>">	<div class="imgplaneasmall" style="background: url('<?php echo url($itinerarios[$a]->getCover()) ?>')no-repeat center;background-size: cover;"></div></a>
					<div class="desc">
					<p class="text-uppercase"><?php echo $itinerarios[$a]->name ?></p>
				</div>
				</div>
				<?php } if($a==2){ ?>
				<div class="small-box">
					<img src="<?php echo url($itinerarios[$a]->getIcon()) ?>" style="width: 30px">
				<a href="<?php echo url('itinerario/'.$destinycode.'/'.$itinerarios[$a]->code) ?>">	<div class="imgplaneasmall" style="background: url('<?php echo url($itinerarios[$a]->getCover() ) ?>')no-repeat center;background-size: cover;"></div></a>
					<div class="desc">
					<p class="text-uppercase"><?php echo $itinerarios[$a]->name ?></p>
				</div>
				</div>
				<?php } if($a==0){ ?>
				<br><br>

				<div class="large-box">
					<div class="imgplanealarge" style="background: url('<?php echo URL::to('/'); ?>/img/planealargeimage.jpg')no-repeat center;background-size: cover;">
						        <ul style="list-style: none;">
        						<?php for($b=0;$b<count($imperdibles);$b++){ ?>
        						    <?php $childs= $imperdibles[$b]->childs()->get(); $url=''; ?>
        						    <?php if(count($childs)>0){ $url=URL::to('/').'/parallax/'.$childs[0]->code; }; ?>
        						    <li><a href=" <?php echo $url ?>"  style="text-decoration:none;color: #de4a4a;font-family: 'robotomedium';background-color: rgba(0,0,0,0.2);"><?php echo $imperdibles[$b]->name; ?></a></li>
        						<?php }  ?>

        						</ul>
					</div>
					<div class="desc">
						<p><span class="text-uppercase" style="color: #de4a4a;">5 lugares</span> imperdibles</p>
					</div>
				</div>
				<?php }} ?>
			</div>			
		</div>

	</section>
	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$itinerario,'contents_partial'=>$content,'code_text'=>'life-destiny-itinerario'])

<script>
		var codefrase = '';
		var codedestiny = "<?php echo $itinerario->code?>";

		$("#imgfront").change(function ()
		{
			saveImage(this, 'itinerario', 'front', '{{ url('addimagemultimedia') }}', codedestiny);
		});
		$(".text1name").focusout(function () {
			editcontent($(this).parent().attr('name'), 'text1-activity', $(this).text(), 'name', 'activity_day', '<?php echo url("content-editable")?>')
		});
		$(".text1content").focusout(function () {
			editcontent($(this).parent().attr('name'), 'text1-activity', $(this).text(), 'content', 'activity_day', '<?php echo url("content-editable")?>')
		});
		$(".urltext").focusout(function () {
			editcontent($(this).parent().attr('name'), 'url-activity', $(this).text(), 'content', 'activity_day', '<?php echo url("content-editable")?>')
		});
		$("#fraseitinerario").focusout(function () {
			editcontent(codedestiny, 'frase', $(this).text(), 'content', 'itinerario', '<?php echo url("content-editable")?>')
		});
		$("#welcomeitinerario").focusout(function () {
			editcontent(codedestiny, 'welcome', $(this).html(), 'content', 'itinerario', '<?php echo url("content-editable")?>')
		});
		$("#text1contentitinerario").focusout(function () {
			editcontent(codedestiny, 'text1', $(this).text(), 'content', 'itinerario', '<?php echo url("content-editable")?>')
		});
		$("#text1nameitinerario").focusout(function () {
			editcontent(codedestiny, 'text1', $(this).text(), 'name', 'itinerario', '<?php echo url("content-editable")?>')
		});

		$("#frasedestacadanew").click(function (event) {
        var data_page={name: $("#frasedestacadatext").val(), type: 'frasedestacada',code:$("#frasedestacadatext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);
		});
		$("#selectsitenew").click(function () {
			if ($('#selectsite').val() != '') {
				$.ajax({
					url: '{{ url('attach-page') }}',
					type: "post",
					data: {child_code: $('#selectsite').val(), page_code: codedestiny},
					success: function (data) {
						window.location.reload();
					}
				}, "json");
			}
		});
		$("#daynew").click(function (event)
		{
        var data_page={name: $("#daytext").val(), type: 'itinerario_day',code:$("#daytext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);

		});
		$("#selectdaynew").click(function ()
		{

			if ($('#selectday').val() != '')
			{
			$.ajax({
				url: '<?php echo url('content-editable-page')?>',
				type: "post",
				data: {
					name:'actividad',
					type: 'activity_day',
					codedestiny: $('#selectday').val(),
					typefather: 'itinerario_day'
				},
				success: function (data)
				{
					window.location.reload();
				}
			}, "json");
			}
		});
</script>


@endsection