@extends('website/appsite')

@if(count($place->description) > 5)
@section('description') {{ $place->description}} @endsection
@endif

@section('title') {{$place->parent()->name}} > {{ $place->name }} @endsection

@section('content')


	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($multimedia['front']))
					|
					<button class="button-admin"
							onclick="cropImage('subdestino','{{ $multimedia['front']->id }}','{{ $place->code }}','{{ url($multimedia['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif

		<div class="swiper-container sub-destiny"
			 style="background-image: url({{ isset($multimedia['front'])?asset($multimedia['front']->source_crop):'' }});">
			@include('partials.social_buttons')

			<div class="temperatura">
				<ul id="list-weather" class="list-inline"></ul>
				<button class="btn-more">+</button>
			</div>

			<div class="slider-caption small">
				<h1 class="name"><span>{!! $place->name !!}</span><br></h1>
				<h3 class="slogan" id="frasesubdestino" {{ $isAdmin?'code=frase type=content contenteditable':'' }}>
					{!! $contents["frase"]->content  or '[frase]-subdestino-contenidos:contenido' !!}
				</h3>
			</div>

			<button id="favorite-star" onclick="favorito('{{$place->id}}', this, '{{ url('save-favorite') }}')"
					class="favorito {{ ($favorite == 1)?'added':'' }}">
				<i class="icon-star"></i>
			</button>

			<div class="menu-subdestino">
				<ul class="list-menu-subdestino">
					<li>
						<a href="{{ url('/sub-destino/'  .$place->code) }}"> <img
									src="{{ asset('img/subdesoption2active.png') }}">
							<p class="text-uppercase">Qué Visitar</p></a>
					</li>
					<li>
						<a href="{{ url('/sub-destino-comer/' . $place->code) }}"> <img
									src="{{ asset('img/subdesoption.png') }}">
							<p class="text-uppercase">dónde comer</p></a>
					</li>
					<li>
						<a href="{{ url('/sub-destino-hospedaje/'.$place->code) }}"> <img
									src="{{ asset('img/subdesoption3.png') }}">
							<p class="text-uppercase">hospedaje</p></a>
					</li>
					<li>
						<a href="{{ url('/sub-destino-comprar/'.$place->code) }}"> <img
									src="{{ asset('img/subdesoption5.png') }}">
							<p class="text-uppercase">dónde comprar</p></a>
					</li>
					<li>
						<a href="{{ url('/sub-destino-fiesta/'.$place->code) }}"> <img
									src="{{ asset('img/subdesoption4.png') }}">
							<p class="text-uppercase">fiestas</p></a>
					</li>
				</ul>
			</div>
		</div>
	</section>

	<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="editable" id="welcomesubdestino" {{ $isAdmin?'code=welcome type=content contenteditable':'' }}>{!!  $contents["welcome"]->content or '[welcome]-subdestino-contenidos:contenido'!!}</div>
				</div>
			</div>
		</div>
	</section>

<section class="bread-crumb">
	<ol class="breadcrumb container">
		<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
		<li><a href="destino/{{$place->parent()->code}}">{{$place->parent()->name}}</a></li>
		<li class="active">{{$place->name}}</li>
	</ol>
</section>

	<section class="destiny-history container">
		<div id="resenia-region" class="block-history">
			<div class="row">
				<div class="col-md-8">
					<h2 class="title"
						id="text1subdestinoname" {{ $isAdmin?'code=text1 type=name contenteditable':'' }}>{{ $contents["text1"]->name  or '[text1] Titulo historia' }}</h2>
					<div class="editable" id="text1subdestinocontent" {{ $isAdmin?'code=text1 type=content contenteditable':'' }}>
						{!! $contents["text1"]->content or '[text1] Contenido historia' !!}
					</div>
					<hr class="visible-sm visible-xs">
				</div>
				<div class="col-md-4">
					<div class="details">
						<h5 class="sub-title" id="olvidarsubdestinoname" {{ $isAdmin?'code=olvidar type=name contenteditable':'' }}>
							{{ $contents["olvidar"]->name  or '[olvidar] - Para no olvidar' }}
						</h5>
						<p id="olvidarsubdestinocontent" {{ $isAdmin?'code=olvidar type=content contenteditable':'' }}>
							{{ $contents["olvidar"]->content  or '[olvidar] contenido' }}
						</p>
						<hr>
						<h5 class="sub-title" id="curiosidadsubdestinoname" {{ $isAdmin?'code=curiosidad type=name contenteditable':'' }}>
							{{ $contents["curiosidad"]->name  or '[curiosidad] Curiosidad' }}
						</h5>
						<p id="curiosidadsubdestinocontent" {{ $isAdmin?'code=curiosidad type=content contenteditable':'' }}>
							{{ $contents["curiosidad"]->content  or '[curiosidad]-destino-contenidos:contenido' }}
						</p>
					</div>
				</div>
			</div>


			<div class="history-legend">
				<div class="row">
					<div class="col-sm-3">
						<img src="{{ asset('img/visita.png') }}">
						<div>
							<span class="value" id="visitassubdestinocontent" {{ $isAdmin?'code=visitas type=content contenteditable':'' }}>{{ $contents["visitas"]->content  or '[visitas] Dato' }}</span>
							<br>
							<span id="visitassubdestinoname" {{ $isAdmin?'code=visitas type=name contenteditable':'' }}>{{ $contents["visitas"]->name  or '[visitas] Label' }}</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/grupo.png') }}">
						<div>
						<span class="value" id="poblacionsubdestinocontent" {{ $isAdmin?'code=poblacion type=content contenteditable':'' }}>{{ $contents["poblacion"]->content  or '[poblacion] Dato' }}</span><br>
						<span id="poblacionsubdestinoname" {{ $isAdmin?'code=poblacion type=name contenteditable':'' }}>{{ $contents["poblacion"]->name  or '[poblacion] Label' }}</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/mapaicon.png') }}">
						<div>
						<span class="value" id="extencionsubdestinocontent" {{ $isAdmin?'code=extencion type=content contenteditable':'' }}>{{ $contents["extencion"]->content  or '[extencion] Dato' }}</span><br>
						<span id="extencionsubdestinoname" {{ $isAdmin?'code=extencion type=name contenteditable':'' }}>{{ $contents["extencion"]->name  or '[extencion] Label' }}</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/dinero.png') }}">
						<div>
						<span class="value" id="costosubdestinocontent" {{ $isAdmin?'code=costo type=content contenteditable':'' }}>{{ $contents["costo"]->content  or '[costo] Dato' }}</span><br>
						<span id="costosubdestinoname" {{ $isAdmin?'code=costo type=name contenteditable':'' }}>{{ $contents["costo"]->name  or '[costo] Label' }}</span>
						</div>
					</div>
				</div>
			</div><!-- End legend -->
		</div>
		<div class="text-center">
			<a href="" class="vermas" id="imgdestino">
				VER MÁS <i class="icon-arrow-down"></i>
			</a>
		</div>
	</section>

	<section class="imperdibles">
		<div class="container">
			<h2 class="title-content">
				<span id="text2subdestinoname" {{ $isAdmin?'code=text2 type=name contenteditable':'' }}>{!! $contents["text2"]->name or '[text2]-subdestino-contenidos:nombre' !!}</span>
			</h2>
			<div class="editable" id="text2subdestinocontent" {{ $isAdmin?'code=text2 type=content contenteditable':'' }}> {!! $contents["text2"]->content or '[text2]-subdestino-contenidos:contenido' !!} </div>

			@if($isAdmin)
				<div class="block-admin">
					<input id="sitetext" type="text" class="input-small" placeholder="SITIO">
					<button class="button-admin" id="sitenew">Crear sitio</button>
					|
					<button class="button-admin" onclick="openpopuporder('listsubdestiny','{{  $place->id }}')">Editar
						posiciones
					</button>
				</div>
			@endif
			<ul id="listsubdestiny" class=" list-thumbnail">
				@foreach($places as $plac)
					<li data-id="{{$plac->id}}">
						@if($isAdmin)
							<button class="button-admin"
									onclick="eliminarpage('{{$plac->id}}', '{{ url('content-delete-page') }}')">
								Eliminar
							</button>
							<button class="button-admin"
									onclick="cropImage('sitio','{{ $plac->getImageCropId('cover') }}','{{ $plac->code }}','{{ asset($plac->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',400)">
								<i class="icon-crop"></i> Editar imagen
							</button>
						@endif
						<a title="{{$plac->id}}" href="{{ url('/parallax')."/".$plac->code}}">
							<div class="title small"><span>{{$plac->name}}</span></div>
							<input type="hidden" value="{{$plac->code}}"/>

							<div class="pic"
								 style="background-image: url('{{ asset($plac->getImageCropImg('cover')) }}');"></div>
						</a>
					</li>
				@endforeach
			</ul>


			<div class="mapadestino">
				<p class="text-left">
					<button onclick="clickbutton()"><i class="icon-pin"></i> Regresar a ubicación original</button>
					@if($isAdmin)
						<button class="button-admin pull-right"
								onclick="saveMapPosition('{{$place->id}}','{{ url('savepoint') }}')"> Guardar
							posición del mapa
						</button>
					@endif
				</p>
				<div id="map-canvas" class="map-canvas"></div>
			</div>
		</div>
	</section>

	<section class="testimonios">
		<div class="container">
			@if(count($frasedestacada) > 0)
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="profile pull-left">
							<div style="background-image: url('{{ asset($frasedestacada[0]->getImageCropImg('cover')) }}');"></div>
						</div>
						<div class="text-left cont-tes">
							<p class="name-tes" id="frasename1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=name contenteditable':'' }}>
								<span>{!! $frasecontent[0]["text1"]->name  or '[text1]-Persona:nombre' !!}</span></p>
							<p class="des-tes"
							   id="frasetext1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=content contenteditable':'' }}>{!! $frasecontent[0]["text1"]->content  or '[text1]-Bio:contenido' !!}</p>
							<p id="frasetext2" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text2 type=content contenteditable':'' }}>{!! $frasecontent[0]["text2"]->content  or '[text2]-Frase:contenido' !!}</p>
							@if($isAdmin && isset($frasedestacada[0]))
								<button class="button-admin"
										onclick="cropImage('frasedestacada','{{ $frasedestacada[0]->getImageCropId('cover')  }}','{{ $frasedestacada[0]->code }}','{{url($frasedestacada[0]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',400)">
									<i class="icon-crop"></i> Editar imagen
								</button>
								<br> @endif
						</div>
					</div>
				</div>
			@else
				@if($isAdmin)
					<div class="block-admin text-center">
						<input id="frasedestacadatext" type="hidden" value="testimonio-{{ $place->code }}">
						<h4>No hay un testimonio en esta página</h4>
						<p class="info-admin">Haz clic en el botón para crear uno</p>
						<button id="frasedestacadanew" class="button-admin"><i class="icon-plus"></i> Crear testimonio
						</button>
					</div>
				@endif
			@endif


		</div>
	</section>
	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'contents_partial'=>$contents,'code_text'=>'life-destiny-lugar'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$place, 'comments'=>$comments])

	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
		//img cover frase

		var codefrase = '';
		$('#resenia-region').slideUp();
		var codedestiny = "<?php echo $place->code?>";

		$("#imgfront").change(function ()
		{
			saveImage(this, 'subdestino', 'front', '{{ url('add-image-multimedia') }}', codedestiny);
		});


		$("#sitenew").click(function (event)
		{
        var data_page={name: $("#sitetext").val(), type: 'sitio',code:$("#sitetext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);

		});
		$("#frasedestacadanew").click(function (event)
		{
        var data_page={name: $("#frasedestacadatext").val(), type: 'frasedestacada',code:$("#frasedestacadatext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);
		});

		var weatherCode = "{{ $place->postalcode }}";
		var lang = "{{ Session::get('my.locale') }}";
		var url = "https://query.yahooapis.com/v1/public/yql?u=c&q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20woeid%20%3D%20" + weatherCode + ")&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

		$(document).ready(initPage);

		function initPage()
		{
			$.ajax({url: url, success: callbackFunction});
			$('.btn-more').click(expandTemp);
		}

		function expandTemp()
		{
			var lista = $('#list-weather');
			if (lista.hasClass('expanded'))
				lista.removeClass('expanded');
			else
				lista.addClass('expanded');
		}

		function callbackFunction(data)
		{
			var weatherElement = $('#list-weather');
			if (data.query.results.channel.item.forecast != null)
			{
				var weatherData = data.query.results.channel.item.forecast;
				for (var i in weatherData)
				{
					if (weatherData.hasOwnProperty(i))
					{
						var h = '<span class="temp-high">' + Math.round(((((weatherData[i].high - 32) * 5) / 9) * 100) / 100) + 'ºC</span>';
						var l = '<span class="temp-low">' + Math.round(((((weatherData[i].low - 32) * 5) / 9) * 100) / 100) + 'ºC</span>';
						if (i == 0) h += '<br>';
						var htmlDay = '<li title="' + weatherData[i].text + '"><div class="day">' + weatherData[i].day + '</div><img src="{{ asset('img/temperatura.png') }}"><div class="temp">' + h + ' ' + l + '</div></li>';
						weatherElement.append(htmlDay);
					}
				}
			}
		}

		var coordinates = [

			@foreach($places as $plac)

			<?php
			$plac->ubications();
			$name = $plac->name;
			$lat = -12.601475166388834;
			$lon = -75.11077880859375;
			if (count($plac->ubications) > 0) {

				$lat = $plac->ubications[0]->content;
				$lon = $plac->ubications[1]->content;
			}

			?>
			@if(isset($lat) && isset($lon))
			@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
			{!! '{id:'.$plac->id.',code:"'.$plac->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
			@else
			{!! '{id:'.$plac->id.',code:"'.$plac->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
			@endif
			@endif
			<?php


			?>
			@endforeach

		];
				<?php
				$lat=-12.046623;
				$lon =-77.042828;
				$zoom=8;
				$name=''?>
		var pstn = [
					@foreach($position as $pos)
					<?php

					$lat  = $position[0]->content;
					$lon =  $position[1]->content;
					if(count($position)>2){
					$zoom=$position[2]->content;
					}
					?>
					@endforeach

					@if( isset($lat) && isset($lon))
					{!! '{lat:'.$lat.',lon:'.$lon.',name:"'.$name.'",zoom:'.$zoom.'},' !!}
					@endif
				];
		var map;
		function initialize()
		{
			var mapCanvas = document.getElementById('map-canvas');
			var mapOptions = {
				center: new google.maps.LatLng(pstn[0].lat, pstn[0].lon),
				zoom: parseInt(pstn[0].zoom),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false
			};
			var pinIcon = new google.maps.MarkerImage(
					"{{ asset("/img/icon_map_a.png") }}",
					null, /* size is determined at runtime */
					null, /* origin is 0,0 */
					null, /* anchor is bottom center of the scaled image */
					new google.maps.Size(56, 70)
			);
			map = new google.maps.Map(mapCanvas, mapOptions);
			//  	var bounds = new google.maps.LatLngBounds();

			for (var i = 0; i < coordinates.length; i++)
			{
				var infowindow = new google.maps.InfoWindow();
				var position = new google.maps.LatLng(coordinates[i].lat, coordinates[i].lon);
				//	bounds.extend(position);

				var marker = new google.maps.Marker({
					position: position,
					map: map,
					icon: pinIcon,
					@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
					draggable: true,
					@endif
					title: coordinates[i].name
				});

				google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
				{
					return function ()
					{
						infowindow.setContent(coordinates[i].name);
						infowindow.open(map, this);
						window.location = "{{ url('/')."/parallax/" }}" + coordinates[i].code;
					};
				})(marker, i, infowindow));
				@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
				google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
				{
					return function ()
					{
						savemappoint(coordinates[i].id, '{{ url('save-point') }}', marker.position.lat(), marker.position.lng(), map.getZoom());
					};
				})(marker, i, infowindow));
				@endif
			}


			var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
			map.setZoom(parseInt(pstn[0].zoom));
			map.panTo(latlong);
		}

		google.maps.event.addDomListener(window, 'load', initialize);
		function clickbutton()
		{
			var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
			map.setZoom(parseInt(pstn[0].zoom));
			map.panTo(latlong);
		}
	</script>
@endsection
