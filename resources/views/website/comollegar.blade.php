<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($destiny->id);?>
@extends('website/appsite')

@section('content')

                <div id="myModalescala" class="modal fade" role="dialog">
                   <div class="modal-dialog">

                     <!-- Modal content-->
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <h4 class="modal-title">Editar</h4>
                       </div>
                       <div class="modal-body">
                           <div class="form-group">
                             <label for="nombred">titulo-punto-escala-name</label>
                             <input type="text" class="form-control" id="titlecontentescala" >
                           </div>
                           <div class="form-group">
                             <label for="nombred">titulo-punto-escala-contenido</label>
                             <input type="text" class="form-control" id="titlenameescala" >
                           </div>
                           <div class="form-group">
                             <label for="nombred">text1-escala-nombre</label>
                             <input type="text" class="form-control" id="text1nameescala"  >
                           </div>
                           <div class="form-group">
                             <label for="nombred">text1-escala-contenido</label>
                             <input type="text" class="form-control" id="text1contentescala" >
                           </div>
                           <div class="form-group">
                             <label for="nombred">Latitud</label>
                             <input type="text" class="form-control" id="latescala">
                             <label for="nombred">Longitud</label>
                             <input type="text" class="form-control" id="lngescala">
                           </div>
                           <div class="form-group">
                             <label for="nombred">text2-escala-nombre</label>
                             <input type="text" class="form-control" id="text2nameescala"  >
                           </div>
                           <div class="form-group">
                             <label for="nombred">text2-escala-contenido</label>
                             <input type="text" class="form-control" id="text2contentescala">
                           </div>
                           <div class="form-group">
                             <label for="nombred">text3-escala-contenido</label>
                             <input type="text" class="form-control" id="text3contentescala">
                           </div>
                           <a  class="btn btn-success" onclick="saveescalacontent()">Guardar</a>
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                       </div>
                     </div>

                   </div>
                 </div>
	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($multimedia['front2']))
					|
					<button class="button-admin"
							onclick="cropImage('destino','{{ $multimedia['front2']->id }}','{{ $destiny->code }}','{{ url($multimedia['front2']->source) }}','{{ url('save-image-crop') }}','front2',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif
		<div class="swiper-container" style="background: url('{{ URL::to('/') }}/{{ $multimedia['front2']->source_crop or '' }}')no-repeat center;background-size: cover;background-position: center center;">
			@include('partials.social_buttons')
			<div class="slider-caption">
				<h3 class="slogan" id="frase2destino" {{ $isAdmin?'code=frase2 type=content contenteditable':'' }}> {{ $contents["frase2"]->content  or '[frase2]destino-contenidos:contenido' }} </h3>
				<h1 class="name">{{ $destiny->name or  'page:name' }}</h1>
			</div>

			<div onclick="favorito({{$destiny->id}},this,'<?php echo url('save-favorite')?>')" class="favorito <?php echo ($favorite==1)?'added':''; ?>">
				<i class="icon-star"></i>
				<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
			</div>
			@if( !isset($multimedia["front2"]) )
				<div class="resource" style="color: black;margin-top: 250px;width: 100%;position: absolute;text-align: center;">
					    @if($isAdmin)
                              [front2]-destino-multimedia:src
                        @endif
				</div>
			@endif

		</div>
		<ul class="list-destiny-menu">
			<li><a href="{{ url('/destino')."/".$destiny->code  }}" >{{ $destiny->name }}</a></li>
			<li><a href="{{ url('/como-llegar')."/".$destiny->code }}" class="active">¿cómo llegar?</a></li>
			<li><a href="{{ url('/aventura').'/'.$destiny->code }}">aventura</a></li>
			<li><a href="{{ url('/naturaleza')."/".$destiny->code }}"> naturaleza</a></li>
			<li><a href="{{ url('/cultura')."/".$destiny->code }}">cultura</a></li>
			<li><a href="{{ url('/descanso')."/".$destiny->code }}">descanso</a></li>
		</ul>
	</section>
	<section class="bread-crumb">
		<ol class="breadcrumb container">
			<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
			<li >{{$destiny->name}}</li>
			<li class="active">Como llegar</li>
		</ol>
	</section>
	@if($isAdmin)
    <div class="block-admin">
			<select id="selecttravel">
				<option value=""><-- Días disponibles --></option>
				<option value="como_llegar-tierra-bus">por bus</option>
				<option value="como_llegar-tierra-auto">por auto</option>
				<option value="como_llegar-aire">por aire</option>
			</select>
			<input id="traveltext" type="text" class="input-small" placeholder="como llegar">
			<button id="selecttravelnew" class="button-admin"> Agregar</button>
		</div>
	@endif
  <!--Oculto ya ordenado de la anterior version, habilitar en css class comollegar-->
	<section class="comollegar">
		<div class="content">
      <div data-pws-tab="por-tierra" data-pws-tab-name="POR BUS">
				<p>Desde: </p>
				<select id="comollegar_desdetierra">
					<option value=""><--Seleccione--></option>
					<?php for( $a=0;$a<count($comollegar_tierra);$a++) { ?>
					<option value="<?php echo $comollegar_tierra[$a]->code ?>"><?php echo $comollegar_tierra[$a]->name ?></option>
					<?php } ?>
				</select>
  			@if($isAdmin)
          <div class="block-admin">
  					<input id="escalabustext" type="text" class="input-small" placeholder="escala">
  					<button id="escalabusnew" class="button-admin"> Agregar</button>
  				</div>
          <div class="block-admin">
  					<select id="selectescalabus" onchange="codeescalas(this)">
  						<option value=""><-- Escala disponibles --></option>
  					</select>
  					<button id="selectescalabusedit" class="button-admin" onclick="editescala()"> Editar</button>
  				</div>
  			@endif
	      <section class="guiallegar">
					<div class="content">
						<div>
  						<img id="img-tierra1" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png">
  						<img id="img-tierra2" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 40px;">
  						<img id="img-tierra3" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 28px;">
  						<img id="img-tierra4" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 26px;">
						</div>
						<div>
							<img id="img-cajetierra1" src="<?php echo URL::to('/'); ?>/img/info.jpg">
							<img id="img-cajetierra2" src="<?php echo URL::to('/'); ?>/img/info.jpg">
							<img id="img-cajetierra3" src="<?php echo URL::to('/'); ?>/img/info.jpg">
							<img id="img-cajetierra4" src="<?php echo URL::to('/'); ?>/img/info.jpg">
						</div>
						<div class="bow1">
							<div id="t1">
			          <?php if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray())){ ?>
                  <p id="msj-tierra">[escala]-destino-comollegar-multimedia:src</p>
                <?php } ?>
							</div>
						</div>
						<div class="bow2">
              <div id="t2">
							</div>
						</div>
						<div class="bow3">
							<div id="t3">
							</div>
						</div>
						<div class="bow4">
            	<div id="t4">
            	</div>
            </div>
            <div class="barr1">
              <div id="tr1">
                <?php if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray())){ ?>
                  [text1-escala]-destino-comollegar-escala-contenidos:contenido
                  <br>
                  <p>text1_escalacontent='[text1-escala]-destino-comollegar-escala-contenidos:contenido</p>
                  <br>
                  [escala]-destino-comollegar-escala-multimedia:src
                  [text2-escala]-destino-comollegar-escala-contenidos:contenido<br>
                  <p>text2_escalacontent='[text2-escala]-destino-comollegar-escala-contenidos:contenido</p>
                <?php } ?>
              </div>
            </div>
            <div class="barr2">
                <div id="tr2">
                </div>
            </div>
            <div class="barr3">
                <div id="tr3">
                </div>
            </div>
            <div class="barr4">
                <div id="tr4">
                </div>
            </div>
					</div>
				</section>
          <div id="contentgeneraltierra" class="contgeneral">
          	<div class="contlist">
          		<p class="text-uppercase">ITINERARIO</p>
          		<div id="content-tierra">
          		  <?php if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray())){ ?>
                  [como-llegar]-destino-comollegar-contenidos:contenido
                <?php } ?>
              </div>
          	</div>
          </div>
	    </div>
		  <div data-pws-tab="por-tierra-auto" data-pws-tab-name="POR AUTO">
  			<p>Desde: </p>
        <select id="comollegar_desdetierraauto">
              <option value=""><--Seleccione--></option>
              <?php for( $a=0;$a<count($comollegar_tierraauto);$a++) { ?>
              <option value="<?php echo $comollegar_tierraauto[$a]->code ?>"><?php echo $comollegar_tierraauto[$a]->name ?></option>
               <?php } ?>
        </select>
  			@if($isAdmin)
          <div class="block-admin">
  					<input id="escalaautotext" type="text" class="input-small" placeholder="escala">
  					<button id="escalaautonew" class="button-admin"> Agregar</button>
  				</div>
          <div class="block-admin">
  					<select id="selectescalaauto" onchange="codeescalas(this)">
  						<option value=""><-- Escala disponibles --></option>
  					</select>
  					<button class="button-admin" onclick="editescala()"> Editar</button>
  				</div>
  			@endif
        <section class="guiallegar">
          <div class="content">
  					<div>
  					  <img id="img-tierraauto1" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png">
  						<img id="img-tierraauto2" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 40px;">
  						<img id="img-tierraauto3" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 28px;">
  						<img id="img-tierraauto4" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 26px;">
  					</div>
						<div>
							<img id="img-cajetierraauto1" src="<?php echo URL::to('/'); ?>/img/info.jpg">
              <img id="img-cajetierraauto2" src="<?php echo URL::to('/'); ?>/img/info.jpg">
              <img id="img-cajetierraauto3" src="<?php echo URL::to('/'); ?>/img/info.jpg">
              <img id="img-cajetierraauto4" src="<?php echo URL::to('/'); ?>/img/info.jpg">
						</div>
  					<div class="bow1">
							<div id="a1">
				        <?php if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray())){ ?>
                  <h1></h1>
                  <p id="msj-tierraauto">[escala]-destino-comollegar-multimedia:src</p>
                <?php } ?>
							</div>
  					</div>
						<div class="bow2">
							<div id="a2">
							</div>
						</div>
						<div class="bow3">
							<div id="a3">
							</div>
						</div>
						<div class="bow4">
            	<div id="a4">
            	</div>
            </div>
            <div class="barr1">
                <div id="ar1">
                </div>
            </div>
            <div class="barr2">
                <div id="ar2">
                </div>
            </div>
            <div class="barr3">
                <div id="ar3">
                </div>
            </div>
            <div class="barr4">
                <div id="ar4">
                </div>
            </div>
					</div>
				</section>
      </div>
		  <div data-pws-tab="por-aire" data-pws-tab-name="POR AIRE">
				<p>Desde: </p>
				<select id="comollegar_desdeaire">
					<option value=""><--Seleccione--></option>
					<?php for( $a=0;$a<count($comollegar_aire);$a++) { ?>
					<option value="<?php echo $comollegar_aire[$a]->code ?>"><?php echo $comollegar_aire[$a]->name ?></option>
					<?php } ?>
        </select>
  			@if($isAdmin)
          <div class="block-admin">
  					<input id="escalaairetext" type="text" class="input-small" placeholder="escala">
  					<button id="escalaairenew" class="button-admin"> Agregar</button>
          </div>
          <div class="block-admin">
  					<select id="selectescalaaire" onchange="codeescalas(this)">
  						<option value=""><-- Escala disponibles --></option>
  					</select>
  					<button class="button-admin" onclick="editescala()"> Editar</button>
  				</div>
  			@endif
	      <section class="guiallegar">
					<div class="content">
						<div>
  						<img id="img-aire1" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png">
  						<img id="img-aire2" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 40px;">
  						<img id="img-aire3" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 28px;">
  						<img id="img-aire4" src="<?php echo URL::to('/'); ?>/img/guiallegar2.png" style="margin-left: 26px;">
						</div>
						<div>
							<img id="img-cajeaire1" src="<?php echo URL::to('/'); ?>/img/info.jpg">
              <img id="img-cajeaire2" src="<?php echo URL::to('/'); ?>/img/info.jpg">
              <img id="img-cajeaire3" src="<?php echo URL::to('/'); ?>/img/info.jpg">
              <img id="img-cajeaire4" src="<?php echo URL::to('/'); ?>/img/info.jpg">
						</div>
						<div class="bow1">
							<div id="v1">
			          <?php if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray())){ ?>
                  <p id="msj-aire">[escala]-destino-comollegar-multimedia:src</p>
                <?php } ?>
							</div>
      			</div>
						<div class="bow2">
							<div id="v2">
							</div>
						</div>
						<div class="bow3">
							<div id="v3">
							</div>
						</div>
						<div class="bow4">
            	<div id="v4">
            	</div>
            </div>
            <div class="barr1">
              <div id="vr1">
              </div>
            </div>
            <div class="barr2">
              <div id="vr2">
              </div>
            </div>
            <div class="barr3">
              <div id="vr3">
              </div>
            </div>
            <div class="barr4">
              <div id="vr4">
              </div>
            </div>
					</div>
				</section>
        <div id="contentgeneralaire" class="contgeneral">
        	<div class="contlist">
        		<p class="text-uppercase">ITINERARIO</p>
        		<div id="content-aire">
        		  <?php if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray())){ ?>
                [como-llegar]-destino-comollegar-contenidos:contenido
              <?php } ?>
            </div>
        	</div>
        </div>
		  </div>
    </div>
	</section>
  <!--Oculto ya ordenado de la anterior version, habilitar en css class comollegar-->
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$destiny,'number'=>0])
	<section class="imperdibles" style="margin-bottom:0px; background: #ededed;padding: 30px 0;">
		<div class="container">
				<h2 class="title-content">
    				<span id="text1comollegarcontent" {{ $isAdmin?'code=text1-comollegar type=content contenteditable':'' }}>{!! $contents["text1-comollegar"]->content  or '[text1-comollegar]-destino-contenidos:contenido' !!}</span>
    			</h2>

            <ul id="" class=" list-thumbnail">
				@for($i = 0 ; $i < count($places) ; $i++)
					<li>
						<a title="{{$places[$i]->id}}" href="{{ url('/sub-destino')."/".$places[$i]->code}}">
							<div class="title small"><span>{{$places[$i]->name}}</span></div>

							<div class="pic"
								 style="background-image: url('{{ asset($places[$i]->getCover()) }}');"></div>
						</a>
					</li>
				@endfor
			</ul>

		</div>
	</section>
<script src="{{ asset('slick/slick.js') }}"></script>

    	    <script>
            var codedestiny="<?php echo $destiny->code?>";
		$("#selecttravelnew").click(function ()
		{

			if ($('#selecttravel').val() != '' && $('#traveltext').val()!='')
			{
			$.ajax({
				url: '<?php echo url('content-editable-page')?>',
				type: "post",
				data: {
					name:$('#traveltext').val(),
					type: $('#selecttravel').val(),
					codedestiny: codedestiny,
					typefather: 'destino'
				},
				success: function (data)
				{
					window.location.reload();
				}
			}, "json");
			}
		});

		$("#escalabusnew").click(function ()
		{
			$.ajax({
				url: '<?php echo url('content-editable-page')?>',
				type: "post",
				data: {
					name: $("#escalabustext").val(),
					type: 'escala_punto',
					codedestiny: $('#comollegar_desdetierra').val(),
					typefather: 'como_llegar-tierra-bus'
				},
				success: function (data)
				{
					window.location.reload();
				}
			}, "json");
		});
		$("#escalaautonew").click(function ()
		{
			$.ajax({
				url: '<?php echo url('content-editable-page')?>',
				type: "post",
				data: {
					name: $("#escalaautotext").val(),
					type: 'escala_punto',
					codedestiny: $('#comollegar_desdetierraauto').val(),
					typefather: 'como_llegar-tierra-auto'
				},
				success: function (data)
				{
					window.location.reload();
				}
			}, "json");
		});
		$("#escalaairenew").click(function ()
		{
			$.ajax({
				url: '<?php echo url('content-editable-page')?>',
				type: "post",
				data: {
					name: $("#escalaairetext").val(),
					type: 'escala_punto',
					codedestiny: $('#comollegar_desdeaire').val(),
					typefather: 'como_llegar-aire'
				},
				success: function (data)
				{
					window.location.reload();
				}
			}, "json");
		});

	function readfrontimg(input) {

                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('.portada').attr("style", "background-image: url(" + e.target.result + ");background-repeat: no-repeat;background-size: cover");
                    };
                   var img= reader.readAsDataURL(input.files[0]);
                   var data = new FormData();
                       data.append('code', codedestiny);
                       data.append('file', input.files[0]);
                       data.append('type', 'destino');
                       data.append('codechild', 'front2');
                                $.ajax({
                                  url: '<?php echo URL::to('add-image-multimedia')?>',
                                  type: "post",
                                  processData: false,
                                  contentType: false,
                                  data: data,
                                   success: function(data){
                                swal('','se guardaron los cambios correctamente','success');
                                }
                          },"json");
                }
            }
            $("#imgfront").change(function(){
                    readfrontimg(this);

                });
            </script>
	<script type="text/javascript">
	        var dataescala;
	        var codeescala;
	        var codecomollegar;
	        function codeescalas(input){
	            codeescala=$(input).val();
	        }
	        function saveescalacontent()
	        {
	        $('#myPleaseWait').modal('show');
	            editcontent(codeescala,'punto-escala',$("#titlenameescala").val(),'name','escala_punto','<?php echo url("content-editable")?>');
	            editcontent(codeescala,'punto-escala',$("#titlecontentescala").val(),'content','escala_punto','<?php echo url("content-editable")?>');
	            editcontent(codeescala,'text1-escala',$("#text1nameescala").val(),'name','escala_punto','<?php echo url("content-editable")?>');
	            editcontent(codeescala,'text1-escala',$("#text1contentescala").val(),'content','escala_punto','<?php echo url("content-editable")?>');
	            editcontent(codeescala,'text2-escala',$("#text2nameescala").val(),'name','escala_punto','<?php echo url("content-editable")?>');
	            editcontent(codeescala,'text2-escala',$("#text2contentescala").val(),'content','escala_punto','<?php echo url("content-editable")?>');
                editcontent(codeescala,'lat-escala',$("#latescala").val(),'content','escala_punto','<?php echo url("content-editable")?>');
               	editcontent(codeescala,'lng-escala',$("#lngescala").val(),'content','escala_punto','<?php echo url("content-editable")?>');
                editcontent(codecomollegar,'como-llegar',$("#text3contentescala").val(),'content','como_llegar-tierra-bus','<?php echo url("content-editable")?>');
                 $(document).ajaxStop(function () {
                 $('#myPleaseWait').modal('hide');
                     window.location.reload();
                 });
	          //
	         }
        function editescala()
        {
            for(var a=0;a<dataescala.escalas.length;a++)
                {
                if(dataescala.escalas[a].code==codeescala )
                {
                      var nums=a+1;
                      if(a<dataescala.escalas.length)
                      {
                          if(dataescala.escalas[a].infocontent["punto-escala"]!=undefined)
                          {
                               $("#titlenameescala").val(dataescala.escalas[a].infocontent["punto-escala"].name);
                                $("#titlecontentescala").val(dataescala.escalas[a].infocontent["punto-escala"].content);

                          }
                          if(dataescala.escalas[a].infocontent["text1-escala"]!=undefined)
                          {
                                        $("#text1nameescala").val(dataescala.escalas[a].infocontent["text1-escala"].name);
                                        $("#text1contentescala").val(dataescala.escalas[a].infocontent["text1-escala"].content);

                          }

                          if(dataescala.escalas[a].infocontent["lng-escala"]!=undefined )
                          {
                                   $("#lngescala").val(dataescala.escalas[a].infocontent["lng-escala"].content);
                          }
                          if(dataescala.escalas[a].infocontent["lat-escala"]!=undefined )
                          {
                                  $("#latescala").val(dataescala.escalas[a].infocontent["lat-escala"].content);
                          }
                          if(dataescala.escalas[a].infocontent["text2-escala"]!=undefined)
                          {
                                       $("#text2nameescala").val(dataescala.escalas[a].infocontent["text2-escala"].name);
                                        $("#text2contentescala").val(dataescala.escalas[a].infocontent["text2-escala"].content);

                          }
                         if(dataescala.contents['como-llegar']!=undefined )
                         {
                          $("#text3contentescala").val(dataescala.contents["como-llegar"].content);
                         }
                      }
                    }
             }
             $('#myModalescala').modal('show');
        }
	    $(document).ready(function(){
	        ocultartodo();
	        var image = document.getElementById('img-tierra1');

            image.onload = function () {
	         $('a[data-tab-id="por-tierra"]').click();
	        };
	        var codedestiny="<?php echo $destiny->code?>";
	        var punto_escalaname='';
	        var punto_escalacontent='';
	        var text1_escalaname='';
	        var text1_escalacontent='';
	        var text2_escalaname='';
            var text2_escalacontent='';
	        var escalaimg='';
	        var itinerario='';
	      <?php if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray())){ ?>
	      	 punto_escalaname='[punto-escala]-destino-comollegar-escala-contenidos:nombre';
	         punto_escalacontent='[punto-escala]-destino-comollegar-escala-contenidos:nombre';
             text1_escalaname='[text1-escala]-destino-comollegar-escala-contenidos:contenido';
             text1_escalacontent='[text1-escala]-destino-comollegar-escala-contenidos:contenido';
             text2_escalaname='[text2-escala]-destino-comollegar-escala-contenidos:contenido';
             text2_escalacontent='[text2-escala]-destino-comollegar-escala-contenidos:contenido';
             escalaimg='[escala]-destino-comollegar-escala-multimedia:src';
             itinerario='[como-llegar]-destino-comollegar-contenidos:contenido';
	      <?php } ?>
             var comollegar='';
             $('a[data-tab-id="por-tierra"]').click(function(event) {
                     $("#comollegar_desdetierra").show();
                     $("#comollegar_desdetierraauto").hide();
                     $("#comollegar_desdeaire").hide();

             });
               $('a[data-tab-id="por-tierra-auto"]').click(function(event) {
                                  $("#comollegar_desdetierra").hide();
                                  $("#comollegar_desdeaire").hide();
                                   $("#comollegar_desdetierraauto").show();

                          });
             $('a[data-tab-id="por-aire"]').click(function(event) {
                    $("#comollegar_desdetierra").hide();
                    $("#comollegar_desdeaire").show();
                     $("#comollegar_desdetierraauto").hide();
               });
               $('#comollegar_desdetierraauto').on('change', function() {
                             if(this.value!='')
                             {
                               codecomollegar=this.value;
                                   for(var a=0;a<4;a++)
                                   {
                                   var nums=a+1;
                                     $("#a"+nums).empty();
                                     $("#ar"+nums).empty();
                                     $("#img-tierraauto"+nums).show();
                                     $("#img-cajetierraauto"+nums).show();
                                     }
                                               jQuery.ajax({
                                                   url: '<?php echo url("desde")?>',
                                                   type: "post",
                                                   data: {codedestiny:codedestiny,code: this.value,type:'como_llegar-tierra-auto'},
                                                   success: function (data) {
                                                     document.getElementById('selectescalaauto').options.length = 0;
                                     $( "#selectescalaauto" ).append( "<option value=''><-- Escala disponibles --></option>" );
                                     dataescala=data;

                               for(var a=0;a<4;a++)
                                    {
                                    var nums=a+1;
                                    if(a<data.escalas.length)
                                    {
                                           $("#a"+nums).empty();
                                           $("#ar"+nums).empty();
                                           $("#img-tierraauto"+nums).css('visibility','visible');
                                           $("#img-cajetierraauto"+nums).css('visibility','visible');
                                           if(data.escalas[a].infocontent["punto-escala"]!=undefined)
                                           {
                                            $("#a"+nums).append("<h1>"+data.escalas[a].infocontent["punto-escala"].name+"</h1>");
                                            $("#a"+nums).append(data.escalas[a].infocontent["punto-escala"].content);
                                           }
                                           else
                                           {
                                               $("#a"+nums).append("<h1>"+punto_escalaname+"</h1>");
                                               $("#a"+nums).append("<p>"+punto_escalacontent+"</p>");
                                           }

                                           if(data.escalas[a].infocontent["text1-escala"]!=undefined)
                                           {
                                           $("#ar"+nums).append(data.escalas[a].infocontent["text1-escala"].name);
                                           $("#ar"+nums).append(data.escalas[a].infocontent["text1-escala"].content+"<br>");
                                           }
                                           else
                                           {
                                              $("#ar"+nums).append(text1_escalaname);
                                              $("#ar"+nums).append("<p>"+text1_escalacontent+"</p>"+"<br>");
                                            }
                                        if(data.escalas[a].infocontent["lng-escala"]!=undefined && data.escalas[a].infocontent["lat-escala"]!=undefined  )
                                             {
                                                      $("#ar"+nums).append('<img src="https://maps.googleapis.com/maps/api/staticmap?center='+data.escalas[a].infocontent["lat-escala"].content+','+data.escalas[a].infocontent["lng-escala"].content+'&zoom=13&size=200x113&maptype=roadmap&markers=color:blue%7Clabel:S%7C'+data.escalas[a].infocontent["lat-escala"].content+','+data.escalas[a].infocontent["lng-escala"].content+'">'+"<br>");
                                             }
                                            else
                                             {
                                               $("#ar"+nums).append(escalaimg+"<br>");

                                            }


                                           if(data.escalas[a].infocontent["text2-escala"]!=undefined)
                                           {
                                           $("#ar"+nums).append(data.escalas[a].infocontent["text2-escala"].name);
                                           $("#ar"+nums).append(data.escalas[a].infocontent["text2-escala"].content);
                                           }
                                           else
                                           {
                                              $("#ar"+nums).append(text2_escalaname);
                                              $("#ar"+nums).append("<p>"+text2_escalacontent+"</p>"+"<br>");
                                           }

   $('a[data-tab-id="por-tierra-auto"]').click();
                                                $( "#selectescalaauto" ).append( "<option value='"+(data.escalas[a].code +"'>"+data.escalas[a].name +"</option>" ));


                                    }
                                    else
                                    {
                                        if(data.escalas.length==0)
                                        {
                                     $("#a"+nums).empty();
                                     $("#ar"+nums).empty();
                                     $("#img-tierraauto"+nums).hide();
                                     $("#img-cajetierraauto"+nums).hide();
                                        }
                                        else
                                        {
                                             $("#a"+nums).empty();
                                             $("#ar"+nums).empty();
                                             $("#img-tierraauto"+nums).css('visibility','hidden');
                                             $("#img-cajetierraauto"+nums).css('visibility','hidden');
                                        }

                                    }
                                    }


                                                   }
                                               }, "json");
                                               }
                                     else
                                     {
                                   for(var a=0;a<4;a++)
                                   {
                                   var nums=a+1;
                                     $("#a"+nums).empty();
                                     $("#ar"+nums).empty();
                                     $("#img-tierraauto"+nums).hide();
                                     $("#img-cajetierraauto"+nums).hide();
                                     }
                                     }
                             });

              $('#comollegar_desdetierra').on('change', function() {
              if(this.value!='')
              {
               $("#contentgeneraltierra").show();
                codecomollegar=this.value;
                                   for(var a=0;a<4;a++)
                                   {
                                   var nums=a+1;
                                     $("#t"+nums).empty();
                                     $("#tr"+nums).empty();
                                     $("#img-tierra"+nums).show();
                                     $("#img-cajetierra"+nums).show();

                                     }
                                jQuery.ajax({
                                    url: '<?php echo url("desde")?>',
                                    type: "post",
                                    data: {codedestiny:codedestiny,code: this.value,type:'como_llegar-tierra-bus'},
                                    success: function (data) {
                                    document.getElementById('selectescalabus').options.length = 0;
                                     $( "#selectescalabus" ).append( "<option value=''><-- Escala disponibles --></option>" );
                                     dataescala=data;

                                for(var a=0;a<4;a++)
                                    {
                                    var nums=a+1;
                                    if(a<data.escalas.length)
                                    {
                                           $("#t"+nums).empty();
                                           $("#tr"+nums).empty();
                                           $("#img-tierra"+nums).css('visibility','visible');
                                           $("#img-cajetierra"+nums).css('visibility','visible');
                                           if(data.escalas[a].infocontent["punto-escala"]!=undefined)
                                           {
                                            $("#t"+nums).append("<h1>"+data.escalas[a].infocontent["punto-escala"].name+"</h1>");
                                            $("#t"+nums).append(data.escalas[a].infocontent["punto-escala"].content);
                                           }
                                           else
                                           {
                                               $("#t"+nums).append("<h1>"+punto_escalaname+"</h1>");
                                               $("#t"+nums).append("<p>"+punto_escalacontent+"</p>");
                                           }

                                           if(data.escalas[a].infocontent["text1-escala"]!=undefined)
                                           {
                                           $("#tr"+nums).append(data.escalas[a].infocontent["text1-escala"].name);
                                           $("#tr"+nums).append(data.escalas[a].infocontent["text1-escala"].content+"<br>");
                                           }
                                           else
                                           {
                                              $("#tr"+nums).append(text1_escalaname);
                                              $("#tr"+nums).append("<p>"+text1_escalacontent+"</p>"+"<br>");
                                            }
                                        if(data.escalas[a].infocontent["lng-escala"]!=undefined && data.escalas[a].infocontent["lat-escala"]!=undefined  )
                                             {
                                                      $("#tr"+nums).append('<img src="https://maps.googleapis.com/maps/api/staticmap?center='+data.escalas[a].infocontent["lat-escala"].content+','+data.escalas[a].infocontent["lng-escala"].content+'&zoom=13&size=200x113&maptype=roadmap&markers=color:blue%7Clabel:S%7C'+data.escalas[a].infocontent["lat-escala"].content+','+data.escalas[a].infocontent["lng-escala"].content+'">'+"<br>");
                                             }
                                           else
                                           {
                                               $("#tr"+nums).append(escalaimg+"<br>");

                                           }


                                           if(data.escalas[a].infocontent["text2-escala"]!=undefined)
                                           {
                                           $("#tr"+nums).append(data.escalas[a].infocontent["text2-escala"].name);
                                           $("#tr"+nums).append(data.escalas[a].infocontent["text2-escala"].content);
                                           }
                                           else
                                           {
                                              $("#tr"+nums).append(text2_escalaname);
                                              $("#tr"+nums).append("<p>"+text2_escalacontent+"</p>"+"<br>");
                                           }

                        $( "#selectescalabus" ).append( "<option value='"+(data.escalas[a].code +"'>"+data.escalas[a].name +"</option>" ));


                                    }
                                    else
                                    {
                                        if(data.escalas.length==0)
                                        {
                                      $("#t"+nums).empty();
                                      $("#tr"+nums).empty();
                                      $("#img-tierra"+nums).hide();
                                      $("#img-cajetierra"+nums).hide();
                                        }
                                        else
                                        {
                                          $("#t"+nums).empty();
                                          $("#tr"+nums).empty();
                                           $("#img-tierra"+nums).css('visibility','hidden');
                                            $("#img-cajetierra"+nums).css('visibility','hidden');
                                        }

                                    }
                                    }
                                    if(data.contents['como-llegar']!=undefined )
                                    {
                                        $("#content-tierra").empty();
                                        $("#content-tierra").append(data.contents['como-llegar'].content);
                                    }
                                    else
                                    {
                                     $("#content-tierra").empty();
                                     $("#content-tierra").append(itinerario);
                                    }
$('a[data-tab-id="por-tierra"]').click();
                                    }
                                }, "json");
                                }
                                else
                                {
                                $("#contentgeneraltierra").hide();
                                   for(var a=0;a<4;a++)
                                   {
                                   var nums=a+1;
                                     $("#t"+nums).empty();
                                     $("#tr"+nums).empty();
                                     $("#img-tierra"+nums).hide();
                                     $("#img-cajetierra"+nums).hide();
                                     }
                                }
              });
              $('#comollegar_desdeaire').on('change', function() {
                 if(this.value!='')
                 {
                 $("#contentgeneralaire").show();
                   codecomollegar=this.value;
                                   for(var a=0;a<4;a++)
                                   {
                                   var nums=a+1;
                                     $("#v"+nums).empty();
                                     $("#vr"+nums).empty();
                                     $("#img-aire"+nums).show();
                                     $("#img-cajeaire"+nums).show();
                                     }
                                jQuery.ajax({
                                    url: '<?php echo url("desde")?>',
                                    type: "post",
                                    data: {codedestiny:codedestiny,code: this.value,type:'como_llegar-aire'},
                                    success: function (data) {
                                      document.getElementById('selectescalaaire').options.length = 0;
                                     $( "#selectescalaaire" ).append( "<option value=''><-- Escala disponibles --></option>" );
                                     dataescala=data;
                                    for(var a=0;a<4;a++)
                                    {
                                    var nums=a+1;
                                    if(a<data.escalas.length)
                                    {
                                           $("#v"+nums).empty();
                                           $("#vr"+nums).empty();
                                           $("#img-aire"+nums).css('visibility','visible');
                                           $("#img-cajeaire"+nums).css('visibility','visible');
                                           if(data.escalas[a].infocontent["punto-escala"]!=undefined)
                                           {
                                            $("#v"+nums).append("<h1>"+data.escalas[a].infocontent["punto-escala"].name+"</h1>");
                                            $("#v"+nums).append(data.escalas[a].infocontent["punto-escala"].content);
                                           }
                                           else
                                           {
                                               $("#v"+nums).append("<h1>"+punto_escalaname+"</h1>");
                                               $("#v"+nums).append("<p>"+punto_escalacontent+"</p>");
                                           }

                                           if(data.escalas[a].infocontent["text1-escala"]!=undefined)
                                           {
                                           $("#vr"+nums).append(data.escalas[a].infocontent["text1-escala"].name);
                                           $("#vr"+nums).append(data.escalas[a].infocontent["text1-escala"].content+"<br>");
                                           }
                                           else
                                           {
                                              $("#vr"+nums).append(text1_escalaname);
                                              $("#vr"+nums).append("<p>"+text1_escalacontent+"</p>"+"<br>");
                                            }
                                        if(data.escalas[a].infocontent["lng-escala"]!=undefined && data.escalas[a].infocontent["lat-escala"]!=undefined  )
                                             {
                                                      $("#vr"+nums).append('<img src="https://maps.googleapis.com/maps/api/staticmap?center='+data.escalas[a].infocontent["lat-escala"].content+','+data.escalas[a].infocontent["lng-escala"].content+'&zoom=13&size=200x113&maptype=roadmap&markers=color:blue%7Clabel:S%7C'+data.escalas[a].infocontent["lat-escala"].content+','+data.escalas[a].infocontent["lng-escala"].content+'">'+"<br>");
                                             }
                                           else
                                           {
                                               $("#vr"+nums).append(escalaimg+"<br>");

                                           }


                                           if(data.escalas[a].infocontent["text2-escala"]!=undefined)
                                           {
                                           $("#vr"+nums).append(data.escalas[a].infocontent["text2-escala"].name);
                                           $("#vr"+nums).append(data.escalas[a].infocontent["text2-escala"].content);
                                           }
                                           else
                                           {
                                              $("#vr"+nums).append(text2_escalaname);
                                              $("#vr"+nums).append("<p>"+text2_escalacontent+"</p>"+"<br>");
                                           }
                                       $( "#selectescalaaire" ).append( "<option value='"+(data.escalas[a].code +"'>"+data.escalas[a].name +"</option>" ));

                                    }
                                    else
                                    {
                                        if(data.escalas.length==0)
                                        {
                                        $("#v"+nums).empty();
                                        $("#vr"+nums).empty();
                                        $("#img-aire"+nums).hide();
                                        $("#img-cajeaire"+nums).hide();
                                        }
                                        else{
                                         $("#v"+nums).empty();
                                         $("#vr"+nums).empty();
                                         $("#img-aire"+nums).css('visibility','hidden');
                                           $("#img-cajeaire"+nums).css('visibility','hidden');
                                           }
                                    }
                                    }
                                    if(data.contents['como-llegar']!=undefined )
                                    {
                                        $("#content-aire").empty();
                                        $("#content-aire").append(data.contents['como-llegar'].content);
                                    }
                                    else
                                    {
                                     $("#content-aire").empty();
                                     $("#content-aire").append(itinerario);
                                    }
                                        $('a[data-tab-id="por-aire"]').click();
                                    }
                                }, "json");
                 }
                 else
                 {
                 $("#contentgeneralaire").hide();
                  for(var a=0;a<4;a++)
                  {
                  var nums=a+1;
                    $("#v"+nums).empty();
                    $("#vr"+nums).empty();
                    $("#img-aire"+nums).hide();
                    $("#img-cajeaire"+nums).hide();
                    }
                 }
              });
              function ocultartodo()
              {
                    for(var a=0;a<4;a++)
                    {
                        var nums=a+1;
                        $("#v"+nums).empty();
                        $("#vr"+nums).empty();
                        $("#img-aire"+nums).hide();
                        $("#img-cajeaire"+nums).hide();
                        $("#t"+nums).empty();
                        $("#tr"+nums).empty();
                        $("#img-tierra"+nums).hide();
                        $("#img-cajetierra"+nums).hide();
                        $("#a"+nums).empty();
                        $("#ar"+nums).empty();
                        $("#img-tierraauto"+nums).hide();
                        $("#img-cajetierraauto"+nums).hide();
                        $("#contentgeneraltierra").hide();
                        $("#contentgeneralaire").hide();
                    }
              }

                     function mostrartodo()
                            {
                                  for(var a=0;a<4;a++)
                                  {
                                      var nums=a+1;
                                      $("#v"+nums).empty();
                                      $("#vr"+nums).empty();
                                      $("#img-aire"+nums).show();
                                      $("#img-cajeaire"+nums).show();
                                      $("#t"+nums).empty();
                                      $("#tr"+nums).empty();
                                      $("#img-tierra"+nums).show();
                                      $("#img-cajetierra"+nums).show();
                                      $("#a"+nums).empty();
                                      $("#ar"+nums).empty();
                                      $("#img-tierraauto"+nums).show();
                                      $("#img-cajetierraauto"+nums).show();
                                  }
                            }
	      $('.slideslick').slick({
			  infinite: true,
			  slidesToShow: 4,
			  slidesToScroll: 4,
			  prevArrow: ("<img src='<?php echo URL::to('/'); ?>/img/slideleft.png' style='  position: absolute;top: calc(50% - 35px);left: -10px;cursor:pointer;'>"),
			  nextArrow: ("<img src='<?php echo URL::to('/'); ?>/img/slideright.png' style='  position: absolute;top: calc(50% - 35px);right: -10px;cursor:pointer;'>")
			});
	    });
	  </script>

@endsection

