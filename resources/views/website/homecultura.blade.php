<?php

function convertArrayContents($array)
{
	$sortArray = [];
	for ($i=0; $i < count($array); $i++) {
		$sortArray[$array[$i]->code] = $array[$i];
	}
	return $sortArray;
}
?>
<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($page->id);?>
@extends('website/appsite')

@section('content')
                 <!-- Modal -->
                 <div id="myModal" class="modal fade" role="dialog">
                   <div class="modal-dialog">
                     <!-- Modal content-->
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <h4 class="modal-title">Editar</h4>
                       </div>
                       <div class="modal-body">
                           <div class="form-group">
                             <label for="nombred">Nombre</label>
                             <input type="text" class="form-control" id="named" placeholder="Nombre" value="{{ $page->name  }}">
                           </div>
                           <div class="form-group">
                             <label for="descriptiond">Descripción</label>
                             <textarea class="form-control" rows="3" id="descriptiond">{{ $page->description  }}</textarea>

                           </div>
                           <div class="form-group">
                             <label for="codepostald">Clima</label>
                             <input type="text" class="form-control" id="codepostald" placeholder="Código del clima" value="{{ $page->postalcode  }}">
                           </div>
                           <a  class="btn btn-success" onclick="editpage({{ $page->id  }},'<?php echo URL::to('contenteditablepage')?>','{{ $page->type  }}')">Guardar</a>
                       </div>
                       <div class="modal-footer">
                         <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                       </div>
                     </div>
                   </div>
                 </div>

	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($infomultimedia['front']))					|
					<button class="button-admin"
							onclick="cropImage('','{{ $infomultimedia['front']->id }}','home-cultura','{{ url($infomultimedia['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('addimagemultimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif
			<div class="swiper-container" style="background: url('{{ URL::to('/') }}/{{ $infomultimedia['front']->source_crop or '' }}')no-repeat center;background-size: cover;background-position: center center;">
				@include('partials.social_buttons')
				<div class="slider-caption">
                	<h3 class="slogan"{{ $isAdmin?'contenteditable':'' }}> {{ $content["frase"]->content  or '[frase]-cultura:contenidos:contenido' }} </h3>
                	<h1 class="name">{{ $page->name or  'page:name' }}</h1>
                </div>

			</div>
	</section>
	<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<span class="editable" id="welcome" {{ $isAdmin?'contenteditable':'' }}> {!! $content["welcome"]->content or '[welcome]-cultura-contenidos:contenido' !!} </span>
				</div>
			</div>
		</div>
	</section>

	<section class="circleoption">
				@if($isAdmin)
    				<div class="block-admin">
    					<input id="categoriatext" type="text" class="input-small" placeholder="CATEGORIA">
    					<button class="button-admin" id="categorianew">CREAR CATEGORIA</button>
    					|
    					<button class="button-admin" onclick="openpopuporder('dragListsub_destino','{{$page->id}}')">Editar
    						posiciones
    					</button>

    				</div>
    			@endif
		<div class="circle-tabs" style="width: 1000px;">
			<div class="tabs" style="width: 1000px;">

				@if( count($categorias) == 0 )
					<div data-pws-tab="circulo-uno" data-pws-tab-name="<i class='icon-ICONOTREKKING' style='font-size: 140px;top: -23px;left: 24px;'></i><span style='top: 30px;left: 47px;'>[category]nombre</span>">
						<section class="mapa" style="background:none; margin-bottom: 30px;">
							<div class="content">
								<div style="background: #ededed;border-radius: 4px;border: solid 1px #e0e0e0;padding: 32px;text-align:justify;  font-size: 13px; color: #828282;  margin-bottom: 40px;width:91%;margin-left:11px;">
	                  @if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))

 		<p style="color: #df5555;  font-size: 15px;"> [pregunta]-category-contenidos:nombre </p>
 									<br>
 									[respuesta]-category-contenidos:nombre
                                @else
		<p style="color: #df5555;  font-size: 15px;">  </p>
									<br>

                             @endif

								</div>
								<div class="mapa-left">
									<div class="cordenadas">
										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d16121555.667112703!2d-73.86595004687499!3d-9.427832234234!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses!2spe!4v1434655690454" width="512" height="692" frameborder="0" style="border:0" allowfullscreen></iframe>
									</div>
								</div>
								<div class="mapa-rigth">
									<div class="slidevertical">
									</div>
								</div>
							</div>
						</section>
					</div>
				@else
					<div class="circle-tabs">
						<div class="pws_tabs_container pws_tabs_horizontal pws_tabs_horizontal_top"  >
							<ul class="pws_tabs_controll" >

								<div class="slideslick" style="visibility: hidden;">
									@foreach($categorias as $key=>$category )
										<?php $categoryContents = convertArrayContents($category->contents()->get());?>

										<li>
				                     @if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
     					                <p class="text-uppercase" style="text-align:center;"><span onclick="moverminiatura(this,'atras')"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';"><--</span>
                                        <span onclick="moverminiatura(this,'adelante')"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">--></span></p>
                					        @endif
											<a title="{{$category->id}}"  href="#{{$category->code}}" style="margin-bottom: 15px;" data-tab-id="{{$category->id}}" class="circle-tab <?php if($key==0)echo'pws_tab_active'?>">
												<i  class="{!! $categoryContents['icon']->content or '' !!}" style="font-size: 54px;    margin-left: -40px;margin-top: 10px;"></i>

												<span style='top: 60px;left: 0;display:block;width:100%;text-transform: uppercase;'>{{ $category->name }}</span>
											</a>
											@if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray()))
											<input type="hidden" value="{{$category->code}}">
	                                        <button class="button-admin"
                    		                    onclick="modalselecticon(this,'category')">
                    	                        <i class="icon-crop"></i> Editar icono
                                            </button>
											<p class="text-uppercase" style="text-align:center;"><span onclick="eliminarpage({{$category->id}}, '<?php echo URL::to('contentdeletepage')?>')"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">eliminar</span></p>

											@endif

										</li>
									@endforeach
								</div>
							</ul>
						</div>

						@foreach($categorias as $keyc=> $category )
							<?php if(\Session::has('my.locale'))
							{
								$local=\Session::get('my.locale');
							}
							else
							{
								$local='es';
							}
							$categoryContents = convertArrayContents($category->contents()->where('lang','=',$local)->get());
							$places = $category->childs()->where('type','sitio')->get();
							?>
							<?php $cover=$category->getCover()?>

							<div id="{{$category->id}}" class="tab_content" style="<?php if($keyc!=0)echo'display: none'?>">
								<section class="mapa" style=";background:none; margin-bottom: 30px;">
									<div class="content">
									<div style="background: #ededed;border-radius: 4px;border: solid 1px #e0e0e0;padding: 32px;text-align:justify;  font-size: 13px; color: #828282;  margin-bottom: 40px;width:91%;margin-left:11px;">
	                  @if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))

                                            <input type="hidden" id="{{$category->code}}">
											<div  style="color: #df5555;  font-size: 15px;" class="preguntaname" contenteditable="true">{!! $categoryContents["pregunta"]->content or '[pregunta]-category-contenidos:contenido' !!}</div>
											<br>
											<div class="preguntacontent editable" contenteditable="true">{!! $categoryContents["respuesta"]->content or '[respuesta]-category-contenidos:contenido' !!}</div>
                                @else

											<div style="color: #df5555;  font-size: 15px;" >{!! $categoryContents["pregunta"]->content or '' !!}</div>
											<br>
											{!! $categoryContents["respuesta"]->content or '' !!}
                      @endif

										</div>

										<div class="mapa-left">
											        @if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray()))
                                                	<button onclick="saveMapPosition({{$category->id}},'<?php echo URL::to('savepoint')?>',1)" style="color:white;font-size:17px;background: #dc5656;font-family: 'robotolight';">Guardar posición</button>
                                                	@endif
											<div class="cordenadas">
												<div id="map-canvas-{{ $category->code }}" style="position:absolute;position: absolute;top: 12px;width:512px;height:692px;left: 10px;border:0"></div>
											</div>
										</div>
										<div class="mapa-rigth" style="vertical-align:top;">
                                                                                    	 @if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray()))
                                                                                    	  <?php $selectsites= \App\Page::where('type','sitio')->whereNotIn('id', $places->lists('id'))->get(); ?>
                                                                                    			            <div style="margin-bottom: 40px;">
                                                                                    							<select class="selectsitios" >
                                                                                                					<option value=""><--Seleccione--></option>
                                                                                                					<?php for( $a=0;$a<count($selectsites);$a++) { ?>
                                                                                                					<option value="<?php echo $selectsites[$a]->code ?>"><?php echo $selectsites[$a]->name.'-'.$selectsites[$a]->type ?></option>
                                                                                                					<?php } ?>
                                                                                                				</select>
                                                                                                				    <p class="text-uppercase" style="text-align:center;"><button class="selectsitionew"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">crear</button></p>
                                                                                    				        </div>

                                                                   		                    <div style="margin-bottom: 40px;">

                                                                               				        <p class="text-uppercase" style="text-align:center;"><button id="savemovelistcategorysite"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">Guardar posiciones</button></p>
                                                                   				            </div>
                                                                   		             @endif
                                                                                    	 <div class="slidevertical">
                                        											@for($i = 0;$i < count($places);$i++ )
                                        								<div>
                                                        				 @if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
                                                             					<p class="text-uppercase" style="text-align:center;"><a onclick="moverminiatura(this,'atras')"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';"><--</a>
                                                                                 <a onclick="moverminiatura(this,'adelante')"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">--></a></p>
                                                                         @endif
                                        												<a title="{{$places[$i]->id}}"  href="{{ URL::to('/')."/parallax/".$places[$i]->code }}">
                                        													<div class="smallbox">
                                        														<div class="intersmallmap" style="background: url('{{ URL::to('/')."/".$places[$i]->getCover() }}') no-repeat center; background-size:cover;"></div>
                                        														<p class="title text-uppercase"><span>{{ $places[$i]->name }}</span></p>
                                        													</div>
                                        												</a>
                                        											 @if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
                                                                                     <p class="text-uppercase" style="text-align:center;"><a onclick="eliminarrelationpage({{$places[$i]->id}}, '<?php echo URL::to('deleteattachpage')?>','{{$category->code}}')"  style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">eliminar</a></p>
                                                            					     @endif

                                                                                        </div>
                                        											@endfor
                                        											</div>
                                        										</div>
									</div>
								</section>
							</div>
						@endforeach
					</div>
				@endif

			</div>

		</div>
	</section>
@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$page,'contents_partial'=>$content,'code_text'=>'life-destiny-homecultura'])


<script type="text/javascript" src="{{  URL::to('/')."/" }}slick/slick.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
    	var codedestiny='';
    	 var tab_id;
                  $("#savemovelistcategories").click(function(){
                     var id="<?php echo $page->id?>";
                     var arry=[];
                     var ct=0;
                     $(".slick-track > li").each(function (index)
                     {
                         arry[ct]=[{id:id,id_child:$(this).children('a').attr('title')}];
                         ct++;

                    });
                     saveOrderLIst(arry,'<?php echo url("guardarordenlista")?>');

                  });
                   $("#savemovelistcategorysite").click(function(){
                         if(codedestiny=='')
                         {
                         	@for($a=0;$a<1;$a++ )
                         	@if(isset($categorias[$a]))
                         	  codedestiny='{{$categorias[$a]->code }}';
                         	  tab_id='{{$categorias[$a]->id }}';
                         	  @endif
                         	@endfor
                         }
                      var id=tab_id;
                      var arry=[];
                      var ct=0;
                      $(".slick-track > div").each(function (index)
                      {
                          arry[ct]=[{id:id,id_child:$(this).children('a').attr('title')}];
                          ct++;

                     });
                      saveOrderLIst(arry,'<?php echo url("guardarordenlista")?>');

                   });
        var btn;
        function modalselecticon(input,type){
               btn=input;
               $('#myModalicons').modal('show');
         }

    		$(document).ready(function(){

        			$('.slideslick').css("visibility","visible");
        			var hash=window.location.hash;
        			if(hash!='')
        			{
        			var tabhash=$("a[href='"+hash+"']:first");
                    			tabhash.click();
        			}

        			initialize();



        		});
                    $(".selectsitionew").click(function(){
                        if(codedestiny=='')
                        {
                        	@for($a=0;$a<1;$a++ )
                        	@if(isset($categorias[$a]))
                        	  codedestiny='{{$categorias[$a]->code }}';
                        	  tab_id='{{$categorias[$a]->id }}';
                        	  @endif
                        	@endfor
                        }
                        if($(this).parent('p').siblings('select').val()!='')
                                {
                                   $.ajax({
                                               url: '<?php echo URL::to('attachpage')?>',
                                               type: "post",
                                               data: {child_code:$(this).parent('p').siblings('select').val(),page_code:codedestiny},
                                               success: function(data){
                                                  window.location.reload();
                                               }
                                           },"json");
                                }
                    });

        				$('.circle-tab').click(function(){

                			$('.circle-tab').removeClass('pws_tab_active');
                			$(this).addClass('pws_tab_active');

                			tab_id='#'+$(this).attr('data-tab-id');
                			$('.tab_content').hide();
                			$(tab_id).show();
                			codedestiny=$(tab_id).children('section').children('div').children('div:first').children('input').attr('id');
                			console.log(codedestiny);
                			initialize();
                		});
    	</script>
<script>
var codepage='home-cultura';
        $("#categorianew").click(function(){

               $.ajax({
                     url: '<?php echo URL::to('contenteditablepage')?>',
                     type: "post",
                     data: {name:$("#categoriatext").val(),type:'category',codedestiny:codepage,typefather:""},
                     success: function(data){
                        window.location.reload();
                     }
                 },"json");
        });
		$("#imgfront").change(function () {
			saveImage(this, '','front','{{ url('addimagemultimedia') }}',codepage);
		});
                        function saveiconpage(input)
                        {
                        codedestiny= $(btn).siblings('input').val();
                        var i =$(btn).siblings('a').children('i');
                            var text=$(input).attr('class');
                            $(i).attr('class',text);
                            $('#myModalicons').modal('hide');
                            editcontent(codedestiny,'icon',text,'content','category','<?php echo url("contenteditable")?>');
                        }
                       $(".preguntaname").focusout(function() {
                          codedestiny= $(this).siblings('input').attr('id');
                         editcontent(codedestiny,'pregunta',$(this).text(),'content','category','<?php echo url("contenteditable")?>')
                       });
                       $(".preguntacontent").focusout(function() {
                             codedestiny= $(this).siblings('input').attr('id');
                            editcontent(codedestiny,'respuesta',$(this).html(),'content','category','<?php echo url("contenteditable")?>')
                       });

                       $("#welcome").focusout(function() {
                            editcontent(codepage,'welcome',$(this).html(),'content','','<?php echo url("contenteditable")?>')
                       });
                       $("#frase").focusout(function() {
                            editcontent(codepage,'frase',$(this).text(),'content','','<?php echo url("contenteditable")?>')
                       });
</script>
	<script>


		


		var count=0;
		var map=[];
		var coordinates=[];
		function initialize() {
        count=0;
		map=[];
		coordinates=[];
			@foreach($categorias as $category )
            <?php
                $places = $category->childs()->where('type','sitio')->get();
                $position = $category->ubications()->get();
            ?>
            coordinates[count] = [
			@foreach($places as $place)

		    	 <?php
		    	 $place->ubications();
		    	 $name=$place->name;
                 $lat  = -12.601475166388834 ;
                 $lon =  -75.11077880859375;
		    	 if(count($place->ubications)>0)
		    	 {
                 		    	 $lat  = $place->ubications[0]->content;
                 		    	 $lon =  $place->ubications[1]->content;
		    	 }

		    	 ?>
		    	 @if(isset($lat) && isset($lon))
		    	 @if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		    	 	{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		    	 	@else
		    	 	{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		    	 @endif
		    	 @endif
		    @endforeach
            ];
	    	 <?php
                         $lat=-12.046623;
                         $lon =-77.042828;
                         $zoom=8;
                         $name=''?>
    	var pstn = [

    			@foreach($position as $pos)

    		    	 <?php

                     		    	 $lat  = $position[0]->content;
                     		    	 $lon =  $position[1]->content;
                                     if(count($position)>2)
                                        {
                                            $zoom=$position[2]->content;
                                        }

    		    	 ?>


    		    @endforeach

                         @if( isset($lat) && isset($lon))
                        		    	 	{!! '{lat:'.$lat.',lon:'.$lon.',name:"'.$name.'",zoom:'.$zoom.'},' !!}
                        		   @endif

            ];
			var mapCanvas = document.getElementById('map-canvas-{{ $category->code }}');
			var mapOptions = {
				center: new google.maps.LatLng(pstn[0].lat,pstn[0].lon),
				zoom:  parseInt(pstn[0].zoom),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false
			};
			var goldStar = {
				path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
				fillColor: 'yellow',
				fillOpacity: 0.8,
				scale: 0.1,
				strokeColor: 'gold',
				strokeWeight: 5
			};

			map[count] = new google.maps.Map(mapCanvas, mapOptions);
			//  	var bounds = new google.maps.LatLngBounds();

			for (var i = 0; i < coordinates[count].length; i++) {
				var infowindow = new google.maps.InfoWindow();
				var position = new google.maps.LatLng( coordinates[count][i].lat , coordinates[count][i].lon);
				//	bounds.extend(position);

				var marker = new google.maps.Marker({
					position: position,
					map: map[count],
					icon:goldStar,
					title: coordinates[count][i].name,
					code:coordinates[count][i].code
				});

				google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)  {
					return function () {
						infowindow.setContent(marker.title);
						infowindow.open(marker.map, this);
						 window.location="{{ URL::to('/')."/parallax/" }}"+marker.code;
					};
				})(marker, i, infowindow));
			};
			/*	if(coordinates.length > 0){
			 map.fitBounds(bounds);
			 }*/
			var latlong= new google.maps.LatLng(pstn[0].lat,pstn[0].lon);
			map[count].setZoom(parseInt(pstn[0].zoom));
			map[count].panTo(latlong);
			/*map[count].addListener('click', function(e) {
				var latlong= new google.maps.LatLng(pstn[0].lat,pstn[0].lon);
				map[count].setZoom(parseInt(pstn[0].zoom));
				map[count].panTo(latlong);
			});*/
			count++;
			@endforeach

          }
		google.maps.event.addDomListener(window, 'load', initialize);

	</script>

@endsection