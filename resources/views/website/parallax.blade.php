<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($place->id);?>
@extends('website/appsite')

@section('content')


<section class="header">
	@if($isAdmin)
		<div class="block-admin-float">
			<span class="info-admin">Cambiar imagen: </span>
			@if(isset($multimedia['front']))
				<button class="button-admin"
						onclick="cropImage('sitio','{{ $multimedia['front']->id }}','{{ $place->code }}','{{ url($multimedia['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
					<i class="icon-crop"></i> Editar imagen
				</button>
			@endif
		</div>
	@endif
	<div class="swiper-container sub-destiny" style="background: url('{{ URL::to('/') }}/{{ $multimedia['front']->source_crop or ''}}') no-repeat center;background-size: cover;background-position: center center;">
			@include('partials.social_buttons')

		<div class="temperatura">
			<ul id="list-weather" class="list-inline"></ul>
			<button class="btn-more">+</button>
		</div>

		<div class="slider-caption small">
			<h1 class="name"> <span>{{ $place->name }}</span></h1>
			<h3 class="slogan">{{ $place->description }}</h3>
		</div>

		<div id="favorite-star" onclick="favorito({{$place->id}},this,'<?php echo url('save-favorite')?>')" class="favorito <?php echo ($favorite==1)?'added':''; ?>">
			<i class="icon-star"></i>
		</div>
	</div>
</section>
<section class="destiny-history">
	<div id="resenia-region" class="block-history container">
	    <div class="row">
			<div class="col-md-8">
				<h2 class="title" id="text1parallaxname"
				   {{ $isAdmin?'code=text1 type=name contenteditable':'' }}>  {!! $contents["text1"]->name or '[tex1]-sitio-contenidos:nombre' !!}</h2>

				<div class="editable" id="text1parallaxcontent"
					 {{ $isAdmin?'code=text1 type=content contenteditable':'' }}>{!! $contents["text1"]->content or '[tex1]-sitio-contenidos:contenido' !!}</div>
            <hr class="visible-sm visible-xs">
			</div>
			<div class="col-md-4">
            	<div class="details">
				    <h5 class="sub-title" id="idealparallaxname"
				       {{ $isAdmin?'code=ideal type=name contenteditable':'' }}>  {!! $contents["ideal"]->name or '[ideal]-sitio-contenidos:nombre' !!} </h5>
				    <div class="editable" style="padding-bottom: 20px;border-bottom: dotted 1px;" id="idealparallaxcontent"
				    	 {{ $isAdmin?'code=ideal type=content contenteditable':'' }}>{!! $contents["ideal"]->content or '[ideal]-sitio-contenidos:contenido' !!}</div>
				    <h5 class="sub-title" id="olvidarparallaxname"
				       {{ $isAdmin?'code=olvidar type=name contenteditable':'' }}>  {!! $contents["olvidar"]->name or 'olvidar:nombre' !!} </h5>
				    <div class="editable" style="padding-bottom: 20px;border-bottom: dotted 1px;" id="olvidarparallaxcontent"
				    	 {{ $isAdmin?'code=olvidar type=content contenteditable':'' }}>{!! $contents["olvidar"]->content or '[olvidar]-sitio-contenidos:contenido' !!}</div>
				    <h5 class="sub-title" style="padding-top:20px;" id="curiosidadparallaxname"
				      {{ $isAdmin?'code=curiosidad type=name contenteditable':'' }}> {!! $contents["curiosidad"]->name or '[curiosidad]-sitio-contenidos:nombre' !!} </h5>
				    <div class="editable" id="curiosidadparallaxcontent"
				    	 {{ $isAdmin?'code=curiosidad type=content contenteditable':'' }}>{!! $contents["curiosidad"]->content or '[curiosidad]-sitio-contenidos:contenido' !!}</div>
			    </div>
            </div>
            <br>
        </div>
	</div>
	<div class="text-center">
		<a href="" class="vermas" id="imgdestino">
			VER MÁS <i class="icon-arrow-down"></i>
		</a>
	</div>
</section>
<section class='parallax-section'>
	@if(count($parallax) == 0)
		<div class="parallax-element">
			@if($isAdmin)
				<div class="parallax-window" data-parallax="scroll"
					 data-image-src="<?php echo URL::to('/'); ?>/img/casona1.jpg">[img]-sitio-multimedia:src
				</div>
				<div class="parallax-description">
					<div class="parallax-content">
						<p class='parallax-title'>[img]-sitio-multimedia:name</p>
						<p>[img]-sitio-multimedia:descripcion </p>
					</div>
				</div>
			@else
				<div class="parallax-window" data-parallax="scroll"
					 data-image-src="<?php echo URL::to('/'); ?>/img/casona1.jpg"></div>
				<div class="parallax-description">
					<div class="parallax-content">
						<p class='parallax-title'></p>
						<p></p>
					</div>
				</div>
			@endif

		</div>
	@else
		@foreach( $parallax as $item )
			<div class="parallax-element">
				<div class="parallax-window" data-parallax="scroll"
					 data-image-src="{{ URL::to('/')."/".$item->source }}"></div>
				<div class="parallax-description">
					<div class="parallax-content">
						@if($isAdmin)
							<input type='file' class="imgInp" style="margin-left: 20px;"/>
							<input type="hidden" id="{{$item->id}}">
							<p class='parallax-title' {{ $isAdmin?' contenteditable':'' }}> {{ $item->name }} </p>
							<p class="parallax-desc"  {{ $isAdmin?' contenteditable':'' }}> {{ $item->description }} </p>
						@else
							<p class='parallax-title'> {{ $item->name }} </p>
							<p> {{ $item->description }} </p>
						@endif
					</div>
				</div>
			</div>
		@endforeach
	@endif

</section>


@if($isAdmin)
	<div class="block-admin text-center">
		<h4>Activar Parallax</h4>
		<a class="button-admin" href="">Activar</a>
	</div>
@endif

	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'contents_partial'=>$contents,'code_text'=>'life-destiny-parallax'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$place, 'comments'=>$comments])


<script type="text/javascript" src="{{  asset('js/parallax.min.js') }}"></script>


<script>
	var codedestiny = "{{  $place->code  }}";
		$("#imgfront").change(function () {
			saveImage(this, 'sitio','front','{{ url('add-image-multimedia') }}',codedestiny);
		});


	$(".parallax-title").focusout(function () {
		var cod = $(this).siblings('input[type="hidden"]').attr('id');
		var desc = $(this).siblings('.parallax-desc').text();

		$.ajax({
			url: '<?php echo URL::to('edit-multimedia')?>',
			type: "post",
			data: {id: cod, name: $(this).text(), description: desc, code: 'img'},
			success: function (data) {
				// window.location.reload();
			}
		}, "json");
	});
	$(".parallax-desc").focusout(function () {
		var cod = $(this).siblings('input[type="hidden"]').attr('id');
		var desc = $(this).siblings('.parallax-title').text();

		$.ajax({
			url: '<?php echo URL::to('edit-multimedia')?>',
			type: "post",
			data: {id: cod, description: $(this).text(), name: desc, code: 'img'},
			success: function (data) {
				// window.location.reload();
			}
		}, "json");
	});
</script>
<script>
	function readURL(input) {
		var cod = $(input).siblings('input[type="hidden"]').attr('id');
		var desc = $(input).siblings('.parallax-title').text();
		var title = $(input).siblings('.parallax-desc').text();
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				var img = $(input).parent('div').parent('div').siblings('.parallax-window');
				img.attr("style", "background-image: url(" + e.target.result + ");background-repeat: no-repeat;background-size: cover");
			};
			var img = reader.readAsDataURL(input.files[0]);
		}
		var data = new FormData();
		data.append('id', cod);
		data.append('description', desc);
		data.append('name', title);
		data.append('file', input.files[0]);
		data.append('code', 'img');
		$.ajax({
			url: '<?php echo URL::to('edit-multimedia')?>',
			type: "post",
			processData: false,
			contentType: false,
			data: data,
			success: function (data) {
				// window.location.reload();
			}
		}, "json");
	}
	$(".imgInp").change(function () {
		readURL(this);
	});

</script>
<script>
	var inputfile;

	$("#sitenew").click(function () {

		if (inputfile.files && inputfile.files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
			};
			var data = new FormData();
			data.append('code', codedestiny);
			data.append('file', inputfile.files[0]);
			data.append('type', 'sitio');
			data.append('codechild', 'img');
			$.ajax({
				url: '<?php echo URL::to('add-image-multimedia')?>',
				type: "post",
				processData: false,
				contentType: false,
				data: data,
				success: function (data) {
					window.location.reload();
				}
			}, "json");
		}
	});
	$("#imgInp").change(function () {
		inputfile = this;

	});

</script>
@endsection
