<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($destiny->id);?>
<?php

function convertArrayContents($array)
{
	$sortArray = [];
	for ($i=0; $i < count($array); $i++) {
		$sortArray[$array[$i]->code] = $array[$i];
	}
	return $sortArray;
}
?>
@extends('website/appsite')

@section('content')

	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($multimedias['front5']))
					|
					<button class="button-admin"
							onclick="cropImage('destino','{{ $multimedias['front5']->id }}','{{ $destiny->code }}','{{ url($multimedias['front5']->source) }}','{{ url('save-image-crop') }}','front5',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif
		<div class="swiper-container" style="background: url('{{ URL::to('/') }}/{{ $multimedias['front5']->source_crop or ''}}')no-repeat center;background-size: cover;background-position: center center;">

			@include('partials.social_buttons')
			<div class="slider-caption">
				<h3 class="slogan" id="frase5destino" {{ $isAdmin?'code=frase5 type=content contenteditable':'' }}> {{ $content["frase5"]->content  or '[frase5]destino-contenidos:contenido' }} </h3>
				<h1 class="name">{{ $destiny->name or  'page:name' }}</h1>
			</div>

			<div onclick="favorito({{$destiny->id}},this,'<?php echo url('savefavorite')?>')" class="favorito <?php echo ($favorite==1)?'added':''; ?>">
				<i class="icon-star"></i>
				<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
			</div>
			@if( !isset($multimedias["front5"]) )
				<div class="resource" style="color: black;margin-top: 250px;width: 100%;position: absolute;text-align: center;">
					    @if($isAdmin)
                              [front5]-destino-multimedia:src
                        @endif
				</div>
			@endif
		</div>
		<ul class="list-destiny-menu">
			<li><a href="{{ url('/destino')."/".$destiny->code  }}" >{{ $destiny->name }}</a></li>
			<li><a href="{{ url('/como-llegar')."/".$destiny->code }}">¿cómo llegar?</a></li>
			<li><a href="{{ url('/aventura').'/'.$destiny->code }}">aventura</a></li>
			<li><a href="{{ url('/naturaleza')."/".$destiny->code }}"> naturaleza</a></li>
			<li><a href="{{ url('/cultura')."/".$destiny->code }}" class="active">cultura</a></li>
			<li><a href="{{ url('/descanso')."/".$destiny->code }}">descanso</a></li>
		</ul>
	</section>
	<section class="bread-crumb">
		<ol class="breadcrumb container">
			<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
			<li >{{$destiny->name}}</li>
			<li class="active">Cultura</li>
		</ol>
	</section>
	<section class="imperdibles" style="margin-bottom:14px;">
		<div class="container">
			<h2 class="title-content">
				<span id="text2culturacontent" {{ $isAdmin?'code=text2-cultura type=content contenteditable':'' }}>{!! $content["text2-cultura"]->content  or '[text2-cultura]-destino-contenidos:contenido' !!}</span>
			</h2>

		</div>
	</section>
		<?php $regpos=0; ?>
    	@for ( ; $regpos < count($categories)  && $regpos < count($categories)/6; $regpos++)

    	<section class="casilla">
    		<div class="content">
    				<div class="casilla-left">
                				<div>
                					<p class="text-uppercase">{{$categories[$regpos]->name}}</p>
			                            <?php $categoryContents = convertArrayContents($categories[$regpos]->contents()->get());?>
					                    <i class="{!! $categoryContents['icon']->content or '' !!}" style="padding-top: 20px;padding-bottom: 10px;"></i>
                					<p style="font-size:14.46px;">{{$categories[$regpos]->description}}</p>
                					<a href="{{URL::to('categoria/cultura')}}" style="color:white"><p style="font-size:13px;padding-top: 10px;">Ver mas...</p></a>
                				</div>
                			</div>
                			<div class="casilla-right">
                				<div class="slideaventura">
                				@foreach ( $categories[$regpos]->childs  as $site)
                					<a href="{{  URL::to('/parallax')."/".$site->code }}">
                                       <div style="height:366px;background: url('{{  URL::to('/')."/".$site->getCover() }}');background-size: cover;">
                                       </div>
                                    </a>
                				@endforeach


                				</div>
                			</div>
                		</div>


    		</div>
    	</section>
    	@endfor
	<section class="testimonios">
		<div class="container">
			@if(count($frasedestacada) > 0)
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="profile pull-left"><div style="background-image: url('{{ asset($frasedestacada[0]->getImageCropImg('cover')) }}');"></div></div>
						<div class="text-left cont-tes">
							<p class="name-tes" id="frasename1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=name contenteditable':'' }}><span>{!! $frasecontent[0]["text1"]->name  or '[text1]-Persona:nombre' !!}</span></p>
							<p class="des-tes" id="frasetext1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=content contenteditable':'' }}>{!! $frasecontent[0]["text1"]->content  or '[text1]-Bio:contenido' !!}</p>
							<p id="frasetext2" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text2 type=content contenteditable':'' }}>{!! $frasecontent[0]["text2"]->content  or '[text2]-Frase:contenido' !!}</p>
							@if($isAdmin && isset($frasedestacada[0]))
                              <button class="button-admin"
                              		onclick="cropImage('frasedestacada','{{ $frasedestacada[0]->getImageCropId('cover')  }}','{{ $frasedestacada[0]->code }}','{{url($frasedestacada[0]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('addimagemultimedia') }}',550)">
                              	<i class="icon-crop"></i> Editar imagen
                              </button>
                              <br> @endif
						</div>
					</div>
				</div>
			@else
				@if($isAdmin)
					<div class="block-admin text-center">
						<input id="frasedestacadatext" type="hidden" value="testimonio-{{ $destiny->code }}">
						<h4>No hay un testimonio en esta página</h4>
						<p class="info-admin">Haz clic en el botón para crear uno</p>
						<button id="frasedestacadanew" class="button-admin"><i class="icon-plus"></i> Crear testimonio</button>
					</div>
				@endif
			@endif
		</div>
	</section>
	<?php $count=0; ?>
        	@for ( ; $regpos < count($categories)  && $count < count($categories)/6; $regpos++)
            <?php $count++; ?>
            	<section class="casilla">
            		<div class="content">
            				<div class="casilla-left">
                        				<div>
                        					<p class="text-uppercase">{{$categories[$regpos]->name}}</p>
			                            <?php $categoryContents = convertArrayContents($categories[$regpos]->contents()->get());?>
					                    <i class="{!! $categoryContents['icon']->content or '' !!}" style="padding-top: 20px;padding-bottom: 10px;"></i>
                        					<p style="font-size:14.46px;">{{$categories[$regpos]->description}}</p>
                        					<a href="{{URL::to('categoria/cultura')}}" style="color:white"><p style="font-size:13px;padding-top: 10px;">Ver mas...</p></a>
                        				</div>
                        			</div>
                        			<div class="casilla-right">
                        				<div class="slideaventura">
                        				@foreach ( $categories[$regpos]->childs  as $site)
                        					<a href="{{  URL::to('/parallax')."/".$site->code }}">
                                               <div style="height:366px;background: url('{{  URL::to('/')."/".$site->getCover() }}');background-size: cover;">
                                               </div>
                                            </a>
                        				@endforeach


                        				</div>
                        			</div>
                        		</div>


            		</div>
            	</section>
            	@endfor
  	<!-- Banner-->
  	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$destiny,'number'=>0])
<?php $count=0; ?>
    	@for ( ; $regpos < count($categories)  && $count < count($categories)/6; $regpos++)
        <?php $count++; ?>
        	<section class="casilla">
        		<div class="content">
        				<div class="casilla-left">
                    				<div>
                    					<p class="text-uppercase">{{$categories[$regpos]->name}}</p>
			                            <?php $categoryContents = convertArrayContents($categories[$regpos]->contents()->get());?>
					                    <i class="{!! $categoryContents['icon']->content or '' !!}" style="padding-top: 20px;padding-bottom: 10px;"></i>
                    					<p style="font-size:14.46px;">{{$categories[$regpos]->description}}</p>
                    					<a href="{{URL::to('categoria/cultura')}}" style="color:white"><p style="font-size:13px;padding-top: 10px;">Ver mas...</p></a>
                    				</div>
                    			</div>
                    			<div class="casilla-right">
                    				<div class="slideaventura">
                    				@foreach ( $categories[$regpos]->childs  as $site)
                    					<a href="{{  URL::to('/parallax')."/".$site->code }}">
                                           <div style="height:366px;background: url('{{  URL::to('/')."/".$site->getCover() }}');background-size: cover;">
                                           </div>
                                        </a>
                    				@endforeach


                    				</div>
                    			</div>
                    		</div>


        		</div>
        	</section>
        	@endfor
	<section class="testimonios">
		<div class="container">
			@if(count($frasedestacada) > 1)
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="profile pull-left"><div style="background-image: url('{{ asset($frasedestacada[1]->getImageCropImg('cover')) }}');"></div></div>
						<div class="text-left cont-tes">
							<p class="name-tes" id="frase2name1" {{ $isAdmin?'page-id='.$frasedestacada[1]->id.' code=text1 type=name contenteditable':'' }}><span>{!! $frasecontent[1]["text1"]->name  or '[text1]-Persona:nombre' !!}</span></p>
							<p class="des-tes" id="frase2text1" {{ $isAdmin?'page-id='.$frasedestacada[1]->id.' code=text1 type=content contenteditable':'' }}>{!! $frasecontent[1]["text1"]->content  or '[text1]-Bio:contenido' !!}</p>
							<p id="frase2text2" {{ $isAdmin?'page-id='.$frasedestacada[1]->id.' code=text2 type=content contenteditable':'' }}>{!! $frasecontent[1]["text2"]->content  or '[text2]-Frase:contenido' !!}</p>
							@if($isAdmin && isset($frasedestacada[1]))
                              <button class="button-admin"
                              		onclick="cropImage('frasedestacada','{{ $frasedestacada[1]->getImageCropId('cover')  }}','{{ $frasedestacada[1]->code }}','{{url($frasedestacada[1]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('addimagemultimedia') }}',550)">
                              	<i class="icon-crop"></i> Editar imagen
                              </button>
                              <br> @endif
						</div>
					</div>
				</div>
			@else
				@if($isAdmin)
					<div class="block-admin text-center">
						<input id="frasedestacadatext2" type="hidden" value="testimonio-{{ $destiny->code }}">
						<h4>No hay un testimonio en esta página</h4>
						<p class="info-admin">Haz clic en el botón para crear uno</p>
						<button id="frasedestacadanew2" class="button-admin"><i class="icon-plus"></i> Crear testimonio</button>
					</div>
				@endif
			@endif
		</div>
	</section>
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$destiny,'number'=>1])
<?php $count=0; ?>
    	@for ( ; $regpos < count($categories)  && $count < count($categories)/6; $regpos++)
        <?php $count++; ?>
        	<section class="casilla">
        		<div class="content">
        				<div class="casilla-left">
                    				<div>
                    					<p class="text-uppercase">{{$categories[$regpos]->name}}</p>
                    					<img src="{{  URL::to('/')."/".$categories[$regpos]->getCover() }}" style="padding-top: 20px;padding-bottom: 10px;">
                    					<p style="font-size:14.46px;">{{$categories[$regpos]->description}}</p>
                    					<a href="{{URL::to('categoria/cultura')}}" style="color:white"><p style="font-size:13px;padding-top: 10px;">Ver mas...</p></a>
                    				</div>
                    			</div>
                    			<div class="casilla-right">
                    				<div class="slideaventura">
                    				@foreach ( $categories[$regpos]->childs  as $site)
                    					<a href="{{  URL::to('/parallax')."/".$site->code }}">
                                           <div style="height:366px;background: url('{{  URL::to('/')."/".$site->getCover() }}');background-size: cover;">
                                           </div>
                                        </a>
                    				@endforeach


                    				</div>
                    			</div>
                    		</div>


        		</div>
        	</section>
        	@endfor
        <!-- Banner-->
        @include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$destiny,'number'=>2])
<?php $count=0; ?>
    	@for ( ; $regpos < count($categories)  && $count < count($categories)/6; $regpos++)
        <?php $count++; ?>
        	<section class="casilla">
        		<div class="content">
        				<div class="casilla-left">
                    				<div>
                    					<p class="text-uppercase">{{$categories[$regpos]->name}}</p>
			                            <?php $categoryContents = convertArrayContents($categories[$regpos]->contents()->get());?>
					                    <i class="{!! $categoryContents['icon']->content or '' !!}" style="padding-top: 20px;padding-bottom: 10px;"></i>
                    					<p style="font-size:14.46px;">{{$categories[$regpos]->description}}</p>
                    					<a href="{{URL::to('categoria/cultura')}}" style="color:white"><p style="font-size:13px;padding-top: 10px;">Ver mas...</p></a>
                    				</div>
                    			</div>
                    			<div class="casilla-right">
                    				<div class="slideaventura">
                    				@foreach ( $categories[$regpos]->childs  as $site)
                    					<a href="{{  URL::to('/parallax')."/".$site->code }}">
                                           <div style="height:366px;background: url('{{  URL::to('/')."/".$site->getCover() }}');background-size: cover;">
                                           </div>
                                        </a>
                    				@endforeach


                    				</div>
                    			</div>
                    		</div>


        		</div>
        	</section>
        	@endfor
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$destiny,'number'=>3])
<?php $count=0; ?>
    	@for ( ; $regpos < count($categories)  && $count < count($categories)/6; $regpos++)
        <?php $count++; ?>
        	<section class="casilla">
        		<div class="content">
        				<div class="casilla-left">
                    				<div>
                    					<p class="text-uppercase">{{$categories[$regpos]->name}}</p>
			                            <?php $categoryContents = convertArrayContents($categories[$regpos]->contents()->get());?>
					                    <i class="{!! $categoryContents['icon']->content or '' !!}" style="padding-top: 20px;padding-bottom: 10px;"></i>
                    					<p style="font-size:14.46px;">{{$categories[$regpos]->description}}</p>
                    					<a href="{{URL::to('categoria/cultura')}}" style="color:white"><p style="font-size:13px;padding-top: 10px;">Ver mas...</p></a>
                    				</div>
                    			</div>
                    			<div class="casilla-right">
                    				<div class="slideaventura">
                    				@foreach ( $categories[$regpos]->childs  as $site)
                    					<a href="{{  URL::to('/parallax')."/".$site->code }}">
                                           <div style="height:366px;background: url('{{  URL::to('/')."/".$site->getCover() }}');background-size: cover;">
                                           </div>
                                        </a>
                    				@endforeach


                    				</div>
                    			</div>
                    		</div>


        		</div>
        	</section>
        	@endfor
	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$destiny,'contents_partial'=>$content,'code_text'=>'life-destiny-cultura'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$destiny, 'comments'=>$comments])
    	    	    <script>
                    var codedestiny="<?php echo $destiny->code?>";
  //img cover frase
		$("#imgfront").change(function ()
		{
			saveImage(this, 'destino','front5','{{ url('addimagemultimedia') }}',codedestiny);
		});
                    </script>
@endsection