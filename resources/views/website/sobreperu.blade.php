@extends('website/appsite')

@section('content')


	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($multimediainfo['front']))
					|
					<button class="button-admin"
							onclick="cropImage('','{{ $multimediainfo['front']->id }}','sobre-peru','{{ url($multimediainfo['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif
		<div class="swiper-container"
			 style="background: url('{{ url('/') }}/{{ $multimediainfo['front']->source_crop or '' }}')no-repeat center;background-size: cover;background-position: {{ $multimediainfo['front']->offsetx or 0 }}px {{ $multimediainfo['front']->offsety or 0}}px;">

			@include('partials.social_buttons')
			<div class="slider-caption">
				<h3 class="slogan"> {{ $page->description or  'page:description' }}</h3>
				<h1 class="name">{{ $page->name or  'page:name' }}</h1>
			</div>


			@if( !isset($multimediainfo["front"]) )
				@if($isAdmin)
					<div class="resource"
						 style="color: black;margin-top: 250px;width: 100%;position: absolute;text-align: center;">
						[front]-sobreperu-multimedia:src
					</div>
				@else
					<div class="resource"
						 style="color: black;margin-top: 250px;width: 100%;position: absolute;text-align: center;">

					</div>
				@endif
			@endif
		</div>
	</section>

	<section class="msj-bienvenida">
		<div class="container">
			<div class="home-msj col-md-8 col-md-offset-2">
			<div class="editable"  {{ $isAdmin?' code=welcome type=content contenteditable':'' }}>
				{!! $content["welcome"]->content  or '[welcome]:contenido' !!}
			</div>
			</div>
		</div>
	</section>

	<section class="destiny-history container">
		<div style="margin: 0 25px;">
			<div class="history-legend">
				<div class="row">
					<div class="col-sm-3">
						<img src="{{ asset('img/visita.png') }}">
						<div>
							<span class="value " id="visitassobreperucontent" {{ $isAdmin?' code=visitas type=content contenteditable':'' }}>{{ $content["visitas"]->content  or '[visitas] Dato' }}</span>
							<br>
							<span id="visitassobreperuname" {{ $isAdmin?' code=visitas type=name contenteditable':'' }}>{{ $content["visitas"]->name  or '[visitas] Label' }}</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/grupo.png') }}">
						<div>
					<span class="value " id="poblacionsobreperucontent" {{ $isAdmin?' code=poblacion type=content contenteditable':'' }}>
						{{ $content["poblacion"]->content  or '[poblacion] Dato' }}
					</span><br>
					<span id="poblacionsobreperuname" {{ $isAdmin?' code=poblacion type=name contenteditable':'' }}>
						{{ $content["poblacion"]->name  or '[poblacion] Label' }}
					</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/mapaicon.png') }}">
						<div>
					<span class="value " id="extencionsobreperucontent" {{ $isAdmin?' code=extencion type=name contenteditable':'' }}>
						{{ $content["extencion"]->content  or '[extencion] Dato' }}
					</span><br>
					<span id="extencionsobreperuname" {{ $isAdmin?' code=extencion type=name contenteditable':'' }}>
						{{ $content["extencion"]->name  or '[extencion] Label' }}
					</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/dinero.png') }}">
						<div>
					<span class="value " id="costosobreperucontent" {{ $isAdmin?' code=costo type=content contenteditable':'' }}>
						{{ $content["costo"]->content  or '[costo] Dato' }}
					</span><br>
					<span id="costosobreperuname" {{ $isAdmin?' code=costo type=name contenteditable':'' }}>
						{{ $content["costo"]->name  or '[costo] Label' }}
					</span>
						</div>
					</div>


				</div>
			</div>
		</div>
	</section>

	<section>
				@if($isAdmin)
    			<div class="block-admin">
    				<input id="sobre-perutext" type="text" class="input-small" placeholder="Sobre Perú">
    				<button class="button-admin" id="sobre-perunew">Crear Item</button>
    			</div>
    			@endif
    		 <div class="about-peru container">
    	@foreach($sobreperu_items as $key=>$item)
    	      @if($key%2==0 )
                <div class="about-peru-item">
                	<div class="row">
                		<div class="col-md-8">
                			<h3  {{ $isAdmin?'page-id='.$item->id.' code=text1 type=name contenteditable':'' }}>{!! $item->contents["text1"]->name  or $item->name !!}</h3>

                			<div class="editable"
                				 {{ $isAdmin?'page-id='.$item->id.' code=text1 type=content contenteditable':'' }}>{!! $item->contents["text1"]->content or $item->description !!}</div>
                		    @if($isAdmin)
                            <button class="button-admin" onclick="eliminarpage('{{ $item->id }}', '<?php echo url('content-delete-page')?>')"> Eliminar</button>
                            @endif
                		</div>

                		<div class="col-md-4 text-center">
                			<img src="{{ asset($item->getImageCropImg('cover'))}}" width="180">
                			@if($isAdmin)
                			        <br>
                            	    <button class="button-admin"
                                    		onclick="cropImage('sobreperu_item','{{ $item->getImageCropId('cover') }}','{{ $item->code }}','{{ asset($item->getCover()) }}','{{ url('save-image-crop') }}','cover',400,400,'{{ url('add-image-multimedia') }}',550)">
                                    	<i class="icon-image"></i> Editar imagen
                                    </button>
                            @endif
                		</div>
                	</div>
                </div>
            @else
               <div class="about-peru-item">
               	<div class="row">
               		<div class="col-md-4 text-center">
               			<img src="{{ asset($item->getImageCropImg('cover'))}}" width="180">
               			@if($isAdmin)
               			        <br>
                        	    <button class="button-admin"
                                		onclick="cropImage('sobreperu_item','{{ $item->getImageCropId('cover') }}','{{ $item->code }}','{{ asset($item->getCover()) }}','{{ url('save-image-crop') }}','cover',400,400,'{{ url('add-image-multimedia') }}',550)">
                                	<i class="icon-image"></i> Editar imagen
                                </button>
                        @endif
               		</div>
               		<div class="col-md-8">
                			<h3 {{ $isAdmin?'page-id='.$item->id:'' }} {{ $isAdmin?'code=text1 type=name contenteditable':'' }}>{!! $item->contents["text1"]->name  or $item->name !!}</h3>
                			<div class="editable" {{ $isAdmin?'page-id='.$item->id:'' }}
                				 {{ $isAdmin?'code=text1 type=content contenteditable':'' }}>{!! $item->contents["text1"]->content or $item->description !!}</div>
                		    @if($isAdmin)
                            <button class="button-admin" onclick="eliminarpage('{{ $item->id }}', '<?php echo url('content-delete-page')?>')"> Eliminar</button>
                            @endif
                    </div>
               	</div>
               </div>
            @endif
    	@endforeach

		</div>
	</section>
	<script>

		var codedestiny = 'sobre-peru';
		function readfrontimg(input) {

			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function (e) {
					$('.portada').attr("style", "background-image: url(" + e.target.result + ");background-repeat: no-repeat;background-size: cover");
				};
				var img = reader.readAsDataURL(input.files[0]);
				var data = new FormData();
				data.append('code', codedestiny);
				data.append('file', input.files[0]);
				data.append('type', '');
				data.append('codechild', 'front');
				$.ajax({
					url: '<?php echo URL::to('add-image-multimedia')?>',
					type: "post",
					processData: false,
					contentType: false,
					data: data,
					success: function (data) {

					}
				}, "json");
			}
		}
		$("#imgfront").change(function () {
			readfrontimg(this);

		});

		$("#sobre-perunew").click(function (event)
		{
		   var data_page={name: $("#sobre-perutext").val(), type: 'sobreperu_item',code:$("#sobre-perutext").val(),parent_id:'{{ $page->id}}'};
           createNewPage(event,data_page);
		});
	</script>
@endsection
