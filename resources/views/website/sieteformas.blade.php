<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($page->id);?>
@extends('website/appsite')

@section('content')
    <style>
        .map {
            width: 550px;
            height: 300px;
        }
    </style>
    @if($isAdmin)
		<div id="myModalmap" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Editar información de página</h4>
					</div>
					<div class="modal-body">
                        <div id="map-canvas" class="embed-responsive-16by9"></div>
						<p class="text-center">
							<button class="button-admin" onclick="savepuntosforma()">
								Guardar
							</button>
						</p>
					</div>
				</div>
			</div>
		</div>
		@endif
    <section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($multimedia['front']))
					|
					<button class="button-admin"
							onclick="cropImage('','{{ $multimedia['front']->id }}','{{ $page->code }}','{{ url($multimedia['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif


        <div class="swiper-container" style="background: url('{{ URL::to('/') }}/{{ $multimedia['front']->source_crop or ''}}')no-repeat center;background-size: cover;background-position: {{ $multimedia['front']->offsetx or 0 }}px {{ $multimedia['front']->offsety or 0}}px;">
            @include('partials.social_buttons')
            <div class="formas" style="position:absolute;  top: calc(74% - 96px);left: calc(22% - 121px);">
                <img src="{{asset('img/7formasban.png')}}">
            </div>

        </div>
    </section>


<section class="bread-crumb">
	<ol class="breadcrumb container">
		<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
		<li class="active">7 Formas de llegar a Machu Pichhu</li>
	</ol>
</section>
    <section class="formascont">
        <div class="content">
            <div class="headforma">

                    <p class="text-uppercase" id="text1name" {{ $isAdmin?'contenteditable':'' }}>{{$content['text1']->name or 'sieteformas[titulo]'}}</p>
                    <br>
                    <div class="editable" id="text1content"  style="text-align: justify" {{ $isAdmin?'contenteditable':'' }}>{!!$content['text1']->content or 'sieteformas[contenido]'!!}</div>


            </div>
			@if($isAdmin)
			<div class="block-admin">
				<input id="formatext" type="text" class="input-small" placeholder="FORMA">
				<button class="button-admin" id="formanew">Crear forma</button>

			</div>
			@endif
            @foreach($childs as $key=>$formas)


            <div class="boxforma">
                <div class="miniaturasforma">

                        <div class="pic" style="background: url('{{$formas->getImageCropImg('cover')}}')no-repeat center;background-size: cover;"></div>
                        <img src="{{asset('img/cuadroformas.png')}}">
             	@if($isAdmin)
             		<button class="button-admin" onclick="eliminarpage('{{ $formas->id }}', '<?php echo url('contentdeletepage')?>')"> Eliminar</button>
             		<button class="button-admin" onclick="cropImage('siete_formas','{{ $formas->getImageCropId('cover') }}','{{ $formas->code }}','{{ asset($formas->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)"> <i class="icon-crop"></i> Editar imagen </button>
             	@endif
                </div>

                <div class="textcenter" code="{{$formas->code}}">

                        <p class="text-uppercase textt1namenforma" style="font-size:20px;"  {{ $isAdmin?'contenteditable':'' }}>{!!$formas->content['text1-forma']->name or 'forma[titulo]'!!}</p>

                    <br>


                        <p  {{ $isAdmin?'contenteditable':'' }} class="textt1descriptionnforma editable">{!!$formas->content['text1-forma']->content or 'forma[descripcion]'!!} </p>

                </div>
                <div class="textright">
                    <button id-ver="vermas{{$key+1}}" class="ver-mas" style="background: #FA5050;border: none;height: 56px;width: 56px;border-radius: 50%;outline:none;cursor:pointer;color:white;"><img style="display: none" id="icon-vermas" src="{{asset('img/btn-up.png')}}"><p id="vermas">Leer más</p></button>
                </div>

                <div style="margin-left: 25%">
                    <div id="vermas{{$key+1}}" style="display:none;" code="{{$formas->code}}">

                           <div {{ $isAdmin?'contenteditable':'' }} class="textt1forma editable"> {!!$formas->content['text1']->content or 'forma[contenido]'!!}</div>

                    <br>
                        <img src="https://maps.googleapis.com/maps/api/staticmap?center={!!$formas->content['lat-forma']->content or -12.601475166388834!!},{!!$formas->content['lng-forma']->content or -75.11077880859375!!}&zoom=8&size=500x300&maptype=roadmap&markers=color:blue%7Clabel:S%7C{!!$formas->content['lat-forma']->content or -12.601475166388834!!},{!!$formas->content['lng-forma']->content or -75.11077880859375!!}">
                	@if($isAdmin)
                		<button class="button-admin" onclick="changemarkerforma({!!$formas->content['lat-forma']->content or -12.601475166388834!!},{!!$formas->content['lng-forma']->content or -75.11077880859375!!},'{{$formas->code}}')"> <i class="icon-crop"></i> Editar imagen </button>
                	@endif
                    </div>
                </div>

            </div>
            @endforeach

        </div>
    </section>

	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$page, 'comments'=>$comments,'contents'=>$content])

	<script src="https://maps.googleapis.com/maps/api/js"></script>

    <script>
        $('#icon-vermas').hide();
        $('.ver-mas').click(function(){

            var idver='#'+$(this).attr("id-ver");
            if($(idver).is(":visible")){
                $(idver).hide('fade');
                /*$('#vermas').hide();
                $('#icon-vermas').show();*/
            }
            else{
                $(idver).show('fade');
                /*$('#vermas').show();
                $('#icon-vermas').hide();*/

            }

        });
    @if($isAdmin)
    var points={lat:-34.397,lng:150.644};
    var code;
    function changemarkerforma(lat,lng,cod)
    {
    code=cod;
    points={lat:lat,lng:lng};
     $('#myModalmap').modal('show');
    }
        $('#myModalmap').on('show.bs.modal', function () {setTimeout( function(){
          google.maps.event.trigger(map, "resize");
              initialize();
              },400);
        });

    		$("#imgfront").change(function ()
    		{
    			saveImage(this, '', 'front', '{{ url('add-image-multimedia') }}', '{{$page->code}}');
    		});
            $("#text1name").focusout(function ()
            {
            	editcontent('{{$page->code}}', 'text1', $(this).html(), 'name', '', '<?php echo url("content-editable")?>')
            });
            $("#text1content").focusout(function ()
            {
            	editcontent('{{$page->code}}', 'text1', $(this).html(), 'content', '', '<?php echo url("content-editable")?>')
            });

            $(".textt1namenforma").focusout(function ()
            {
            	editcontent($(this).parent().attr('code'), 'text1-forma', $(this).html(), 'name', 'siete_formas', '<?php echo url("content-editable")?>')
            });
            $(".textt1descriptionnforma").focusout(function ()
            {
            	editcontent($(this).parent().attr('code'), 'text1-forma', $(this).html(), 'content', 'siete_formas', '<?php echo url("content-editable")?>')
            });
            $(".textt1forma").focusout(function ()
            {
            	editcontent($(this).parent().attr('code'), 'text1', $(this).html(), 'content', 'siete_formas', '<?php echo url("content-editable")?>')
            });
		$("#formanew").click(function ()
		{
        var data_page={name: $("#formatext").val(), type: 'siete_formas',code:$("#formatext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);

		});

        var map;
        var marker;
function initialize() {

  var mapOptions = {
    zoom: 8,
    center: new google.maps.LatLng(points.lat, points.lng)
  };

  map = new google.maps.Map(document.getElementById('map-canvas'),
      mapOptions);

  			marker = new google.maps.Marker({
  				position: new google.maps.LatLng(points.lat, points.lng),
  				map: map,
  				draggable: true
  			});
}
    function savepuntosforma()
    {
           var lat = marker.position.lat();
           var lng = marker.position.lng();
           	editcontent(code, 'lat-forma', lat, 'content', 'siete_formas', '<?php echo url("content-editable")?>');
           		editcontent(code, 'lng-forma', lng, 'content', 'siete_formas', '<?php echo url("content-editable")?>');
    }
google.maps.event.addDomListener(window, 'load', initialize);

@endif
    </script>
@endsection