<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($place->id);?>
@extends('website/appsite')

@section('content')

<section class="header">
	@if($isAdmin)
		<div class="block-admin-float">
			<span class="info-admin">Cambiar imagen: </span>
			<input type='file' id="imgfront"/>
			@if(isset($multimedia['front3']))
			|
					<button class="button-admin"
							onclick="cropImage('subdestino','{{ $multimedia['front3']->id }}','{{ $place->code }}','{{ url($multimedia['front3']->source) }}','{{ url('save-image-crop') }}','front3',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
			@endif
		</div>
	@endif
	<div class="swiper-container sub-destiny"
		 style="background: url('{{ URL::to('/') }}/{{ $multimedia['front3']->source or '' }}')no-repeat center;background-size: cover;background-position: center center;">
			@include('partials.social_buttons')

			<div class="temperatura">
				<ul id="list-weather" class="list-inline"></ul>
				<button class="btn-more">+</button>
			</div>
		<div class="slider-caption small">

				<h1 class="name"><span>{{ $place->name }}</span><br></h1>
					<h3 class="slogan" id="frase3subdestino" {{ $isAdmin?'code=frase3 type=content contenteditable':'' }}>{!! $content["frase3"]->content  or '[frase3]-subdestino-contenidos:contenido' !!}</h3>



		</div>
		<div id="favorite-star" onclick="favorito({{$place->id}},this,'<?php echo url('save-favorite')?>')"
			 class="favorito <?php echo ($favorite == 1) ? 'added' : ''; ?>">
			<i class="icon-star"></i>
			<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
		</div>



		<div class="menu-subdestino">
			<ul class="list-menu-subdestino">
				<li>
					<a href="{{ url('/sub-destino/'  .$place->code) }}"> <img src="{{ asset('img/subdesoption2.png') }}"> <p class="text-uppercase">Qué Visitar</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comer/' . $place->code) }}"> <img src="{{ asset('img/subdesoption.png') }}"> <p class="text-uppercase">dónde comer</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-hospedaje/'.$place->code) }}"> <img src="{{ asset('img/subdesoption3active.png') }}"> <p class="text-uppercase">hospedaje</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comprar/'.$place->code) }}"> <img src="{{ asset('img/subdesoption5.png') }}"> <p class="text-uppercase">dónde comprar</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-fiesta/'.$place->code) }}"> <img src="{{ asset('img/subdesoption4.png') }}"> <p class="text-uppercase">fiestas</p> </a>
				</li>
			</ul>
		</div>
	</div>
</section>
<section class="msj-bienvenida">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="editable" id="welcomesubdestino" {{ $isAdmin?'code=welcome-hospedaje type=content contenteditable':'' }}>{!!  $content["welcome-hospedaje"]->content or '[welcome-hospedaje]-subdestino-contenidos:contenido'!!}</div>
			</div>
		</div>
	</div>
</section>
<section class="bread-crumb">
	<ol class="breadcrumb container">
		<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
		<li><a href="destino/{{$place->parent()->code}}">{{$place->parent()->name}}</a></li>
		<li class="active">{{$place->name}}</li>
	</ol>
</section>
<section class="destiny-history container">
	<div id="resenia-region" class="block-history">
		<div class="row">
		    <div class="col-md-8">
				<h2 class="title" id="text1subdestinoname"
				   {{ $isAdmin?'code=text1-hospedaje type=name contenteditable':'' }}>{!! $content["text1-hospedaje"]->name  or '[text1-hospedaje]-subdestino-contenidos:nombre' !!}</h2>

				<div class="editable"  id="text1subdestinocontent"
					 {{ $isAdmin?'code=text1-hospedaje type=content contenteditable':'' }}>    {!! $content["text1-hospedaje"]->content or '[text1-hospedaje]-subdestino'!!}</div>
                <hr class="visible-sm visible-xs">
            </div>
		<div class="col-md-4">
		    <div class="details">
				<h5 class="sub-title" id="olvidarsubdestinoname"
				   {{ $isAdmin?'code=olvidar-hospedaje type=name contenteditable':'' }}>{!! $content["olvidar-hospedaje"]->name  or '[olvidar-hospedaje]-subdestino-contenidos:nombre' !!}</h5>
				<div class="editable" id="olvidarsubdestinocontent"
					 {{ $isAdmin?'code=olvidar-hospedaje type=content contenteditable':'' }}>{!! $content["olvidar-hospedaje"]->content  or '[olvidar-hospedaje]-subdestino-contenidos:contenido' !!}</div>
				<h5 class="sub-title"  id="sabersubdestinoname"
					 {{ $isAdmin?'code=saber-hospedaje type=name contenteditable':'' }}> {!! $content["saber-hospedaje"]->name  or '[saber-hospedaje]-subdestino-contenidos:nombre' !!} </h5>
				<div class="editable" id="sabersubdestinocontent"
							 {{ $isAdmin?'code=saber-hospedaje type=content contenteditable':'' }}> {!! $content["saber-hospedaje"]->content  or '[saber-hospedaje]-subdestino-contenidos:contenido' !!}</div>


			</div>
		</div>
		</div>
	</div>
		<div class="text-center">
			<a href="" class="vermas" id="imgdestino">
				VER MÁS <i class="icon-arrow-down"></i>
			</a>
		</div>
</section>
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>0])


<section class="imperdibles" style="margin-bottom:14px;">
	<div class="container">
		<h2 class="title-content">
			<span>hospedaje</span>
		</h2>
		<div class="hospedaje">
			<div class="profile-tabs">
				<div class="tabs">
					<div data-pws-tab="especiales" data-pws-tab-name="Especiales">
						<div style="background:#ebebeb;  padding-bottom: 40px;margin-bottom: 35px;">
							<p style=";font-size:15px;padding:25px 0;color:#555555;">Monto máximo por noche <input
										type="text" style="  margin: 0 15px;width: 20px;padding: 0 10px;">
								<select>
									<option>$ Dolares</option>
									<option>$ 200</option>
									<option>$ 300</option>
									<option>$ 400</option>
									<option>$ 500</option>
									<option>$ 600</option>
								</select>
							</p>
							@if($isAdmin)
								<div class="block-admin">
									<input id="sitetextespeciales" type="text" class="input-small"
										   placeholder="ESPECIALES">
									<button class="button-admin" id="sitenewespeciales">Crear sitio</button>
									|
									<button class="button-admin"
											onclick="openpopuporder('dragespeciales',{{$place->id}})">Editar posiciones
									</button>
								</div>
							@endif
							<ul id="dragespeciales" class=" list-thumbnail">
								@foreach($hospedajeEspeciales as $plac)
									<li data-id="{{ $plac->id }}">
										@if($isAdmin)
											<button class="button-admin"
													onclick="eliminarpage({{$plac->id}}, '<?php echo url('content-delete-page')?>')">
												Eliminar
											</button>
										@endif
										<a href="{{ url('/parallax')."/".$plac->code}}">
											<div class="title small"><span>{{$plac->name}}</span></div>
											<input type="hidden" value="{{$plac->code}}"/>

											<div class="pic"
												 style="background-image: url('{{ asset($plac->getCover()) }}');"></div>
										</a>
										@if($isAdmin)
								<button class="button-admin"
										onclick="cropImage('sitio','{{ $plac->getImageCropId('cover') }}','{{ $plac->code }}','{{ asset($plac->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>
										@endif
									</li>
								@endforeach
							</ul>


						</div>
					</div>
					<div data-pws-tab="hoteles" data-pws-tab-name="Hoteles">
						<div style="background:#ebebeb;  padding-bottom: 40px;margin-bottom: 35px;">
							<p style=";font-size:15px;padding:25px 0;color:#555555;">Monto máximo por noche <input
										type="text" style="  margin: 0 15px;width: 20px;padding: 0 10px;">
								<select>
									<option>$ Dolares</option>
									<option>$ 200</option>
									<option>$ 300</option>
									<option>$ 400</option>
									<option>$ 500</option>
									<option>$ 600</option>
								</select>
							</p>
							@if($isAdmin)
								<div class="block-admin">
									<input id="sitetexthoteles" type="text" class="input-small" placeholder="HOTELES">
									<button class="button-admin" id="sitenewhoteles">Crear sitio</button>
									|
									<button class="button-admin" onclick="openpopuporder('draghoteles',{{$place->id}})">
										Editar posiciones
									</button>
								</div>
							@endif
							<ul id="draghoteles" class=" list-thumbnail">
								@foreach($hospedajeHoteles as $plac)
									<li data-id="{{ $plac->id }}">
										@if($isAdmin)
											<button class="button-admin"
													onclick="eliminarpage({{$plac->id}}, '<?php echo url('content-delete-page')?>')">
												Eliminar
											</button>
										@endif
										<a href="{{ url('/parallax')."/".$plac->code}}">
											<div class="title small"><span>{{$plac->name}}</span></div>
											<input type="hidden" value="{{$plac->code}}"/>

											<div class="pic"
												 style="background-image: url('{{ asset($plac->getCover()) }}');"></div>
										</a>
										@if($isAdmin)
								            <button class="button-admin"
								            		onclick="cropImage('sitio','{{ $plac->getImageCropId('cover') }}','{{ $plac->code }}','{{ asset($plac->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
								            	<i class="icon-crop"></i> Editar imagen
								            </button>
										@endif
									</li>
								@endforeach
							</ul>

						</div>
					</div>
					<div data-pws-tab="hostales" data-pws-tab-name="Hostales">
						<div style="background:#ebebeb;  padding-bottom: 40px;margin-bottom: 35px;">
							<p style="font-size:15px;padding:25px 0;color:#555555;">Monto máximo por noche <input
										type="text" style="  margin: 0 15px;width: 20px;padding: 0 10px;">
								<select>
									<option>$ Dolares</option>
									<option>$ 200</option>
									<option>$ 300</option>
									<option>$ 400</option>
									<option>$ 500</option>
									<option>$ 600</option>
								</select>
							</p>
							@if($isAdmin)
								<div class="block-admin">
									<input id="sitetexthostales" type="text" class="input-small" placeholder="HOSTALES">
									<button class="button-admin" id="sitenewhostales">Crear sitio</button>
									|
									<button class="button-admin"
											onclick="openpopuporder('draghostales',{{$place->id}})">Editar posiciones
									</button>
								</div>
							@endif
							<ul id="draghostales" class=" list-thumbnail">
								@foreach($hospedajeHostales as $plac)
									<li data-id="{{ $plac->id }}">
										@if($isAdmin)
											<button class="button-admin"
													onclick="eliminarpage({{$plac->id}}, '<?php echo url('content-delete-page')?>')">
												Eliminar
											</button>
										@endif
										<a href="{{ url('/parallax')."/".$plac->code}}">
											<div class="title small"><span>{{$plac->name}}</span></div>
											<input type="hidden" value="{{$plac->code}}"/>

											<div class="pic"
												 style="background-image: url('{{ asset($plac->getCover()) }}');"></div>
										</a>
										@if($isAdmin)
							                <button class="button-admin"
							                		onclick="cropImage('sitio','{{ $plac->getImageCropId('cover') }}','{{ $plac->code }}','{{ asset($plac->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
							                	<i class="icon-crop"></i> Editar imagen
							                </button>
										@endif
									</li>
								@endforeach
							</ul>

						</div>
					</div>
					<div data-pws-tab="casa-hospedaje" data-pws-tab-name="Casa de Hospedaje">
						<div style="background:#ebebeb;  padding-bottom: 40px;margin-bottom: 35px;">
							<p style="font-size:15px;padding:25px 0;color:#555555;">Monto máximo por noche <input
										type="text" style="  margin: 0 15px;width: 20px;padding: 0 10px;">
								<select>
									<option>$ Dolares</option>
									<option>$ 200</option>
									<option>$ 300</option>
									<option>$ 400</option>
									<option>$ 500</option>
									<option>$ 600</option>
								</select>
							</p>
							@if($isAdmin)
								<div class="block-admin">
									<input id="sitetextcasa" type="text" class="input-small" placeholder="CASAS">
									<button class="button-admin" id="sitenewcasa">Crear sitio</button>
									|
									<button class="button-admin" onclick="openpopuporder('dragcasas',{{$place->id}})">
										Editar posiciones
									</button>
								</div>
							@endif
							<ul id="dragcasas" class=" list-thumbnail">
								@foreach($hospedajeCasa as $plac)
									<li data-id="{{ $plac->id }}">
										@if($isAdmin)
											<button class="button-admin"
													onclick="eliminarpage({{$plac->id}}, '<?php echo url('content-delete-page')?>')">
												Eliminar
											</button>
										@endif
										<a href="{{ url('/parallax')."/".$plac->code}}">
											<div class="title small"><span>{{$plac->name}}</span></div>
											<input type="hidden" value="{{$plac->code}}"/>

											<div class="pic"
												 style="background-image: url('{{ asset($plac->getCover()) }}');"></div>
										</a>
										@if($isAdmin)
								            <button class="button-admin"
								            		onclick="cropImage('sitio','{{ $plac->getImageCropId('cover') }}','{{ $plac->code }}','{{ asset($plac->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
								            	<i class="icon-crop"></i> Editar imagen
								            </button>
										@endif
									</li>
								@endforeach
							</ul>

						</div>
					</div>
					<div data-pws-tab="mochileros" data-pws-tab-name="Mochileros">
						<div style="background:#ebebeb;  padding-bottom: 40px;margin-bottom: 35px;">
							<p style="font-size:15px;padding:25px 0;color:#555555;">Monto máximo por noche <input
										type="text" style="  margin: 0 15px;width: 20px;padding: 0 10px;">
								<select>
									<option>$ Dolares</option>
									<option>$ 200</option>
									<option>$ 300</option>
									<option>$ 400</option>
									<option>$ 500</option>
									<option>$ 600</option>
								</select>
							</p>
							@if($isAdmin)
								<div class="block-admin">
									<input id="sitetextmochileros" type="text" class="input-small"
										   placeholder="MOCHILEROS">
									<button class="button-admin" id="sitenewmochileros">Crear sitio</button>
									|
									<button class="button-admin"
											onclick="openpopuporder('dragmochileros',{{$place->id}})">Editar posiciones
									</button>
								</div>
							@endif
							<ul id="dragmochileros" class=" list-thumbnail">
								@foreach($hospedajeMochileros as $plac)
									<li data-id="{{ $plac->id }}">
										@if($isAdmin)
											<button class="button-admin"
													onclick="eliminarpage({{$plac->id}}, '<?php echo url('content-delete-page')?>')">
												Eliminar
											</button>
										@endif
										<a href="{{ url('/parallax')."/".$plac->code}}">
											<div class="title small"><span>{{$plac->name}}</span></div>
											<input type="hidden" value="{{$plac->code}}"/>

											<div class="pic"
												 style="background-image: url('{{ asset($plac->getCover()) }}');"></div>
										</a>
										@if($isAdmin)
								            <button class="button-admin"
								            		onclick="cropImage('sitio','{{ $plac->getImageCropId('cover') }}','{{ $plac->code }}','{{ asset($plac->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
								            	<i class="icon-crop"></i> Editar imagen
								            </button>
										@endif
									</li>
								@endforeach
							</ul>

						</div>
					</div>
				</div>
			</div>
		</div>
<div class="mapadestino" >
    <p class="text-left">
	@if($isAdmin)
		<button onclick="saveMapPosition({{$place->id}},'<?php echo URL::to('save-point')?>')" class="button-admin">
			Guardar posición
		</button>
	@endif
	</p>
		<div id="map-canvas" class="map-canvas"></div>


</div>
	</div>
</section>



	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>1])
	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'contents_partial'=>$content,'code_text'=>'life-destiny-hospedaje'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$place, 'comments'=>$comments])

<script type="text/javascript" src="{{  URL::to('/')."/" }}slick/slick.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>

	var codedestiny = "<?php echo $place->code?>";
			$("#imgfront").change(function () {
    			saveImage(this, 'subdestino','front3','{{ url('addimagemultimedia') }}',codedestiny);
    		});
</script>
<script>
	$("#sitenewespeciales").click(function (event)
	{
	    var data_page={name: $("#sitetextespeciales").val(), type: 'sitio-hospedaje-especiales',code:$("#sitetextespeciales").val(),parent_id:'{{ $page->id}}'};
        createNewPage(event,data_page);
	});
	$("#sitenewhoteles").click(function (event)
	{
	    var data_page={name: $("#sitetexthoteles").val(), type: 'sitio-hospedaje-hoteles',code:$("#sitetexthoteles").val(),parent_id:'{{ $page->id}}'};
        createNewPage(event,data_page);
	});
	$("#sitenewhostales").click(function (event)
	{
	    var data_page={name: $("#sitetexthostales").val(), type: 'sitio-hospedaje-hostales',code:$("#sitetexthostales").val(),parent_id:'{{ $page->id}}'};
        createNewPage(event,data_page);
	});
	$("#sitenewcasa").click(function (event)
	{
	    var data_page={name: $("#sitetextcasa").val(), type: 'sitio-hospedaje-casa',code:$("#sitetextcasa").val(),parent_id:'{{ $page->id}}'};
        createNewPage(event,data_page);
	});
	$("#sitenewmochileros").click(function (event)
	{
	    var data_page={name: $("#sitetextmochileros").val(), type: 'sitio-hospedaje-mochileros',code:$("#sitetextmochileros").val(),parent_id:'{{ $page->id}}'};
        createNewPage(event,data_page);
	});
</script>
<script>


	var especiales = [

		@foreach($hospedajeEspeciales as $place)
		<?php
		$place->ubications();
		$name = $place->name;
		$lat = -12.601475166388834;
		$lon = -75.11077880859375;
		if (count($place->ubications) > 0) {
			$lat = $place->ubications[0]->content;
			$lon = $place->ubications[1]->content;
		}
		?>
		@if(isset($lat) && isset($lon))
		@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@else
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@endif
		@endif
		@endforeach
	];
	var hoteles = [
		@foreach($hospedajeHoteles as $place)
		<?php
		$place->ubications();
		$name = $place->name;
		$lat = -12.601475166388834;
		$lon = -75.11077880859375;
		if (count($place->ubications) > 0) {
			$lat = $place->ubications[0]->content;
			$lon = $place->ubications[1]->content;
		}
		?>
		@if(isset($lat) && isset($lon))
		@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@else
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@endif
		@endif
		@endforeach
	];
	var hostales = [
		@foreach($hospedajeHostales as $place)
		<?php
		$place->ubications();
		$name = $place->name;
		$lat = -12.601475166388834;
		$lon = -75.11077880859375;
		if (count($place->ubications) > 0) {
			$lat = $place->ubications[0]->content;
			$lon = $place->ubications[1]->content;
		}
		?>
		@if(isset($lat) && isset($lon))
		@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@else
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@endif
		@endif
		@endforeach
	];
	var casas = [
		@foreach($hospedajeCasa as $place)
		<?php
		$place->ubications();
		$name = $place->name;
		$lat = -12.601475166388834;
		$lon = -75.11077880859375;
		if (count($place->ubications) > 0) {
			$lat = $place->ubications[0]->content;
			$lon = $place->ubications[1]->content;
		}
		?>
		@if(isset($lat) && isset($lon))
		@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@else
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@endif
		@endif
		@endforeach
	];
	var mochileros = [
		@foreach($hospedajeMochileros as $place)
		<?php
		$place->ubications();
		$name = $place->name;
		$lat = -12.601475166388834;
		$lon = -75.11077880859375;
		if (count($place->ubications) > 0) {
			$lat = $place->ubications[0]->content;
			$lon = $place->ubications[1]->content;
		}
		?>
		@if(isset($lat) && isset($lon))
		@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@else
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@endif
		@endif
		@endforeach
	];
			<?php
			$lat = -12.046623;
			$lon = -77.042828;
			$zoom = 8;
			$name = ''?>
	var pstn = [
				@foreach($position as $pos)

				<?php

				$lat = $position[0]->content;
				$lon = $position[1]->content;
				if (count($position) > 2) {
					$zoom = $position[2]->content;
				}
				?>
				@endforeach

				@if( isset($lat) && isset($lon))
				{!! '{lat:'.$lat.',lon:'.$lon.',name:"'.$name.'",zoom:'.$zoom.'},' !!}
				@endif
			];
	var map;
	var markersArray = [];
	function initialize()
	{
		var mapCanvas = document.getElementById('map-canvas');
		var mapOptions = {
			center: new google.maps.LatLng(pstn[0].lat, pstn[0].lon),
			zoom: parseInt(pstn[0].zoom),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		}
		map = new google.maps.Map(mapCanvas, mapOptions)
		//	var bounds = new google.maps.LatLngBounds();
		for (var i = 0; i < especiales.length; i++)
		{
			var infowindow = new google.maps.InfoWindow();
			var position = new google.maps.LatLng(especiales[i].lat, especiales[i].lon);
			//bounds.extend(position);
			var marker = new google.maps.Marker({
				position: position,
				map: map,
				icon: "{{ asset('img/icon_dormir.png') }}",
				@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
				draggable: true,
				@endif
				title: especiales[i].name
			});
			markersArray.push(marker);
			google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
			{
				return function ()
				{
					infowindow.setContent(especiales[i].name);
					infowindow.open(map, this);
					window.location = "{{ URL::to('/')."/parallax/" }}" + especiales[i].code;
				};
			})(marker, i, infowindow));

			@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
			  google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
			{
				return function ()
				{
					savemappoint(especiales[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());
				};
			})(marker, i, infowindow));
			@endif
		}
		;
		var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
		map.setZoom(parseInt(pstn[0].zoom));
		map.panTo(latlong);
		/*map.addListener('click', function(e) {
		 var latlong= new google.maps.LatLng(pstn[0].lat,pstn[0].lon);
		 map.setZoom(parseInt(pstn[0].zoom));
		 map.panTo(latlong);
		 });*/
	}
	google.maps.event.addDomListener(window, 'load', initialize);


</script>
<script>
	$(document).ready(function ()
	{
		$('a[data-tab-id="hoteles"]').click(function (event)
		{
			clearmarker('hoteles');
		});
		$('a[data-tab-id="especiales"]').click(function (event)
		{
			clearmarker('especiales');
		});
		$('a[data-tab-id="casa-hospedaje"]').click(function (event)
		{
			clearmarker('casa');
		});
		$('a[data-tab-id="mochileros"]').click(function (event)
		{
			clearmarker('mochileros');
		});
		$('a[data-tab-id="hostales"]').click(function (event)
		{
			clearmarker('hostales');
		});
	});
	function clearmarker(tipo)
	{
		console.log(tipo);
		if (markersArray)
		{
			for (i in markersArray)
			{
				markersArray[i].setMap(null);
			}
			markersArray.length = 0;
		}
		switch (tipo)
		{
			case 'hoteles':
				for (var i = 0; i < hoteles.length; i++)
				{
					var infowindow = new google.maps.InfoWindow();
					var position = new google.maps.LatLng(hoteles[i].lat, hoteles[i].lon);
					var marker = new google.maps.Marker({
						position: position,
						map: map,
						icon: "{{ asset('img/icon_dormir.png') }}",
						@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
						draggable: true,
						@endif
						title: hoteles[i].name
					});
					markersArray.push(marker);

					google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
					{
						return function ()
						{
							infowindow.setContent(hoteles[i].name);
							infowindow.open(map, this);
							window.location = "{{ URL::to('/')."/parallax/" }}" + hoteles[i].code;
						};
					})(marker, i, infowindow));
					@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
					  google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
					{
						return function ()
						{
							savemappoint(hoteles[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());
						};
					})(marker, i, infowindow));
					@endif
				}
				;
				break;
			case 'especiales':
				for (var i = 0; i < especiales.length; i++)
				{
					var infowindow = new google.maps.InfoWindow({
						content: especiales[i].name
					});
					var position = new google.maps.LatLng(especiales[i].lat, especiales[i].lon);
					var marker = new google.maps.Marker({
						position: position,
						map: map,
						icon: "{{ asset('img/icon_dormir.png') }}",
						@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
						draggable: true,
						@endif
						title: especiales[i].name
					});
					markersArray.push(marker);
					google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
					{
						return function ()
						{
							infowindow.setContent(especiales[i].name);
							infowindow.open(map, this);
							window.location = "{{ URL::to('/')."/parallax/" }}" + especiales[i].code;
						};
					})(marker, i, infowindow));
					@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
					  google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
					{
						return function ()
						{
							savemappoint(especiales[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());
						};
					})(marker, i, infowindow));
					@endif

				}
				;
				break;
			case 'hostales':
				for (var i = 0; i < hostales.length; i++)
				{
					var infowindow = new google.maps.InfoWindow({
						content: hostales[i].name
					});
					var position = new google.maps.LatLng(hostales[i].lat, hostales[i].lon);
					var marker = new google.maps.Marker({
						position: position,
						map: map,
						icon: "{{ asset('img/icon_dormir.png') }}",
						@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
						draggable: true,
						@endif
						title: hostales[i].name
					});
					markersArray.push(marker);
					google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
					{
						return function ()
						{
							infowindow.setContent(hostales[i].name);
							infowindow.open(map, this);
							window.location = "{{ URL::to('/')."/parallax/" }}" + hostales[i].code;
						};
					})(marker, i, infowindow));
					@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
					google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
					{
						return function ()
						{
							savemappoint(hostales[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());
						};
					})(marker, i, infowindow));
					@endif
				}
				;
				break;
			case 'casa':
				for (var i = 0; i < casas.length; i++)
				{
					var infowindow = new google.maps.InfoWindow({
						content: casas[i].name
					});
					var position = new google.maps.LatLng(casas[i].lat, casas[i].lon);
					var marker = new google.maps.Marker({
						position: position,
						map: map,
						icon: "{{ asset('img/icon_dormir.png') }}",
						@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
						draggable: true,
						@endif
						title: casas[i].name
					});
					markersArray.push(marker);
					google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
					{
						return function ()
						{
							infowindow.setContent(casas[i].name);
							infowindow.open(map, this);
							window.location = "{{ URL::to('/')."/parallax/" }}" + casas[i].code;
						};
					})(marker, i, infowindow));
					@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
					google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
					{
						return function ()
						{
							savemappoint(casas[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());
						};
					})(marker, i, infowindow));
					@endif
				}
				break;
			case 'mochileros':
				for (var i = 0; i < mochileros.length; i++)
				{
					var infowindow = new google.maps.InfoWindow({
						content: mochileros[i].name
					});
					var position = new google.maps.LatLng(mochileros[i].lat, mochileros[i].lon);
					var marker = new google.maps.Marker({
						position: position,
						map: map,
						icon: "{{ asset('img/icon_dormir.png') }}",
						@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
						draggable: true,
						@endif
						title: mochileros[i].name
					});
					markersArray.push(marker);
					google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
					{
						return function ()
						{
							infowindow.setContent(mochileros[i].name);
							infowindow.open(map, this);
							window.location = "{{ URL::to('/')."/parallax/" }}" + mochileros[i].code;
						};
					})(marker, i, infowindow));
					@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
					google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
					{
						return function ()
						{
							savemappoint(mochileros[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());
						};
					})(marker, i, infowindow));
					@endif
				}
				;
				break

		}
	}
</script>
@endsection