@extends('website/appsite')

@section('content')
 <div id="myModalUserImage" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Editar imagen
				    <button class="button-admin"
							onclick="saveAvatar()">guardar
					</button>
					<input type='file' id="uploadimageAvatar"/>
				</h4>
			</div>
			<div class="modal-body" id="aqui_imagen">

			</div>
		</div>
	</div>
 </div>
  <section class="profile-section">
  @if(Session::has("message"))
    <div style="background:#de4a4a;width:800px;heigth:100px;margin:0 auto;margin-bottom:10px;text-align:center;color:white">{{ Session::get("message") }}</div>
  @endif
    <div class="profile-content">
      <div class="profile-information">
        <div class="profile-biography">
          <div class="profile-photo">
            <div class="profile-photo-circular">
              <img src="{{$profile_img}}" alt="Nombre del Perfil">

            </div>
          </div>

          <div class="profile-name">
                        <button class="button-admin" data-toggle="modal" data-target="#myModalUserImage">Cambiar Avatar
                        </button>
            <p>{{ \Illuminate\Support\Facades\Auth::user()->name }}</p>
          </div>
          <div class="profile-text">
            Son características las coplas de carnaval y los disfraces de abundante colorido.
          </div>
        </div>
        <div class="profile-contact">
          <div class="profile-city">
            <span class="ico ico-profile-city"></span>
            <span>Lima, Perú</span>
          </div>
          <div class="profile-email">
            <span class="ico ico-profile-email"></span>
            <span>{{ \Illuminate\Support\Facades\Auth::user()->email }}</span>
          </div>
          <div class="profile-member-date">
            <span class="ico ico-profile-member-date"></span>
            <span>Miembro desde {{ \Illuminate\Support\Facades\Auth::user()->created_at  }}</span>
          </div>
        </div>
        <div class="profile-map">
          <div class="profile-interactive-map">

          </div>
          <div class="profile-percent-map" style="text-align:center">
            <img src="<?php echo URL::to('/'); ?>/img/mapausuario.png" style="border: solid 1px;padding: 18px;">
            <p style="font-size: 14.34px;"><span style="font-size:21px;color:#de4a4a;">{{$conoce or '0'}}%</span> del Perú conocido</p>
            <progress max="100" value="{{$conoce or '0'}}" ></progress>
            <div></div>
          </div>
        </div>
        <div class="profile-share">
        </div>
      </div>
      <div class="profile-tabs">
        <div class="tabs">
          <div data-pws-tab="mis-actividades" data-pws-tab-name="Mis Actividades">
            <div>
              <div style="border-bottom: solid 1px #cccccc;">
                <p style="font-size: 15.99px;color: #de4a4a;text-decoration:underline;  margin-top: 15px;">CANOTAJE EN EL COLCA</p>
                <p style="color: #575757;margin: 15px 0;font-size: 13.98px;">Son caracteristicas las coplas de carnaval y los disfraces de abundnte colorido. Lamusica y la alegria son el tenor de las festividades y juegos.</p>
              </div>
              <div style="border-bottom: solid 1px #cccccc;">
                <p style="font-size: 15.99px;color: #de4a4a;text-decoration:underline;  margin-top: 15px;">KAJAC EN AREQUIPA</p>
                <p style="color: #575757;margin: 15px 0;font-size: 13.98px;">Son caracteristicas las coplas de carnaval y los disfraces de abundnte colorido. Lamusica y la alegria son el tenor de las festividades y juegos.</p>
              </div>
              <div style="border-bottom: solid 1px #cccccc;">
                <p style="font-size: 15.99px;color: #de4a4a;text-decoration:underline;  margin-top: 15px;">CANOTAJE EN EL COLCA</p>
                <p style="color: #575757;margin: 15px 0;font-size: 13.98px;">Son caracteristicas las coplas de carnaval y los disfraces de abundnte colorido. Lamusica y la alegria son el tenor de las festividades y juegos.</p>
              </div>
            </div>
          </div>
          <div data-pws-tab="mis-viajes" data-pws-tab-name="Mis Viajes">
            <div>
              <div style="border-bottom: solid 1px #cccccc;position:relative;height:112px;">
                <div style="float:left;width:25%;text-align:center;">
                  <img src="<?php echo URL::to('/'); ?>/img/viajes3.jpg" style="width: 120px;height: 74px; border-radius:3px;  margin-top: 18px;">
                </div>
                <div style="float:left;width:75%;margin-top: 16px;">
                  <p style="font-size: 15.99px;color: #de4a4a;text-decoration:underline;  margin-top: 15px;">RUTA DEL SUR</p>
                  <p style="color: #575757;margin: 8px 0;font-size: 13.98px;">5 Destino agregados</p>
                </div>
              </div>
              <div style="border-bottom: solid 1px #cccccc;position:relative;height:112px;">
                <div style="float:left;width:25%;text-align:center;">
                  <img src="<?php echo URL::to('/'); ?>/img/viajes2.jpg" style="width: 120px;height: 74px; border-radius:3px;  margin-top: 18px;">
                </div>
                <div style="float:left;width:75%;margin-top: 16px;">
                  <p style="font-size: 15.99px;color: #de4a4a;text-decoration:underline;  margin-top: 15px;">AMAZONÍA</p>
                  <p style="color: #575757;margin: 8px 0;font-size: 13.98px;">8 Destinos agregados</p>
                </div>
              </div>
              <div style="border-bottom: solid 1px #cccccc;position:relative;height:112px;">
                <div style="float:left;width:25%;text-align:center;">
                  <img src="<?php echo URL::to('/'); ?>/img/viajes1.jpg" style="width: 120px;height: 74px; border-radius:3px;margin-top: 18px;">
                </div>
                <div style="float:left;width:75%;margin-top: 16px;">
                  <p style="font-size: 15.99px;color: #de4a4a;text-decoration:underline;  margin-top: 15px;">MONTAÑAS</p>
                  <p style="color: #575757;margin: 8px 0;font-size: 13.98px;">5 Destinos</p>
                </div>
              </div>
              <button style="  position: absolute;right: 15px;margin-top: 18px;background: #de4a4a;border: none;color: white;border-radius: 3px;padding: 7px 18px;outline:none;">Editar</button>
            </div>
          </div>
          <div data-pws-tab="mis-favoritos" data-pws-tab-name="Mis Favoritos">
                    @foreach($favorites as $favorite)

					<?php   $url_pref = '';   switch ($favorite->type) {
						case 'destino':
							$url_pref = 'destino/';
							break;
						case 'category':
							$url_pref = 'categoria/';
							break;
						case 'subdestino':
							$url_pref = 'sub-destino/';
							break;
						case 'hecho_engrupo':
							$url_pref = 'hechoen/';
							break;
						case 'hecho_enperu':
							$url_pref = 'hechoen/';
							break;
						case 'region':
							$url_pref = 'region/';
							break;
						case '':
							switch ($favorite->code) {
								case '/home':
									$url_pref = '';
									break;
								case '/sobre-peru':
									$url_pref = 'sobre-peru';
									break;
								case '/home-aventura':
									$url_pref = 'categoria/aventura';
									break;
								case '/home-naturaleza':
									$url_pref = 'categoria/naturaleza';
									break;
								case '/home-cultura':
									$url_pref = 'categoria/cultura';
									break;
								case '/home-descanso':
									$url_pref = 'categoria/descanso';
									break;
							}
							$res['code'] = '';
							break;
						default:
							$url_pref = 'parallax';
							break;
					} ?>

				<a href="{{ url($url_pref.$favorite->code)  }}">

              <div style="border-bottom: solid 1px #cccccc;position:relative;height:112px;">
                <div style="float:left;width:25%;text-align:center;">
                  <img src="<?php echo url($favorite->getCover()) ?>" style="width: 120px;height: 74px; border-radius:3px;  margin-top: 18px;">
                </div>
                <div style="float:left;width:75%;margin-top: 16px;">
                  <p style="font-size: 15.99px;color: #de4a4a;text-decoration:underline;  margin-top: 15px;">{{$favorite->name}}</p>
                  <p style="color: #575757;margin: 8px 0;font-size: 13.98px;">{{$favorite->description or 'Descripción del destino'}}</p>
                </div>
              </div>


			</a>


				@endforeach
          </div>
          <div data-pws-tab="mis-badges" data-pws-tab-name="Mis Badges">
            Mis Badges
          </div>
          <div data-pws-tab="mis-compras" data-pws-tab-name="Mis Compras">
            Mis Compras
          </div>
          <div data-pws-tab="mis-reseñas" data-pws-tab-name="Mis Reseñas">
            Mis Reseñas
          </div>
        </div>
      </div>
    </div>
  </section>
  <script type="text/javascript" src="{{ URL::to('/') }}/slick/slick.min.js"></script>
<script>
		function changeAvatar(input)
		{
            var navegador = window.URL || window.webkitURL;
            var  objeto_url = navegador.createObjectURL(input.files[0]);
			$('#aqui_imagen').append('<img src="' +objeto_url+ '" style="width:500px;">');

		}
        function saveAvatar()
        {
        var archivos = document.getElementById('uploadimageAvatar').files;
			if (archivos[0]!=undefined)
			{
				$('#myPleaseWait').modal('show');
				var data = new FormData();
                data.append('file', archivos[0]);
				$.ajax({
					url: '{{ url('save-avatar') }}',
					type: "post",
					processData: false,
					contentType: false,
					data: data,
					success: function (data)
					{
						$('#myPleaseWait').modal('hide');
						window.location.reload();
					}
				}, "json");
			}
			else
			{
			swal('Alerta', 'Por favor elija una imagen antes de guardar', 'info');
			}
        }
		$("#uploadimageAvatar").change(function ()
		{
			changeAvatar(this);
		});
</script>

@endsection