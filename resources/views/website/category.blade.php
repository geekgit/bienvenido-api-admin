@extends('website/appsite')
@section('description') {{ $page->description}} @endsection
@section('title') {{ $page->name }} @endsection
@section('content')
	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($m['front'])) |
				<button class="button-admin"
						onclick="cropImage('','{{ $m['front']->id }}','home-aventura','{{ url($m['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
					<i class="icon-crop"></i> Editar imagen
				</button>
				@endif
			</div>
		@endif
		<div class="swiper-container"
			 style="background-image: url('{{ asset(isset($m['front'])?$m['front']->source_crop:'img/default.jpg') }}');">
			@include('partials.social_buttons')
			<div class="slider-caption">
				<h3 class="slogan"{{ $isAdmin?'contenteditable':'' }}> {{ $c["frase"]->content  or '[frase]-aventura:contenidos:contenido' }} </h3>
				<h1 class="name">{{ $page->name or  'page:name' }}</h1>
			</div>
		</div>
	</section>
	<!-- End header -->
	<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
				<div class="editable"
					  id="welcome" {{ $isAdmin?'contenteditable':'' }}> {!! $c["welcome"]->content or '[welcome]-aventura-contenidos:contenido' !!} </div>
				</div>
			</div>
		</div>
	</section>

	<section class="categories">
		<div class="container">
			@if($isAdmin)
				<div class="block-admin">
					<input id="categoriatext" type="text" class="input-small" placeholder="CATEGORIA">
					<button class="button-admin" id="categorianew">CREAR CATEGORIA</button>
					|
					<button class="button-admin" onclick="openpopuporder('dragListsub_destino','{{$page->id}}')">Editar
						posiciones
					</button>
				</div>
			@endif

			<div class="swiper-container menu-categories" style="height: 200px;">
				<div class="swiper-wrapper">
					@foreach($categories as $category )
						<div class="swiper-slide">
							@if($isAdmin)
								<p>
									<input type="hidden" value="{{$category->code}}">
									<button class="button-admin" onclick="modalselecticon(this,'category')">
										<i class="icon-dots-three-vertical"></i> Editar
									</button>
									<button class="button-admin"
											onclick="eliminarpage('{{$category->id}}', '<?php echo URL::to('content-delete-page')?>')">
										<i class="icon-trash-o"></i> Eliminar
									</button>
								</p>
							@endif
							<a href="#{{$category->code}}" data-tab-id="{{$category->id}}">
								<i data-code="{{$category->code}}"
								   class="icon {{ $category->contents['icon']->content or 'icon-caminando' }}"></i>
								<span class="name">{{ $category->name }}</span>
							</a>
						</div>
					@endforeach
				</div>
				<div class="swiper-pagination"></div>
			</div>

			<div class="swiper-container info-categories">
				<div class="swiper-wrapper">
					@foreach($categories as $category )
						<div id="{{$category->code}}" class="swiper-slide">
							<h4 contenteditable="true">{{ $category->contents["pregunta"]->content or '[pregunta]:name' }}</h4>
							<div class="editable" contenteditable="true">
								{!! $category->contents["respuesta"]->content or '[respuesta]:content' !!}
							</div>
						</div>
					@endforeach
				</div>
			</div>

			<!-- End category info -->

			<div class="row">
				<div class="col-md-6">
					@if($isAdmin)
						<button class="button-admin" onclick="saveMapPositionBefore()"> Guardar posición</button>
					@endif
					<div id="map-canvas"></div>
				</div>
				<div class="col-md-6">
					@if($isAdmin)
				        <div class="block-admin">
				        	<label for="selectdestiny">Selecciona un Sitio</label>
				        	<select id="selectdestiny">
				        		<option value=""><-- Sitios disponibles --></option>
				        		@forelse($selectsites as $d)
				        			<option value="{{ $d->code  }}">{{ $d->name . ' - ' . $d->type  }}</option>
				        		@empty
				        			<option value="">No hay sitios disponibles</option>
				        		@endforelse
				        	</select>
				        	<button id="selectdestinynew" class="button-admin"> Agregar</button>
				        </div>
                	@endif
					<ul id="list-sites" class="list-thumbnail">

					</ul>
				</div><!-- End col places -->
			</div>
		</div><!-- end category info -->
	</section>

	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$page,'contents_partial'=>$c,'code_text'=>'life-destiny-homeaventura'])

	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
		var menuCategories;
		var iconToEdit;
		var codepage = '{{$page->code}}';
		var codedestiny;
		var slideInfoCategories;
		var slideMenuCategories;
        var coordinates;
        var mapPosition ;
        var isAdmin='{{$isAdmin}}';
        var category_id;
        var categorycode;
		$(document).ready(initCategoriesPage);

		function modalselecticon(input) {
			iconToEdit = input;
			$('#myModalicons').modal('show');
		}

		function saveiconpage(input) {
			codedestiny = $(iconToEdit).siblings('input').val();
			var text = $(input).children().attr('class');
			$('[code-page=' + codedestiny + ']').addClass(text);
			$('#myModalicons').modal('hide');
			editcontent(codedestiny, 'icon', text, 'content', 'category', '{{ url("content-editable") }}');
		}

		function initCategoriesPage() {
			initMenuCategories();

		}
		function initMenuCategories() {
			slideMenuCategories = new Swiper('.menu-categories', {
				slidesPerView: 6,
				nextButton: '.cat-next',
				prevButton: '.cat-prev',
				breakpoints: {750: {slidesPerView: 2}, 990: {slidesPerView: 4}}
			});

			slideInfoCategories = new Swiper('.info-categories');

			menuCategories = $('.menu-categories');
			menuCategories.find('a').click(selectCategory);
			if(menuCategories.find('a').length>0)
			{
			    menuCategories.find('a')[0].click();
			}
		}

		function selectCategory(evt) {
			slideInfoCategories.slideTo(slideMenuCategories.clickedIndex);
			menuCategories.find('a').removeClass('active');
			$(this).addClass('active');
			category_id=$(this).attr('data-tab-id');
			categorycode=$(this).attr('href');
			categorycode=categorycode.replace('#','');
			changeList(category_id);
			evt.preventDefault();
		}
		var points={lat:-12.046623,lon:-77.042828,zoom:8};
        var map;
function initialize() {

 var mapOptions = {
 	center: new google.maps.LatLng(mapPosition.lat, mapPosition.lon),
 	zoom: parseInt(mapPosition.zoom),
 	mapTypeId: google.maps.MapTypeId.ROADMAP,
 	scrollwheel: false
 };
 var pinIcon = new google.maps.MarkerImage("{{ url("/img/icon_map_a.png") }}",
 		null, /* size is determined at runtime */
 		null, /* origin is 0,0 */
 		null, /* anchor is bottom center of the scaled image */
 		new google.maps.Size(38, 48)
 );
  map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
      			for(var i=0;i<coordinates.length;i++)
      			{
                    var infowindow = new google.maps.InfoWindow({
                    	content: '<h4>' + coordinates[i].name + '</h4><img style="width: 30%;"  src="'+ coordinates[i].img +'">  <div>' + coordinates[i].description + '</div><a href="{{ url('/parallax/') }}'+coordinates[i].code+'">Ver más</a>',
                    	maxWidth: 300
                    });
                    var position = new google.maps.LatLng(coordinates[i].lat, coordinates[i].lon);
                    var marker = new google.maps.Marker({
                    	position: position,
                    	map: map,
                    	icon: pinIcon,
                    	title: coordinates[i].name
                    });
                    google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
                    {
                    	return function () { infowindow.open(map, this); };
                    })(marker, i, infowindow));
      			}
}

function changeList(dataid){
	$.ajax({
		url: '{{url('category-list')}}',
		type: "post",
		data: {category_id: dataid},
		success: function (data)
		{

			coordinates=[];
			mapPosition={lat:-12.046623,lon:-77.042828,zoom:8};
			$('#list-sites').empty();

			for(var i=0;i<data.length;i++)
			{
            categorycode=data[i].codeheader;
				if(isAdmin==1)
				{

				 $('#list-sites').append('<li><button class="button-admin" onclick=eliminarrelationpage('+data[i].id+',"'+base_url+'delete-attach-page","'+data[i].codeheader+'") >Quitar</button><a href="'+base_url+ 'parallax/'+data[i].code +'"><div class="title small"><span>'+data[i].name +'</span></div><div class="pic" style="background-image: url('+base_url+data[i].cover+');"></div>  </a></li>');
				}
				else
				{
				 $('#list-sites').append('<li><a href="'+base_url+ 'parallax/'+data[i].code +'"><div class="title small"><span>'+data[i].name +'</span></div><div class="pic" style="background-image: url('+base_url+data[i].cover+');"></div>  </a></li>');
				}
				coordinates[i] =     		{
            			id: data[i].id,
            			code: data[i].code,
            			lat: data[i].lat,
            			lon: data[i].lon,
            			name: data[i].name,
            			description: data[i].description,
            			img:base_url+data[i].cover
            		} ;
            	 mapPosition = {
                	lat: data[i].category_lat,
                	lon: data[i].category_lon,
                	zoom: data[i].category_zoom
                };
			}

			initialize();
		}
	}, "json");
}
function saveMapPositionBefore()
{
saveMapPosition(category_id,base_url+'save-point');
}
$("#selectdestinynew").click(function ()
{
	if ($('#selectdestiny').val() != '')
	{
		$('#myPleaseWait').modal('show');
		$.ajax({
			url: '{{ url('attach-page') }}',
			type: "post",
			data: {child_code: $('#selectdestiny').val(), page_code: categorycode},
			success: function (data)
			{
				$('#myPleaseWait').modal('hide');
				window.location.reload();
			}
		}, "json");
	}
});
google.maps.event.addDomListener(window, 'load', initialize);
	</script>
@endsection