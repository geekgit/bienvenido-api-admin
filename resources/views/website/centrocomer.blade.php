<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($place->id);?>
@extends('website/appsite')

@section('content')

<section class="header">
	@if($isAdmin)
		<div class="block-admin-float">
			<span class="info-admin">Cambiar imagen: </span>
			<input type='file' id="imgfront"/>
			@if(isset($multimedia['front2']))
				|
					<button class="button-admin"
							onclick="cropImage('subdestino','{{ $multimedia['front2']->id }}','{{ $place->code }}','{{ url($multimedia['front2']->source) }}','{{ url('save-image-crop') }}','front2',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
			@endif
		</div>
	@endif
	<div class="swiper-container sub-destiny"
		 style="background-image: url('{{ url('/') }}/{{ $multimedia['front2']->source or 'default.png' }}');">
			@include('partials.social_buttons')

			<div class="temperatura">
				<ul id="list-weather" class="list-inline"></ul>
				<button class="btn-more">+</button>
			</div>
		<div class="slider-caption small">
				<h1 class="name"><span>{{ $place->name }}</span><br></h1>
				<h3 class="slogan" id="frase2subdestino" {{ $isAdmin?'code=frase2 type=content contenteditable':'' }}>{!! $content["frase2"]->content  or '[frase2]-subdestino-contenidos:contenido' !!}</h3>



		</div>
		<div onclick="favorito({{$place->id}},this,'<?php echo url('save-favorite')?>')"
			 class="favorito <?php echo ($favorite == 1) ? 'added' : ''; ?>">
			<i class="icon-star"></i>
			<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
		</div>



		<div class="menu-subdestino">
			<ul class="list-menu-subdestino">
				<li>
					<a href="{{ url('/sub-destino/'  .$place->code) }}"> <img
								src="{{ asset('img/subdesoption2.png') }}">
						<p class="text-uppercase">Qué Visitar</p></a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comer/' . $place->code) }}"> <img
								src="{{ asset('img/subdesoptionactive.png') }}">
						<p class="text-uppercase">dónde comer</p></a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-hospedaje/'.$place->code) }}"> <img
								src="{{ asset('img/subdesoption3.png') }}">
						<p class="text-uppercase">hospedaje</p></a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comprar/'.$place->code) }}"> <img
								src="{{ asset('img/subdesoption5.png') }}">
						<p class="text-uppercase">dónde comprar</p></a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-fiesta/'.$place->code) }}"> <img
								src="{{ asset('img/subdesoption4.png') }}">
						<p class="text-uppercase">fiestas</p></a>
				</li>
			</ul>
		</div>
	</div>
</section>
<section class="msj-bienvenida">
	<div class="content" style="position:relative;">
			<div class="region-msj" style="padding: 20px;padding-bottom: 30px;"><div class="editable" id="welcomecomersubdestino"	  {{ $isAdmin?'code=welcome-comer type=content contenteditable':'' }}> {!! $content["welcome-comer"]->content or '[welcome-comer]-subdestino-contenidos:contenido'!!} </div>
			</div>
	</div>
</section>
<section class="bread-crumb">
	<ol class="breadcrumb container">
		<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
		<li><a href="destino/{{$place->parent()->code}}">{{$place->parent()->name}}</a></li>
		<li class="active">{{$place->name}}</li>
	</ol>
</section>
<section class="imperdibles" style="margin-bottom:14px;">
	<div class="container">
		<h2 class="title-content">
			<span id="text1comersubdestinoname" {{ $isAdmin?'code=text1-comer type=name contenteditable':'' }}>{!! $content["text1-comer"]->name or '[text1-comer]-subdestino-contenidos:nombre' !!}</span>
		</h2>
		<div class="editable" id="text1comersubdestinocontent" {{ $isAdmin?'code=text1-comer type=content contenteditable':'' }}>{!! $content["text1-comer"]->content or '[text1-comer]-subdestino-contenidos:contenido' !!} </div>


		@if($isAdmin)
			<div class="block-admin">
				<input id="sitetext" type="text" class="input-small" placeholder="DONDE COMER">
				<button class="button-admin" id="sitenew">Crear sitio</button>
				|
				<button class="button-admin" onclick="openpopuporder('listsubdestinycomer',{{$place->id}})">Editar
					posiciones
				</button>

			</div>
		@endif
		<ul id="listsubdestinycomer" class=" list-thumbnail">
			@foreach($places as $plac)
				<li data-id="{{$plac->id}}">
					@if($isAdmin)
						<button class="button-admin"
								onclick="eliminarpage({{$plac->id}}, '<?php echo url('content-delete-page')?>')"> Eliminar
						</button>
					@endif
					<a title="{{$plac->id}}" href="{{ url('/parallax')."/".$plac->code}}">
						<div class="title small"><span>{{$plac->name}}</span></div>
						<input type="hidden" value="{{$plac->code}}"/>

						<div class="pic"
							 style="background-image: url('{{ asset($plac->getImageCropImg('cover')) }}');"></div>
					</a>
					@if($isAdmin)
								<button class="button-admin"
										onclick="cropImage('sitio-comer','{{ $plac->getImageCropId('cover') }}','{{ $plac->code }}','{{ asset($plac->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>
					@endif
				</li>
			@endforeach
		</ul>


		<div class="mapadestino" style="  padding-bottom: 40px;">
			<p class="text-left">
				<button onclick="clickbutton()"><i class="icon-pin"></i> Regresar a ubicación original</button>
				@if($isAdmin)
					<button class="button-admin pull-right"
							onclick="saveMapPosition({{$place->id}},'<?php echo url('save-point')?>')"> Guardar
						posición del mapa
					</button>
				@endif
			</p>
			<div id="map-canvas" style="width:100%;height:465px;border:0"></div>
		</div>
	</div>
</section>
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>0])
<section class="imperdibles" style="margin-bottom:14px;">
	<div class="container">
		<h2 class="title-content">
			<span id="text2subdestinoname" {{ $isAdmin?'code=text2-comer type=name contenteditable':'' }}>{!! $content["text2-comer"]->name or '[text2-comer]-subdestino-contenidos:nombre' !!}</span>
		</h2>
		<p id="text2subdestinocontent" {{ $isAdmin?'code=text2-comer type=content contenteditable':'' }}>{!! $content["text2-comer"]->content or '[text2-comer]-subdestino-contenidos:contenido' !!} </p>


		<ul id="" class=" list-thumbnail">
			@foreach($places as $plac)
				<li>
					<a href="{{ url('/parallax')."/".$plac->code}}">
						<div class="title small"><span>{{$plac->name}}</span></div>
						<input type="hidden" value="{{$plac->code}}"/>

						<div class="pic"
							 style="background-image: url('{{ asset($plac->getImageCropImg('cover')) }}');"></div>
					</a>
				</li>
			@endforeach
		</ul>

		<!--	<div id="panelexperto" style="display:none;">
			@foreach($places as $plac)

				<a href="{{ URL::to('/')."/parallax/".$plac->code }}">
        						<div class="miniaturasdestino">
        							<p class="title text-uppercase"><span>{!!  $plac->name !!}</span></p>
        							<div class="pic" style="background: url('<?php echo URL::to('/'); ?>/{{ $plac->getCover() }}')no-repeat center;background-size: cover;"></div>
        							<img src="<?php echo URL::to('/'); ?>/img/imperdibledestino.png">
        						</div>
        					</a>

        				@endforeach
				<!--		<a href="centrohistorico.html">
						<div class="miniaturasdestino">
							<p class="title text-uppercase"><span>arequipa banquete</span></p>
							<div class="pic" style="background: url('<?php echo URL::to('/'); ?>/img/banqueteare.jpg')no-repeat center;background-size: cover;"></div>
						<img src="<?php echo URL::to('/'); ?>/img/imperdibledestino.png">
					</div>
				</a>
				<a href="#">
					<div class="miniaturasdestino">
						<p class="title text-uppercase"><span>guía donde comer</span></p>
						<div class="pic" style="background: url('<?php echo URL::to('/'); ?>/img/guiacomer.jpg')no-repeat center;background-size: cover;"></div>
						<img src="<?php echo URL::to('/'); ?>/img/imperdibledestino.png">
					</div>
				</a>
				<a href="#">
					<div class="miniaturasdestino">
						<p class="title text-uppercase"><span>picanterías</span></p>
						<div class="pic" style="background: url('<?php echo URL::to('/'); ?>/img/picanteria.jpg')no-repeat center;background-size: cover;"></div>
						<img src="<?php echo URL::to('/'); ?>/img/imperdibledestino.png">
					</div>
				</a>
				<a href="#">
					<div class="miniaturasdestino">
						<p class="title text-uppercase"><span>manjares bajo el misti</span></p>
						<div class="pic" style="background: url('<?php echo URL::to('/'); ?>/img/manjares.jpg')no-repeat center;background-size: cover;"></div>
						<img src="<?php echo URL::to('/'); ?>/img/imperdibledestino.png">
					</div>
				</a>
			</div>-->
	</div>
	<!--		<div style="position:relative;">
			<p id="alterexperto" style="padding-top:35px;font-size:16px;color:#de4a4a;text-decoration:underline;">Leer más </p>
			<img id="expertobu" src="<?php echo URL::to('/'); ?>/img/vermas.png" class="vermas">
		</div>-->
</section>
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>1])

	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'contents_partial'=>$content,'code_text'=>'life-destiny-comer'])

	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$place, 'comments'=>$comments])


<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
	var codedestiny = "<?php echo $place->code?>";


			$("#imgfront").change(function () {
    			saveImage(this, 'subdestino','front2','{{ url('add-image-multimedia') }}',codedestiny);
    		});

</script>
<script>
	$("#sitenew").click(function (event)
	{
	    var data_page={name: $("#sitetext").val(), type: 'sitio-comer',code:$("#sitetext").val(),parent_id:'{{ $page->id}}'};
        createNewPage(event,data_page);
	});

</script>
<script>

	var coordinates = [

		@foreach($places as $place)

		<?php
		$place->ubications();
		$name = $place->name;
		$lat = -12.601475166388834;
		$lon = -75.11077880859375;
		if (count($place->ubications) > 0) {
			$lat = $place->ubications[0]->content;
			$lon = $place->ubications[1]->content;
		}

		?>
		@if(isset($lat) && isset($lon))
		@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@else
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@endif
		@endif
		@endforeach

	];
			<?php
			$lat = -12.046623;
			$lon = -77.042828;
			$zoom = 8;
			$name = ''?>
	var pstn = [

				@foreach($position as $pos)

				<?php

				$lat = $position[0]->content;
				$lon = $position[1]->content;
				if (count($position) > 2) {
					$zoom = $position[2]->content;
				}

				?>


				@endforeach

				@if( isset($lat) && isset($lon))
				{!! '{lat:'.$lat.',lon:'.$lon.',name:"'.$name.'",zoom:'.$zoom.'},' !!}
				@endif

			];
	var map;
	function initialize()
	{

		var mapCanvas = document.getElementById('map-canvas');
		var goldStar = {
			path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
			fillColor: 'yellow',
			fillOpacity: 0.8,
			scale: 0.1,
			strokeColor: 'gold',
			strokeWeight: 5
		};
		var mapOptions = {
			center: new google.maps.LatLng(pstn[0].lat, pstn[0].lon),
			zoom: parseInt(pstn[0].zoom),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		}

		map = new google.maps.Map(mapCanvas, mapOptions)
		//	var bounds = new google.maps.LatLngBounds();

		for (var i = 0; i < coordinates.length; i++)
		{
			var infowindow = new google.maps.InfoWindow();
			var position = new google.maps.LatLng(coordinates[i].lat, coordinates[i].lon);
			//bounds.extend(position);
			var marker = new google.maps.Marker({
				position: position,
				map: map,
				icon: "{{ asset('img/icon_comer.png') }}",
				@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
				draggable: true,
				@endif
				title: coordinates[i].name
			});

			google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
			{
				return function ()
				{
					infowindow.setContent(coordinates[i].name);
					infowindow.open(map, this);
					window.location = "{{ URL::to('/')."/parallax/" }}" + coordinates[i].code;
				};
			})(marker, i, infowindow));
			@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
			google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
			{
				return function ()
				{
					savemappoint(coordinates[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());
				};
			})(marker, i, infowindow));
					@endif
			var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
			map.setZoom(parseInt(pstn[0].zoom));
			map.panTo(latlong);

			/*map.addListener('click', function(e) {
			 var latlong= new google.maps.LatLng(pstn[0].lat,pstn[0].lon);
			 map.setZoom(parseInt(pstn[0].zoom));
			 map.panTo(latlong);
			 });*/
		}
	}
	google.maps.event.addDomListener(window, 'load', initialize);
	function clickbutton()
	{
		var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
		map.setZoom(parseInt(pstn[0].zoom));
		map.panTo(latlong);
	}
</script>
@endsection
