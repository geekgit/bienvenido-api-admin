@extends('website/appsite')

@section('content')

	<section class="panel">
		<div class="portada" style="background: url('img/caminomachu.jpg')no-repeat center 10%;background-size: cover;">
			<div class="social-buttons">
				<a href=""><img src="img/face-white.png"></a>
				<a href=""><img src="img/instagram-white.png" style="border-top: dotted 1px white;margin-left: 14px;padding-left: 0;padding-top: 10px;"></a>
			</div>
			<div class="formas" style="position:absolute;  top: calc(74% - 96px);left: calc(22% - 121px);">
				<img src="img/7formasban.png">
			</div>
			<div class="favorito" style="width: initial;">
				<img src="img/favoritoff.png">
			</div>
		</div>
	</section>
	<section class="escala">
		<div class="content">
			<p>Home > <span>7 Formas de llegar a Machu Pichhu</span></p>
		</div>
	</section>

	<section class="formascont">
		<div class="content">
			<div class="headforma">
				<p class="text-uppercase">7 Formas de llegar a machu Picchu</p>
				<br>
				<p>
					Machu Picchu, llaqta (ciudad) inca, es una de las siete maravillas modernas del mundo, un espacio mágico que se ha convertido en el lugar más visitado por viajeros nacionales y extranjeros.
				</p>
				<br>
				<p>
					Esta maravilla enclavada en el batolito de Vilcabamba, entre montañas verdes y nevados imponentes, constituye la frontera entre el mundo andino y el mundo amazónico, una especie de urbe elevada, sobre un espacio desde donde se domina el río Vilcanota (Wilkamayu o Río Sagrado) y  desde todas las latitudes distingues diversas formas de acercarte, de explorar y de llegar a la ciudad perdida de los incas.
				</p>
				<br>
				<p>
					Te proponemos siete opciones distintas para que tu elijas la mejor, la que vaya con tu personalidad y con la experiencia que quieras vivir. Machu Picchu y su entorno es un mundo inacabable, un espacio que aunque uno lo visite alguna vez en la vida siempre querrá regresar. Estas siete rutas te permitiran una y otra vez,  reencontrarte con la magia y misterio de Machu Picchu.
				</p>
			</div>
			<div class="boxforma">
				<div class="miniaturasforma">
					<div class="pic" style="background: url('img/turistas.jpg')no-repeat center;background-size: cover;"></div>
					<img src="img/cuadroformas.png">
				</div>
				<div class="textcenter">
					<p class="text-uppercase" style="font-size:20px;">1. por tren</p>
					<br>
					<p>
						Esta forma de viajar hacia Machu Picchu es la tradicional, usada desde 1928 y que hasta 1998 cubría la Ruta Cusco – Quillabamba, hoy en día existen 02 empresas que brindan diversos servicios para las diversas necesidades que tienen los viajeros, aquí les mostramos estos servicios.
					</p>
				</div>
				<div class="textright">
					<button style="background: #e56e6e;border: none;height: 56px;width: 56px;border-radius: 50%;outline:none;cursor:pointer;color:white;">Ver<br>más</button>
				</div>
			</div>
			<div class="boxforma">
				<div class="miniaturasforma">
					<div class="pic" style="background: url('img/turistas.jpg')no-repeat center;background-size: cover;"></div>
					<img src="img/cuadroformas.png">
				</div>
				<div class="textcenter">
					<p class="text-uppercase" style="font-size:20px;">2. Por el Camino Inca</p>
					<br>
					<p>
						La caminata más famosa del Perú y considerado uno de los mejores trekkings del mundo. Un camino duro pero maravilloso, subidas que ponen a prueba tu espíritu y paisajes nubosos cerca del cielo. Existen opciones de 2 y 4 días, dependiendo de tu capacidad física y tus ganas de aventurarte...
					</p>
				</div>
				<div class="textright">
					<button style="background: #e56e6e;border: none;height: 56px;width: 56px;border-radius: 50%;outline:none;cursor:pointer;color:white;">Ver<br>más</button>
				</div>
			</div>
			<div class="boxforma">
				<div class="miniaturasforma">
					<div class="pic" style="background: url('img/turistas.jpg')no-repeat center;background-size: cover;"></div>
					<img src="img/cuadroformas.png">
				</div>
				<div class="textcenter">
					<p class="text-uppercase" style="font-size:20px;">3. Bordeando el Salkantay</p>
					<br>
					<p>
						El nevado Salkantay es uno de los dos más importantes del Cusco, un macizo glaciar mágico y misterioso. La caminata que dura entre 4 y 6 días, desde Marcocasa, cruzando lagunillas y cumbres siguiendo un canal Inca, permite tener otro acercamiento al gran Machu Picchu. Caminos escarpados pero seguros que bordean el nevado hasta adentrarse...
					</p>
				</div>
				<div class="textright">
					<button>Ver<br>más</button>
				</div>
			</div>
			<div class="boxforma">
				<div class="miniaturasforma">
					<div class="pic" style="background: url('img/turistas.jpg')no-repeat center;background-size: cover;"></div>
					<img src="img/cuadroformas.png">
				</div>
				<div class="textcenter">
					<p class="text-uppercase" style="font-size:20px;">4. De Choquequirao a Machu Picchu</p>
					<br>
					<p>
						Unir las dos ciudades incas a traves de esta ruta es extraordinario, pero requiere de experiencia y buenas condiciones de cuerpo y espíritu. Su significado puede ser más que un simple camino, es recorrer la vía por donde los últimos incas caminaron conquistando la montaña y el bosque para hacer ciudades perfectas, bucólicas y llenas de vida...
					</p>
				</div>
				<div class="textright">
					<button style="background: #e56e6e;border: none;height: 56px;width: 56px;border-radius: 50%;outline:none;cursor:pointer;color:white;">Ver<br>más</button>
				</div>
			</div>
			<div class="boxforma">
				<div class="miniaturasforma">
					<div class="pic" style="background: url('img/turistas.jpg')no-repeat center;background-size: cover;"></div>
					<img src="img/cuadroformas.png">
				</div>
				<div class="textcenter">
					<p class="text-uppercase" style="font-size:20px;">5. EN AUTO O VAN POR EL ABRA DE MALAGA</p>
					<br>
					<p>
						Esta forma de llegar a Machu Picchu requiere más tiempo pero a veces resulta más económica. Tomar un auto o una Van desde Cusco u Ollantaytambo no es difícil, debe uno asegurarse que las empresas sean confiables. Por casi 3 horas uno va asciendiendo hasta los 4500 msnm y luego se baja por una carretera asfaltada y serpenteante...
					</p>
				</div>
				<div class="textright">
					<button style="background: #e56e6e;border: none;height: 56px;width: 56px;border-radius: 50%;outline:none;cursor:pointer;color:white;">Ver<br>más</button>
				</div>
			</div>
			<div class="boxforma">
				<div class="miniaturasforma">
					<div class="pic" style="background: url('img/turistas.jpg')no-repeat center;background-size: cover;"></div>
					<img src="img/cuadroformas.png">
				</div>
				<div class="textcenter">
					<p class="text-uppercase" style="font-size:20px;">6. En Bicicleta por la Selva de los Incas</p>
					<br>
					<p>
						Esta forma de llegar a Machu Picchu requiere más tiempo pero a veces resulta más económica. Tomar un auto o una Van desde Cusco u Ollantaytambo no es difícil, debe uno asegurarse que las empresas sean confiables. Por casi 3 horas uno va asciendiendo hasta los 4500 msnm y luego se baja por una carretera asfaltada y serpenteante...
					</p>
				</div>
				<div class="textright">
					<button style="background: #e56e6e;border: none;height: 56px;width: 56px;border-radius: 50%;outline:none;cursor:pointer;color:white;">Ver<br>más</button>
				</div>
			</div>
			<div class="boxforma">
				<div class="miniaturasforma">
					<div class="pic" style="background: url('img/turistas.jpg')no-repeat center;background-size: cover;"></div>
					<img src="img/cuadroformas.png">
				</div>
				<div class="textcenter">
					<p class="text-uppercase" style="font-size:20px;">7. De Vilcabamba a Machu Picchu</p>
					<br>
					<p>
						Vilcabamba es el último refugio de los Inca ante la conquista española, un pequeño pueblo que en sus cercanías tienen vestigios incas que son las últimas obras de arquitectura de esa gran civilización pero también muestras de arquitectura colonial sobre todo religiosa. Desde aquí, uno puede emprender camino, y durante 5 o 6 días...
					</p>
				</div>
				<div class="textright">
					<button style="background: #e56e6e;border: none;height: 56px;width: 56px;border-radius: 50%;outline:none;cursor:pointer;color:white;">Ver<br>más</button>
				</div>
			</div>
		</div>
	</section>
	<section style="margin-top: 36px;margin-bottom:36px;text-align:center;">
		<div class="content">
			<div class="comentarios">
				<p class="text-uppercase" style="  padding-top: 40px;padding-bottom:30px;"><span style="border-left: dotted 1px #afafaf;border-right: dotted 1px #afafaf;padding: 0 30px;font-size:30.17px;color:#e05e5e;">cuenta tu experiencia</span></p>
				<p style="font-size:16.01px;color: #787878;padding-bottom:30px;">Comparte tu experiencia, hazle saber que hiciste en este destino.</p>
				<div class="coninter" style="padding: 15px 9%;">
					<input type="text" placeholder="TITULO DE COMENTARIO:">
					<textarea placeholder="CUERPO DE COMENTARIO:"></textarea>
					<p class="text-uppercase"><span style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">enviar</span></p>
					<div style="border-bottom:dotted 1px;margin: 44px 0;color: #ababab;"></div>
				</div>
				<p class="text-uppercase"><span style="color:white;font-size:25px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">comentarios</span></p>
				<div class="coninter" style="  padding: 15px 9%;">
					<div class="cuenta">
						<div class="imgper">
							<div class="circleimage" style="background:url('img/foto-comentario1.jpg') no-repeat center;background-size: cover;"></div>
						</div>
						<div class="comentper">
							<div class="descripcom">
								<p class="text-uppercase titular">el mejor lugar para ir en familia</p>
								<p style="padding-bottom: 8px;">15/01/2015<span class="text-uppercase"><a href="">Arequipa</a></span></p>
								<p>Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades y juegos.</p>
							</div>
						</div>
					</div>
					<div class="cuenta">
						<div class="imgper">
							<div class="circleimage" style="background:url('img/foto-comentario2.jpg') no-repeat center;background-size: cover;"></div>
						</div>
						<div class="comentper">
							<div class="descripcom">
								<p class="text-uppercase titular">las mejores olas de sudamerica</p>
								<p style="padding-bottom: 8px;">15/01/2015<span class="text-uppercase"><a href="">trujillo</a></span></p>
								<p>Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades y juegos.</p>
							</div>
						</div>
					</div>
					<div class="cuenta" style="border: none;">
						<div class="imgper">
							<div class="circleimage" style="background:url('img/foto-comentario3.jpg') no-repeat center;background-size: cover;"></div>
						</div>
						<div class="comentper">
							<div class="descripcom">
								<p class="text-uppercase titular">hoy probé la mejor comida del mundo</p>
								<p style="padding-bottom: 8px;">15/01/2015<span class="text-uppercase"><a href="">ayacucho</a></span></p>
								<p>Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades y juegos.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@endsection
