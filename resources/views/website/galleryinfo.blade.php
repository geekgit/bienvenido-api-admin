<html>
<head>
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
</head>
<body>
<h3> sitios </h3>
@foreach($pages as $page)

<div class="imggallery">
  <a target="_blank" href="">
    <img src="{{url($page->getImageCropImg('cover'))}}" alt="Forest" width="300" height="200"><br>
    {{$page->name or ''}}
  </a>
  <div class="descgallery"><input type="checkbox" name="subdestiny[]" value="{{$page->code}}"><br>
  {{$page->description or ''}}</div>
</div>
@endforeach
<br>
<button class="button-admin"
		onclick="guardarInfo('')">
	Guardar
</button>
</body>
<script src="{{ asset('js/jquery-1.12.1.min.js') }}"></script>
<script>
var listaValoresCheckboxes = $("input[name='subdestiny[]']:checked").map(function () {
 return this.value;
}).get();
function guardarInfo()
{
var listaValoresCheckboxes=[];
        $("input[name='subdestiny[]']:checked").each(function(){
                listaValoresCheckboxes.push($(this).val());
        });
			$.ajax({
				url: '{{ url('registers/save-information')}}',
				type: "post",
				data: {lista: listaValoresCheckboxes},
				success: function (data)
				{
					window.location.href = '/';
				}
			}, "json");
}
</script>

</html>
