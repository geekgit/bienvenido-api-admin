@extends('website/appsite')

@section('content')

	<section class="magazine">
		<div class="container">
			<div class="block-magazine">
				<h1 class="text-center">Revistas</h1>
				<ul class="list-magazine">
					<li>
						<img src="{{ asset('img/portadamazonia.jpg') }}">
						<p>Bienvenida, Vive la Amazonía - Marzo 2015</p>
						<button>PRONTO</button>
					</li>
					<li>
						<img src="{{ asset('img/portadamachu.jpg') }}">
						<p>vive el Machu picchu Julio 2015</p>
						<button>PRONTO</button>
					</li>
					<li>
						<img src="{{ asset('img/portadalima.jpg') }}">
						<p>Bienvenida, Vive la Amazonía - Marzo 2015</p>
						<button>PRONTO</button>
					</li>
					<li>
						<img src="{{ asset('img/portadaplayas.jpg') }}">
						<p>Conoce las playas del norte - Marzo 2015</p>
						<button>PRONTO</button>
					</li>
				</ul>
			</div>
		</div>
	</section>

@endsection