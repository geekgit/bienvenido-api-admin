<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($place->id);?>
@extends('website/appsite')

@section('content')

<section class="header">
	@if($isAdmin)
		<div class="block-admin-float">
			<span class="info-admin">Cambiar imagen: </span>
			<input type='file' id="imgfront"/>
			@if(isset($multimedia['front4']))
			|
					<button class="button-admin"
							onclick="cropImage('subdestino','{{ $multimedia['front4']->id }}','{{ $place->code }}','{{ url($multimedia['front4']->source) }}','{{ url('save-image-crop') }}','front4',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
			@endif
		</div>
	@endif
	<div class="swiper-container sub-destiny"
		 style="background: url('{{ url('/') }}/{{ $multimedia['front4']->source or '' }}');">
			@include('partials.social_buttons')

			<div class="temperatura">
				<ul id="list-weather" class="list-inline"></ul>
				<button class="btn-more">+</button>
			</div>
		<div class="slider-caption small">

				<h1 class="name"><span>{{ $place->name }}</span><br></h1>
					<h3 class="slogan" id="frase4subdestino"
						 {{ $isAdmin?'code=frase4 type=content contenteditable':'' }}>{!! $content["frase4"]->content  or '[frase4]-subdestino-contenidos:contenido' !!}</h3>


		</div>
		<div id="favorite-star" onclick="favorito({{$place->id}},this,'<?php echo url('save-favorite')?>')"
			 class="favorito <?php echo ($favorite == 1) ? 'added' : ''; ?>">
			<i class="icon-star"></i>
			<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
		</div>



		<div class="menu-subdestino">
			<ul class="list-menu-subdestino">
				<li>
					<a href="{{ url('/sub-destino/'  .$place->code) }}"> <img src="{{ asset('img/subdesoption2.png') }}"> <p class="text-uppercase">Qué Visitar</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comer/' . $place->code) }}"> <img src="{{ asset('img/subdesoption.png') }}"> <p class="text-uppercase">dónde comer</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-hospedaje/'.$place->code) }}"> <img src="{{ asset('img/subdesoption3.png') }}"> <p class="text-uppercase">hospedaje</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comprar/'.$place->code) }}"> <img src="{{ asset('img/subdesoption5active.png') }}"> <p class="text-uppercase">dónde comprar</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-fiesta/'.$place->code) }}"> <img src="{{ asset('img/subdesoption4.png') }}"> <p class="text-uppercase">fiestas</p> </a>
				</li>
			</ul>
		</div>
	</div>
</section>
<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="editable" id="welcomesubdestino" {{ $isAdmin?'code=welcome type=content contenteditable':'' }}>{!!  $contents["welcome"]->content or 'OJO AQUI [welcome]-subdestino-contenidos:contenido'!!}</div>
				</div>
			</div>
		</div>
	</section>
<section class="bread-crumb">
	<ol class="breadcrumb container">
		<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
		<li><a href="destino/{{$place->parent()->code}}">{{$place->parent()->name}}</a></li>
		<li class="active">{{$place->name}}</li>
	</ol>
</section>
<section class="destiny-history container">
	<div id="resenia-region" class="block-history">
		<div class="row">
		    <div class="col-md-8">
				<h2 class="title" id="text1subdestinoname"
				   {{ $isAdmin?'code=text1-comprar type=name contenteditable':'' }}>{!! $content["text1-comprar"]->name  or '[text1-comprar]-subdestino-contenidos:nombre' !!}</h2>

				<div class="editable" id="text1subdestinocontent"
					 {{ $isAdmin?'code=text1-comprar type=content contenteditable':'' }}>    {!! $content["text1-comprar"]->content or '[text1-comprar]-subdestino-contenidos:contenido' !!}</div>

            <hr class="visible-sm visible-xs">
		    </div>
		<div class="col-md-4">
			<div class="details">
				<h5 class="sub-title"  id="olvidarsubdestinoname"
				   {{ $isAdmin?'code=olvidar-comprar type=name contenteditable':'' }}> {!! $content["olvidar-comprar"]->name  or '[olvidar-comprar]-subdestino-contenidos:nombre' !!} </h5>
				<div class="editable" style="padding-bottom: 20px;border-bottom: dotted 1px;" id="olvidarsubdestinocontent"
					 {{ $isAdmin?'code=olvidar-comprar type=content contenteditable':'' }}>{!! $content["olvidar-comprar"]->content or '[olvidar-comprar]-subdestino-contenidos:contenido' !!}</div>


            </div>
		</div>
		</div>
	</div>
		<div class="text-center">
			<a href="" class="vermas" id="imgdestino">
				VER MÁS <i class="icon-arrow-down"></i>
			</a>
		</div>
</section>
<section class="imperdibles">
	<div class="container">
		<div class="mapadestino">
			<p class="text-left">
				<button onclick="clickbutton()"><i class="icon-pin"></i> Regresar a ubicación original</button>
				@if($isAdmin)
					<button class="button-admin pull-right"
							onclick="saveMapPosition('{{$place->id}}','{{ url('savepoint') }}')"> Guardar
						posición del mapa
					</button>
				@endif
			</p>
			<div id="map-canvas" class="map-canvas"></div>
		</div>
	</div>
</section>
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>0])
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>1])
	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'contents_partial'=>$content,'code_text'=>'life-destiny-comprar'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$place, 'comments'=>$comments])



<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
	var codedestiny = "<?php echo $place->code?>";

		$("#imgfront").change(function () {
			saveImage(this, 'subdestino','front4','{{ url('add-image-multimedia') }}',codedestiny);
		});

</script>
<script>
	$("#sitenew").click(function (event)
	{
        var data_page={name: $("#sitetext").val(), type: 'sitio-compras',code:$("#sitetext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);
	});
</script>
<script>

	var coordinates = [

		@foreach($places as $place)

		<?php
		$place->ubications();
		$name = $place->name;
		$lat = -12.601475166388834;
		$lon = -75.11077880859375;
		if (count($place->ubications) > 0) {
			$lat = $place->ubications[0]->content;
			$lon = $place->ubications[1]->content;
		}

		?>
		@if(isset($lat) && isset($lon))
		@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@else
		{!! '{id:'.$place->id.',code:"'.$place->code.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
		@endif
		@endif
		@endforeach

	];
			<?php
			$lat = -12.046623;
			$lon = -77.042828;
			$zoom = 8;
			$name = ''?>
	var pstn = [

				@foreach($position as $pos)

				<?php

				$lat = $position[0]->content;
				$lon = $position[1]->content;
				if (count($position) > 2) {
					$zoom = $position[2]->content;
				}

				?>


				@endforeach

				@if( isset($lat) && isset($lon))
				{!! '{lat:'.$lat.',lon:'.$lon.',name:"'.$name.'",zoom:'.$zoom.'},' !!}
				@endif

			];
	var map;
	function initialize()
	{

		var mapCanvas = document.getElementById('map-canvas');

		var mapOptions = {
			center: new google.maps.LatLng(pstn[0].lat, pstn[0].lon),
			zoom: parseInt(pstn[0].zoom),
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			scrollwheel: false
		}

		map = new google.maps.Map(mapCanvas, mapOptions)
		//	var bounds = new google.maps.LatLngBounds();

		for (var i = 0; i < coordinates.length; i++)
		{
			var infowindow = new google.maps.InfoWindow({
				content: coordinates[i].name
			});
			var position = new google.maps.LatLng(coordinates[i].lat, coordinates[i].lon);
			//bounds.extend(position);
			var marker = new google.maps.Marker({
				position: position,
				map: map,
				icon: "{{ asset('img/icon_comprar.png') }}",
				@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
				draggable: true,
				@endif
				title: coordinates[i].name
			});
			google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
			{
				return function ()
				{
					infowindow.setContent(coordinates[i].name);
					infowindow.open(map, this);
					window.location = "{{ URL::to('/')."/parallax/" }}" + coordinates[i].code;
				};
			})(marker, i, infowindow));
			@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
						   google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
			{
				return function ()
				{
					savemappoint(coordinates[i].id, '<?php echo URL::to('save-point')?>', marker.position.lat(), marker.position.lng(), map.getZoom());

				};
			})(marker, i, infowindow));
			@endif

		}
		;
		var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
		map.setZoom(parseInt(pstn[0].zoom));
		map.panTo(latlong);
		/*map.addListener('click', function(e) {
		 var latlong= new google.maps.LatLng(pstn[0].lat,pstn[0].lon);
		 map.setZoom(parseInt(pstn[0].zoom));
		 map.panTo(latlong);
		 });*/
	}

	google.maps.event.addDomListener(window, 'load', initialize);
	function clickbutton()
	{
		var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
		map.setZoom(parseInt(pstn[0].zoom));
		map.panTo(latlong);
	}
</script>
@endsection
