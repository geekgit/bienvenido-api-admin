<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($page->id);?>
@extends('website/appsite')

@section('content')

<section class="header">
	@if($isAdmin)
		<div class="block-admin-float">
			<span class="info-admin">Cambiar imagen: </span>
			<input type='file' id="imgfront"/>
			@if(isset($multimedia['front']))
				|
					<button class="button-admin"
							onclick="cropImage('{{ $page->type  }}','{{ $multimedia['front']->id }}','{{ $page->code  }}','{{ url($multimedia['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
			@endif
		</div>
	@endif
	<div class="swiper-container"
		 style="background: url('{{ URL::to('/') }}/{{ $multimedia['front']->source_crop or ''}}')no-repeat center;background-size: cover;background-position: center center;">
		@include('partials.social_buttons')
		<div class="slider-caption">
			<h3 class="slogan" id="frase" {{ $isAdmin?'code=frase type=content contenteditable':'' }}> {{ $contents["frase"]->content  or 'destino-contenidos:contenido' }} </h3>
			<h1 class="name">{{ $page->name or  'page:name' }}</h1>
		</div>
			<div onclick="favorito({{$page->id}},this,'<?php echo url('save-favorite')?>')" class="favorito <?php echo ($favorite==1)?'added':''; ?>">
			<i class="icon-star"></i>
			<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
		</div>



					<!-- <div class="optiondestino">
                <div class="box">
                    <a href="{{ URL::to('/destino')."/".$page->code }}"><p class="text-uppercase">{{ $page->name }}</p></a>
                </div>
            </div>-->
	</div>
</section>
<section class="msj-bienvenida">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="editable" id="welcome" {{ $isAdmin?'code=welcome type=content contenteditable':'' }}>{!!  $contents["welcome"]->content or '[welcome]-subdestino-contenidos:contenido'!!}</div>
			</div>
		</div>
	</div>

</section>
<section class="bread-crumb">
	<ol class="breadcrumb container">
		<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
		<li class="active">Hecho en Perú</li>
		<li class="active">{{$page->name}}</li>
	</ol>
</section>
<section class="container">
	<hr>
	<div class="well">
		<h3>{{$page->name or $page->code.':name' }}  </h3>

		<div {{ $isAdmin?'code=text1 type=content contenteditable':'' }} class="editable" id="text1text">{!!  $contents["text1"]->content or '[text1]-subdestino-contenidos:contenido'!!}</div>
	</div>
	<div class="row">
			<div class="col-md-6">
				@if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray()))
					<button onclick="saveMapPosition({{$page->id}},'{{ url('save-point') }}')" class="button-admin">Guardar posición </button>
				@endif
				<div id="map-canvas" class="embed-responsive-16by9"></div>
			</div>
			<div class="col-md-6">
				@if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray()))
                    <div class="block-admin">
					@if($page->type=="hecho_enperu")
							<select id="selecthechoen">
								<option value=""><--Seleccione--></option>
								<?php for( $a = 0;$a < count($selecthechoen);$a++) { ?>
								<option value="<?php echo $selecthechoen[$a]->code ?>"><?php echo $selecthechoen[$a]->name . '-' . $selecthechoen[$a]->type ?></option>
								<?php } ?>
							</select>
							<button id="selectedhechoennew" class="button-admin">Agregar hecho en </button>
					@else
							<input id="hechoentext" type="text" value="" style="" placeholder="HECHO EN">
							<button id="hechoennew" class="button-admin"> Crear hecho en </button>

					@endif
					|
                    <button class="button-admin" onclick="openpopuporder('dragListsub_destino','{{$page->id}}')">
                    	Editar posiciones
                    </button>
                    </div>
				@endif
				<div class="slidevertical">
                <ul id="dragListsub_destino" class=" list-thumbnail">
                	@foreach($childs as $place)
                		<li data-id="{{$place->id}}">
                			@if($isAdmin)
                			    @if($page->type=='hecho_engrupo')
                                <button class="button-admin" onclick="eliminarpage('{{ $place->id }}', '<?php echo url('content-delete-page')?>')"> Eliminar</button>
                                <button class="button-admin" onclick="cropImage('hecho_en','{{ $place->getImageCropId('cover') }}','{{ $place->code }}','{{ asset($place->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)"> <i class="icon-crop"></i> Editar imagen </button>
                                @else
                                <button class="button-admin"
                                		onclick="eliminarrelationpage('{{ $place->id }}', '{{ url('delete-attach-page') }}','{{$page->code}}')">
                                	<i class="icon-trash"></i> Quitar
                                </button>
                                @endif
                			@endif
                			<a href="{{ url('/parallax')."/".$place->code}}">
                				<div class="title small"><span>{{$place->name}}</span></div>
                				<input type="hidden" value="{{$place->code}}"/>
                				<div class="pic" style="background-image: url('{{ asset($place->getImageCropImg('cover')) }}');"></div>
                			</a>
                		</li>
                	@endforeach
                </ul>
				</div>
			</div>
	</div>
</section>

	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$page,'contents_partial'=>$contents,'code_text'=>'life-destiny-hechoen'])



<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
	var codedestiny = "<?php echo $page->code?>";
	$("#selectedhechoennew").click(function () {

		if ($('#selecthechoen').val() != '') {
			$.ajax({
				url: '{{ url('attach-page') }}',
				type: "post",
				data: {child_code: $('#selecthechoen').val(), page_code: codedestiny},
				success: function (data) {
					window.location.reload();
				}
			}, "json");
		}
	});
	$("#hechoennew").click(function () {
        var data_page={name: $("#hechoentext").val(), type: 'hecho_en',code:$("#hechoentext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);
	});
		$("#imgfront").change(function ()
		{
			saveImage(this, '{{$page->type}}', 'front', '{{ url('add-image-multimedia') }}', codedestiny);
		});

</script>
	<script>
		var coordinates = [
			@foreach($childs  as $place)
			{
				id: '{{ $place->id  }}',
				code: '{{$place->code}}',
				lat: '{{ (count($place->ubications)>0)?$place->ubications[0]->content:'-12.601475166388834'}}',
				lon: '{{ (count($place->ubications)>0)?$place->ubications[1]->content:'-75.11077880859375'}}',
				name: '{{$place->name}}',
				description: '{{$place->description}}',
				img:'{{url($place->getImageCropImg('cover'))}}'
			},
			@endforeach
		];

		var mapPosition = {
			lat: '{{ (count($position)>0)?$position[0]->content:'-12.046623'}}',
			lon: '{{ (count($position)>1)?$position[1]->content:'-77.042828'}}',
			zoom: '{{ (count($position)>2)?$position[2]->content:'8'}}'
		};
		var map;

		function initialize()
		{
			var mapCanvas = document.getElementById('map-canvas');
			var pinIcon = new google.maps.MarkerImage("{{ url("/img/icon_map_a.png") }}",
					null, /* size is determined at runtime */
					null, /* origin is 0,0 */
					null, /* anchor is bottom center of the scaled image */
					new google.maps.Size(38, 48)
			);
			var mapOptions = {
				center: new google.maps.LatLng(mapPosition.lat, mapPosition.lon),
				zoom: parseInt(mapPosition.zoom),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false
			};

			map = new google.maps.Map(mapCanvas, mapOptions);

			for (var i = 0; i < coordinates.length; i++)
			{
				var infowindow = new google.maps.InfoWindow({
					content: '<h4>' + coordinates[i].name + '</h4><img style="width: 30%;"  src="'+ coordinates[i].img +'">  <div>' + coordinates[i].description + '</div><a href="{{ url('/parallax/') }}'+coordinates[i].code+'">Ver más</a>',
					maxWidth: 300
				});
				var position = new google.maps.LatLng(coordinates[i].lat, coordinates[i].lon);

				var marker = new google.maps.Marker({
					position: position,
					map: map,
					@if($isAdmin)
					draggable: true,
					@endif
					icon: pinIcon,
					title: coordinates[i].name
				});

				google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
				{
					return function () { infowindow.open(map, this); };
				})(marker, i, infowindow));


				@if($isAdmin)
				google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
				{
					return function ()
					{
						savemappoint(coordinates[i].id, '{{ url('save-point') }}', marker.position.lat(), marker.position.lng(), map.getZoom());
					};
				})(marker, i, infowindow));
				@endif

			}

			map.setZoom(parseInt(mapPosition.zoom));
			map.panTo(new google.maps.LatLng(mapPosition.lat, mapPosition.lon));
		}

		google.maps.event.addDomListener(window, 'load', initialize);

		function resetPositionMap()
		{
			var latlong = new google.maps.LatLng(mapPosition.lat, mapPosition.lon);
			map.setZoom(parseInt(mapPosition.zoom));
			map.panTo(latlong);
		}
	</script>
@endsection
