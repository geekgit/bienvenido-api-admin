@extends('website/appsite')

@section('content')
	<section class="search">
		<div class="container">
			<h1>Resultado de busqueda para: {{ $search }}</h1>
			<ul class="list-results">
				@foreach($results as $res)

					<?php $cnt=\App\Page::where('id',$res['parent_id'])->get(); if(count($cnt)>0){   $res['code'] = '/' . $res['code']; $url_pref = '';   switch ($res['type']) {
						case 'destino':
							$url_pref = 'destino';
							break;
						case 'category':
							$url_pref = 'categoria';
							break;
						case 'subdestino':
							$url_pref = 'sub-destino';
							break;
						case 'hecho_engrupo':
							$url_pref = 'hechoen';
							break;
						case 'hecho_enperu':
							$url_pref = 'hechoen';
							break;
						case 'region':
							$url_pref = 'region';
							break;
						case '':
							switch ($res['code']) {
								case '/home':
									$url_pref = '';
									break;
								case '/sobre-peru':
									$url_pref = 'sobre-peru';
									break;
								case '/home-aventura':
									$url_pref = 'categoria/aventura';
									break;
								case '/home-naturaleza':
									$url_pref = 'categoria/naturaleza';
									break;
								case '/home-cultura':
									$url_pref = 'categoria/cultura';
									break;
								case '/home-descanso':
									$url_pref = 'categoria/descanso';
									break;
							}
							$res['code'] = '';
							break;
						default:
							$url_pref = 'parallax';
							break;
					} ?>
					<li>
						<a href="{{ url($url_pref.$res['code'])  }}">
							<h3 class="title">{{$res['name']}}</h3>
							<p>{{$res['description'] or 'Descripción del destino'}}</p>
						</a>
					</li>
					<?php } ?>
				@endforeach
			</ul>
		</div>
	</section>
@endsection