@extends('website/appsite')

@section('content')

	<section class="panel">
		<div class="portada" style="background: url('img/backcentrohistorico.jpg')no-repeat center 10%;background-size: cover;height:650px;">
			<div class="social-buttons">
				<a href=""><img src="img/face-white.png"></a>
				<a href=""><img src="img/instagram-white.png" style="border-top: dotted 1px white;margin-left: 14px;padding-left: 0;padding-top: 10px;"></a>
			</div>
			<div class="temperatura">
				<img src="img/temperatura.png"><span>25ºC</span>
			</div>
			<div  class="leyendasubdes">
				<p class="text-uppercase"><span>ciudad y centro histórico</span><br>envuélvete en la magia</p>
			</div>
			<div class="favorito" style="bottom: 120px;">
				<img src="img/star.png">
				<p class="text-uppercase">añadido  a<br>favoritos</p>
			</div>
			<div class="menu-subdestino">
				<img src="img/icondown.png" style="  position: absolute;left: calc(50% - 11.5px);">
				<div class="box">
					<a href="centrohistorico.html">
						<img src="img/subdesoption2active.png">
						<p class="text-uppercase">qué visitar</p>
					</a>
				</div>
				<div class="box">
					<a href="centrocomer.html">
						<img src="img/subdesoption.png">
						<p class="text-uppercase">dónde comer</p>
					</a>
				</div>
				<div class="box">
					<a href="centrohospedaje.html">
						<img src="img/subdesoption3.png">
						<p class="text-uppercase">hospedaje</p>
					</a>
				</div>
				<div class="box">
					<a href="dondecomprar.html">
						<img src="img/subdesoption5.png">
						<p class="text-uppercase">dónde comprar</p>
					</a>
				</div>
				<div class="box" style="border:none;">
					<a href="fiestas.html">
						<img src="img/subdesoption4.png">
						<p class="text-uppercase">fiestas</p>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="msj-bienvenida">
		<div class="content" style="position:relative;">

			<p class="region-msj" style="padding: 20px;padding-bottom: 30px;"><span>Son características las coplas de carnaval y los disfraces de abundante colorido.<br>La música y la alegría son el tenor de las festividades, además de los juegos con pistolas de<br> agua, globos con agua y tinta.</span></p>
		</div>
	</section>
	<section class="escala">
		<div class="content">
			<p>Home > Arequipa > <span>Ciudad y Centro Histórico</span></p>
		</div>
	</section>
	<section class="destiny-history">
		<div class="content">
			<div class="resenia-left">
				<p class="text-uppercase title">Ciudad y Centro Histórico</p>
				<p style="  width: 100%;border-bottom: solid 2px #de4a4a;padding: 2px 0;margin-bottom: 18px;"></p>
				<p style="color: #555555;">Etum et alit, sum im fuga. Itae que explaceratem idelis a cor atia doloruptamus ut venda volendem sa quiberumqui ad quos ut auta sum ulparci taecabo. Obis ipsa suntem cusapiciet liquisit est officaborum fuga. Nam dollantus acestest ad utendae de of minusdandus enda aut officia nusapel ipsundanimil mi, sint doluptatur.
				</p>
				<br>
				<p>Qui debis miligniatis deles que debisin ctinciamus anis que ommolest magnisquid moluptatum di simus voluptatat quam vitibus nobis quos aut est acepuda con cum qui reptate nditaquos nus.</p>
				<br>
				<p>Tem aut ratis pora num cones dolupti atissequam conet lam ide reicaer ovidele stinctem aut volorep udantemque eatquia nam, officimus, sit, quate plant re vel inimo verum fuga. Itas num res debitae vit volore, odit deruptas reium duciatur acest dolora sed quasped quia doluptur, voluptatus, nulpa ad molupta ecabo. Aximinia volo iuscid et autectus parunt, quibus in parupta temporest, torae reptaquas aut ut quidus ipsapidit que volore moluptam rendam faccusd ameturiatem.</p>
				<br>
				<p>Itata cusam, consenest repello rehenem quaere doloreptae dolupta tiost, officie nditaque pel ipsuntore idi tem nimi, ut occus sit as essitam, utatquatur? Quia que vendist ibusapic tenimilis ipsapersped ut pel ius enihici asiminc iisinum quatissed et aut ea pro quibus del magnam repere sollesecae. Ut essintiore, nonem sed eaturibus ut id qui dolestiuntis mi, suntium sum volupit et quo dolore veni a quis sandam acerita tibusape dite mollorumqui aut faccum abori volenim quia con pel molor ma simodia aut et utectur erchiligenis.</p>
			</div>
			<div class="resenia-right">
				<p class="text-uppercase title">PARA NO OLVIDAR</p>
				<p style="padding-bottom: 20px;border-bottom: dotted 1px;">Qui debis miligniatis deles que debisin ctinciamus anis que Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis.</p>
				<p class="text-uppercase title" style="padding-top:20px;">datos curiosos</p>
				<p>Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos.</p>
			</div>
			<div class="optiondestino">
				<div class="boxdestino">
					<div style="display:inline-block;width:60%;text-align: right;">
						<p class="text-uppercase"><span>12500</span><br>visitas</p>
					</div>
					<div style="display:inline-block;width:38%;text-align: left;">
						<img src="img/visita.png">
					</div>
				</div>
				<div class="boxdestino">
					<div style="display:inline-block;width:60%;text-align: right;">
						<p class="text-uppercase"><span>25</span><br>población</p>
					</div>
					<div style="display:inline-block;width:38%;text-align: left;">
						<img src="img/grupo.png">
					</div>
				</div>
				<div class="boxdestino">
					<div style="display:inline-block;width:60%;text-align: right;">
						<p class="text-uppercase"><span>32</span><br>extención</p>
					</div>
					<div style="display:inline-block;width:38%;text-align: left;">
						<img src="img/mapaicon.png">
					</div>
				</div>
				<div class="boxdestino" style="border:none;">
					<div style="display:inline-block;width:60%;text-align: right;">
						<p class="text-uppercase"><span>$10 <b style="font-size: 14px !important;">costo</b></span><br>IOIOIOIOIO</p>
					</div>
					<div style="display:inline-block;width:38%;text-align: left;">
						<img src="img/dinero.png">
					</div>
				</div>
			</div>
			<div style="height: 7px; background: #e15c5c;margin-top:3px;position:relative">
				<img src="img/vermas.png" class="vermas" style="position: absolute;top:-7px;;border:solid 2px white;border-radius: 50%;left: calc(47% - 9px);">
			</div>
		</div>
	</section>
	<section class="imperdibles" style="margin-bottom:14px;">
		<div class="content">
			<div class="titular">
				<p class="text-uppercase"><span>los imperdibles</span></p>
				<p class="text-center subdes">Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, <br>además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval.</p>
			</div>
			<div class="slideslick">
				<div>
					<a href="centrohistorico.html">
						<div class="miniaturasdestino">
							<p class="title text-uppercase"><span>plaza de armas</span></p>
							<div class="pic" style="background: url('img/plazarmas.jpg')no-repeat center;background-size: cover;"></div>
							<img src="img/imperdibledestino.png">
						</div>
					</a>
				</div>
				<div>
					<a href="#">
						<div class="miniaturasdestino">
							<p class="title text-uppercase"><span>M. santa catalina</span></p>
							<div class="pic" style="background: url('img/santacatalina.jpg')no-repeat center;background-size: cover;"></div>
							<img src="img/imperdibledestino.png">
						</div>
					</a>
				</div>
				<div>
					<a href="#">
						<div class="miniaturasdestino">
							<p class="title text-uppercase"><span>iglesia y claustros</span></p>
							<div class="pic" style="background: url('img/iglesiaclaustro.jpg')no-repeat center;background-size: cover;"></div>
							<img src="img/imperdibledestino.png">
						</div>
					</a>
				</div>
				<div>
					<a href="#">
						<div class="miniaturasdestino">
							<p class="title text-uppercase"><span>casonas</span></p>
							<div class="pic" style="background: url('img/casona.jpg')no-repeat center;background-size: cover;"></div>
							<img src="img/imperdibledestino.png">
						</div>
					</a>
				</div>
				<div>
					<a href="centrohistorico.html">
						<div class="miniaturasdestino">
							<p class="title text-uppercase"><span>plaza de armas</span></p>
							<div class="pic" style="background: url('img/plazarmas.jpg')no-repeat center;background-size: cover;"></div>
							<img src="img/imperdibledestino.png">
						</div>
					</a>
				</div>
			</div>
		</div>
	</section>
	<section class="mapadestino" style="  padding-bottom: 40px;">
		<div class="content" style="position:relative;">
			<img src="img/mapadestino.png" style=" width: 100%;">
			<img src="img/mapa-centrohistorico.jpg" style="position:absolute;position: absolute;top: 9px;width: 98%;left: 10px;">
		</div>
	</section>
	<section class="testimonios" style="margin-bottom:40px;">
		<div class="content" style="text-align:left;">
			<div  class="leftcont">
				<div class="borderpunter" style="top: 19px;left: 136px;"></div>
				<div class="picture" style="background: url('img/jorgerodriguez.jpg')no-repeat center;background-size: cover;"></div>
			</div>
			<div class="rightcont">
				<p class="text-uppercase name-tes"><span>jorge rodriguez<span></span></span></p>
				<p class="text-uppercase des-tes" style="padding-bottom: 0;">fotógrafo del día</p>
				<p class="con-tes" style="font-size:14px;" style="margin:0;">Son características las coplas de carnaval y los disfraces de abundante colorido.<br>
				La música y la alegría son el tenor de las festividades.</p>
			</div>
		</div>
	</section>
	<section class="viveperu">

		<div class="content" style="border:none;padding-bottom:0;">
			<div class="titular">
				<p class="text-uppercase"><span>vive arequipa</span></p>
			</div>
			<p class="text-center subdes">Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además los juegos con pistolas de agua, globos con agua. Un elemento característico del carnaval también lo constituye la chicha.<br>Sobre las «coplas de carnaval tienen un carácter tradicional regional; y se cantan acompañadas por guitarras.</p>
			<div class="social-left">
				<div class="imgsocialbig" style="background: url('img/big-region.jpg') no-repeat center; background-size:cover;"></div>
				<div class="desc">
					<div class="iconosocial">
						<img src="img/socialftrans.png">
					</div>
					<div class="mensajesocial">
						<div style="position:absolute;">
							<p class="text-uppercase title">Machupicchu</p>
							<p>Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades, además de los juegos.</p>
						</div>
					</div>
				</div>
			</div>
						<div class="social-right">
            				<div class="small-box">
            					<img src="<?php echo URL::to('/'); ?>/img/socialinsta.png">
            					<div class="imgsocialsmall num1"> <div id="playfeed"></div></div>
            				</div>
            				<div class="small-box">
            					<img src="<?php echo URL::to('/'); ?>/img/socialface.png">
            					<div class="imgsocialsmall num2"></div>
            				</div>
            				<br><br>
            				<div class="small-box">
            					<img src="<?php echo URL::to('/'); ?>/img/socialface.png">
            					<div class="imgsocialsmall num3"></div>
            				</div>
            				<div class="small-box">
            					<img src="<?php echo URL::to('/'); ?>/img/socialinsta.png">
            					<div class="imgsocialsmall num4" style="background: url('<?php echo URL::to('/'); ?>/img/small-region.jpg') no-repeat center; background-size:cover;"><div id="tagfeed"></div></div>
            				</div>
            			</div>
		</div>
	</section>
	<section style="margin-top: 66px;margin-bottom:36px;text-align:center;">
		<div class="content">
			<div class="comentarios">
				<p class="text-uppercase" style="  padding-top: 40px;padding-bottom:30px;"><span style="border-left: dotted 1px #afafaf;border-right: dotted 1px #afafaf;padding: 0 30px;font-size:30.17px;color:#e05e5e;">cuenta tu experiencia</span></p>
				<p style="font-size:16.01px;color: #787878;padding-bottom:30px;">Comparte tu experiencia, hazle saber que hiciste en este destino.</p>
				<div class="coninter" style="padding: 15px 9%;">
					<input type="text" placeholder="TITULO DE COMENTARIO:">
					<textarea placeholder="CUERPO DE COMENTARIO:"></textarea>
					<p class="text-uppercase"><span style="color:white;font-size:17px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">enviar</span></p>
					<div style="border-bottom:dotted 1px;margin: 44px 0;color: #ababab;"></div>
				</div>
				<p class="text-uppercase"><span style="color:white;font-size:25px;background: #dc5656;padding: 3px 25px;font-family: 'robotolight';">comentarios</span></p>
				<div class="coninter" style="  padding: 15px 9%;">
					<div class="cuenta">
						<div class="imgper">
							<div class="circleimage" style="background:url('img/foto-comentario1.jpg') no-repeat center;background-size: cover;"></div>
						</div>
						<div class="comentper">
							<div class="descripcom">
								<p class="text-uppercase titular">el mejor lugar para ir en familia</p>
								<p style="padding-bottom: 8px;">15/01/2015<span class="text-uppercase"><a href="">Arequipa</a></span></p>
								<p>Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades y juegos.</p>
							</div>
						</div>
					</div>
					<div class="cuenta">
						<div class="imgper">
							<div class="circleimage" style="background:url('img/foto-comentario2.jpg') no-repeat center;background-size: cover;"></div>
						</div>
						<div class="comentper">
							<div class="descripcom">
								<p class="text-uppercase titular">las mejores olas de sudamerica</p>
								<p style="padding-bottom: 8px;">15/01/2015<span class="text-uppercase"><a href="">trujillo</a></span></p>
								<p>Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades y juegos.</p>
							</div>
						</div>
					</div>
					<div class="cuenta" style="border: none;">
						<div class="imgper">
							<div class="circleimage" style="background:url('img/foto-comentario3.jpg') no-repeat center;background-size: cover;"></div>
						</div>
						<div class="comentper">
							<div class="descripcom">
								<p class="text-uppercase titular">hoy probé la mejor comida del mundo</p>
								<p style="padding-bottom: 8px;">15/01/2015<span class="text-uppercase"><a href="">ayacucho</a></span></p>
								<p>Son características las «coplas de carnaval» y los disfraces de abundante colorido. La música y la alegría son el tenor de las festividades y juegos.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="assets/slick/slick.min.js"></script>
	<script type="text/javascript">
	    $(document).ready(function(){
	      $('.slideslick').slick({
			  infinite: true,
			  slidesToShow: 4,
			  slidesToScroll: 4,
			  prevArrow: ("<img src='img/slideleft.png' style='  position: absolute;top: calc(50% - 35px);left: -10px;cursor:pointer;'>"),
			  nextArrow: ("<img src='img/slideright.png' style='  position: absolute;top: calc(50% - 35px);right: -10px;cursor:pointer;'>")
			});
	    });
	</script>
	
@endsection
