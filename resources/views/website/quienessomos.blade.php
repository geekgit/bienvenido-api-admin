@extends('website/appsite')

@section('content')
<div id="myModalescala" class="modal fade" role="dialog">
                   <div class="modal-dialog">

                     <!-- Modal content-->
                     <div class="modal-content">
                       <div class="modal-header">
                         <button type="button" class="close" data-dismiss="modal">&times;</button>
                         <h4 class="modal-title">Editar</h4>
                       </div>
                       <div class="modal-body">
                           <div class="form-group">
                             <label for="nombred">Colaborador</label>
                             <input type="text" class="form-control" id="collaborator" >
                             <input type="hidden" id="keycoll">
                             <input type="hidden" id="typecoll">
                           </div>
                           <a  class="btn btn-success" onclick="editCollaborator()" >Guardar</a>
                       </div>
                     </div>

                   </div>
                 </div>
	<section class="qsomos">
		<div class="content">
			<div class="container">
				<div class="text-uppercase principal"><span>{{$page->name or 'page:name'}}</span></div>
				<div class="row"><div class="col-sm-12">
					@if(isset($multimedia['cover1']->source))
						<div class="profile">
							<div style="background-image: url('{{ URL::to('/')."/".$multimedia['cover1']->source_crop}}');"></div>
						</div>
					@else
						<div class="circle-primary">cover1</div>
					@endif
                    <br>
						@if($isAdmin)
							<button class="button-admin"
									onclick="cropImage('','{{isset($multimedia['cover1'])?$multimedia['cover1']->id:0 }}','quienes-somos','{{ isset($multimedia['cover1'])? asset($multimedia['cover1']->source):'' }}','{{ url('save-image-crop') }}','cover1',200,200,'{{ url('add-image-multimedia') }}',400)">
								<i class="icon-crop"></i> Editar imagen
							</button>
						@endif
                    <div class="name" id="title1" {{ $isAdmin?'code=title1 type=content contenteditable':'' }}>{!! $content["title1"]->content or '[title1]-quienessomos-contenidos:title'  !!}</div>
					<div class="editable" id="content1" {{ $isAdmin?'code=content1 type=content contenteditable':'' }}>{!! $content["content1"]->content or '[content1]-quienessomos-contenidos:contenido'  !!}</div>
				</div></div>
				<div class="row">
					<div class="col-sm-4">
						@if(isset($multimedia['cover2']->source))
							<div class="profile">
								<div style="background-image: url('{{ URL::to('/')."/".$multimedia['cover2']->source_crop}}');"></div>
							</div>
						@else
							<div class="circle-defualt">cover2</div>
						@endif
						<br>
							@if($isAdmin)
								<button class="button-admin"
										onclick="cropImage('','{{isset($multimedia['cover2'])?$multimedia['cover2']->id:0 }}','quienes-somos','{{ isset($multimedia['cover2'])? asset($multimedia['cover2']->source):'' }}','{{ url('save-image-crop') }}','cover2',200,200,'{{ url('add-image-multimedia') }}',400)">
									<i class="icon-crop"></i> Editar imagen
								</button>
							@endif
                    	<div class="name" id="title2" {{ $isAdmin?'code=title2 type=content contenteditable':'' }}>{!! $content["title2"]->content or '[title2]-quienessomos-contenidos:title'  !!}</div>
						<div class="editable"  id="content2" {{ $isAdmin?'code=content2 type=content contenteditable':'' }}>{!! $content["content2"]->content or '[content2]-quienessomos-contenidos:contenido'  !!}</div>
					</div>
					<div class="col-sm-4">
						@if(isset($multimedia['cover3']->source))
							<div class="profile">
								<div style="background-image: url('{{ URL::to('/')."/".$multimedia['cover3']->source_crop}}');"></div>
							</div>
						@else
							<div class="circle-defualt">cover3</div>
						@endif
						<br>
							@if($isAdmin)
								<button class="button-admin"
										onclick="cropImage('','{{isset($multimedia['cover3'])?$multimedia['cover3']->id:0 }}','quienes-somos','{{ isset($multimedia['cover3'])? asset($multimedia['cover3']->source):'' }}','{{ url('save-image-crop') }}','cover3',200,200,'{{ url('add-image-multimedia') }}',400)">
									<i class="icon-crop"></i> Editar imagen
								</button>
							@endif
						<div class="name" id="title3" {{ $isAdmin?'code=title3 type=content contenteditable':'' }}>{!! $content["title3"]->content or '[title3]-quienessomos-contenidos:title'  !!}</div>
    					<div class="editable"  id="content3" {{ $isAdmin?'code=content3 type=content contenteditable':'' }}>{!! $content["content3"]->content or '[content3]-quienessomos-contenidos:contenido'  !!}</div>
					</div>
					<div class="col-sm-4">
						@if(isset($multimedia['cover4']->source))
							<div class="profile">
								<div style="background-image: url('{{ URL::to('/')."/".$multimedia['cover4']->source_crop}}');"></div>
							</div>
						@else
							<div class="circle-defualt">cover4</div>
						@endif
						<br>
							@if($isAdmin)
								<button class="button-admin"
										onclick="cropImage('','{{isset($multimedia['cover4'])?$multimedia['cover4']->id:0 }}','quienes-somos','{{ isset($multimedia['cover4'])? asset($multimedia['cover4']->source):'' }}','{{ url('save-image-crop') }}','cover4',200,200,'{{ url('add-image-multimedia') }}',400)">
									<i class="icon-crop"></i> Editar imagen
								</button>
							@endif
                    	<div class="name" id="title4" {{ $isAdmin?'code=title4 type=content contenteditable':'' }}>{!! $content["title4"]->content or '[title4]-quienessomos-contenidos:title'  !!}</div>
						<div class="editable"  id="content4" {{ $isAdmin?'code=content4 type=content contenteditable':'' }}>{!! $content["content4"]->content or '[content4]-quienessomos-contenidos:contenido'  !!}</div>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-sm-6">
	                    <p class="titulo-lista" id="content5name" {{ $isAdmin?'code=content5 type=name contenteditable':'' }}>{{$content["content5"]->name or '[content5]-quienessomos-contenidos:nombre' }}</p>
						<div class="lista">
							<div class="row">
								@if(isset($content["collaborator1"]))
						        <div class="col-sm-6">
						            @foreach(json_decode($content["collaborator1"]->content) as $key=>$collaborator)
						                @if($key%2==0)
                                	        <li><span>{{$collaborator}}</span>
                                	           @if($isAdmin)
                                               <button class="button-admin" onclick="deleteCollaborator({{$key}},'collaborator1',this)" >	<i class="icon-trash-o"></i> </button>
                                               <button class="button-admin" onclick="modalCollaborator('{{$key}}','collaborator1',this)" >	<i class="icon-dots-three-vertical"></i> </button>
                                               @endif
                                             </li>
                                	    @endif
                                    @endforeach
						        </div>
						        <div class="col-sm-6">
						        @foreach(json_decode($content["collaborator1"]->content) as $key=>$collaborator)
						          @if($key%2==1)
                                	        <li><span>{{$collaborator}}</span>
                                            @if($isAdmin)
                                            <button class="button-admin" onclick="deleteCollaborator({{$key}},'collaborator1',this)" >	<i class="icon-trash-o"></i> </button>
                                            <button class="button-admin" onclick="modalCollaborator('{{$key}}','collaborator1',this)" >	<i class="icon-dots-three-vertical"></i> </button>
                                            @endif </li>
                                  @endif
                                  @endforeach
                                </div>
								@endif
							</div>
							@if($isAdmin)
								<br>
                               	<div class="block-admin">
                               		<input  type="text" class="input-small" placeholder="Colaborador">
                               		<button class="button-admin" onclick="saveColaboratorBefore(this,'collaborator1')">Crear nuevo</button>
                               	</div>
                            @endif
						</div>
					</div>
					<div class="col-sm-6">
						<p class="titulo-lista" id="content6name" {{ $isAdmin?'code=content6 type=name contenteditable':'' }}>{{$content["content6"]->name or '[content6]-quienessomos-contenidos:nombre' }}</p>
						<div class="lista">
						    <div class="row">
							@if(isset($content["collaborator2"]))
						        <div class="col-sm-6">
						            @foreach(json_decode($content["collaborator2"]->content) as $key=>$collaborator)
						                @if($key%2==0)
                               	        <li><span>{{$collaborator}}</span>
                                            @if($isAdmin)
                                            <button class="button-admin" onclick="deleteCollaborator({{$key}},'collaborator2',this)" >	<i class="icon-trash-o"></i> </button>
                                            <button class="button-admin" onclick="modalCollaborator('{{$key}}','collaborator2',this)" >	<i class="icon-dots-three-vertical"></i> </button>
                                            @endif </li>
                                	    @endif
                                    @endforeach
						        </div>
						        <div class="col-sm-6">
						        @foreach(json_decode($content["collaborator2"]->content) as $key=>$collaborator)
						          @if($key%2==1)
                               	        <li><span>{{$collaborator}}</span>
                                            @if($isAdmin)
                                            <button class="button-admin" onclick="deleteCollaborator({{$key}},'collaborator2',this)" >	<i class="icon-trash-o"></i> </button>
                                            <button class="button-admin" onclick="modalCollaborator('{{$key}}','collaborator2',this)" >	<i class="icon-dots-three-vertical"></i> </button>
                                            @endif </li>
                                  @endif
                                  @endforeach
                                </div>

                            @endif
                            </div>
							@if($isAdmin)
								<br>
	                         	<div class="block-admin">
	                         		<input  type="text" class="input-small" placeholder="Colaborador">
	                         		<button class="button-admin" onclick="saveColaboratorBefore(this,'collaborator2')">Crear nuevo</button>
	                         	</div>
                        	@endif
						</div>
					</div>
				</div>
				<div class="row"><div class="col-sm-12">
					<p class="titulo-lista" id="content7name" {{ $isAdmin?'code=content7 type=name contenteditable':'' }}>{{$content["content7"]->name or '[content7]-quienessomos-contenidos:nombre' }}</p>
					<div class="lista">
						<div class="row">
							@if(isset($content["collaborator3"]))
						        <div class="col-sm-6">
						            @foreach(json_decode($content["collaborator3"]->content) as $key=>$collaborator)
						                @if($key%2==0)
                               	        <li><span>{!!str_replace(" "," &nbsp",$collaborator)!!}</span>
                                            @if($isAdmin)
                                            <button class="button-admin" onclick="deleteCollaborator({{$key}},'collaborator3',this)" >	<i class="icon-trash-o"></i> </button>
                                            <button class="button-admin" onclick="modalCollaborator('{{$key}}','collaborator3',this)" >	<i class="icon-dots-three-vertical"></i> </button>
                                            @endif </li>
                                	    @endif
                                    @endforeach
						        </div>
						        <div class="col-sm-6">
						        @foreach(json_decode($content["collaborator3"]->content) as $key=>$collaborator)
						          @if($key%2==1)
                               	        <li><span>{!!str_replace(" "," &nbsp",$collaborator)!!}</span>
                                            @if($isAdmin)
                                            <button class="button-admin" onclick="deleteCollaborator({{$key}},'collaborator3',this)" >	<i class="icon-trash-o"></i> </button>
                                            <button class="button-admin" onclick="modalCollaborator('{{$key}}','collaborator3',this)" >	<i class="icon-dots-three-vertical"></i> </button>
                                            @endif </li>
                                  @endif
                                  @endforeach
                                </div>
                            @endif
						</div>
						@if($isAdmin)
							<br>
                         	<div class="block-admin">
                         		<input  type="text" class="input-small" placeholder="Colaborador">
                         		<button class="button-admin" onclick="saveColaboratorBefore(this,'collaborator3')">Crear nuevo</button>
                         	</div>
                        @endif
					</div>
				</div></div>
				<hr>
				<div class="row "  id="content8" >
					<div class="col-sm-12 editable" {{ $isAdmin?'code=content8 type=content contenteditable':'' }}>
						{!!$content["content8"]->content or '[content8]-quienessomos-contenidos:contenido' !!}
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
	@if($isAdmin)

	var col1=jQuery.parseJSON({!!(isset($content["collaborator1"]))?json_encode($content["collaborator1"]->content):json_encode('[]')!!});
	var col2=jQuery.parseJSON({!!(isset($content["collaborator2"]))?json_encode($content["collaborator2"]->content):json_encode('[]')!!});
	var col3=jQuery.parseJSON({!!(isset($content["collaborator3"]))?json_encode($content["collaborator3"]->content):json_encode('[]')!!});
	var dlt1=0;
	var dlt2=0;
	var dlt3=0;
	var input_coll;
	function saveColaboratorBefore(input,code)
	{
        var text=$(input).siblings('input').val();
         var collaborator;
         var count_del=0;
       switch(code) {
           case 'collaborator1':
               collaborator=col1;
               dlt1--;
               break;
           case 'collaborator2':
               collaborator=col2;
               dlt2--;
               break;
           case 'collaborator3':
               collaborator=col3;
               dlt3--;
               break;
       }
       collaborator[collaborator.length-dlt3]=text;
       var  content=JSON.stringify(collaborator);
       editcontentv2(code, content,'content',page_id);
      // $(input).parent().parent().append('  <li><span>'+text+'</span><button class="button-admin" onclick="deleteCollaborator('+collaborator.length+count_del-1 +',this)" >	<i class="icon-trash-o"></i> </button><button class="button-admin" onclick=modalCollaborator('+collaborator.length+count_del-1 +',"collaborator1",this) >	<i class="icon-dots-three-vertical"></i> </button>  </li>');
       // saveCollaborator(code,text,page_id);
	}

function modalCollaborator(key,type,input)
{
    var text;
   switch(type) {
       case 'collaborator1':
           text=col1[key];
           break;
       case 'collaborator2':
           text=col2[key];
           break;
       case 'collaborator3':
           text=col3[key];
           break;
   }
   input_coll=input;
   $('#collaborator').val(text);
   $('#keycoll').val(key);
   $('#typecoll').val(type);
   $('#myModalescala').modal('show');
}
function editCollaborator()
{
    var text=$('#collaborator').val();
        var collaborator;
        var count_del=0;
       switch($('#typecoll').val()) {
           case 'collaborator1':
               collaborator=col1;
               count_del=dlt1;
               break;
           case 'collaborator2':
               collaborator=col2;
               count_del=dlt2;
               break;
           case 'collaborator3':
               collaborator=col3;
               count_del=dlt3;
               break;
       }
          if(count_del>=0)
          {
          collaborator[$('#keycoll').val()-count_del]=text;
          }
          else{
          collaborator[$('#keycoll').val()]=text;
          }
        $(input_coll).siblings('span').text(text);
        var  content=JSON.stringify(collaborator);
        editcontentv2($('#typecoll').val(), content,'content',page_id);
}
function deleteCollaborator(key,type,input)
{
    var collaborator;
    var count_del;
   switch(type) {
       case 'collaborator1':
           collaborator=col1;
           count_del=dlt1;
           dlt1++;
           break;
       case 'collaborator2':
           collaborator=col2;
           count_del=dlt2;
           dlt2++;
           break;
       case 'collaborator3':
           collaborator=col3;
           count_del=dlt3;
           dlt3++;
           break;
   }
   if(count_del>=0)
   {
    collaborator.splice(key-count_del,1);
   }
   else
   {
    collaborator.splice(key,1);
   }
   $(input).parent('li').hide();
   var  content=JSON.stringify(collaborator);
   editcontentv2(type, content,'content',page_id);
}
@endif
	</script>
@endsection