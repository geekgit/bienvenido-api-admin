@extends('website/appsite')

@section('content')

	<section class="bank-photos">
		<div class="bank-search container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<input type="text" class="form-control" placeholder="Ingresa la palabra clave...">
					<button>Buscar</button>
				</div>
			</div>
		</div>
	</section>

	<section class="contbanco">
		<div class="content" style="border-bottom:solid 5px red;margin-bottom:60px;">
			<p class="text-uppercase">pronto encuentra y descarga la foto perfecta<br>para tu próximo proyecto</p>
			<div style="background: #f2f2f2;margin: 0 28px;padding-top:20px;">
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-cusco.png">
					<p class="text-uppercase">destinos</p>
				</div>
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-canotaje.png">
					<p class="text-uppercase">aventura</p>
				</div>
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-pintura.png">
					<p class="text-uppercase">cultura</p>
				</div>
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-pato.png">
					<p class="text-uppercase">naturaleza</p>
				</div>
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-pisco.png">
					<p class="text-uppercase">hecho en perú</p>
				</div>
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-baile.png">
					<p class="text-uppercase">fiestas</p>
				</div>
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-gente.png">
					<p class="text-uppercase">retratos</p>
				</div>
				<div class="banco-min">
					<img src="<?php echo URL::to('/'); ?>/img/banco-paisaje.png">
					<p class="text-uppercase">paises</p>
				</div>
			</div>
			<p style="margin:40px;">Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis, eum<br>liber hendrerit an. Quit ut wisi vocibus suscipiantur, quo dicit ridens.</p>
		</div>
	</section>

@endsection
