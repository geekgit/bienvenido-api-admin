<?php
$isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;
$links = App\Http\Middleware\WebSiteUtils::getSiteLinks();
$currentDesitny = 0;
$regiones = App\Http\Middleware\WebSiteUtils::getStaticRegiones(); $regpos2 = 0;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="theme-color" content="#EE1C25">
	<meta name="description"
		  content="@yield('description', 'Bienvenida es un portal para viajeros, un lugar donde encontrar la mejor información para soñar, planear, organizar y compartir las mejores experiencias')">
	<title>@yield('title', 'Perú Travel') :. Bienvenida</title>

	<link rel="icon" sizes="192x192" href="{{ asset('favicon.png') }}">
	<link rel="icon" type="image/png" href="{{ asset('favicon.png') }}"/>
	{{--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300|Quicksand:400,700' rel='stylesheet' type='text/css'>--}}
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
	{{--<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,300|Cinzel' rel='stylesheet' type='text/css'>--}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
	{{--	<link rel="stylesheet" type="text/css" href="{{ asset('css/styleicons.css') }}">--}}
	{{--<link rel="stylesheet" type="text/css" href="{{ asset('css/magnific-popup.css') }}">--}}
	<link rel="stylesheet" type="text/css" href="{{ asset('css/jPages.css') }}">
	{{--<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">--}}
{{--	<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.pwstabs-1.2.1.min.css') }}">--}}
	<link rel="stylesheet" type="text/css" href="{{ asset('output/utils.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('icons.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script src="{{ asset('js/jquery-1.12.1.min.js') }}"></script>
	<script src="{{ asset('output/instafeed.min.js') }}"></script>
	<script src="{{ asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('output/utils.js') }}"></script>
	{{--<script src="{{ asset('js/jquery.pwstabs-1.2.1.min.js') }}"></script>--}}
	{{--<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>--}}
	<script src="{{ asset('js/jPages.min.js') }}"></script>
	<script>var base_url = "{{url('/')}}/";</script>
	@if($isAdmin)
		<link rel="stylesheet" type="text/css" href="{{ asset('css/croppie.css') }}">
		<script src="{{ asset('js/croppie.min.js') }}"></script>
		<script src="{{ asset('js/admin_home.js') }}"></script>
	@endif
	<script src="{{ asset('js/all.js') }}"></script>
</head>
<body>
<div id="fb-root"></div>
<div class="overlay"></div>
@if($isAdmin)
	@include('partials.modals_admin')
@endif
<nav>
	<div class="container">
		<div class="row">
			<div class="col-sm-3 col-xs-5">
				<a href="{{url('/')}}"><img src="{{ asset('img/logo.png') }}" class="logo" alt="Bienvenida Perú"></a>
			</div>

			<div class="col-sm-9 col-xs-7">
				<div class="dropdown pull-right visible-sm visible-xs">
					<button class="button" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Menú <span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dLabel">
						<li><a>DESTINOS <span class="caret"></span></a>
							<ul class="sub-drop">
								@foreach ($links['destinies'] as $reg)
									<li><a href="{{  url('/destino/' . $reg->code) }}">{{ $reg->name }}</a></li>
								@endforeach
							</ul>
						</li>
						<li><a href="{{url('sobre-peru')}}">SOBRE PERÚ</a></li>
						@foreach($links['categories'] as $c)
						<li><a href="{{ url('categoria/'.$c->code) }}">{{$c->name}}</a> </li>
						@endforeach
					</ul>
				</div>
				<ul class="hidden-sm hidden-xs subnav">
					@if($isAdmin)
						<li>
							<button class="button-admin" data-toggle="modal" data-target="#editPageModal"><i
										class="icon-open-book"></i> Editar página
							</button>
						</li>
					@endif
					<li><a href="{{ url('revistas') }}">REVISTAS</a></li>
					<li>-</li>
					<li><a href="{{ url('banco-fotos') }}">BANCO DE FOTOS</a></li>
					<li>-</li>
					<li><a href="{{ url('quienes-somos') }}">QUIENES SOMOS</a></li>
					<li>-</li>
					<li class="active">
						@if(auth()->check())
							<span class="">Hola {{ auth()->user()->name }}</span>
							| <a href="{{ url('/perfil') }}">Mi cuenta</a>
							| <a href="{{ url('auth/logout') }}">Cerrar sesión</a>
						@else
							<a href="#" onmouseover="showLoginForm()">INICIAR SESIÓN</a>
							<div id="login-form">
								<button class="pull-right" onclick="hideLoginForm()"><i class="icon-close"></i></button>
								<h3>Bienvenido</h3>
								<form method="POST" action="{{ url('auth/login') }}">
									<div class="form-group">
										<label for="login-usuario">Usuario:</label>
										<input type="text" name="email" id="login-usuario" class="form-control"/>
									</div>
									<div class="form-group">
										<label for="login-password">Contraseña:</label>
										<input type="password" name="password" id="login-password" class="form-control">
									</div>
									<div class="form-group">
										<a href="#" class="">¿Olvidaste tu contraseña?</a>
										<button type="submit" class="button">Ingresar</button>
									</div>
									<div class="form-group">
										<span>¿Eres nuevo?</span>
										<span><a href="#register-popup" class="account-create">Crea tu cuenta</a></span>
									</div>
								</form>
							</div>
						@endif

					</li>
					{{--<li class="idioma">
						<form action="{{url('idioma')}}" method="post">
							<select id="locale" name="locale"
									onchange="language(this.options[this.selectedIndex].value)">
								<option value="es" {{(\Session::get('my.locale') == 'es')? 'selected': ' '}}>Spanish
								</option>
								<option value="en" {{(\Session::get('my.locale') == 'en')? 'selected': ' '}} >English
								</option>
							</select>
						</form>
					</li>--}}
				</ul>
				<!-- End sub nav -->
				<ul class="hidden-xs hidden-sm generalnav">
					<li class="destinoac"><a href="#">DESTINOS <i class="icon-chevron-down"></i></a>
						<div class="hovdestino">
							<div class="row">
								<div class="col-sm-7 left-block">
									@if($isAdmin)
										<div class="pull-right">
											<form id="formNewDestiny" action="" method="post" class="inline-form">
												<input id="destinytext" type="text" placeholder="Nombre de destino"
													   class="input-small">
												<button type="submit" class="button-admin">Nuevo</button>
											</form>
										</div>
									@endif
									<p class="title">REGIONES</p>
									<ul>
										@for (; $currentDesitny< count($links['destinies'])  && $currentDesitny< 8; $currentDesitny++)
											<li>
												<a href="{{  url('/destino/')."/".$links['destinies'][$currentDesitny]->code }}">{{ $links['destinies'][$currentDesitny]->name }}</a>
												@if($isAdmin)
													<span class="icon-trash"
														  onclick="eliminarpage({{ $links['destinies'][$currentDesitny]->id }}, '{{ url('contentdeletepage') }}')"></span>
												@endif
											</li>
										@endfor

									</ul>
									<ul>
										@for ( ; $currentDesitny< count($links['destinies'])  && $currentDesitny< 16; $currentDesitny++)
											<li>
												<a href="{{  url('/destino/')."/".$links['destinies'][$currentDesitny]->code }}">{{ $links['destinies'][$currentDesitny]->name }}</a>
												@if($isAdmin)
													<span class="icon-trash"
														  onclick="eliminarpage({{ $links['destinies'][$currentDesitny]->id }}, '{{ url('contentdeletepage') }}')"></span>
												@endif
											</li>

										@endfor
									</ul>
									<ul class="last">
										@for ( ; $currentDesitny< count($links['destinies'])  && $currentDesitny< 32; $currentDesitny++)
											<li>
												<a href="{{  url('/destino/')."/".$links['destinies'][$currentDesitny]->code }}">{{ $links['destinies'][$currentDesitny]->name }}</a>
												@if($isAdmin)
													<span class="icon-trash"
														  onclick="eliminarpage({{ $links['destinies'][$currentDesitny]->id }}, '{{ url('contentdeletepage') }}')"></span>
												@endif
											</li>

										@endfor
									</ul>
								</div>
								<div class="col-sm-5 rigth-block">
									<p class="title">DESTINOS ESPECIALES</p>
									<ul class="list-specials">
										@foreach($regiones as $region)
											<li>
												<a href="{{  url('/region/')."/".$region->code }}">
													@if(count($region->multimedia)>0)
														<img src="{{  url('/')."/".$region->getIcon() }}"
															 style="width: 81px;">
													@endif
													<p>{{ $region->name }}</p>
												</a>
											</li>
										@endforeach
									</ul>
								</div>
							</div>
						</div>
					</li>
					<li><a href="{{url('sobre-peru')}}">SOBRE PERÚ</a></li>
					@foreach($links['categories'] as $c)
					<li><a href="{{ url('categoria/'.$c->code) }}">{{$c->name}}</a> </li>
					@endforeach
					<li class="search-box">
						<input id="inputsearch" type="text" placeholder="Buscar" onkeypress="keypressenter(event)">
						<button class="button" onclick="searchResult()"><i class="icon-search"></i></button>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>
<!-- End header menu -->

	@yield('content')

	<section class="footer-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<ul>
						<li class="listitle text-uppercase">Destinos</li>
						@foreach($links['destinies'] as $d)
							<li><a href="{{ url('destino/' . $d->code) }}">{{$d->name }}</a></li>
						@endforeach
					</ul>
					<ul>
						<li><a href="{{ url('sobre-peru') }}">SOBRE PERÚ</a></li>
						<li><a href="{{ url('sobre-peru#ubicacion') }}">Ubicación</a></li>
						<li><a href="{{ url('sobre-peru#clima') }}">Clima</a></li>
						<li><a href="{{ url('sobre-peru#moneda') }}">Moneda</a></li>
						<li><a href="{{ url('sobre-peru#idioma') }}">Idioma</a></li>
						<li><a href="{{ url('sobre-peru#religion') }}">Religión</a></li>
						<li><a href="{{ url('sobre-peru#temporada') }}">Temporada alta</a></li>
					</ul>
					@foreach($links['categories'] as $c)
					<ul>
						<li><a href="{{ url('categoria/'.$c->code) }}">{{$c->name}}</a> </li>
					</ul>
					@endforeach
				</div>
				<div class="col-md-4 footer-social">
					<p class="fb-like-box" data-href="https://www.facebook.com/bienvenidaperu" ></p>
					<p>
						<a class="button-icon" href="https://www.facebook.com/bienvenidaperu/" target="_blank"><i class="icon-facebook-f"></i></a>
						<a class="button-icon" href="#"><i class="icon-twitter"></i></a>
						<a class="button-icon" href="https://www.instagram.com/bienvenidaperu/"><i class="icon-instagram2"></i></a>
					</p>
				</div>
			</div><!-- End row -->
		</div>
	</section>

	<section class="footer-info">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="parta">
						<p class="title">Acerca de Bienvenida</p>
						<p>Son características las "coplas de carnaval" y los disfraces de abundante colorido. La música
							y la alegría son el tenor de las festividades, además los juegos con pistolas de agua,
							globos de agua. Un elemento característico del carnaval de verano también lo constituyen la chincha
							alta y baja fin de tempoarada.</p>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="text-right">
						<p>Av. 28 de Julio 789, Miraflores, Lima. <span class="icon-map"></span></p>
						<p>(+51) 01 565-8577</p>
						<p><a href="mailto:contacto@bienvenida.pe">contacto@bienvenida.pe</a></p>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer>
		<div class="text-center">
			Copyright © 2015 | <span class="text-uppercase">todos los derechos reservados 2014 - Bienvenida / privacy</span>
		</div>
	</footer>

	@if(!auth()->check())
	<div id="register-popup" class="white-popup">
		<div class="register-popup">
			<div class="register-content">
				<div class="text-center">
					<h3>REGISTRATE</h3>
					<p>y forma parte de <span class="text-danger">Bienvenida</span></p>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<form id="form-register" method="POST" action="{{ url('auth/create-account') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="form-group">
								<input type="text" placeholder="Nombres" class="form-control" name="name"/>
							</div>
							<div class="form-group">
								<input type="text" placeholder="Apellidos" class="form-control" name="last_name"/>
							</div>
							<div class="form-group">
								<input type="email" placeholder="E - mail" class="form-control" name="email"/>
							</div>
							<div class="form-group">
								<input type="password" placeholder="Contraseña" class="form-control" name="password"/>
							</div>
							{{--<label for="country">
								<select name="country">
									<option value="1">Perú</option>
								</select>
							</label>--}}
							<div>
								<button type="submit" class="pull-right">Registrate</button>
								<p>¿Ya tienes una cuenta? <br> <a href="#login" onclick="showLoginForm()">Inicia
										sesión</a></p>
							</div>
						</form>
						<div class="register-bottom">
							<a onclick="apply()" class="fb-button"><i class="icon-social-facebook"></i> Registrarme con Facebook</a>
						</div>
					</div>
					<div class="col-md-6 register-info hidden-xs">
						<hr class="visible-xs">
						<h5>Forma parte de la aventura de conocer Perú</h5>
						<p>
							<span class="label label-danger">Comenta</span> : Danos tu opinión sobre los diferentes
							destinos que ofrece el Perú, recomienda otros
							lugares o comparte tu experiencia. Otros viajeros te lo agradecerán.</p>
						<p>
							<span class="label label-danger">Favoritos</span> : Marca un sitio como tu favorito. Desde
							tu perfil podrás acceder rapidamente a el.
						</p>
						<p><span class="label label-danger">Crear ruta</span> : Elige los sitios que deseas visitar y
							crea tu propia ruta. Compartela con tus
							amigos o imprímela.</p>

						<p><span class="label label-danger">Viaja</span> : Eso, viaja, conoce, descubre y completa los
							destinos recomendados en Bienvenida.
							¿Llegarás a conocer todos los destinos en Perú?</p>
					</div>
				</div>
			</div>
		</div>
	</div><!-- End register form -->
	@endif

	@yield('scripts')
</body>
<script>
	$(document).ready(initAppHeader);
    var page_id;
	function initAppHeader() {
		//Eventos
		$('#form-register').submit(createNewAccount);

		loadInstagramImages();

		@if($isAdmin)
		$("h1").on('click', function () {
			$(this).focus()
		});
		var dragdinamyc;
		sortabledrag('draglistdynamic', dragdinamyc);

		page_id='{{$page->id}}';
		@endif

	}


	$("#saveOrderDragList").click(function () {
		var id = $("#draglistdynamic").attr('title');
		var arry = [];
		var ct = 0;
		$("#draglistdynamic > li").each(function (index) {
			arry[ct] = [{id: id, id_child: $(this).attr('title')}];
			ct++;
		});
		saveOrderLIst(arry, '{{ url("save-order-list") }}');
	});

	function loadInstagramImages() {
		var playfeed = new Instafeed({
			get: 'user',
			target: 'playfeed',
			resolution: 'standard_resolution',
			tagName: 'viajerosperu',
			clientId: '4b263c48f3b24cf590bcaf93b0d85662',
			userId: '2099163136',
            accessToken: '2099163136.4b263c4.c761703a3b24424da5b78d9506a91e42',
			limit: 6,
			links: true,
			template: '<li style="background-image:url(@{{image}})"><a href="@{{link}}"><div class="title"><img src="{{ asset('img/socialinsta.png') }}" class="icon"> @{{location}}</div></a></li>'
		});
		playfeed.run();
	}

	function apply() {
		FB.login(function (response) {
			console.log(response);
		}, {scope: 'email,public_profile'});

		FB.api('/me?fields=id,name,email', function (response) {
			console.log('Successful login for: ' + response.name);
			console.log(response);
			$.ajax({
				url: '<?php echo url('auth/create-account')?>',
				type: "post",
				data: {
					email: response.id + '@hotmail.com',
					password: response.id,
					name: response.name.split(' ')[0],
					fb: 'facebook'
				},
				success: function (data) {
					if (data.status == '200') {
						window.location.reload();
					}
					else {
						swal('', data.message, 'error');
					}
				}
			}, "json");
		});
	}

	window.fbAsyncInit = function () {
		FB.init({appId: '1700790606837968', xfbml: true, version: 'v2.4'});
	};

	(function (d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	function language(lang) {
		console.log(lang);
		$.ajax({
			url: '{{ url('idioma') }}',
			type: "post",
			data: {lenguaje: lang},
			success: function (data) {
				window.location.reload();
			}
		}, "json");
	}

	function createNewAccount(evt) {
		evt.preventDefault();

		var data = new FormData(this);
		console.log(data);
		$('#myPleaseWait').modal('show');
		$.ajax({
			url: '<?php echo url('auth/create-account')?>',
			processData: false,
			contentType: false,
			type: "post",
			data: data,
			success: function (_data) {
				console.log(_data);
				$('#myPleaseWait').modal('hide');
				/*if (data.status == '200')
				 {
				 console.log(_data);
				 //window.location.reload();
				 }
				 else
				 {
				 swal('', data.message, 'error');
				 }*/
			}
		}, "json");
	}

	function searchResult() {
		var text = $('#inputsearch').val();
		if (text.length > 0) {
			window.location.href = "{{ url('/busqueda')."/"  }}" + text;
		}
	}

	function keypressenter(e) {
		if (e.keyCode == 13) {
			searchResult();
		}
	}
</script>

</html>
