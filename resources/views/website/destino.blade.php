@extends('website/appsite')

@if(count($page->description) > 5)
	@section('description') {{ $page->description}} @endsection
@endif

@section('title') {{ $page->name }} @endsection

@section('content')


	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				@if(isset($multimedia['front']))
					<button class="button-admin"
							onclick="cropImage('destino','{{ $multimedia['front']->id }}','{{ $page->code }}','{{ url($multimedia['front']->source) }}','{{ url('save-image-crop') }}','front',600,200,'{{ url('add-image-multimedia') }}',1400)">
						<i class="icon-crop"></i> Editar imagen
					</button>
				@endif
			</div>
		@endif
		<div class="swiper-container" style="background-image: url({{ isset($multimedia['front'])?asset($multimedia['front']->source_crop):'' }});">
			@include('partials.social_buttons')
			<div class="slider-caption">
				<h3 class="slogan"
					id="frasedestino" {{ $isAdmin?'code=frase type=content contenteditable':'' }}> {{ $contents["frase"]->content  or 'destino-contenidos:contenido' }} </h3>
				<h1 class="name">{{ $page->name or  'page:name' }}</h1>
			</div>

			<button id="favorite-star" onclick="favorito('{{$page->id}}', this, '{{ url('save-favorite') }}')"
					class="favorito {{ ($favorite == 1)?'added':'' }}">
				<i class="icon-star"></i>
			</button>
		</div>
		<ul class="list-destiny-menu">
			<li><a href="{{ url('/destino')."/".$page->code  }}" class="active">{{ $page->name }}</a></li>
			<li><a href="{{ url('/como-llegar')."/".$page->code }}">¿cómo llegar?</a></li>
			<li><a href="{{ url('/aventura').'/'.$page->code }}">aventura</a></li>
			<li><a href="{{ url('/naturaleza')."/".$page->code }}"> naturaleza</a></li>
			<li><a href="{{ url('/cultura')."/".$page->code }}">cultura</a></li>
			<li><a href="{{ url('/descanso')."/".$page->code }}">descanso</a></li>
		</ul>
	</section>

	<section class="bread-crumb">
		<ol class="breadcrumb container">
			<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
			<li class="active">{{$page->name}}</li>
		</ol>
	</section>

	<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="editable" id="welcome" {{ $isAdmin?'code=welcome type=content contenteditable':'' }}> {!! $contents["welcome"]->content or '[welcome]:contenido' !!} </div>
				</div>
			</div>
		</div>
	</section>

	<section class="destiny-history container">
		<div id="resenia-region" class="block-history container">
			<div class="row">
				<div class="col-md-8">
					<h2 class="title "
						id="text1name" {{ $isAdmin?'code=text1 type=name contenteditable':'' }}>{{ $contents["text1"]->name  or '[text1] Titulo historia' }}</h2>
					<div class="editable" id="text1content" {{ $isAdmin?'code=text1 type=content contenteditable':'' }}>
						{!! $contents["text1"]->content or '[text1] Contenido historia' !!}
					</div>
					<hr class="visible-sm visible-xs">
				</div>
				<div class="col-md-4">
					<div class="details">
						<h5 class="sub-title " id="olvidarname" {{ $isAdmin?'code=olvidar type=name contenteditable':'' }}>
							{{ $contents["olvidar"]->name  or '[olvidar] - Para no olvidar' }}
						</h5>
						<div class="editable" id="olvidarcontent" {{ $isAdmin?'code=olvidar type=content contenteditable':'' }}>
							{!! $contents["olvidar"]->content  or '[olvidar] contenido' !!}
						</div>
						<hr>
						<h5 class="sub-title " id="curiosidadname" {{ $isAdmin?'code=curiosidad type=name contenteditable':'' }}>
							{{ $contents["curiosidad"]->name  or '[curiosidad] Curiosidad' }}
						</h5>
						<div class="editable" id="curiosidadcontent" {{ $isAdmin?'code=curiosidad type=content contenteditable':'' }}>
							{!! $contents["curiosidad"]->content  or '[curiosidad]-destino-contenidos:contenido' !!}
						</div>
						<p class="text-center">
							@if(isset($multimedia["minimapa"]))
								<img src="<?php echo url('/'); ?>/{{ $multimedia["minimapa"]->source }}"
									 class="img-responsive">
							@else
								<img src="{{ asset('img/default.jpg') }}" alt="" class="img-responsive">
							@endif
						</p>
						<input type='file' id="imgminmap"/>
					</div>
				</div>
			</div><!-- End row-->
			<div class="history-legend">
				<div class="row">
					<div class="col-sm-3">
						<img src="{{ asset('img/visita.png') }}">
						<div>
							<span class="value " id="visitascontent" {{ $isAdmin?'code=visitas type=content contenteditable':'' }}>
								{{ $contents["visitas"]->content  or '[visitas] Dato' }}
							</span><br>
							<span id="visitasname" {{ $isAdmin?'code=visitas type=name contenteditable':'' }}>>
								{{ $contents["visitas"]->name  or '[visitas] Label' }}
							</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/grupo.png') }}">
						<div>
						<span class="value" id="poblacioncontent" {{ $isAdmin?'code=poblacion type=content contenteditable':'' }}>
							{{ $contents["poblacion"]->content  or '[poblacion] Dato' }}
						</span><br>
						<span id="poblacionname" {{ $isAdmin?'code=poblacion type=name contenteditable':'' }}>>
							{{ $contents["poblacion"]->name  or '[poblacion] Label' }}
						</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/mapaicon.png') }}">
						<div>
						<span class="value" id="extencioncontent" {{ $isAdmin?'code=extencion type=content contenteditable':'' }}>
							{{ $contents["extencion"]->content  or '[extencion] Dato' }}
						</span><br>
						<span id="extencionname" {{ $isAdmin?'code=extencion type=name contenteditable':'' }}>>
							{{ $contents["extencion"]->name  or '[extencion] Label' }}
						</span>
						</div>
					</div>
					<div class="col-sm-3">
						<img src="{{ asset('img/dinero.png') }}">
						<div>
						<span class="value" id="costocontent" {{ $isAdmin?'code=costo type=content contenteditable':'' }}>
							{{ $contents["costo"]->content  or '[costo] Dato' }}
						</span><br>
						<span id="costoname" {{ $isAdmin?'code=costo type=name contenteditable':'' }}>
							{{ $contents["costo"]->name  or '[costo] Label' }}
						</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="text-center">
			<a href="" class="vermas" id="imgdestino">
				VER MÁS <i class="icon-arrow-down"></i>
			</a>
		</div>
	</section>

	<section class="imperdibles">
		<div class="container">
			<h2 class="title-content">
				<span  id="text2name" {{ $isAdmin?'code=text2 type=name contenteditable':'' }}>{{ $contents["text2"]->name  or '[text2]-destino-contenidos:nombre' }}</span>
			</h2>
			<div class="editable" id="text2content" {{ $isAdmin?'code=text2 type=content contenteditable':'' }}>{!!$contents["text2"]->content or '[text2]-destino-contenidos:contenido' !!} </div>

			@if($isAdmin)
			<div class="block-admin">
				<input id="subdestinytext" type="text" class="input-small" placeholder="SUB-DESTINO">
				<button class="button-admin" id="subdestinynew">Crear subdestino</button>
				|
				<button class="button-admin" onclick="openpopuporder('dragListsub_destino','{{$page->id}}')">
					Editar posiciones
				</button>
			</div>
			@endif
			<ul id="dragListsub_destino" class=" list-thumbnail">
				@foreach($page->childs()->places()->get() as $place)
					<li data-id="{{$place->id}}">
						@if($isAdmin)
							<button class="button-admin" onclick="eliminarpage('{{ $place->id }}', '<?php echo url('content-delete-page')?>')"> Eliminar</button>
							<button class="button-admin" onclick="cropImage('subdestino','{{ $place->getImageCropId('cover') }}','{{ $place->code }}','{{ asset($place->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)"> <i class="icon-crop"></i> Editar imagen </button>
						@endif
						<a href="{{ url('/sub-destino')."/".$place->code}}">
							<div class="title small"><span>{{$place->name}}</span></div>
							<input type="hidden" value="{{$place->code}}"/>
							<div class="pic" style="background-image: url('{{ asset($place->getImageCropImg('cover')) }}');"></div>
						</a>
					</li>
				@endforeach
			</ul>

			<!-- mapa -->

			<div class="text-left">
				<p>
					<button class="button" onclick="resetPositionMap()"><i class="icon-rotate-left"></i> Regresar a ubicación original</button>
					@if($isAdmin)
						<button class="button-admin pull-right" onclick="saveMapPosition('{{$page->id}}','<?php echo url('save-point')?>')">
							Guardar posición
						</button>
					@endif
				</p>
				<div id="map-canvas" class="embed-responsive-16by9"></div>
			</div>
		</div>
	</section>

	<section class="testimonios">
		<div class="container">
			@if(count($frasedestacada) > 0)
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<div class="profile pull-left">
							<div style="background-image: url('{{ asset($frasedestacada[0]->getImageCropImg('cover')) }}');"></div>
						</div>
						<div class="text-left cont-tes">
							<p class="name-tes " id="frasename1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=name contenteditable':'' }}>
								<span>{{ $frasecontent[0]["text1"]->name  or '[text1]-Persona:nombre' }}</span></p>
							<p class="des-tes "
							   id="frasetext1" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text1 type=content contenteditable':'' }}>{!! $frasecontent[0]["text1"]->content  or '[text1]-Bio:contenido' !!}</p>
							<div  id="frasetext2" {{ $isAdmin?'page-id='.$frasedestacada[0]->id.' code=text2 type=content contenteditable':'' }}>{!! $frasecontent[0]["text2"]->content  or '[text2]-Frase:contenido' !!}</div>
							@if($isAdmin && isset($frasedestacada[0]))
								<button class="button-admin"
										onclick="cropImage('frasedestacada','{{ $frasedestacada[0]->getImageCropId('cover')  }}','{{ $frasedestacada[0]->code }}','{{url($frasedestacada[0]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>
								<br> @endif
						</div>
					</div>
				</div>
			@else
				@if($isAdmin)
					<div class="block-admin text-center">
						<input id="frasedestacadatext" type="hidden" value="testimonio-{{ $page->code }}">
						<h4>No hay un testimonio en esta página</h4>
						<p class="info-admin">Haz clic en el botón para crear uno</p>
						<button id="frasedestacadanew" class="button-admin"><i class="icon-plus"></i> Crear testimonio
						</button>
					</div>
				@endif
			@endif
		</div>
	</section>

	<section class="hperu">
		<div class="container">
			<h2 class="title-content">
				<span  id="text3name" {{ $isAdmin?'code=text3 type=name contenteditable':'' }}>{{ $contents["text3"]->name  or '[text3]-destino-contenidos:nombre' }}</span>
			</h2>
			<div class="editable" id="text3content" {{ $isAdmin?'code=text3 type=content contenteditable':'' }}>{!! $contents["text3"]->content or '[text3]-destino-contenidos:contenido' !!} </div>
			@if($isAdmin)
				<div class="block-admin">
					<input id="hechoentext" type="text" class="input-small" placeholder="hecho en(grupo)">
					<button class="button-admin" id="hechoennew">Crear nuevo</button>
					|
					<button class="button-admin" onclick="openpopuporder('hecho_grupoorder','{{$page->id}}')">Editar
						posiciones
					</button>
				</div>
			@endif
			<ul id="hecho_grupoorder" class="list-thumbnail">
				@foreach ( $page->childs()->madeIn()->get() as $hecho)
					<li data-id="{{$hecho->id }}">
						@if($isAdmin)
							<button class="button-admin" onclick="eliminarpage('{{$hecho->id}}', '<?php echo url('content-delete-page')?>')">
								Eliminar
							</button>
							<button class="button-admin" onclick="cropImage('hecho_engrupo','{{ $hecho->getImageCropId('cover') }}','{{ $hecho->code }}','{{ asset($hecho->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
								<i class="icon-crop"></i> Editar imagen
							</button>
						@endif
						<a href="{{ url('/')."/hechoen/".$hecho->code }}">
							<div class="title"><span>{{$hecho->name}}</span></div>
							<input type="hidden" value="{{$hecho->code}}"/>
							<div class="pic"
								 style="background-image: url('{{ asset($hecho->getImageCropImg('cover')) }}');"></div>
						</a>
					</li>
				@endforeach
			</ul>

			<div class="text-center">
				<a href="#">
					Ver todos <img src="{{ asset('/img/vermas.png') }}" >
				</a>
			</div>
		</div>
	</section>

	<section class="planea">
		<div class="container">
			<h2 class="title-content text-center">
				<span>Planea tu viaje</span>
			</h2>
			@if($isAdmin)
				<div class="block-admin">
					<input id="itinerariotext" type="text" class="input-small" placeholder="Itinerario">
					<button class="button-admin" id="itinerarionew">Crear nuevo</button>
				</div>
			@endif
			<div class="row">
				<?php for($a = 0;$a < count($itinerarios);$a++){ ?>
				@if($a == 0)
				<div class="col-md-6">
					<a href="<?php echo url('itinerario/' . $page->code . '/' . $itinerarios[$a]->code) ?>" class="big-picture">
						<div style="background-image: url({{ asset($itinerarios[$a]->getImageCropImg('cover')) }});" class="pic"></div>
						<div class="title">
							<img src="<?php echo url($itinerarios[$a]->getImageCropImg('icon')) ?>" style="width: 50px" class="pull-right">
							<h3> {{ $itinerarios[$a]->name }} </h3>
							<p> {{ $itinerarios[$a]->description }} </p>
						</div>
					</a>
					@if($isAdmin)
					<p>
						<button class="button-admin"
								onclick="eliminarpage('{{$itinerarios[$a]->id}}', '<?php echo url('content-delete-page')?>')">
							Eliminar
						</button>
						<button class="button-admin"
								onclick="cropImage('itinerario','{{ $itinerarios[$a]->getImageCropId('cover')  }}','{{ $itinerarios[$a]->code }}','{{url($itinerarios[$a]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
							<i class="icon-crop"></i> Editar imagen
						</button>
						<button class="button-admin"
								onclick="cropImage('itinerario','{{ $itinerarios[$a]->getImageCropId('icon')  }}','{{ $itinerarios[$a]->code }}','{{url($itinerarios[$a]->getCover('icon')) }}','{{ url('save-image-crop') }}','icon',200,200,'{{ url('add-image-multimedia') }}',550)">
							<i class="icon-crop"></i> Editar icono
						</button>
					</p>
					@endif
				</div>
				@endif

				<div class="col-md-6">
					<?php if($a == 1){ ?>
					<div class="small-box">
						<img src="<?php echo url($itinerarios[$a]->getImageCropImg('icon')) ?>" style="width: 30px">
						<a href="<?php echo url('itinerario/' . $page->code . '/' . $itinerarios[$a]->code) ?>">
							<div class="imgplaneasmall" style="background: url('<?php echo url($itinerarios[$a]->getImageCropImg('cover')) ?>')no-repeat center;background-size: cover;"></div>
						</a>
						<div class="desc">
							@if($isAdmin)
								<p>
									<button class="button-admin"
											onclick="eliminarpage({{$itinerarios[$a]->id}}, '<?php echo url('content-delete-page')?>')">
										Eliminar
									</button>
									<button class="button-admin"
											onclick="cropImage('itinerario','{{ $itinerarios[$a]->getImageCropId('cover')  }}','{{ $itinerarios[$a]->code }}','{{url($itinerarios[$a]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
										<i class="icon-crop"></i> Editar imagen
									</button>
									<button class="button-admin"
											onclick="cropImage('itinerario','{{ $itinerarios[$a]->getImageCropId('icon')  }}','{{ $itinerarios[$a]->code }}','{{url($itinerarios[$a]->getCover('icon')) }}','{{ url('save-image-crop') }}','icon',200,200,'{{ url('add-image-multimedia') }}',550)">
										<i class="icon-crop"></i> Editar icono
									</button>
								</p>
							@endif
							<p class="text-uppercase"><?php echo $itinerarios[$a]->name ?></p>
						</div>
					</div>
					<?php } if($a == 2){ ?>
					<div class="small-box">
						<img src="<?php echo url($itinerarios[$a]->getImageCropImg('icon')) ?>" style="width: 30px">
						<a href="<?php echo url('itinerario/' . $page->code . '/' . $itinerarios[$a]->code) ?>">
							<div class="imgplaneasmall"
								 style="background: url('<?php echo url($itinerarios[$a]->getImageCropImg('cover')) ?>')no-repeat center;background-size: cover;"></div>
						</a>
						<div class="desc">
							@if($isAdmin)
								<p>
									<button class="button-admin"
											onclick="eliminarpage({{$itinerarios[$a]->id}}, '<?php echo url('content-delete-page')?>')">
										Eliminar
									</button>
									<button class="button-admin"
											onclick="cropImage('itinerario','{{ $itinerarios[$a]->getImageCropId('cover')  }}','{{ $itinerarios[$a]->code }}','{{url($itinerarios[$a]->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
										<i class="icon-crop"></i> Editar imagen
									</button>
									<button class="button-admin"
											onclick="cropImage('itinerario','{{ $itinerarios[$a]->getImageCropId('icon')  }}','{{ $itinerarios[$a]->code }}','{{url($itinerarios[$a]->getCover('icon')) }}','{{ url('save-image-crop') }}','icon',200,200,'{{ url('add-image-multimedia') }}',550)">
										<i class="icon-crop"></i> Editar icono
									</button>
								</p>
							@endif

							<p class="text-uppercase"><?php echo $itinerarios[$a]->name ?></p>
						</div>
					</div>
					<?php } if($a == 0){ ?>
					<br><br>
					<?php } ?>

					<div class="large-box">
						<div class="imgplanealarge"
							 style="background: url('<?php echo url('/'); ?>/img/planealargeimage.jpg')no-repeat center;background-size: cover;">
							<ul style="list-style: none;">
								<?php for($b = 0;$b < count($imperdibles);$b++){ ?>
								<?php $childs = $imperdibles[$b]->childs()->get(); $url = ''; ?>
								<?php if (count($childs) > 0) {
									$url = url('/') . '/parallax/' . $childs[0]->code;
								}; ?>
								<li><a href=" <?php echo $url ?>"
									   style="text-decoration:none;color: #de4a4a;background-color: rgba(0,0,0,0.2);"><?php echo $imperdibles[$b]->name; ?></a>
								</li>
								<?php }  ?>

							</ul>
						</div>
						<div class="desc">
							<p><span class="text-uppercase" style="color: #de4a4a;">5 lugares</span> imperdibles</p>
						</div>
					</div>
				</div>
				<?php } ?>
			</div><!-- End row -->
		</div><!-- En container -->
	</section>

	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$page,'number'=>0])


	<!-- Fiestas -->
	<section class="fiestas">
		<div class="container">
			<h2 class="title-content">
				<span  id="text4name" {{ $isAdmin?'code=text4 type=name contenteditable':'' }}>{{ $contents["text4"]->name or 'text3:nombre' }}</span>
			</h2>
			<div class="editable" id="text4content" {{ $isAdmin?'code=text4 type=content contenteditable':'' }}>{!! $contents["text4"]->content or 'text3:contenido' !!}</div>
			@if($isAdmin)
				<div class="block-admin">
					Para crear nuevas festividades o reordenar su posición es necesario hacerlo desde el luegar al que
					pertenecen
				</div>
			@endif

			<ul id="moveListEvento" class="list-thumbnail detail">
				@foreach($fiestas as $fiestasPlace)
					@foreach($fiestasPlace as $fiesta)
						<li data-id="{{$fiesta->id}}">
							<a href="{{ url('/parallax/' . $fiesta->code) }}">
								<div class="title">
									<div class="time">
										<div class="date">
										    <div class="number">{{$fiesta->getDay()}}</div>
										    {{$fiesta->getMonth()}}
										</div>
										<img src="{{ asset('img/datatime.png') }}">
									</div>
									<h3>{{ $fiesta->name }}</h3>
									<p>{{ substr( $fiesta->description, 0,60) }}...</p>
								</div>
								<div class="pic"
									 style="background-image:url('{{ asset($fiesta->getCover()) }}');"></div>
							</a>

						</li>
					@endforeach
				@endforeach
			</ul>

			<div style="position:relative;">
				<p style="padding-top:35px;font-size:16px;color:#de4a4a;" id="alterfiesre">Ver todos</p>
				<img src="{{ asset('img/vermas.png') }}" class="vermas" id="fesrebu">
			</div>
		</div>
	</section>
	<!-- A -->

	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$page,'number'=>1])

	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$page,'contents_partial'=>$contents,'code_text'=>'life-destiny-destino'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$page, 'comments'=>$comments])



	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
		var codefrase = '';
		var codedestiny = "{{ $page->code}}";
		//img minmap
		$("#imgminmap").change(function ()
		{
			saveImage(this, 'destino', 'minimapa', '{{ url('add-image-multimedia') }}', codedestiny);
		});
		//img cover frase

		//img front
		$("#imgfront").change(function ()
		{
			saveImage(this, 'destino', 'front', '{{ url('add-image-multimedia') }}', codedestiny);
		});
		//crear subdestinos
	</script>

	<script>




	</script>
	<script>
		$("#frasedestacadanew").click(function (event)
		{
           var data_page={name: $("#frasedestacadatext").val(), type: 'frasedestacada',code:$("#frasedestacadatext").val(),parent_id:'{{ $page->id}}'};
           createNewPage(event,data_page);
		});
		$("#subdestinynew").click(function (event)
		{
		   var data_page={name: $("#subdestinytext").val(), type: 'subdestino',code:$("#subdestinytext").val(),parent_id:'{{ $page->id}}'};
           createNewPage(event,data_page);
		});
		$("#hechoennew").click(function (event)
		{
		     var data_page={name: $("#hechoentext").val(), type: 'hecho_engrupo',code:$("#hechoentext").val(),parent_id:'{{ $page->id}}'};
             createNewPage(event,data_page);
		});
		$("#itinerarionew").click(function (event)
		{
             var data_page={name: $("#itinerariotext").val(), type: 'itinerario',code:$("#itinerariotext").val(),parent_id:'{{ $page->id}}'};
             createNewPage(event,data_page);
		});
	</script>

	<script>
		var coordinates = [
			@foreach($page->childs()->places()->get() as $place)
			{
				id: '{{ $place->id  }}',
				code: '{{$place->code}}',
				lat: '{{ (count($place->ubications)>0)?$place->ubications[0]->content:'-12.601475166388834'}}',
				lon: '{{ (count($place->ubications)>0)?$place->ubications[1]->content:'-75.11077880859375'}}',
				name: '{{$place->name}}',
				description: '{{$place->description}}',
				img:'{{url($place->getImageCropImg('cover'))}}'
			},
			@endforeach
		];

		var mapPosition = {
			lat: '{{ (count($position)>0)?$position[0]->content:'-12.046623'}}',
			lon: '{{ (count($position)>1)?$position[1]->content:'-77.042828'}}',
			zoom: '{{ (count($position)>2)?$position[2]->content:'8'}}'
		};
		var map;

		function initialize()
		{
			var mapCanvas = document.getElementById('map-canvas');
			var pinIcon = new google.maps.MarkerImage("{{ url("/img/icon_map_a.png") }}",
					null, /* size is determined at runtime */
					null, /* origin is 0,0 */
					null, /* anchor is bottom center of the scaled image */
					new google.maps.Size(38, 48)
			);
			var mapOptions = {
				center: new google.maps.LatLng(mapPosition.lat, mapPosition.lon),
				zoom: parseInt(mapPosition.zoom),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false
			};

			map = new google.maps.Map(mapCanvas, mapOptions);

			for (var i = 0; i < coordinates.length; i++)
			{
				var infowindow = new google.maps.InfoWindow({
					content: '<h4>' + coordinates[i].name + '</h4><img style="width: 30%;"  src="'+ coordinates[i].img +'">  <div>' + coordinates[i].description + '</div><a href="{{ url('/sub-destino/') }}'+coordinates[i].code+'">Ver más</a>',
					maxWidth: 300
				});
				var position = new google.maps.LatLng(coordinates[i].lat, coordinates[i].lon);

				var marker = new google.maps.Marker({
					position: position,
					map: map,
					@if($isAdmin)
					draggable: true,
					@endif
					icon: pinIcon,
					title: coordinates[i].name
				});

				google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
				{
					return function () { infowindow.open(map, this); };
				})(marker, i, infowindow));


				@if($isAdmin)
				google.maps.event.addListener(marker, "dragend", (function (marker, i, infowindow)
				{
					return function ()
					{
						savemappoint(coordinates[i].id, '{{ url('save-point') }}', marker.position.lat(), marker.position.lng(), map.getZoom());
					};
				})(marker, i, infowindow));
				@endif

			}

			map.setZoom(parseInt(mapPosition.zoom));
			map.panTo(new google.maps.LatLng(mapPosition.lat, mapPosition.lon));
		}

		google.maps.event.addDomListener(window, 'load', initialize);

		function resetPositionMap()
		{
			var latlong = new google.maps.LatLng(mapPosition.lat, mapPosition.lon);
			map.setZoom(parseInt(mapPosition.zoom));
			map.panTo(latlong);
		}
	</script>
@endsection
