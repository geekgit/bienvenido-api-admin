<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3; ?>

@extends('website/appsite')

@section('content')

	<section class="slider1">
		<div class="swiper-container">
			<div class="swiper-wrapper">
				@foreach ($slider1 as $multimedia)
					<div class="swiper-slide"
						 style="background-image: url('{{ asset($multimedia->source_crop) }}');">
						@if($isAdmin)
							<div class="block-admin-float">
								<button class="button-admin"
										onclick="cropImage('','{{ $multimedia->id }}','{{ $page->code }}','{{ url($multimedia->source) }}','{{ url('save-image-crop') }}','slider1',600,200,'{{ url('add-image-multimedia') }}',1400)">
									<i class="icon-image"></i> Editar imagen
								</button>
								<button class="button-admin"
										onclick="deletesliderimg('{{$multimedia->id}}','{{ url('delete-img-slider') }}')">
									<i class="icon-trash"></i> Eliminar slide
								</button>
							</div>
						@endif

						<div class="info-slide">
							<input type="hidden" id="{{$multimedia->id}}">
							<span class="slider-title" {{ $isAdmin?'contenteditable':'' }}>{!! $multimedia->name or 'nombre' !!} </span>
							<span class="slider-desc" {{ $isAdmin?'contenteditable':'' }}>{!! $multimedia->description or 'descripcion' !!}</span>
							@if($isAdmin)
								<br>
								<div class="info-admin">Cambiar imagen actual</div>
								<input type='file' class="imgslider"/> <i class="icon-search"></i>
							@endif
						</div>
					</div>
				@endforeach
			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
		</div>
		@if($isAdmin)
			<div class="block-admin form-inline" style="margin-bottom: 0;">


				<div class="form-group">
					<input type='file' id="imgslider"/>

				</div>

				<button id="slidernew" class="button-admin"><i class="icon-plus"></i> Crear nuevo slide</button>
			</div>
		@endif
	</section>
	<!-- End slide home-->

	<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<div class="home-msj">

					<div class="editable" id="welcomehome" {{ $isAdmin?'code=welcome type=content contenteditable':'' }}> {!! $contents["welcome"]->content or 'welcome:contenido'!!}</div>
					</div>
					<div class="estatic-msj text-uppercase" id="welcomehomename" {{ $isAdmin?'code=welcome type=content contenteditable':'' }}>
						{!! $contents["welcome"]->name or 'welcome:name'!!}
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="imperdibles">
		<div class="container">
			<h2 class="title-content">
				<span id="text1ehomename" {{ $isAdmin?'code=text1 type=name contenteditable':'' }}>{{ $contents["text1"]->name or 'text1:nombre' }}</span>
			</h2>
			<div class="editable" id="text1ehomecontent" {{ $isAdmin?'code=text1 type=content contenteditable':'' }}>{!! $contents["text1"]->content or 'text1:contenido' !!}</div>

			@if($isAdmin)
				<div class="block-admin">
					<label for="selectdestiny">Selecciona una destino</label>
					<select id="selectdestiny">
						<option value=""><-- Destinos disponibles --></option>
						@forelse($availableDestinies as $d)
						<option value="{{ $d->code  }}">{{ $d->name . ' - ' . $d->type  }}</option>
						@empty
						<option value="">No hay destinos disponibles</option>
						@endforelse
					</select>
					<button id="selectdestinynew" class="button-admin"> Agregar</button>
					|
					<button class="button-admin" onclick="openpopuporder('dragListImperdibles','{{$page->id}}')">Editar
						posiciones de destinos
					</button>
					|
					<i class="icon-question faq-admin"
					   title="Para odernar la posición de los destinos haz clic en el boton Editar posiciones"
					   data-toggle="tooltip"></i>
				</div>
			@endif

			<div class="swiper-container-list">

			<ul id="dragListImperdibles" class="list-thumbnail swiper-wrapper">
				@foreach($imperdibles as $imperdible)
					<li data-id="{{ $imperdible->id }}" class="swiper-slide">
						@if($isAdmin)
							<button class="button-admin"
									onclick="eliminarrelationpage('{{ $imperdible->id }}', '{{ url('delete-attach-page') }}','home')">
								<i class="icon-trash"></i> Quitar
							</button>
							<button class="button-admin"
									onclick="cropImage('destino','{{ $imperdible->getImageCropId('cover') }}','{{ $imperdible->code }}','{{ asset($imperdible->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
								<i class="icon-image"></i> Editar imágen
							</button>
						@endif
						<a href="{{ url('destino/' . $imperdible->code) }}">
							<span class="pic"
								  style="background-image: url('{{ asset($imperdible->getImageCropImg('cover')) }}');"></span>
							{{--						<img src="{{ asset($imperdible->getImageCropImg('cover')) }}" alt="" class="pic">--}}
							<span class="title"><span>{{ $imperdible->name }}</span></span>
						</a>
					</li>
				@endforeach
			</ul>
		</div>

		</div>
	</section>

	<!-- 7 formas de llegar a machupicchu -->
	<section class="recomendado">
		@if($isAdmin)
			<div class="block-admin-float">
				<p class="info-admin">Elige una imagen, la imagen se cargará automáticamente</p>
				<button class="button-admin"
                   		onclick="cropImage('','{{ $page->getImageCropId('recomendado') }}','{{ $page->code }}','{{ url($page->getCover('recomendado')) }}','{{ url('save-image-crop') }}','recomendado',600,200,'{{ url('add-image-multimedia') }}',1400)">
                   	<i class="icon-image"></i> Editar imagen
                   </button>
			</div>
		@endif
		@if(isset($infomultimedia["recomendado"]) )
			<a href="{{ url('sieteformas') }}"
			   style="background-image: url('{{ asset($page->getImageCropImg('recomendado'))}}');">
				<div class="container">
					<img class="img-responsive" src="{{ asset('img/formas_es.png') }}">
				</div>
			</a>
		@endif
	</section>

	<section class="hperu">
		<div class="container">
			<h2 class="title-content">
				<span id="text2ehomename" {{ $isAdmin?'code=text2 type=name contenteditable':'' }}>{{ $contents["text2"]->name or 'text2:nombre' }}</span>
			</h2>
			<div class="editable" id="text2ehomecontent" {{ $isAdmin?'code=text2 type=content contenteditable':'' }}>{!! $contents["text2"]->content or 'text2:contenido' !!}</div>

			@if($isAdmin)
				<div class="block-admin">
					<label class="info-admin">Crear nuevo grupo de Hecho en Perú: </label>
					<input id="hechoentext" type="text" placeholder="Nombre de grupo" class="input-small">
					<button id="hechoennew" class="button-admin"><i class="icon-plus"></i> Crear grupo</button>
					|
					<button class="button-admin" onclick="openpopuporder('hecho_grupoorder','{{$page->id}}')"><i
								class="icon-list"></i>Editar posiciones
					</button>

				</div>
			@endif
			<div class="row">
				<div class="col-md-6 text-center">
					@if(count($hechoen) > 0)
						@if($isAdmin)
							<button class="button-admin" onclick="eliminarpage('{{$hechoen[0]->id}}', '{{ url('content-delete-page') }}')"> Eliminar</button>
							<button class="button-admin"
									onclick="cropImage('hecho_enperu','{{ $hechoen[0]->getImageCropId('cover') }}','{{ $hechoen[0]->code }}','{{ asset($hechoen[0]->getCover()) }}','{{ url('save-image-crop') }}','cover',400,400,'{{ url('add-image-multimedia') }}',550)">
								<i class="icon-image"></i> Editar imagen
							</button>
						@endif
						<a href="{{ url('hechoen/' . $hechoen[0]->code) }}" class="big-picture">
							<input type="hidden" value="{{$hechoen[0]->code}}"/>
							<div style="background-image: url({{ asset($hechoen[0]->getImageCropImg('cover')) }});" class="pic"></div>
							<div class="title">
								<i class="icon-badge"></i>
								<h3> {{ $hechoen[0]->name }} </h3>
								<p> {{ $hechoen[0]->description }} </p>
							</div>
						</a>
					@endif
				</div>
				<div class="col-md-6">
					<ul id="hecho_grupoorder" class="list-thumbnail">
						@for( $i = 0; $i < count($hechoen); $i++)
							<?php $hecho = $hechoen[$i]; ?>
							<li data-id="{{$hecho->id }}" style="{{($i==0)?'display:none;':''}}">
								@if($isAdmin)
									<button class="button-admin"
											onclick="eliminarpage('{{$hecho->id}}', '{{ url('content-delete-page') }}')">
										Eliminar
									</button>
									<button class="button-admin"
											onclick="cropImage('hecho_enperu','{{ $hecho->getImageCropId('cover') }}','{{ $hecho->code }}','{{ asset($hecho->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
										<i class="icon-image"></i> Editar imagen
									</button>
								@endif
								<a href="{{ url('hechoen/' . $hecho->code) }}">
									<div class="title"><span> {{ $hecho->name }} </span></div>
									<div class="pic" style="background-image: url('{{ asset($hecho->getImageCropImg('cover')) }}');"></div>
								</a>
							</li>
						@endfor
					</ul>
				</div>
			</div>


			<div class="ver-todos text-center">
				<p>Ver todos</p>
				<button class="button-icon"><i class="icon-chevron-down"></i></button>
			</div>
		</div>
	</section>

	<!-- Start regiones naturales -->

	<section class="regiones">
		<div class="slider-region swiper-container">
			<div class="swiper-wrapper">
				@forelse($regiones as $region)
					<div class="swiper-slide" style="background-image: url('{{ asset($region->getImageCropImg('cover')) }}');">
						@if($isAdmin)
							<div class="block-admin-float text-left">
								<p class="info-admin">Para editar los textos y nombres de las regiones<br> debe ingresar
									al su página y usar el boton de Editar Página</p>
								<input type="hidden" value="{{ $region->code }}">
								<p><span class="info-admin">Cambiar fondo: </span><br>
									<button class="button-admin"
    										onclick="cropImage('region','{{ $region->getImageCropId('cover') }}','{{ $region->code }}','{{ url($region->getCover()) }}','{{ url('save-image-crop') }}','cover',600,200,'{{ url('add-image-multimedia') }}',1400)">
    									<i class="icon-image"></i> Editar imagen
    								</button>
														</p>
								{{--<p><span class="info-admin">Cambiar mapa: </span><br>
										<button class="button-admin"
                                        		onclick="cropImage('region','{{ $region->getImageCropId('mapa') }}','{{ $region->code }}','{{ url($region->getCover('mapa')) }}','{{ url('save-image-crop') }}','mapa',200,200,'{{ url('addimagemultimedia') }}',550)">
                                        	<i class="icon-image"></i> Editar imagen
                                        </button>
								</p>--}}
							</div>
						@endif
						<div class="container">
							<div class="row">
								<div class="">
									<a href="{{ url('region/'.$region->code) }}">
										<p class="desc">{{ $region->description }}</p>
										<p class="text-uppercase name">{{ $region->name }}</p>
									</a>
								</div>
								{{--<div class="col-md-2">
									<img src="{{ asset($region->getImageCropImg('mapa')) }}"
										 class="map">
								</div>--}}
							</div>
						</div>

					</div>
				@empty
					<div class="swiper-slide"><p>No hay regiones disponibles</p></div>
				@endforelse

			</div>
			<div class="swiper-pagination"></div>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
		</div><!-- swiper container-->
	</section>

	<section class="aventura">
		<div class="container">
			<h2 class="title-content"><span>Aventura</span></h2>

			<ul class="list-thumbnail">
				<?php
                function convertArrayContents($array)
                {
                    $sortArray = [];
                    for ($i = 0; $i < count($array); ++$i) {
                        $sortArray[$array[$i]->code] = $array[$i];
                    }

                    return $sortArray;
                }
                ?>
				@foreach($aventuras as $aventura)
					<li>
						<?php
                        $categoryContents = convertArrayContents($aventura->contents()->get());?>
						<a href="{{  url('categoria/aventura#' . $aventura->code) }}">
							<span class="title text-left">
								<i class="{!! $categoryContents['icon']->content or '' !!} icon"></i>
								{{ $aventura->name }}
							</span>
							<div class="pic" style="background-image:url('{{ asset($aventura->getImageCropImg('cover')) }}');"></div>
						</a>
						@if($isAdmin)
							<button class="button-admin"
									onclick="cropImage('category','{{ $aventura->getImageCropId('cover') }}','{{ $aventura->code }}','{{ asset($aventura->getCover()) }}','{{ url('save-image-crop') }}','cover',200,200,'{{ url('add-image-multimedia') }}',550)">
								<i class="icon-image"></i> Editar imagen
							</button>
						@endif
					</li>
				@endforeach
			</ul>

			<div style="position:relative;">
				<button class="button-icon" id="aventhomebu">
					<p>Ver todos</p>
					<i class="icon-chevron-down"></i>
				</button>
			</div>
		</div>
	</section>

	<!-- End aventura -->

	<section class="fiestas">
		<div class="container">
			<h2 class="title-content">
				<span id="text3ehomename" {{ $isAdmin?'code=text3 type=name contenteditable':'' }}>{{ $contents["text3"]->name or 'text3:nombre' }}</span>
			</h2>
			<div class="editable" id="text3ehomecontent" {{ $isAdmin?'code=text3 type=content contenteditable':'' }}>{!! $contents["text3"]->content or 'text3:contenido' !!}</div>
			@if($isAdmin)
				<div class="block-admin">
					<label for="selectfiesta">
						Seleccione un evento, fiesta o festival:
					</label>

					<select id="selectfiesta">
						<option value=""><--Seleccione--></option>
						@foreach($selectfiestas as $a)
							<option value="{{ $a->code }}">{{ $a->name }} | {{ $a->type }}</option>
						@endforeach
					</select>
					<button class="button-admin" id="selectfiestanew">Agregar</button>
					|
					<button class="button-admin" onclick="openpopuporder('moveListEvento',{{$page->id}})"><i
								class="icon-list"></i>Editar posiciones de eventos
					</button>

				</div>
			@endif

			<ul id="moveListEvento" class="list-thumbnail detail">
				@foreach($fiestas as $fiesta)
					<li data-id="{{$fiesta->id}}">
						@if($isAdmin)
							<button class="button-admin"
									onclick="eliminarrelationpage({{$fiesta->id}}, '{{ url('delete-attach-page') }}','home')">
								<i class="icon-close"></i> Quitar de la lista
							</button>
						@endif
						<a href="{{ url('/parallax/' . $fiesta->code) }}">
							<div class="title">
								<div class="time">
									<div class="date">
										<div class="number">{{$fiesta->getDay()}}</div>
										{{$fiesta->getMonth()}}
									</div>

								</div>
								<h3>{{ $fiesta->name }}</h3>
								<p>{{ substr( $fiesta->description, 0,60) }}...</p>
							</div>
							<span class="pic" style="background-image:url('{{ asset($fiesta->getImageCropImg('cover')) }}');"></span>
						</a>

					</li>
				@endforeach
			</ul>

			<div>
				<p style="padding-top:35px;font-size:16px;color:#de4a4a;" id="alterfiesre">Ver todos</p>
				<button class="button-icon" id="fesrebu"><i class="icon-chevron-down"></i></button>
			</div>
		</div>
	</section>


	<!-- Frase celebre -->

	@include('partials.phrase')

	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$page,'contents_partial'=>$contents,'code_text'=>'life-destiny-index'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$page, 'comments'=>$comments])

	<script type="text/javascript" src="{{ url('/') }}/js/jquery.magnific-popup.min.js"></script>
	<script>
		var codedestiny = "{{ $page->code }}";
		var mySwiper2 = new Swiper('.swiper-container-list', {
			slidesPerView:'auto',
			freeMode:true
		});
		var mySwiper = new Swiper('.swiper-container', {
			loop: true,
			pagination: '.swiper-pagination',
			nextButton: '.swiper-button-next',
			prevButton: '.swiper-button-prev'
		});


		$("#frasedestacadanew").click(function ()
		{
        var data_page={name: $("#frasedestacadatext").val(), type: 'frasedestacada',code:$("#frasedestacadatext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);

		});
		$("#selectdestinynew").click(function ()
		{
			if ($('#selectdestiny').val() != '')
			{
				$('#myPleaseWait').modal('show');
				$.ajax({
					url: '{{ url('attach-page') }}',
					type: "post",
					data: {child_code: $('#selectdestiny').val(), page_code: codedestiny},
					success: function (data)
					{
						$('#myPleaseWait').modal('hide');
						window.location.reload();
					}
				}, "json");
			}
		});

		$("#selectfiestanew").click(function ()
		{
			if ($('#selectfiesta').val() != '')
			{
				$('#myPleaseWait').modal('show');
				$.ajax({
					url: '{{ url('attach-page') }}',
					type: "post",
					data: {child_code: $('#selectfiesta').val(), page_code: codedestiny},
					success: function (data)
					{
						$('#myPleaseWait').modal('hide');
						window.location.reload();
					}
				}, "json");
			}
		});

		$("#hechoennew").click(function (event)
		{
        var data_page={name: $("#hechoentext").val(), type: 'hecho_enperu',code:$("#hechoentext").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);

		});
		//edit slider
		$(document).ready(function ()
		{

			function updateimgslider(input)
			{
				var cod = $(input).siblings('input[type="hidden"]').attr('id');
				var desc = $(input).siblings('.slider-desc').text();
				var title = $(input).siblings('.slider-title').text();
				if (input.files && input.files[0])
				{
					var reader = new FileReader();
					reader.onload = function (e)
					{
						var img = $(input).parent('div').parent('div');
						img.attr("style", "background-image: url(" + e.target.result + ");background-repeat: no-repeat;background-size: cover");
					};
					var img = reader.readAsDataURL(input.files[0]);
				}
				var data = new FormData();
				data.append('id', cod);
				data.append('description', desc);
				data.append('name', title);
				data.append('file', input.files[0]);
				data.append('code', 'slider1');
				$.ajax({
					url: '{{ url('edit-multimedia') }}',
					type: "post",
					processData: false,
					contentType: false,
					data: data,
					success: function (data)
					{
						window.location.reload();
					}
				}, "json");
			}

			$(".imgslider").change(function ()
			{
				updateimgslider(this);
			});
		})

		$(".slider-title").focusout(function ()
		{
			var cod = $(this).siblings('input[type="hidden"]').attr('id');
			var desc = $(this).siblings('.slider-desc').text();
			$.ajax({
				url: '{{ url('edit-multimedia') }}',
				type: "post",
				data: {id: cod, name: $(this).text(), description: desc, code: 'slider1'},
				success: function (data)
				{
					// window.location.reload();
				}
			}, "json");
		});
		$(".slider-desc").focusout(function ()
		{
			var cod = $(this).siblings('input[type="hidden"]').attr('id');
			var desc = $(this).siblings('.slider-title').text();
			$.ajax({
				url: '{{ url('edit-multimedia') }}',
				type: "post",
				data: {id: cod, description: $(this).text(), name: desc, code: 'slider1'},
				success: function (data)
				{
					// window.location.reload();
				}
			}, "json");
		});
		//new slider
		var inputfile;

		$("#slidernew").click(function ()
		{

			if (inputfile.files && inputfile.files[0])
			{
				var reader = new FileReader();
				reader.onload = function (e)
				{
				};
				var data = new FormData();
				data.append('code', codedestiny);
				data.append('file', inputfile.files[0]);
				data.append('type', '');
				data.append('codechild', 'slider1');
				$.ajax({
					url: '{{ url('add-image-multimedia') }}',
					type: "post",
					processData: false,
					contentType: false,
					data: data,
					success: function (data)
					{
						window.location.reload();
					}
				}, "json");
			}
		});
		$("#imgslider").change(function ()
		{
			inputfile = this;
		});
		//update image slider

	</script>

@endsection
