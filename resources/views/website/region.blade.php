<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($region->id);?>
@extends('website/appsite')

@section('content')

	<section class="header">
		@if($isAdmin)
			<div class="block-admin-float">
				<span class="info-admin">Cambiar imagen: </span>
				<input type='file' id="imgfront"/>
				@if(isset($multimedia['front']))
					|
					<button class="button-admin"
                    		onclick="cropImage('region','{{isset($multimedia['front'])?$multimedia['front']->id:0 }}','{{ $region->code }}','{{ isset($multimedia['front'])? asset($multimedia['front']->source):'' }}','{{ url('save-image-crop') }}','front',200,200,'{{ url('add-image-multimedia') }}',400)">
                    	<i class="icon-crop"></i> Editar imagen
                    </button>
				@endif
			</div>
		@endif
		<div class="swiper-container"
			 style="background: url('{{ URL::to('/') }}/{{ $multimedia['front']->source or '' }}')no-repeat center;background-size: cover;background-position: center center;">
            @include('partials.social_buttons')
			<div class="slider-caption">
				<h3 class="slogan"> {{$region->description or 'page:description'}}  </h3>
				<h1 class="name">{{ $region->name or  'page:name' }}</h1>
			</div>

			<div onclick="favorito({{$region->id}},this,'<?php echo url('save-favorite')?>')"
				 class="favorito <?php echo ($favorite == 1) ? 'added' : ''; ?>">
				<i class="icon-star"></i>
				<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
			</div>
		</div>
	</section>
	<section class="msj-bienvenida">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<span class="editable" id="welcomeregion" {{ $isAdmin?'code=welcome type=content contenteditable':'' }}> {!! $contents["welcome"]->content or '[welcome]-region-contenidos:contenido' !!} </span>
				</div>
			</div>
		</div>
	</section>
	<section class="escala">
		<div class="content">
			<p>Destinos > Regiones > <span>{{$region->name or 'page:name' }}</span></p>
		</div>
	</section>
	<section class="destiny-history container">
		<div id="resenia-region" class="block-history container">
			<div class="row">
				<div class="col-md-8">
					<h2 class="title " id="text1regionname" {{ $isAdmin?'code=text1 type=name contenteditable':'' }}
					   >{{ $contents["text1"]->name or '[text1]-region-contenidos:nombre' }}    </h2>

					<div id="text1regioncontent" class="editable"
						 {{ $isAdmin?'code=text1 type=content contenteditable':'' }}>    {!! $contents["text1"]->content  or '[text1]-region-contenidos:contenido' !!}</div>
            <hr class="visible-sm visible-xs">
            </div>
			<div class="col-md-4">
			<div class="details">
					<h5 class="sub-title "  id="olvidarregionname"
					   {{ $isAdmin?'code=olvidar type=name contenteditable':'' }}>{{ $contents["olvidar"]->name or '[olvidar]-region-contenidos:nombre' }}</h5>
					<div class="editable" style="padding-bottom: 20px;border-bottom: dotted 1px;" id="olvidarregioncontent"
						 {{ $isAdmin?'code=olvidar type=content contenteditable':'' }}> {!! $contents["olvidar"]->content or '[olvidar]-region-contenidos:contenido' !!} </div>
					<hr>
					<h5 class="sub-title "  style="padding-top:20px;" id="curiosidadregionname"
						 {{ $isAdmin?'code=curiosidad type=name contenteditable':'' }}>{!! $contents["curiosidad"]->name or '[curiosidad]-region-contenidos:contenido' !!}</h5>
					<div id="curiosidadregioncontent" class="editable"
						{{ $isAdmin?'code=curiosidad type=content contenteditable':'' }}>{!! $contents["curiosidad"]->content or '[curiosidad]-region-contenidos:contenido' !!}</div>


			</div>
			</div>
			</div>
			<div class="history-legend">
				<div class="row">
						<div class="col-sm-3">
						    <img src="{{ asset('img/naturaleza.png') }}">
							<div >
							    <span class="value" id="naturalezaregioncontent" {{ $isAdmin?'code=naturaleza type=content contenteditable':'' }}>
                                	{{ $contents["naturaleza"]->content  or '[naturaleza] Dato' }}
                                </span><br>
                                <span id="naturalezaregionname" {{ $isAdmin?'code=naturaleza type=name contenteditable':'' }}>
                                	{{ $contents["naturaleza"]->name  or '[naturaleza] Label' }}
                                </span>
							</div>
						</div>
						<div class="col-sm-3">
						    <img src="{{ asset('img/aventura.png') }}">
							<div >
							    <span class="value" id="aventuraregioncontent" {{ $isAdmin?'code=aventura type=content contenteditable':'' }}>
                                   	{{ $contents["aventura"]->content  or '[aventura] Dato' }}
                                   </span><br>
                                   <span id="aventuraregionname" {{ $isAdmin?'code=aventura type=name contenteditable':'' }}>
                                   	{{ $contents["aventura"]->name  or '[aventura] Label' }}
                                </span>
							</div>
						</div>
						<div class="col-sm-3">
						     <img src="{{ asset('img/cultura.png') }}">
							<div >
							    <span class="value" id="culturaregioncontent" {{ $isAdmin?'code=cultura type=content contenteditable':'' }}>
                                   	{{ $contents["cultura"]->content  or '[cultura] Dato' }}
                                   </span><br>
                                   <span id="culturaregionname" {{ $isAdmin?'code=cultura type=name contenteditable':'' }}>
                                   	{{ $contents["cultura"]->name  or '[cultura] Label' }}
                                </span>
							</div>
						</div>
						<div class="col-sm-3" >
						    <img src="{{ asset('img/gastronomia.png') }}">
							<div >
							    <span class="value" id="gastronomiaregioncontent" {{ $isAdmin?'code=gastronomia type=content contenteditable':'' }}>
                                   	{{ $contents["gastronomia"]->content  or '[gastronomia] Dato' }}
                                   </span><br>
                                   <span id="gastronomiaregionname" {{ $isAdmin?'code=gastronomia type=name contenteditable':'' }}>
                                   	{{ $contents["gastronomia"]->name  or '[gastronomia] Label' }}
                                </span>
							</div>
						</div>
				</div>
			</div>
		</div>
		<div class="text-center">
			<a href="" class="vermas" id="imgdestino">
				VER MÁS <i class="icon-arrow-down"></i>
			</a>
		</div>
	</section>
<section class="categories">
		<div class="container">
		    <div class="row">
				<div class="col-md-6">
	                @if($isAdmin)
	                	<button class="button-admin"
	                			onclick="saveMapPosition({{$region->id}},'<?php echo url('save-point')?>')"> Guardar
	                		posición del mapa
	                	</button>
	                @endif
					<div id="map-canvas"></div>
				</div>
				<div class="col-md-6">
					@if($isAdmin)
				        <div class="block-admin">
				        	<label for="selectdestinys">Selecciona un Sitio</label>
				        	<select id="selectdestinys">
				        		<option value=""><-- Sitios disponibles --></option>
				        		@forelse($selectdestinys as $d)
				        			<option value="{{ $d->code  }}">{{ $d->name . ' - ' . $d->type  }}</option>
				        		@empty
				        			<option value="">No hay sitios disponibles</option>
				        		@endforelse
				        	</select>
				            <button id="selectdestinynew" class="button-admin"> Agregar</button>
                            |
                            <button class="button-admin" onclick="openpopuporder('listdestinyandsites','{{$region->id}}')">
                            	Editar posiciones de destinos
                            </button>
				        </div>
                	@endif
					<ul id="listdestinyandsites" class="list-thumbnail">
					    @foreach($places as $place)
					    	<li data-id="{{$place->id}}">
					    	    @if($isAdmin)
                                    <button class="button-admin"
                                    		onclick="eliminarrelationpage('{{ $place->id }}', '{{ url('delete-attach-page') }}','{{$region->code}}')">
                                    	<i class="icon-trash"></i> Quitar
                                    </button>
                                 @endif

					    		<a href="{{ url(($place->type=='destino')?'/destino/':'/sub-destino/' . $place->code) }}">
							        <div class="title"><span>{{$place->name}}</span></div>
							        <input type="hidden" value="{{$place->code}}"/>
							        <div class="pic"
							        	 style="background-image: url('{{ asset($place->getImageCropImg('cover')) }}');"></div>
					    	    </a>

					    	</li>
					    @endforeach
					</ul>
				</div>
            </div>
		</div>
	</section>

	<section class="fiestas">
		<div class="container">
			<h2 class="title-content">
				<span id="text2regionname" {{ $isAdmin?'code=text2 type=name contenteditable':'' }}>{{ $contents["text2"]->name or 'text2:nombre' }}</span>
			</h2>
			<div class="editable" id="text2regioncontent" {{ $isAdmin?'code=text2 type=content contenteditable':'' }}>{!! $contents["text2"]->content or 'text2:contenido' !!}</div>
			@if($isAdmin)
				<div class="block-admin">
					<label for="selectdestiny">
						Selecciona una fiesta
					</label>
					<select id="selectfiesta">
						<option value=""><--Seleccione--></option>
						<?php for( $a = 0;$a < count($selectfiestas);$a++) { ?>
						<option value="<?php echo $selectfiestas[$a]->code ?>"><?php echo $selectfiestas[$a]->name . '-' . $selectfiestas[$a]->type ?></option>
						<?php } ?>
					</select>
					<button id="selectfiestanew" class="button-admin"> Agregar</button>
					|
					<button class="button-admin" onclick="openpopuporder('movelistevento','{{$region->id}}')">Editar
						posiciones de destinos
					</button>


				</div>
			@endif
			<ul id="movelistevento" class="list-thumbnail detail">

				@foreach($fiestas as $fiesta)
					<li data-id="{{$fiesta->id}}">
						<a href="{{ url('/parallax/' . $fiesta->code) }}">
							<div class="title">
								<div class="time">
									<div class="date">
										<div class="number">{{$fiesta->getDay()}}</div>
										{{$fiesta->getMonth()}}
									</div>
									<img src="{{ asset('img/datatime.png') }}">
								</div>
								<h3>{{ $fiesta->name }}</h3>
								<p>{{ substr( $fiesta->description, 0,60) }}...</p>
							</div>

							<div class="pic"
								 style="background-image:url('{{ asset($fiesta->getCover()) }}');"></div>
						</a>
						@if($isAdmin)
							<button class="button-admin"
									onclick="eliminarrelationpage({{$fiesta->id}}, '<?php echo url('delete-attach-page')?>')">
								Eliminar
							</button>
						@endif
					</li>

				@endforeach

			</ul>

			<div>
				<p style="padding-top:35px;font-size:16px;color:#de4a4a;" id="alterfiesre">Ver todos</p>
				<button class="button-icon" id="fesrebu"><i class="icon-chevron-down"></i></button>
			</div>

		</div>
	</section>
	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$region,'contents_partial'=>$contents,'code_text'=>'life-destiny-region'])



	<script type="text/javascript" src="{{  URL::to('/')."/" }}slick/slick.min.js"></script>

	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
		var codedestiny = "<?php echo $region->code?>";

		$("#imgfront").change(function ()
        {
        	saveImage(this, 'region', 'front', '{{ url('add-image-multimedia') }}', codedestiny);
        });
		$("#selectdestinynew").click(function ()
		{
			if ($('#selectdestinys').val() != '')
			{
				$.ajax({
					url: '<?php echo URL::to('attach-page')?>',
					type: "post",
					data: {child_code: $('#selectdestinys').val(), page_code: codedestiny},
					success: function (data)
					{
						window.location.reload();
					}
				}, "json");
			}

		});
		$("#selectfiestanew").click(function ()
		{
			if ($('#selectfiesta').val() != '')
			{
				$.ajax({
					url: '<?php echo URL::to('attach-page')?>',
					type: "post",
					data: {child_code: $('#selectfiesta').val(), page_code: codedestiny},
					success: function (data)
					{
						window.location.reload();
					}
				}, "json");
			}
		});
	</script>
	<script>
		var coordinates = [
			@foreach($places as $place)
			<?php
			$place->ubications();
			$name = $place->name;
			$lat = -12.601475166388834;
			$lon = -75.11077880859375;
			if (count($place->ubications) > 0) {
				$lat = $place->ubications[0]->content;
				$lon = $place->ubications[1]->content;
			}

			?>
			@if(isset($lat) && isset($lon))
			@if( $lat!=-12.601475166388834  && $lon!=-75.11077880859375)
			{!! '{id:'.$place->id.',code:"'.$place->code.'",type:"'.$place->type.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
			@else
			{!! '{id:'.$place->id.',code:"'.$place->code.'",type:"'.$place->type.'",lat:'.$lat.',lon:'.$lon.',name:"'.$name.'"},' !!}
			@endif
			@endif
			@endforeach
		];
				<?php
				$lat = -12.046623;
				$lon = -77.042828;
				$zoom = 8;
				$name = ''?>
		var pstn = [

					@foreach($position as $pos)

					<?php

					$lat = $position[0]->content;
					$lon = $position[1]->content;
					if (count($position) > 2) {
						$zoom = $position[2]->content;
					}

					?>


					@endforeach

					@if( isset($lat) && isset($lon))
					{!! '{lat:'.$lat.',lon:'.$lon.',name:"'.$name.'",zoom:'.$zoom.'},' !!}
					@endif

				];
		var map;
		function initialize()
		{

			var mapCanvas = document.getElementById('map-canvas');

			var mapOptions = {
				center: new google.maps.LatLng(pstn[0].lat, pstn[0].lon),
				zoom: parseInt(pstn[0].zoom),
				mapTypeId: google.maps.MapTypeId.ROADMAP,
				scrollwheel: false
			}
			var goldStar = {
				path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
				fillColor: 'yellow',
				fillOpacity: 0.8,
				scale: 0.1,
				strokeColor: 'gold',
				strokeWeight: 5
			};
			map = new google.maps.Map(mapCanvas, mapOptions)

			var latlong = new google.maps.LatLng(pstn[0].lat, pstn[0].lon);
			map.setZoom(parseInt(pstn[0].zoom));
			map.panTo(latlong);
			for (var i = 0; i < coordinates.length; i++)
			{
				var infowindow = new google.maps.InfoWindow();
				var position = new google.maps.LatLng(coordinates[i].lat, coordinates[i].lon);

				var marker = new google.maps.Marker({
					position: position,
					map: map,
					icon: goldStar,
					title: coordinates[i].name
				});


				google.maps.event.addListener(marker, 'click', (function (marker, i, infowindow)
				{
					return function ()
					{
						infowindow.setContent(coordinates[i].name);
						infowindow.open(map, this);
						if (coordinates[i].type == 'destino')
						{
							window.location = "{{ URL::to('/')."/destino/" }}" + coordinates[i].code;
						}
						else
						{
							window.location = "{{ URL::to('/')."/sub-destino/" }}" + coordinates[i].code;
						}
					};
				})(marker, i, infowindow));
			}
			;


			/*map.addListener('click', function(e) {
			 var latlong= new google.maps.LatLng(pstn[0].lat,pstn[0].lon);
			 map.setZoom(parseInt(pstn[0].zoom));
			 map.panTo(latlong);
			 });*/
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
@endsection