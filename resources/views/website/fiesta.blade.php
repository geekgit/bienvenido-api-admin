<?php $isAdmin = Auth::check() && Auth::user()->roles[0]['pivot']->role_id != 3;?>
<?php $favorite = App\Http\Middleware\WebSiteUtils::getfavorite($place->id);?>
@extends('website/appsite')

@section('content')
<section class="header">
	@if($isAdmin)
		<div class="block-admin-float">
			<span class="info-admin">Cambiar imagen: </span>
			<input type='file' id="imgfront"/>
			@if(isset($multimedia['front5']))
				|
					<button class="button-admin"
							onclick="cropImage('subdestino','{{ $multimedia['front5']->id }}','{{ $place->code }}','{{ url($multimedia['front5']->source) }}','{{ url('save-image-crop') }}','front5',600,200,'{{ url('add-image-multimedia') }},1400')">
						<i class="icon-crop"></i> Editar imagen
					</button>
			@endif
		</div>
	@endif
	<div class="swiper-container sub-destiny"
		 style="background: url('{{ URL::to('/') }}/{{ $multimedia['front5']->source or '' }}')no-repeat center;background-size: cover;background-position: center center;">
		@include('partials.social_buttons')
			<div class="temperatura">
				<ul id="list-weather" class="list-inline"></ul>
				<button class="btn-more">+</button>
			</div>
		<div class="slider-caption small">
				<h1 class="name"><span>{{ $place->name }}</span><br></h1>
					<h3 class="slogan" id="frase5subdestino" {{ $isAdmin?'code=frase5 type=content contenteditable':'' }}>{!! $contents["frase5"]->content  or '[frase5]-subdestino-contenidos:contenido' !!}</h3>


		</div>
		<div onclick="favorito({{$place->id}},this,'<?php echo url('save-favorite')?>')"
			 class="favorito <?php echo ($favorite == 1) ? 'added' : ''; ?>">
			<i class="icon-star"></i>
			<!-- <img src="<?php echo url('/'); ?>/img/favoritoff.png">-->
		</div>

		<div class="menu-subdestino">
			<ul class="list-menu-subdestino">
				<li>
					<a href="{{ url('/sub-destino/'  .$place->code) }}"> <img src="{{ asset('img/subdesoption2.png') }}"> <p class="text-uppercase">Qué Visitar</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comer/' . $place->code) }}"> <img src="{{ asset('img/subdesoption.png') }}"> <p class="text-uppercase">dónde comer</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-hospedaje/'.$place->code) }}"> <img src="{{ asset('img/subdesoption3.png') }}"> <p class="text-uppercase">hospedaje</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-comprar/'.$place->code) }}"> <img src="{{ asset('img/subdesoption5.png') }}"> <p class="text-uppercase">dónde comprar</p> </a>
				</li>
				<li>
					<a href="{{ url('/sub-destino-fiesta/'.$place->code) }}"> <img src="{{ asset('img/subdesoption4active.png') }}"> <p class="text-uppercase">fiestas</p> </a>
				</li>
			</ul>
		</div>
	</div>
</section>
<section class="msj-bienvenida">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="editable" id="welcomefiestasubdestino" {{ $isAdmin?'code=welcome-fiesta type=content contenteditable':'' }}>{!!  $contents["welcome-fiesta"]->content or '[welcome-fiesta]-subdestino-contenidos:contenido'!!}</div>
			</div>
		</div>
	</div>
</section>
<section class="bread-crumb">
	<ol class="breadcrumb container">
		<li><a href="{{ url('/') }}"><img src="{{ asset('favicon.png') }}" width="18" alt="Bienvenida"></a></li>
		<li><a href="destino/{{$place->parent()->code}}">{{$place->parent()->name}}</a></li>
		<li class="active">{{$place->name}}</li>
	</ol>
</section>

<section class="imperdibles" style="margin-bottom:14px;">
	<div class="container">
		<h2 class="title-content">
			<span id="text1fiestasubdestinoname" {{ $isAdmin?'code=text1-fiesta type=name contenteditable':'' }}>{!! $contents["text1-fiesta"]->name or '[text1-fiesta]-subdestino-contenidos:nombre' !!}</span>
		</h2>
		<p id="text1fiestasubdestinocontent" {{ $isAdmin?'code=text1-fiesta type=content contenteditable':'' }}>{!! $contents["text1-fiesta"]->content or '[text1-fiesta]-subdestino-contenidos:contenido' !!} </p>


	</div>
</section>
@if($isAdmin)
	<div style="margin-bottom: 40px;">
		<p>
			<button class="button-admin" id="savemovelistevento">Guardar posiciones</button>
		</p>
	</div>
@endif
<div id="eventslist">
	@if(isset($convencion))
		@foreach($convencion as $conv)

			<section title="{{$conv->id}}" class="festividades">
				<div class="content">
					<div class="fest-left">
						<div class="descripcion">
							<p class="text-uppercase">{{ $conv->name or '[convencion]nombre' }}</p>
							<div></div>
							<p>{{ $conv->description or '[convencion]decription' }}</p>

							<p {{ $isAdmin?'page-id='.$conv->id.' code=dia type=content contenteditable':'' }}>{{ $conv->getDay()  }}</p>
							<p {{ $isAdmin?'page-id='.$conv->id.' code=mes type=content contenteditable':'' }}>{{ $conv->getMonth()  }}</p>
							<br>
							<a href="{{URL::to('/').'/parallax/'.$conv->code}}" style="color:#de4a4a;">Ver más...</a>
						</div>
					</div>

					<?php $img = $conv->multimedia()->where('code', 'cover')->first();  ?>

					@if( $img != null)
						<div class="fest-right"
							 style="background: url('<?php echo URL::to('/'); ?>/{{ $conv->getImageCropImg('cover') }}') no-repeat center; background-size:cover;">
							@if($isAdmin)<input type="hidden" value="{{$conv->code}}"/>
								<button class="button-admin"
										onclick="cropImage('sitio','{{ $conv->getImageCropId('cover') }}','{{ $conv->code }}','{{ asset($conv->getCover()) }}','{{ url('save-image-crop') }}','cover',400,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>

							@endif</div>
					@else
						<div class="fest-right"
							 style="background: url('<?php echo URL::to('/') . '/img/fiestas1.jpg'; ?>') no-repeat center; background-size:cover;">
							@if($isAdmin)
								<input type="hidden" value="{{$conv->code}}"/>
								<button class="button-admin"
										onclick="cropImage('sitio','{{ $conv->getImageCropId('cover') }}','{{ $conv->code }}','{{ asset($conv->getCover()) }}','{{ url('save-image-crop') }}','cover',400,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>

							@endif

						</div>
					@endif


				</div>
			</section>

		@endforeach

	@endif
</div>
@if($isAdmin)
	<div class="block-admin">
		<input type="text" id="sitetextevento" value="" style="" placeholder="EVENTO">
		<button id="sitenewevento" class="button-admin"> Crear evento </button>
	</div>
@endif

	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>0])
<section class="imperdibles" style="margin-bottom:14px;">
	<div class="container">
		<h2 class="title-content">
			<span id="text2fiestasubdestinoname" {{ $isAdmin?'code=text2-fiesta type=name contenteditable':'' }}>{!! $contents["text2-fiesta"]->name or '[text2-fiesta]-subdestino-contenidos:nombre' !!}</span>
		</h2>
		<p id="text2fiestasubdestinocontent" {{ $isAdmin?'code=text2-fiesta type=content contenteditable':'' }}>{!! $contents["text2-fiesta"]->content or '[text2-fiesta]-subdestino-contenidos:contenido' !!} </p>
	</div>
</section>
@if($isAdmin)
	<div style="margin-bottom: 40px;">

		<p>
			<button class="button-admin" id="savemovelistfestival">Guardar posiciones</button>
		</p>
	</div>
@endif
<div id="festivaleslist">
	@if(isset($festival))
		@foreach($festival as $festi)
			<section title="{{$festi->id}}" class="festividades">
				<div class="content">
					<div class="fest-left">
						<div class="descripcion">
							<p class="text-uppercase">{{ $festi->name or '[festival]nombre' }}</p>
							<div></div>
							<p>{{ $festi->description or '[festival]decription' }}</p>
							<p {{ $isAdmin?'page-id='.$festi->id.' code=dia type=content contenteditable':'' }}>{{ $festi->getDay()  }}</p>
                            <p {{ $isAdmin?'page-id='.$festi->id.' code=mes type=content contenteditable':'' }}>{{ $festi->getMonth()  }}</p>
							<br>
							<a href="{{URL::to('/').'/parallax/'.$festi->code}}" style="color:#de4a4a;">Ver más...</a>
						</div>
					</div>

					<?php $img = $festi->multimedia()->where('code', 'cover')->first(); ?>
					@if( $img != null)
						<div class="fest-right"
							 style="background: url('<?php echo URL::to('/'); ?>/{{ $festi->getImageCropImg('cover') }}') no-repeat center; background-size:cover;">
							@if($isAdmin)<input type="hidden" value="{{$festi->code}}"/>
								<button class="button-admin"
										onclick="cropImage('sitio','{{ $festi->getImageCropId('cover') }}','{{ $festi->code }}','{{ asset($festi->getCover()) }}','{{ url('save-image-crop') }}','cover',400,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>
							@endif
						</div>
					@else
						<div class="fest-right"
							 style="background: url('<?php echo URL::to('/'); ?>/img/fiestas1.jpg') no-repeat center; background-size:cover;">
							@if($isAdmin)
								<input type="hidden" value="{{$festi->code}}"/>
								<button class="button-admin"
										onclick="cropImage('sitio','{{ $festi->getImageCropId('cover') }}','{{ $festi->code }}','{{ asset($festi->getCover()) }}','{{ url('save-image-crop') }}','cover',400,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>
							@endif
						</div>
					@endif


				</div>
			</section>
		@endforeach

	@endif
</div>
@if($isAdmin)
	<div class="block-admin">
		<input type="text" id="sitetextfestival" value="" style="" placeholder="FESTIVAL">
		<button id="sitenewfestival" class="button-admin"> Crear festival </button>
	</div>
@endif
<section class="imperdibles" style="margin-bottom:14px;">
	<div class="container">
		<h2 class="title-content">
			<span id="text3fiestasubdestinoname" {{ $isAdmin?'code=text3-fiesta type=name contenteditable':'' }}>{!! $contents["text3-fiesta"]->name or '[text3-fiesta]-subdestino-contenidos:nombre' !!}</span>
		</h2>
		<p id="text3fiestasubdestinocontent" {{ $isAdmin?'code=text3-fiesta type=content contenteditable':'' }}>{!! $contents["text3-fiesta"]->content or '[text3-fiesta]-subdestino-contenidos:contenido' !!} </p>
	</div>
</section>
@if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray()))
	<div style="margin-bottom: 40px;">

		<p>
			<button class="button-admin" id="savemovelistfiesta">Guardar posiciones</button>
		</p>
	</div>
@endif
<div id="fiestaslist">
	@if(isset($fiesta))
		@foreach($fiesta as $fst)
			<section title="{{$fst->id}}" class="festividades">
				<div class="content">
					<div class="fest-left">
						<div class="descripcion">
							<p class="text-uppercase">{{ $fst->name or '[festival]nombre' }}</p>
							<div></div>
							<p>{{ $fst->description or '[festival]decription' }}</p>
							<p {{ $isAdmin?'page-id='.$fst->id.' code=dia type=content contenteditable':'' }}>{{ $fst->getDay()  }}</p>
                             <p {{ $isAdmin?'page-id='.$fst->id.' code=mes type=content contenteditable':'' }}>{{ $fst->getMonth()  }}</p>
							<br>
							<a href="{{URL::to('/').'/parallax/'.$fst->code}}" style="color:#de4a4a;">Ver más...</a>
						</div>
					</div>

					<?php $img = $fst->multimedia()->where('code', 'cover')->first(); ?>
					@if( $img != null)
						<div class="fest-right"
							 style="background: url('<?php echo URL::to('/'); ?>/{{ $fst->getImageCropImg('cover') }}') no-repeat center; background-size:cover;">
							@if(Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray()))
								<input type="hidden" value="{{$fst->code}}"/>
								<button class="button-admin"
										onclick="cropImage('sitio','{{ $fst->getImageCropId('cover') }}','{{ $fst->code }}','{{ asset($fst->getCover()) }}','{{ url('save-image-crop') }}','cover',400,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>
							@endif
						</div>
					@else
						<div class="fest-right"
							 style="background: url('<?php echo URL::to('/'); ?>/img/fiestas1.jpg') no-repeat center; background-size:cover;">
							@if(Auth::check()&& in_array('admin', Auth::user()->roles[0]->toArray()))
								<input type="hidden" value="{{$fst->code}}"/>
								<button class="button-admin"
										onclick="cropImage('sitio','{{ $fst->getImageCropId('cover') }}','{{ $fst->code }}','{{ asset($fst->getCover()) }}','{{ url('save-image-crop') }}','cover',400,200,'{{ url('add-image-multimedia') }}',550)">
									<i class="icon-crop"></i> Editar imagen
								</button>
							@endif
						</div>
					@endif


				</div>
			</section>
		@endforeach

	@endif
</div>
@if($isAdmin)

	<div class="block-admin">
		<input type="text" id="sitetextfiesta" value="" placeholder="FIESTA">
		<button id="sitenewfiesta" class="button-admin"> Crear fiesta </button>
	</div>
@endif
	<!-- Banner-->
	@include('partials.banners', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'number'=>1])


	<!-- Vive perú-->
	@include('partials.social', ['isAdmin'=>$isAdmin, 'page_partial'=>$place,'contents_partial'=>$contents,'code_text'=>'life-destiny-fiesta'])
	<!-- Comentario de página-->
	@include('partials.comments', ['isAdmin'=>$isAdmin, 'page_partial'=>$place, 'comments'=>$comments])

<script type="text/javascript" src="{{  URL::to('/')."/" }}slick/slick.min.js"></script>
<script>
	var codedestiny = "<?php echo $place->code?>";
	$(document).ready(function ()
	{
				@if(Auth::check()&& Auth::user()->roles[0]['pivot']->role_id!=3)
		var eventoslist, fiestaslist, festivaleslist;
		sortabledrag('eventslist', eventoslist);
		sortabledrag('fiestaslist', fiestaslist);
		sortabledrag('festivaleslist', festivaleslist);
		@endif
	});
	$("#savemovelistevento").click(function ()
	{
		var id = "<?php echo $place->id?>";
		var arry = [];
		var ct = 0;
		$("#eventslist > section").each(function (index)
		{
			arry[ct] = [{id: id, id_child: $(this).attr('title')}];
			ct++;

		})
		saveOrderLIst(arry, '<?php echo url("save-order-list")?>');

	});
	$("#savemovelistfiesta").click(function ()
	{
		var id = "<?php echo $place->id?>";
		var arry = [];
		var ct = 0;
		$("#fiestaslist > section").each(function (index)
		{
			arry[ct] = [{id: id, id_child: $(this).attr('title')}];
			ct++;

		})
		saveOrderLIst(arry, '<?php echo url("save-order-list")?>');

	});
	$("#savemovelistfestival").click(function ()
	{
		var id = "<?php echo $place->id?>";
		var arry = [];
		var ct = 0;
		$("#festivaleslist > section").each(function (index)
		{
			arry[ct] = [{id: id, id_child: $(this).attr('title')}];
			ct++;

		})
		saveOrderLIst(arry, '<?php echo url("save-order-list")?>');

	});


		$("#imgfront").change(function () {
			saveImage(this, 'subdestino','front','{{ url('add-image-multimedia') }}',codedestiny);
		});
</script>
<script>
	$("#sitenewevento").click(function (event)
	{
        var data_page={name: $("#sitetextevento").val(), type: 'fiesta-evento',code:$("#sitetextevento").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);

	});
	$("#sitenewfiesta").click(function (event)
	{
        var data_page={name: $("#sitetextfiesta").val(), type: 'fiesta',code:$("#sitetextfiesta").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);

	});
	$("#sitenewfestival").click(function (event)
	{
        var data_page={name: $("#sitetextfestival").val(), type: 'fiesta-festival',code:$("#sitetextfestival").val(),parent_id:'{{ $page->id}}'};
         createNewPage(event,data_page);
	});

</script>
@endsection
