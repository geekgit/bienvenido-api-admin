@extends('app')

@section('content')
<div class="column small-centered large-4 medium-5">
	<p class="text-center">
		<img src="{{ asset('img/logo.png') }}" alt="" width="200">
	</p>
	<h3>Reestablecer contraseña</h3>
	@if (count($errors) > 0)
		<div class="alert-box">
			<ul>
				@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	@endif

	<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<input type="hidden" name="token" value="{{ $token }}">

		<div class="form-group">
			<label class="col-md-4 control-label">E-Mail</label>
			<div class="col-md-6">
				<input type="email" class="form-control" name="email" value="{{ old('email') }}">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-4 control-label">Contraseña</label>
			<div class="col-md-6">
				<input type="password" class="form-control" name="password">
			</div>
		</div>

		<div>
			<label class="col-md-4 control-label">Confirmar contraseña</label>
			<input type="password" class="form-control" name="password_confirmation">
		</div>

			<p class="text-center">
				<button type="submit" class="btn btn-primary">Cambiar contraseña</button>
			</p>
			<p class="text-center">
				<a href="{{url('auth/login')}}">Volver al login</a>
			</p>
	</form>


</div>

@endsection
