@extends('app')

@section('content')
	<div>
		<div id="main-menu" ng-controller="MenuController">
			<div class="text-center">
				<img src="{{ asset('img/logo.png') }}" alt="Logo" width="80" />
			</div>
			<ul>
				<li ng-repeat="s in sections">
					<a tabindex="@{{$index}}" href="@{{s.href}}" ng-class="isSelected(s)"><i class="@{{s.icon}} main-menu-icon"></i><br/>@{{s.label}}</a>
				</li>
			</ul>
		</div>

		<!-- End main menu-->

		<div id="content">
			<div class="content-topbar">
				<div class="right toolbox">
					<span href="#profile">Hola {{ Auth::user()->name }}</span>
					<a href="{{ url('auth/logout') }}"><i class="icon-power"></i></a>
					<a href="" zf-hard-toggle="separate-actionsheet" class="icon-dots-three-vertical">
					</a>
				</div>

				<div class="toolbox hidden-for-small">
					<label for="search" class="icon-search">
						<i class="icon-magnifier"></i>
					</label>
					<input class="hidden-input" name="search" id="search" type="text" placeholder="Haz click para buscar"/>
				</div>
				<div class="show-for-small-only">
					<img src="{{ asset('img/logo.png') }}" width="65" alt="Logo"/>
				</div>
			</div>

			<!-- End top bar -->

			<div class="content-section">
				<div ng-view></div>
			</div>

		</div>
	</div>
@endsection
