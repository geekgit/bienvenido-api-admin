@extends('website/appsite')

@section('content')
	<section class="page404">
		<div class="container">
			<h1 class="text-center">¡Haz llegado a lugares inexplorados!<br><small>ERROR 404</small></h1>
			<div class="text-center">
				<i class="icon-ovni"></i>
				<p>No pudimos encontrar nada por aquí pero puedes usar el menu de arriba para navegar en nuestro sitio.</p>
			</div>
		</div>
	</section>
@endsection