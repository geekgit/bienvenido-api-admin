<div class="" ng-controller="SecurityController">
	<div class="medium-3 column">
		<div class="block-white content">
			<h4>Personas en el sistema</h4>
			<ul class="list-items">
				<li ng-repeat="u in users">
					<div class="list-controls right">
						<a href="#editar"><i class="icon-pencil"></i></a>
					</div>
					<a href="#security/{{$index}}">{{u.name}}</a>
					<span class="list-items-subtitle">{{u.email}}</span>
				</li>
			</ul>
			<p>
				<a href="#add" class="button big small-12">Agregar persona</a>
			</p>
		</div>
	</div>

	<!-- -->

	<div class=" medium-9 column" ng-hide="userID == null">
		<div class="block-white content">
			<h3>Datos de la persona</h3>
			<p><label for="name">Nombre:</label> <input type="text" ng-model="users[userID].name"/></p>
			<p><label for="email">Email:</label> <input type="text" ng-model="users[userID].email"/></p>
			<p><label for="rol">Tipo de usuario:</label>
				<select ng-model="users[userID].rol" name="rol" id="rol">
					<option value="admin">Administrador</option>
					<option value="editor">Editor</option>
					<option value="cliente">Cliente</option>
					<option value="usuario">Usuario</option>
				</select>
			</p>
		</div>
	</div>
</div>

