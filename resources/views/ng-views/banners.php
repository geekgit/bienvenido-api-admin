<div class="" ng-controller="bannerController">

	<div class="medium-3 column">
		<div class="block-white content">
			<h4>Banner disponibles</h4>
			<ul class="list-items">
				<li ng-repeat="ban in banners.data">
					<div class="list-controls right">
						<a id="{{$index}}" ng-click="editBanner5(ban)"><i class="icon-pencil"></i></a>
					</div>
					<a>{{ban.name}}</a>
					<span class="list-items-subtitle">{{ban.description}}</span>
				</li>
			</ul>
			<!--<p>
				<a href="#add" class="button big small-12">Agregar persona</a>
			</p>-->
		</div>
	</div>
	<!-- -->

	<div class=" medium-9 column" ng-show="bannerSelect">
		<div class="block-white content">
            <h3>{{editBanner.name}}</h3>

            <div class="inner-column">
                <div>
                    <div class="column medium-5">
                        <div class="input-float">
                            <label for="name">Nombre</label>
                            <input type="text" name="name" ng-model="editBanner.name"/>
                        </div>
                        <div class="input-float">
                            <label for="name">Descripción</label>
                            <input type="text" name="description" ng-model="editBanner.description"/>
                        </div>
                        <hr>
                        <!-- updaload -->
                        <input id="file" type="file" ngf-select ng-model="fileBanner" ng-click="selectBanner()">
                        <br>
                        <span class="progress" ng-show="picFile[0].progress &gt;= 0">
                            <div style="width:{{picFile[0].progress}}%" ng-bind="picFile[0].progress + '%'" class="ng-binding"></div>
                        </span>
                    </div>
                    <div class="column medium-7">
                        <div class="text-center">
                             <img ng-show="bannercreate"  ngf-src="fileBanner" ngf-default-src="" ngf-accept="'image/*'"  style="">
                             <img ng-show="banneredit" ng-src="<?php echo URL::to('/').'/'?>{{editBanner.multimedia[0].source}}"  alt=""/>
                        </div>
                        <br>
                    </div>
                </div>
            </div>

            <p class="text-center"><button ng-click="uploadBanner(fileBanner)"><i class="icon-check"></i> Guardar cambios</button></p>
            <hr/>

			<h3>Paginas Disponibles</h3>

            <label for="pages">Agregar nueva pagina</label>
            <div class="row no-margin">
                <div class="column medium-9 no-margin">
                    <select name="pages" id="pages" ng-model="selectedPage">
                    <option value="{{p.id}}" ng-repeat="p in pagesAvaliable.data">{{p.name}} - {{p.type}}</option>
                </select>
                </div>
                <div class="column medium-3">
                    <button ng-click="addPage(selectedPage)">Agregar a la lista</button>
                </div>
            </div>

			<table width="100%">
                <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Código</th>
                    <th>Tipo</th>
                    <th width="30">Acciones</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="p in editBanner.pages">
                    <td>{{p.name}}</td>
                    <td>{{p.code}}</td>
                    <td>{{p.type}}</td>
                    <td>
                        <a href="" ng-click="deletePage(p.id)"><i class="icon-trash"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>
		</div>
	</div>
</div>

