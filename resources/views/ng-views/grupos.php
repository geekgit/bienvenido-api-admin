<br/>
<div class="block-content-list" ng-controller="ChatsController">
	<div class="block-content-list-head">
		<i class="icon-bubble"></i> Lista de chats
	</div>
	<div class="block-content-list-body">
		<ul>
			<li ng-repeat="c in chats">

				<div class="item">
					<div class="column medium-11">
						<div class="inner-column">
							<img src="<?php echo asset('images/avatar.png'); ?>" width="50" align="left" alt="<?php echo Auth::user()->name; ?>"/>
							{{c.name}} <br/>
							{{c.area}}
						</div>
					</div>
					<div class="column medium-1 text-right">
						<div class="inner-column">
							<i class="icon-bubble" style="font-size: 25px;"></i>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</li>
		</ul>
	</div>
</div>

