
<div class="" ng-controller="FacebookController">
    <div id="modalImage" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
        <h4 id="modalTitle" class="text-center">Vista Previa</h4>
        <div class="text-center">
            <img src="{{preview}}">
        </div>
    </div>

    <div class="medium-12 column">
        <div class="block-white content">
            <h4>Agrega una imagen de Facebook</h4>
            <input ng-model="urlfb" type="text" placeholder="URL de facebook">
            <button ng-click="saveURL()">Guardar</button>
            <br>
            <br>
            <table width="100%">
                <thead>
                <tr>
                    <th>Ruta</th>
                    <th width="40">Acciones</th>
                </tr>
                </thead>
                <tbody>
                <tr ng-repeat="c in images.data">
                    <td>{{c.source}}</td>
                    <td>
                        <a data-reveal-id="modalImage" ng-click="previewImage(c.source)"><i class="icon-image"></i></a>
                        <a href=""><i class="icon-trash"></i></a>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </div>
</div>
