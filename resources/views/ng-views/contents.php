<div ng-controller="SectionsController" ng-init="setDefaults('destinies');">
	<div id="modalImage" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
		<h4 id="modalTitle" class="text-center">Vista Previa</h4>
		<div class="text-center">
			<img src="<?php echo URL::to ('/') ?>/{{preview}}">
		</div>
	</div>
    <div id="modalResizeImage" class="reveal-modal full" data-reveal aria-labelledby="firstModalTitle" aria-hidden="true" role="dialog">
         <a class="close-reveal-modal right"><i class="icon-close icon16"></i></a>
        <h4 id="modalTitle" class="text-center">Editar posición de imagenes</h4>

          <div class="row" style="width: 100%">
            <br>
            <!--<div class="large-3 columns no-left-margin">
                <div class="input-float">
                    <label for="name">Resolución:</label>
                    <select ng-model="formResize.size" ng-change="dragType()">
                        <option value="800x600">800x600</option>
                        <option value="1024x768">1024x768</option>
                        <option value="1152x864">1152x864</option>
                        <option value="1200x600">1200x600</option>
                        <option value="1280x720">1280x720</option>
                        <option value="1400x900">1440x900</option>
                        <option value="1600x900">1600x900</option>
                    </select>
                </div>
            </div>-->
            <div class="large-4 columns">
                <div class="input-float">
                    <label for="name">Tipo de imagen</label>
                    <select ng-model="formDrag.type" ng-change="dragType()">
                        <option value="Slider">Slider</option>
                        <!--<option value="Thumbnail">Thumbnail</option>-->
                    </select>
                </div>
            </div>

            <div class="large-5 columns">
                <div class="input-float">
                    <label for="name">Imagen:</label>
                     <!--<select ng-model="formResize.image" class="form-control" >
                        <option  ng-repeat="resize in pageEdit.data.multimedia" value="{{resize.id}}">{{resize.source}}</option>
                     </select>-->

                    <select ng-model="formResize.image" ng-options="image.name for image in pageEdit.data.multimedia">
                    </select>
                </div>
            </div>
            <div class="large-2 columns ">
                <br>
                <button ng-click="saveDrag(formResize.image)"><i class="icon-plus"></i> Guardar</button>
            </div>
        </div>
        <br>
            <div id="theparent" style="height: 300px;width: 300px ;max-width: 100%;">
                    <img sb-load="dragType()" id="thepicture"  src="<?php echo \URL::to('/').'/'?>{{formResize.image.source}}">
                <!--<img width="100px" height="100px" id="thepicture" src="http://127.0.0.1:8080/bienvenida/img/mollendo.jpg" >-->
            </div>
        <br>
        <!--<div class="text-center">
            <button ng-click="zoomIn()">Zoom +</button>
            <button ng-click="zoomOut()">Zoom -</button>
        </div>-->
    </div>

	<div class="column medium-4">
		<div class="block-white">
			<div class="content">
				<a ng-click="addPage(null, 'destino')" class="right button small"><i class="icon-plus"></i> Agregar destino</a>
				<h4>Destinos</h4>
				<div ui-tree="options">
					<ol ui-tree-nodes="" ng-model="pages.data">
						<li ng-repeat="page in pages.data" ui-tree-node collapsed="true">
							<div ui-tree-handle>
								<div class="right">
									<a ng-click="addPage(page);expand()" data-nodrag><i class="icon-plus"></i></a>
									<a ng-click="editPage(page)" data-nodrag><i class="icon-pencil"></i></a>
								</div>
								<a ng-click="toggle(this)" data-nodrag><i ng-class="{'icon-chevron-right': collapsed, 'icon-chevron-down': !collapsed}"></i></a> {{page.name}} {{(page.type=='')?'':'('+page.type+')'}}
							</div>

							<ol ui-tree-nodes="" ng-model="page.childs" ng-class="{hide: collapsed}">
								<li ng-repeat="subpage in page.childs" ui-tree-node collapsed="true">
									<div ui-tree-handle>
										<div class="right">
										<a ng-click="addPage(subpage);expand()" data-nodrag><i class="icon-plus"></i></a>
										<a ng-click="editPage(subpage)" data-nodrag><i class="icon-pencil"></i></a>
										</div>
										<a ng-click="toggle(this)" data-nodrag><i ng-class="{'icon-chevron-right': collapsed, 'icon-chevron-down': !collapsed}"></i></a> {{subpage.name}} ({{subpage.type}})
									</div>

									<!-- Segundo nivel Destinos -->

									<ol ui-tree-nodes="" ng-model="subpage.childs" ng-class="{hide: collapsed}">
										<li ng-repeat="endpage in subpage.childs" ui-tree-node collapsed="true">
											<div ui-tree-handle>
												<div class="right">
													<a ng-click="addPage(endpage);expand()" data-nodrag><i class="icon-plus"></i></a>
													<a ng-click="editPage(endpage)" data-nodrag><i class="icon-pencil"></i></a>
												</div>
												<a ng-click="toggle(this)" data-nodrag><i ng-class="{'icon-chevron-right': collapsed, 'icon-chevron-down': !collapsed}"></i></a> {{endpage.name}}
											</div>
										</li>
									</ol>

									<!-- End segundo nivel-->

								</li>
							</ol>
						</li>
					</ol>
				</div>
			</div>
		</div>
	</div>

	<!-- Editor -->

	<div class="column medium-8">
		<div class="" ng-hide="pageEdit">
			<h3>Elige un elemento de la izquierda para poder editarlo</h3>
		</div>
		<div class="content block-white" ng-show="pageEdit">
			<button class="right" ng-click="previzualizar(pageEdit.data.code,pageEdit.data.type)">Previzualizar</button>
			<h4>{{pageEdit.data.name || 'Cargando...'}}</h4>

			<!-- Start tabs -->
<tabset>
				<!-- Start detalles -->
                <tab heading="Detalles">
					<input type="hidden" name="id" ng-model="pageEdit.data.id"/>
					<div class="inner-column">
						<div class="column medium-6">
							<div class="input-float">
								<label for="name">Código</label>
								<input type="text" name="code" ng-model="pageEdit.data.code"/>
							</div>
						</div>
						<div class="column medium-6">
							<div class="input-float">
								<label for="name" class="left">Tipo</label>
                                <select class="inline" name="type" ng-model="pageEdit.data.type">
                                    <option value="">Página</option>
                                    <option value="destino">Destino</option>
                                    <option value="subdestino">Sub Destino</option>
                                    <option value="fiesta">Fiesta</option>
                                    <option value="fiesta-festival">Fiesta - Festival</option>
                                    <option value="fiesta-evento">Fiesta - Evento</option>
                                    <option value="hecho_en">Hecho en</option>
                                    <option value="hecho_engrupo">Hecho en(grupo)</option>
                                    <option value="sitio">Sitio</option>
                                    <option value="imperdible">Imperdible</option>
                                    <option value="itinerario">Itinerario</option>
                                    <option value="especial">especial</option>
                                    <option value="como_llegar-tierra-auto"> como llegar - auto</option>
                                    <option value="como_llegar-tierra-bus"> como llegar - bus</option>
                                    <option value="como_llegar-aire"> como llegar - aire</option>
                                    <option value="escala_punto"> como llegar - escala</option>
                                    <option value="sitio-compras"> - Donde comprar</option>
                                    <option value="sitio-comer"> - Donde comer</option>
                                    <option value="sitio-experto"> - Hacerte Experto</option>
                                    <option value="sitio-hospedaje-especiales">Hospedaje - Especiales</option>
                                    <option value="sitio-hospedaje-hoteles">Hospedaje - Hoteles</option>
                                    <option value="sitio-hospedaje-hostales">Hospedaje - Hostales</option>
                                    <option value="sitio-hospedaje-casa">Hospedaje - Casa</option>
                                    <option value="sitio-hospedaje-mochileros">Hospedaje - Mochileros</option>
                                </select>
							</div>
						</div>
					</div>
                    <div class="inner-column">
                        <div class="column medium-6">
                            <div class="input-float">
                                <label for="name">Nombre</label>
                                <input type="text" name="name" ng-model="pageEdit.data.name"/>
                            </div>
                        </div>
                        <div class="column medium-6">
                            <div class="input-float">
                                <label for="postal">Clima</label>
                                <input type="text" name="postal" ng-model="pageEdit.data.postalcode"/>
                            </div>
                        </div>
                    </div>
					<div class="clearfix"></div>

					<div class="input-float">
						<label for="name">Descripción</label>
						<textarea name="description" id="description" rows="4" ng-model="pageEdit.data.description"></textarea>
					</div>
					<p class="text-center">
						<button ng-click="saveSectionDetail(pageEdit.data.type)"><i class="icon-check"></i> Guardar cambios</button>
						<button ng-click="deleteSection()"><i class="icon-trash"></i> Eliminar</button>
					</p>
				</tab>

				<!-- Start contenidos -->
                <tab heading="Textos">

					<div ng-show="contentEdit">
						<div class="inner-column">
							<div class="row">
								<div class="column medium-4">
									<div class="input-float">
										<label for="name">Nombre</label>
										<input type="text" name="name" ng-model="contentEdit.name"/>
									</div>
									<div class="input-float">
										<label for="name">Código</label>
										<input type="text" name="code" ng-model="contentEdit.code"/>
									</div>
                                    <div class="input-float">
                                        <label for="name">Lenguaje</label>
                                        <select class="inline" name="type" ng-model="contentEdit.lang">
                                            <option value="es" selected>ESPAÑOL</option>
                                            <option value="en">INGLES</option>
                                        </select>
                                    </div>

								</div>
								<div class="column medium-8">
                                    <div class="input-float">
                                        <label for="content" style="margin: 2.4rem 0 0 .6rem;z-index: 1;color: black;">Contenido</label>
                                        <textarea ui-tinymce="tinymceOptions" ng-model="description" rows="4" ></textarea>
                                    </div>
								</div>
							</div>
						</div>

						<p class="text-center"><button ng-click="saveContent()"><i class="icon-check"></i> Guardar cambios</button></p>
						<hr/>
					</div>


					<h5>Contenidos disponibles</h5>



    <div class="input-float left">
        <label for="name">Lenguaje</label>
        <select class="inline" name="type" ng-model="selects.lang">
            <option value="es" selected>ESPAÑOL</option>
            <option value="en">INGLES</option>
        </select>
    </div>
    <a class="right button" ng-click="createContent()">Crear nuevo</a>


					<table width="100%">
						<thead>
						<tr>
							<th>Nombre</th>
							<th>Código</th>
							<th>HTML</th>
							<th width="30">Acciones</th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="c in pageEdit.data.contents  | filter:selects">
							<td>{{c.name}}</td>
							<td>{{c.code}}</td>
							<td>{{c.is_html?'Si':'No'}}</td>
							<td>
								<a href="" ng-click="editContent(c)"><i class="icon-pencil"></i></a>
								<a href="" ng-click="deleteContent(c.id)"><i class="icon-trash"></i></a>
							</td>
						</tr>
						</tbody>
					</table>
				</tab>

				<!-- Start multimedia -->

                <tab heading="Imágenes">

					<div ng-show="multimediaEdit">
						<div class="inner-column">
							<div>
								<div class="column medium-5">
									<div class="input-float">
										<label for="name">Código</label>
										<input type="text" name="code" ng-model="multimediaEdit.code"/>
									</div>
									<div class="input-float">
										<label for="name">Nombre</label>
										<input type="text" name="name" ng-model="multimediaEdit.name"/>
									</div>
									<div class="input-float">
										<label for="name">Descripción</label>
										<input type="text" name="description" ng-model="multimediaEdit.description"/>
									</div>
									<div class="inner-column">
										<div class="input-float column medium-6">
											<label for="name">Offset X</label>
											<input type="text" name="offsetx" ng-model="multimediaEdit.offsetx"/>
										</div>
										<div class="input-float column medium-6">
											<label for="name">Offset Y</label>
											<input type="text" name="offsety" ng-model="multimediaEdit.offsety"/>
										</div>
									</div>
									<hr>
                                    <!-- updaload -->
                                    <div class="button" ngf-select ng-model="file" ng-click="selectimage()">Elegir imagen</div>
									<span class="progress" ng-show="picFile[0].progress &gt;= 0">
										<div style="width:{{picFile[0].progress}}%" ng-bind="picFile[0].progress + '%'" class="ng-binding"></div>
									</span>


                                </div>

                                <div class="column medium-7" >
                                    <img ng-show="selectcreate"  ngf-src="file" ngf-default-src="" ngf-accept="'image/*'" class="thumb">
                                    <img ng-show="selectedit" ng-src="<?php echo URL::to('/').'/'?>{{multimediaEdit.source}}"  alt=""/>

                                </div>
							</div>
						</div>

						<p class="text-center"><button ng-click="upload(file)"><i class="icon-check"></i> Guardar cambios</button></p>
						<hr/>
					</div>

					<a class="right button" ng-click="createMultimedia()">Crear nuevo</a>
					<h5>Imágenes disponibles</h5>
					<table width="100%">
						<thead>
						<tr>
							<th>Nombre</th>
							<th>Código</th>
							<th>Ruta</th>
							<th width="40">Acciones</th>
						</tr>
						</thead>
						<tbody>
						<tr ng-repeat="c in pageEdit.data.multimedia">
							<td>{{c.name}}</td>
							<td>{{c.code}}</td>
							<td>{{c.source}}</td>
							<td><a href="" ng-click="editMultimedia(c)"><i class="icon-pencil"></i></a>
								<a href="" ng-click="deleteMultimedia(c.id)"><i class="icon-trash"></i></a>
                                <a ng-click="selector(c)" data-reveal-id="modalResizeImage"><i class="icon-image"></i></a>
                            </td>
						</tr>
						</tbody>
					</table>
				</tab>

                <tab heading="Banners">
                    <!--<div class="alert-box info">No se muestran banners para este contenido</div>-->
                    <a ng-show="!bannerEdit" class="right button" ng-click="createBanner()">Crear nuevo</a>
                    <div ng-show="bannerEdit">
                        <div class="inner-column">
                            <div>
                                <div class="column medium-5">
                                    <div class="input-float">
                                        <label for="name">Nombre</label>
                                        <input type="text" name="name" ng-model="bannerEdit.name"/>
                                    </div>
                                    <div class="input-float">
                                        <label for="name">Descripción</label>
                                        <input type="text" name="description" ng-model="bannerEdit.description"/>
                                    </div>

                                    <hr>
                                    <!-- updaload -->
                                    <input id="file" type="file" ngf-select ng-model="fileBanner" ng-click="selectBanner()">
                                    <br>
									<span class="progress" ng-show="picFile[0].progress &gt;= 0">
										<div style="width:{{picFile[0].progress}}%" ng-bind="picFile[0].progress + '%'" class="ng-binding"></div>
									</span>
                                </div>

                                <div class="column medium-7" >
                                    <img ng-show="bannercreate"  ngf-src="fileBanner" ngf-default-src="" ngf-accept="'image/*'" class="thumb">
                                    <img ng-show="banneredit" ng-src="<?php echo URL::to('/').'/'?>{{bannerEdit.multimedia[0].source}}"  alt=""/>

                                </div>
                            </div>
                        </div>

                        <p class="text-center"><button ng-click="uploadBanner(fileBanner)"><i class="icon-check"></i> Guardar cambios</button></p>
                        <hr/>
                    </div>

                    <h5>Banners disponibles</h5>
                    <table width="100%">
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Ruta</th>
                            <th width="40">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr ng-repeat="c in pageEdit.data.banners">
                            <td>{{c.name}}</td>
                            <td>{{c.description}}</td>
                            <td>{{c.multimedia[0].source}}</td>
                            <td><a ng-click="editBanner(c)" href="" ><i class="icon-pencil"></i></a>
                                <a data-reveal-id="modalImage" ng-click="previewImage(c.multimedia[0].source)"><i class="icon-image"></i></a>
                                <a href="" ><i class="icon-trash"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </tab>


                <tab heading="Ubicaciones" ng-click="actualizar()">
                    <div>
                        <div class="input-float column medium-6">
                            <label for="name">Longitud</label>
                            <input id="lat" type="text" name="" ng-model="pageEdit.latitud"/>
                        </div>
                        <div class="input-float column medium-6">
                            <label for="name">Latitud</label>
                            <input id="log" type="text" name="" ng-model="pageEdit.longitud"/>
                        </div>
                        <div  class="row content-map">
                            <div class="large-12 columns">
                                <div  id="map-canvas"></div>
                            </div>
                        </div>

                    </div>
                    <br>
                    <p class="text-center">
                        <button id="btnmap" style="display: none;" ng-click="actualizar()"><i class=""></i> Mostrar mapa</button>
                        <button ng-click="savepointmap()" ><i class="icon-check"></i> Guardar cambios</button>
                    </p>
                </tab>

				<!-- Listas -->
                <tab heading="Listas">

					<h5>Listas en la página</h5>
					<p>Las listas se clasificarán en la vista según el tipo</p>
					<div>
						<label for="pages">Agregar</label>
						<select name="pages" id="pages" ng-model="selectedPage">
							<option value="{{p.id}}" ng-repeat="p in pagesAvaliable.data">{{p.name}} - {{p.type}}</option>
						</select>
						<button ng-click="addChild(selectedPage)">Agregar a la lista</button>
					</div>
					<hr>
					<h5>Item en lista</h5>
                    <ul>
                        <li style="list-style:none;" ng-repeat="child in pageEdit.data.childs">
                            <div class="large-3 columns">
                                {{child.name}} - <span>{{child.type}}</span>
                            </div>
                            <div class="large-9 columns">
                                <a ng-click="deleteList(child.id,pageEdit.data.id)" style="margin-left: 30px"> <i class="icon-trash"></i>Eliminar</a>
                            </div>
                        </li>
                    </ul>
				</tab>
			</tabset>





		</div>

	</div>
</div>
