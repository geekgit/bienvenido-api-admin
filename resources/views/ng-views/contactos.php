<br/>
<div class="block-content-list" ng-controller="ChatsController">
	<div class="block-content-list-head">
		<label for="" class="inline-label">
			Seleccionar contacto <input type="text" placeholder="Buscar"/>
		</label>
	</div>
	<div class="block-content-list-body">
		<ul>
			<li ng-repeat="c in chats">
				<div class="item">
					<div>
						<img src="<?php echo asset('images/avatar.png'); ?>" width="50" align="left" alt="<?php echo Auth::user()->name; ?>"/>
						{{c.name}} <br/>
						{{c.area}}
					</div>
					<div class="clearfix"></div>
				</div>
			</li>
		</ul>
	</div>
</div>

