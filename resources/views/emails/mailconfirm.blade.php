<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>verificar su dirección de correo electrónico</h2>

        <div>
            Gracias por crear una cuenta con el sitio de bienvenida.
            Por favor, siga el siguiente enlace para verificar su dirección de correo electrónico
            {{ URL::to('registers/confirm/' . $data->confirmation_code) }}.<br/>
        </div>
    </body>
</html>