<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Support\Facades\Hash;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Validator;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {
//class User extends Eloquent{

	use EntrustUserTrait;
	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'last_name', 'country'];

    function isValid($input)
    {
        $rules = array('name' => 'required','password' => 'required',  'email' => 'required|email|unique:users');
        $validator = Validator::make ($input, $rules);
        return $validator;
    }
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function setPasswordAttribute($value)
	{
		if (Hash::needsRehash($value))
			$this->attributes['password'] = Hash::make($value);
		else
			$this->attributes['password'] = $value;
	}

    public function multimedia()
    {
        return $this->morphToMany('App\Multimedia', 'multimediable');
    }
}
