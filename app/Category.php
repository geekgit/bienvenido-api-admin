<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class Category extends Model
{

	protected $fillable = ['name', 'description'];


	public function destinies()
	{
		return $this->hasMany('App\Destiny');
	}
	public function categories()
	{
		return $this->hasMany('App\Category');
	}
	public function contents()
	{
		return $this->morphToMany('App\Content', 'contenteable');
	}
	
	public function multimedia()
	{
		return $this->morphToMany('App\Multimedia', 'multimediable');
	}
}
