<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class Favorite extends Model
{

    protected $fillable = ['user_id', 'page_id'];

}
