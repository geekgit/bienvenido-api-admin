<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class Content extends Model {

	protected $fillable = ['name', 'code', 'content','lang'];

	public function destinies()
	{


		return $this->morphedByMany('App\Page', 'contenteable')->where('type','=','destino');
	}

}
