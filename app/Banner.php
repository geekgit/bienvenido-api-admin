<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class Banner extends Model
{
    protected $fillable= ['name', 'description'];

    public function multimedia()
    {
        return $this->morphToMany('App\Multimedia', 'multimediable');
    }

    public function pages()
    {
        return $this->BelongsToMany('App\Page', 'banner_page');
    }
}
