<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Page extends Model
{

	protected $fillable = ['name', 'code', 'description', 'metatags', 'type','postalcode','parent_id'];

	function isValid($input)
	{
		$rules = array('name' => 'required', 'code' => 'required');
		$validator = Validator::make ($input, $rules);
		return $validator;
	}
    function  isValidPage($data)
    {
        $re=true;
        if(isset($data['id']))
        {
            $page=Page::where('code',$data['code'])->where('type',$data['type'])->where('parent_id',$data['parent_id'])->whereNotIn('id',[$this->id])->count();
        }
        else
        {
            $page=Page::where('code',$data['code'])->where('type',$data['type'])->count();
        }
        if($page>0)
        {
            $re=false;
        }
        return $re;
    }
    public function banners()
    {
        return $this->belongsToMany('App\Banner', null, 'page_id', 'banner_id');
    }


	public function childs()
	{
		return $this->hasMany('App\Page', 'parent_id')->orderBy('weight', 'asc');
	}
	public function pages()
	{
        return $this->belongsToMany('App\Page', null, 'page_id', 'child_id')->orderBy('page_page.weight', 'asc');
	}

	public function scopePlaces($query)
	{
		return $query->where('type', 'subdestino');
	}

	public function scopeImperdibles($query)
	{
		return $query->where('type', 'imperdible');
	}

	public function scopeMadeIn($query)
	{
		return $query->where('type', 'hecho_engrupo');
	}

    public function childsubication()
    {
        return $this->belongsToMany('App\Page', null, 'page_id', 'child_id')->with('ubications');
    }
	public function contents()
	{
        $array=['lat','lng','zoom'];

		return $this->morphToMany('App\Content', 'contenteable')->whereNotIn('code', $array);
	}

    public function ubications()
    {
        $array=['lat','lng','zoom'];
        return $this->morphToMany('App\Content', 'contenteable')->whereIn('code', $array);
    }
    public function parent($id=null)
    {
        if($id==null)
        {
            $parent=Page::find($this->parent_id);
        }
        else
        {
            $parent=Page::where('code','home')->first();
        }

        return $parent;
    }
	public function multimedia()
	{
		return $this->morphToMany('App\Multimedia', 'multimediable');
	}

	public function getCover($code='cover')
	{
		$cover = $this->multimedia()->where('code', $code)->first();
		if($cover == null)
			$cover = ['source' =>'img/default.jpg'];
        return $cover['source'];
	}
    public function getImageCropId($code)
    {
        $cover = $this->multimedia()->where('code', $code)->first();
        if($cover == null)
            $cover = 0;
        return $cover['id'];
    }
    public function getImageCropImg($code)
    {
        $cover = $this->multimedia()->where('code', $code)->first();
        if($cover == null)
        {
            $cover = ['source_crop' =>'img/default.jpg'];
        }
        else
        {
            if(empty($cover->source_crop))
            {
                $cover = ['source_crop' =>$cover->source];
            }
        }
        return $cover['source_crop'];
    }
    public function getFrontData($code)
    {
        $front = $this->multimedia()->where('code', $code)->first();
        return $front;
    }
    public function getIcon()
    {
        $cover = $this->multimedia()->where('code', 'icon')->first();
		if($cover == null)
			$cover = ['source' =>'img/default.jpg'];

        return $cover['source'];
    }
	public function getContent()
	{
		$text = $this->contents()->get();
		return $text;
	}
	public function getImagen()
	{
		$cover = $this->multimedia()->where('code', 'imagen1')->first();
		return $cover['source'];
	}
    public function getescala()
    {
        $cover = $this->multimedia()->where('code', 'escala')->first();
        return $cover['source'];
    }
	public function getCoverData()
	{
		$cover = $this->multimedia()->where('code', 'cover')->first();
		return $cover;
	}
	public function getDay()
	{
        $content = $this->contents()->where('code', 'dia')->first();
        if($content == null)
            $content = ['content' =>'18'];

        return $content['content'];
	}
	public function getMonth()
	{
		$content = $this->contents()->where('code', 'mes')->first();
        if($content == null)
            $content = ['content' =>'AGO'];

        return $content['content'];
	}

}
