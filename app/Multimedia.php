<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class Multimedia extends Model {

	protected $fillable = ['name', 'code', 'source', 'source_crop', 'description', 'offsetx', 'offsety','width','height'];

	public function pages()
	{
		return $this->morphedByMany('App\Page', 'multimediable');
	}
	public function getSourceAttribute($value)
	{
		if(!$value)
			$value = 'img/default.jpg';

		return $value;
	}

	public function categories()
	{
		return $this->morphedByMany('App\Category', 'multimediable');
	}
    public function users()
    {
        return $this->morphedByMany('App\User', 'multimediable');
    }
	public function destinies()
	{
		return $this->morphedByMany('App\Page', 'multimediable')->where('type','=','destino');
	}

	public function places()
	{
		return $this->morphedByMany('App\Page', 'multimediable')->where('type','=','sitio');
	}
}