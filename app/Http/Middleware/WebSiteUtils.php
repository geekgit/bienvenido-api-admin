<?php namespace App\Http\Middleware;

use App\Category;
use App\Page;
use App\Content;
use App\Multimedia;
use App\User;
use App\Favorite;
use Illuminate\Support\Facades\Auth;

/**
 * Created by PhpStorm.
 * User: GersonD
 * Date: 19/11/13
 * Time: 04:01 PM
 */
class WebSiteUtils
{
	public $status;
	public $data;
	public $pagination;

	public function __construct(Array $_data = NULL)
	{

	}

	public static function getPage($code)
	{
		$data = Page::where('code', '=', $code)->first();
		return $data;
	}

	public static function getCountry($code)
	{
		$data = Page::where('code', '=', $code)->first();
		return $data;
	}

	public static function getContents($contenteable_type, $contenteable_id)
	{
		return Content::whereHas($contenteable_type, function ($q) use ($contenteable_id) {
			$q->where('contenteable_id', '=', $contenteable_id);
		})->get();
	}

	public static function getContentsByType($contenteable_type, $contenteable_id)
	{
		if (\Session::has('my.locale')) {
			$local = \Session::get('my.locale');
		} else {
			$local = 'es';
		}
		return Content::where('lang', '=', $local)->whereHas($contenteable_type, function ($q) use ($contenteable_id) {
			$q->where('contenteable_id', '=', $contenteable_id);
		});
	}

	public static function getMultimedias($contenteable_type, $multimediable_id)
	{
		return Multimedia::whereHas($contenteable_type, function ($q) use ($multimediable_id) {
			$q->where('multimediable_id', '=', $multimediable_id);
		})->get();
	}

	public static function getMultimediasByType($contenteable_type, $multimediable_id)
	{
		return Multimedia::whereHas($contenteable_type, function ($q) use ($multimediable_id) {
			$q->where('multimediable_id', '=', $multimediable_id);
		})->get();
	}

	public static function getMultimediasByTypeAndCode($contenteable_type, $multimediable_id, $code)
	{
		return Multimedia::whereHas($contenteable_type, function ($q) use ($multimediable_id) {
			$q->where('multimediable_id', '=', $multimediable_id);
		})->where('code', '=', $code)->get();
	}


	/*
		public static function getContentsByCode($code)
		{	
			return Content::where('code',$code)->first();
		}
	*/
	public static function getMultimediasByCode($code)
	{
		return Multimedia::where('code', $code)->get();
	}

	public static function orderArrayByCode($array)
	{
		$sortArray = [];
		foreach($array as $d)
		{
			if(isset($sortArray[$d->code]))
			{
				$sortArray[$d->code] = [$sortArray[$d->code]];
				array_push($sortArray[$d->code], $d);
			}else{
				$sortArray[$d->code] = $d;
			}
		}
		return $sortArray;
	}
	/*
		public static function getContentsWithCode($contenteable_type, $contenteable_id , $code)
		{	
			return Content::whereHas($contenteable_type,function ($q) use ( $contenteable_id )
			{
				$q->where('contenteable_id', '=', $contenteable_id);
			})->where('code', '=', $code)->get();
		}
		
		public static function getMultimedias($contenteable_type, $multimediable_id)
		{
			return Multimedia::whereHas($contenteable_type,function ($q) use ( $multimediable_id )
			{
				$q->where('multimediable_id', '=', $multimediable_id);
			})->get();
		}
	
		public static function getRegion($regionCode)
		{
			return Region::where('code', $regionCode)->first();	
		}
	
		public static function getRegionsByCountry($countryId)
		{
			return Region::where('country_id',$countryId)->get();
		}
	*/
	public static function getCategory($categoryCode)
	{
		return Category::where('code', $categoryCode)->first();
	}

	public static function getSubCategories($categoryId)
	{
		return Category::where('category_id', $categoryId)->get();
	}

	/*
		public static function getDestiniesByRegion($regionId)
		{
			return Destiny::where('region_id', $regionId)->get();	
		}
	*/
	public static function getSubPages($destinyId)
	{
		return Page::where('page_id', $destinyId)->get();
	}

	/*
		public static function getDestiny($code)
		{
			return Destiny::where('code','=',$code)->first();			
		}
	
		public static function getSite($code)
		{
			return Place::where('code','=',$code)->first();			
		}
	*/
	public static function getSiteLinks()
	{
		$data = [
			'destinies' => Page::where('type', 'destino')->get(),
			'categories' => Page::where('type', 'category')->where('parent_id', '0')->get()
		];

		return $data;
	}

	public static function getDestinies()
	{
		return Page::where('type', 'destino')->get();
	}

	public static function getStaticRegiones()
	{
		return Page::where('type', 'region')->with(['multimedia' => function ($query) {
			$query->where('code', '=', 'cover');
		}])->get();
	}

	public static function getfavorite($page_id)
	{
		$actual = 2;
		if (Auth::check() && !empty($page_id)) {
			$id = Auth::user()->id;
			$favorite = Favorite::where('page_id', $page_id)->where('user_id', $id)->count();
			if ($favorite > 0) {
				$actual = 1;
			} else {
				$actual = 0;
			}
		}
		return $actual;
	}

	public static function getAvatar($id = null)
	{
		$user = User::find($id);

		if (count($user->multimedia) > 0) {

			$grav_url = url($user->multimedia[0]->source);
		} else {
			//$default = "https://i0.wp.com/developer.files.wordpress.com/2013/01/60b40db1e3946a29262eda6c78f33123.jpg";
			$size = 100;
//			$grav_url = "http://www.gravatar.com/avatar/" . md5(strtolower(trim($user->email))) . "?d=" . urlencode($default) . "&s=" . $size;
			$grav_url = "img/default.jpg";
		}
		return $grav_url;
	}

	public static function getIcons()
	{
		return ["icon-orquesta", "icon-erudito", "icon-correr", "icon-bailar", "icon-mochilero", "icon-nadar", "icon-tabla-surf", "icon-skateboard", "icon-ski", "icon-podio",
			"icon-183", "icon-buseo", "icon-cocinero", "icon-servicio", "icon-alimentos", "icon-pintor", "icon-bandera", "icon-pescar", "icon-escritorio", "icon-montar", "icon-parque", "icon-ahorrar",
			"icon-yoga", "icon-regar", "icon-celebrar", "icon-equipaje", "icon-idea", "icon-alerta", "icon-paraguas", "icon-rezar", "icon-tv", "icon-leer", "icon-viajando", "icon-tina", "icon-yoga-2",
			"icon-vigilar", "icon-volar", "icon-ovni", "icon-fuente", "icon-tsunami", "icon-tiburon", "icon-surfing", "icon-cruzar", "icon-ciclismo", "icon-basura", "icon-lluvia", "icon-adulto", "icon-trekking", "icon-desierto", "icon-descanso", "icon-oso",
			"icon-caminando", "icon-ayudar", "icon-meeting", "icon-captura", "icon-captura2", "icon-dj", "icon-danza", "icon-danza2", "icon-monja", "icon-caminando2", "icon-rock", "icon-playing-football", "icon-Mudriding", "icon-ParaGliding", "icon-WaterGliding",
			"icon-Swimming", "icon-Cycling", "icon-PlayingGolf", "icon-RiverRafting", "icon-Mountainclimbing", "icon-Abseiling", "icon-Scubamouthpiece"];
	}
	public static function getPageTypes()
	{
		return ['category', 'region', 'destino', 'subdestino','region','hecho_enperu','hecho_engrupo','hecho_en','fiesta-evento','sitio','sitio-hospedaje-hoteles','sitio-hospedaje-hostales','sitio-hospedaje-especiales','sitio-compras','sitio-comer',''];
	}
}
