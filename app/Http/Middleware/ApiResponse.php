<?php
/**
 * Created by PhpStorm.
 * User: Gerson
 * Date: 2/06/16
 * Time: 11:45 PM
 */

namespace App\Http\Middleware;


class ApiResponse
{
	public $status;
	public $data;
	public $pagination;

	public function __construct(Array $_data = NULL)
	{
		$this->status = new Status();
		if($_data != NULL)	$this->data = $_data;
		else				$this->data = array();
		$this->pagination = array();
	}

	public function setData($_data)
	{
		$this->data = $_data;
	}
}