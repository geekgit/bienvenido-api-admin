<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::controller('admin', 'HomeController');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
Route::controller('registers', 'RegisterController');
Route::group(['prefix' => 'api/v1'], function()
{
    Route::controller('users', 'Api\ApiUsersController');
    Route::controller('pages', 'Api\ApiPagesController');
    Route::controller('contents', 'Api\ApiContentsController');
    Route::controller('multimedia', 'Api\ApiMultimediaController');
   	Route::controller('comment', 'Api\ApiCommentsController');
    Route::controller('banner', 'Api\ApiBannerController');
    Route::controller('facebook', 'Api\ApiFacebookController');

});

// Opcionales
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

//Docs - Documentación del sistema
Route::group(['prefix' => 'docs'], function()
{
	Route::controller('/', 'Docs\DocsHomeController');
});

//---------------------------------------------------------------------- Dev
$routeConfig = ['namespace' => 'Barryvdh\Debugbar\Controllers', 'prefix' => '_debugbar', ];
Route::group($routeConfig, function($router) {
	Route::get('open', [
		'uses' => 'OpenHandlerController@handle',
		'as' => 'debugbar.openhandler',
	]);

	Route::get('clockwork/{id}', [
		'uses' => 'OpenHandlerController@clockwork',
		'as' => 'debugbar.clockwork',
	]);

	Route::get('assets/stylesheets', [
		'uses' => 'AssetController@css',
		'as' => 'debugbar.assets.css',
	]);

	Route::get('assets/javascript', [
		'uses' => 'AssetController@js',
		'as' => 'debugbar.assets.js',
	]);
});

//Admin
Route::any('url-exists', 'WebSiteController@urlExists');
Route::any('replace-path-img', 'AdminController@pathImg');
Route::any('add-child-code', 'AdminController@addChildCode');
Route::any('edit-multimedia', 'AdminController@contentEditableMultimedia');
Route::any('add-image-multimedia', 'AdminController@multimediaEdit');
Route::any('save-point', 'AdminController@saveMapPoint');
Route::any('content-delete-page', 'AdminController@contentDeletePage');
Route::any('desde', 'AdminController@comollegar_desde');
Route::any('delete-attach-page','AdminController@deleteAttachPage');
Route::any('attach-page', 'AdminController@contentEditablePageAttach');
Route::any('content-editable', 'AdminController@contentEditable');
Route::post('save-content', 'AdminController@save-content');
Route::any('content-editable-page', 'AdminController@contentEditablePage');
Route::any('save-favorite', 'WebSiteController@saveFavorite');
Route::post('save-avatar', 'WebSiteController@saveAvatar');
Route::post('save-order-list', 'AdminController@saveOrderList');
Route::post('save-banner','AdminController@save-banner');
Route::post('delete-img-slider','AdminController@imgSliderDelete');
Route::post('save-image-crop', 'AdminController@saveImageCrop');

//Route::get('/', 'WebSiteController@getIndex');
Route::any('change-position', 'AdminController@changePosition');

Route::group(['prefix' => 'api/v1'], function()
{
	Route::controller('users', 'Api\ApiUsersController');
	Route::controller('pages', 'Api\ApiPagesController');
	Route::controller('contents', 'Api\ApiContentsController');
	Route::controller('multimedia', 'Api\ApiMultimediaController');
	Route::controller('comment', 'Api\ApiCommentsController');
	Route::controller('banner', 'Api\ApiBannerController');
	Route::controller('facebook', 'Api\ApiFacebookController');
});

//---------------------------------------------------------------------- Website
Route::controller('/', 'WebSiteController');
Route::get('region/{tipo?}', 'WebSiteController@routerRegiones');

//Route::get('/{destino}/', 'WebSiteController@getDestino');
//Route::get('/{destino}/{subdestino?}/{sitio?}', 'WebSiteController@mainRouter');