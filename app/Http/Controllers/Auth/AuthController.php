<?php namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\Registrar;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller {
    protected $redirectTo = '';
    protected $redirectAfterLogout = '';
	/*
	|--------------------------------------------------------------------------
	| Registration & Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users, as well as the
	| authentication of existing users. By default, this controller uses
	| a simple trait to add these behaviors. Why don't you explore it?
	|
	*/

	use AuthenticatesAndRegistersUsers;

	/**
	 * Create a new authentication controller instance.
	 *
	 * @param  \Illuminate\Contracts\Auth\Guard  $auth
	 * @param  \Illuminate\Contracts\Auth\Registrar  $registrar
	 * @return void
	 */
	public function __construct(Guard $auth, Registrar $registrar)
	{
		$this->auth = $auth;
		$this->registrar = $registrar;
		$this->redirectTo = \URL::previous();
		$this->redirectAfterLogout = \URL::previous();
		$this->middleware('guest', ['except' => 'getLogout']);
	}

	public function postCreateAccount()
	{
		if(Input::has('name') && Input::has('password') && Input::has('email'))
		{
			$user = new User();
            $validator=$user->isValid(Input::all());
            $status['status']='200';
            $status['message']='Bienvenido y gracias por registrarte';
            if($validator->passes())
            {
                $user->fill(Input::all());
                $user->confirmation_code=str_random(60);
                if($user->save())
                {
                    $email=$user->email;
                    Mail::send('emails.mailconfirm', ['data' => $user], function ($message) use ($email) {
                        $message->subject('¡Gracias por registrarte! Por favor, consultar su correo electrónico');
                        $message->from('noreply@bienvenida.com', 'bienvenida');
                        $message->to(trim($email));
                    });
                    $user->attachRole(3);

                    return $status;
                }
            }else{
                $status['message']='el correo está siendo utilizado';
                $status['status']='220';
                return $status;
            }

		}else{
            $status['message']='Por favor completa los campos para registrarte';
            $status['status']='220';
            return $status;
		}
	}

}
