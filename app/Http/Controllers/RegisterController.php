<?php namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Page;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class RegisterController extends Controller {


    public function getConfirm($confirmation_code)
    {
        if( ! $confirmation_code || $confirmation_code==null)
        {
            return redirect('/');
        }
        $user = User::where('confirmation_code',$confirmation_code)->first();
        if ( ! $user)
        {
            return redirect('/');
        }
        $user->active = 1;
        $user->confirmation_code = null;
        $user->save();
        Auth::login($user);
        //Flash::message('You have successfully verified your account.');
        return redirect('registers/gallery-info');
    }
    public function getGalleryInfo()
    {
       $pages= Page::where('type', 'subdestino')->get();
        $data = [
            'pages' => $pages
        ];
        return view('website/galleryinfo', $data);
    }
    public function postSaveInformation()
    {
        $data=Input::get('lista');
        if(Auth::check())
        {
            foreach($data as $code)
            {
                $page=Page::where('code',$code)->first();
                if(count($page)>0)
                {
                    $comment=new Comment();
                    $comment->page_id=$page->id;
                    $comment->visible=0;
                    $comment->user_id=Auth::user()->id;
                    $comment->save();
                }
            }
        }
        return $data;
    }
}
