<?php namespace App\Http\Controllers\Api;

use App\Http\Middleware\ApiResponse;
use App\Http\Middleware\Status;
use App\Http\Requests\Request;
use App\Multimedia;
use App\Page;
use Illuminate\Support\Facades\Input;

class ApiMultimediaController extends ApiController
{

	public function __construct()
	{
		parent::__construct('App\Multimedia');
	}

	public function anySave($_id = null)
	{
		$this->apiResponse = new ApiResponse();
		$data = Input::all();
		$error = false;

		if (Input::hasFile('file')) {
			$ext = Input::file('file')->getClientOriginalExtension();
			$path = 'uploads/';
			$filename = date('dm') . '_' . uniqid() . '.' . $ext;
			$f = Input::file('file')->move($path, $filename);
			$source = $path . $filename;
            $size=getimagesize($source);
            if(count($size)!=0){
                $data['width']=$size[0];
                $data['height']=$size[1];
            }
		}

		if(isset($data['id']))
		{
			$m = Multimedia::find($data['id']);
			$m->fill($data);
			if(isset($source)) $m->source = $source;
			$m->save();
		}else{
			if(isset($source)) $data['source'] = $source;
			if(isset($data['page_id']))
			{
				$p = Page::find($data['page_id']);
				$m = $p->multimedia()->save(Multimedia::create($data));
			}
		}

		return response()->json($this->apiResponse);
	}
    public function anySetwidth()
    {
       $multimedia= Multimedia::where('type','image')->get();
        foreach($multimedia as $multi)
        {
            $size=getimagesize('../public/'.$multi->source);
            if(count($size)!=0){
                $multi->width=$size[0];
                $multi->height=$size[1];
                $multi->save();
            }
        }

        return response()->json($this->apiResponse);
    }
    public function anyDrag()
    {
        $data=Input::all();
        //return $data;
        if(Input::has('multimedia_id')&&Input::has('guillotine')&&Input::has('size'))
        {
            $size=explode('x',Input::get('size'));
            $multimedia = Multimedia::find(Input::get('multimedia_id'));
            //$multimedia->offsetx='{"'.$size[0].'":"'.(($data['guillotine']['x'])*-1).'"}';
			$multimedia->offsetx=0;
			$multimedia->offsety=(($data['guillotine']['y']*4.7)*-1);//'{"'.$size[1].'":"'.(($data['guillotine']['y'])*-1).'"}';
            //$multimedia->weight=$size[0];
            //$multimedia->height=$data['guillotine']['h'];
            $multimedia->save();
        }
        return response()->json($this->apiResponse);
    }
}
