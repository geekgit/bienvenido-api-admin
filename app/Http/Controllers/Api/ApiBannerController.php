<?php namespace App\Http\Controllers\Api;

use App\Banner;
use App\Page;
use App\Multimedia;
use App\Http\Middleware\ApiResponse;
use App\Http\Middleware\Status;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ApiBannerController extends ApiController{

    public function __construct()
    {
        parent::__construct('App\Banner');
    }

    public function anyGetall()
    {
        $this->apiResponse = new ApiResponse();
        if (Auth::check ()) {
            $banner=Banner::with('multimedia','pages')->get();
            $this->apiResponse->data=$banner;
        }
        return response()->json($this->apiResponse);
    }

    public function anyFind()
    {
        $this->apiResponse = new ApiResponse();
        if (Auth::check()&&Input::has('id') ) {
            $banner=Banner::where('id',Input::get('id'))->with('multimedia','pages')->get();
            if(count($banner)!=0)
                $this->apiResponse->data=$banner;
            else
                $this->apiResponse->data='No se encontraron registros.';
        }
        return response()->json($this->apiResponse);
    }

    public function anySave($id=null)
    {
        if (Auth::check ()) {
            $this->apiResponse = new ApiResponse();

            $data = Input::all();
            $error = false;

            if (Input::hasFile('file')) {
                $ext = Input::file('file')->getClientOriginalExtension();
                $path = 'uploads/banners/';
                $filename = date('dm') . '_' . uniqid() . '.' . $ext;
                $f = Input::file('file')->move($path, $filename);
                $source = $path . $filename;
            }

            if(isset($data['id']))
            {
                $banner= Banner::find($data['id']);
                $banner->fill($data);
                $banner->save();

                if(isset($source)){
                    $b = Banner::find($banner->id);
                    $b->load('multimedia');
                    $b->multimedia[0]->source=$source;
                    $b->multimedia[0]->save();
                }

            }else{
                if(isset($data['page_id']))
                {
                    $page=Page::find($data['page_id']);
                    if(count($page)!=0)
                    {
                        if(isset($source)) $data['source'] = $source;
                        $banner= new Banner();
                        $banner->fill($data);
                        $banner->save();

                        if(isset($banner))
                        {
                            $page->banners()->attach($banner->id);
                            $b = Banner::find($banner->id);
                            $m = $b->multimedia()->save(Multimedia::create($data));
                        }
                    }
                }
            }
            return response()->json($this->apiResponse);
        }
    }

    public function anySavepage()
    {
        $this->apiResponse = new ApiResponse();
        $data=Input::all();
        if (Auth::check () && Input::has('page_id') && Input::has('banner_id')) {
            $this->apiResponse = new ApiResponse();
            $banner=Banner::find(Input::get('banner_id'));
            if($banner)
            {
                $banner->pages()->detach(Input::get('page_id'));
                $banner->pages()->attach(Input::get('page_id'));
            }
        }
        return response()->json($this->apiResponse);
    }

    public function anyDeletepage()
    {
        $this->apiResponse = new ApiResponse();
        $data=Input::all();
        if (Auth::check () && Input::has('page_id') && Input::has('banner_id')) {
            $this->apiResponse = new ApiResponse();
            $banner=Banner::find(Input::get('banner_id'));
            if($banner)
            {
                $banner->pages()->detach(Input::get('page_id'));
            }
        }
        return response()->json($this->apiResponse);
    }


    public function anyImagen()
    {
        $cadena = Input::get('name');
        $longitud = strlen($cadena);
        $path='uploads/letras/';
        $width=0;
        $height=0;
        $ext='.png';

        for ($i=0; $i<=$longitud-1 ;$i++){
            $str=strtoupper($cadena{$i});
            $file=$path.$str.$ext;
            if(file_exists ($file))
            {
                $size=getimagesize($file);
                if(count($size)!=0) {
                    $width = $width + $size[0];
                    $height = $size[1];
                }
            }
        }

        //return $width;
        header("Content-Type: image/png");
        $im = imagecreatetruecolor($width, $height);
        imagefill($im, 0, 0, imagecolorallocate($im, 255, 255, 255));
        imagealphablending($im, false);
        imagesavealpha($im, true);
        $width_png=0;

        for ($i=0; $i<$longitud ;$i++){
            $str=strtoupper($cadena{$i});
            $file=$path.$str.$ext;
            if(file_exists($file))
            {
                $size=getimagesize($file);
                if(count($size)!=0){
                    $png = imagecreatefrompng($file);
                    $src  = imagecreatefrompng($file);
                    $srcw = imagesx($src);
                    $srch = imagesy($src);

                    imagecopy($im, $src, $width_png, 0, 0, 0, $srcw, $srch);
                    $width_png=$width_png+$size[1]-70;

                }
            }
        }

        $src1  = imagecreatefrompng('cristal.png');
        $srcw1 = imagesx($src1);
        $srch1 = imagesy($src1);

        header('Content-Type: image/png');
        $img = imagecreatetruecolor($srcw1, $srch1);
        imagefill($img, 0, 0, imagecolorallocate($img, 255, 255, 255));

        imagecopy($img, $src1, 0, 0, 0, 0, $srcw1, $srch1);


        $thumb = imagecreatetruecolor(300, 70);
        imagefill($thumb, 0, 0, imagecolorallocate($img, 255, 255, 255));
        imagealphablending($thumb, false);
        imagesavealpha($thumb, true);

        imagecopyresampled($thumb, $im, 0, 0, 0, 0, 300, 70, $width_png, 300);


        imagecopy($img, $thumb, 450, 900, 0, 0, 300, 70);

        imagepng($img);
        imagedestroy($img);

        /*$html =
             '<html><body>'.
             '<p>Hello World!</p><br />'.
             '<img style="border:1px solid black;" src="http://www.example.com/resize.php?hmax=300&img=9.jpg" alt="" />'.
             '</body></html>';

             if ( get_magic_quotes_gpc() )
             $html = stripslashes($html);

             $dompdf = new DOMPDF();
             $dompdf->load_html($html);
             $dompdf->render();
             $dompdf->stream("my_pdf.pdf");*/

    }
}