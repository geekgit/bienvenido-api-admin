<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\Request;
use App\Page;
use App\Http\Middleware\ApiResponse;
use Illuminate\Support\Facades\Input;


class ApiPagesController extends ApiController {

	public function __construct()
	{
        $this->middleware('auth.admin');
		parent::__construct('App\Page');
	}

	public function anySave($id = null)
	{
		$this->apiResponse = new ApiResponse();
        $entry=[];
		$data = Input::all();
		if(Input::has('id'))
		{
		    $entry = Page::find(Input::get('id'));
            $entry->fill($data);
            if($entry->isValidPage($data))
            {
                $entry->save();
            }
		}else{
			if(Input::has('parent_id'))
			{
				$parent = Page::find(Input::get('parent_id'));
                if($parent->isValidPage($data))
                {
                    $entry = $parent->childs()->save(Page::create($data));
                }
			}else{
				$entry = new Page();
                $entry->fill($data);
                if($entry->isValidPage($data))
                {
                    $entry->save();
                }
			}
		}
		$this->apiResponse->setData($entry);

		return response()->json($this->apiResponse);
	}

	public function anyAttach()
	{
		$this->apiResponse = new ApiResponse();

		if(Input::has('page_id') && Input::has('child_id'))
		{
			$page = Page::find(Input::get('page_id'));
			$page->childs()->attach(Input::get('child_id'));
			$this->apiResponse->data = [
				'child' => Input::get('child_id')
			];
		}
		return response()->json($this->apiResponse);
	}

	public function anySelect($_id = null)
	{
		$this->apiResponse = new ApiResponse();

		$error = false;

		if ($_id != null && $_id != 'pages' && $_id != 'destinies')
		{
			if (Page::where('id', $_id)->exists())
			{
				$entries = Page::with('childs','multimedia','banners', 'contents','ubications')->find($_id);
                foreach($entries->banners as $banners) {
                     $banners->load('multimedia');
                }
			} else {
				$this->apiResponse->status->code = '220';
				$this->apiResponse->status->description = 'No se encontraron registros con el id: ' . $_id;
				$error = true;
			}
		} else {
			switch($_id)
			{
				case 'pages':
					$entries = Page::where('type', '')->orWhere('type', 'region')->with('childs.childs.childs')->get();
					break;
				case 'destinies':
					$entries = Page::where('type', 'destino')->with('childs.childs.childs')->get();
					break;
				default:
					$entries = Page::with('childs.childs.childs')->get();
					break;
			}
		}

		if (!$error) {
			$this->apiResponse->data = $entries;
		}
		return response()->json($this->apiResponse);
	}


	public function postDeleteList()
	{
		$this->apiResponse = new ApiResponse();
		if(Input::has('child_id')&&Input::has('page_id'))
		{
			$page_list=\DB::table('page_page')->where('page_id',Input::get('page_id'))->where('child_id',Input::get('child_id'))->get();
			if(count($page_list)>0){
				$delete=\DB::table('page_page')->where('page_id',Input::get('page_id'))->where('child_id',Input::get('child_id'))->delete();
			}
			else{
				$this->apiResponse->status->code="220";
				$this->apiResponse->status->description="El registro no existe";
			}
		}
		if($delete!=true)
		{
			$this->apiResponse->status->code="220";
			$this->apiResponse->status->description="Ocurrio un error al intentar eliminar el registro";
		}
		return response()->json($this->apiResponse);
	}

}
