<?php namespace App\Http\Controllers\Api;

use App\Page;
use App\Comment;
use App\Http\Middleware\ApiResponse;
use App\Http\Middleware\Status;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Auth;

	
class ApiCommentsController extends ApiController{

	public function __construct()
	{
		parent::__construct('App\Comment');
	}

	public function anySave($_id = null)
	{
		if(Auth::check()){
			$this->apiResponse = new ApiResponse();

			$data = Input::all();
			$comment = null;

			if(isset($data['id']) && isset($data['title']) && isset($data['comment'])){
				$comment['page_id'] = $data['id'];
				$comment['user_id'] = Auth::user()->id;
				$comment['title'] = $data['title'];
				$comment['comment'] = $data['comment'];	
				$entry = new Comment();
				$entry->fill($comment);
				$entry->save();
			}

			$this->apiResponse->setData($comment);

			return redirect()->back()->withMessage('Comment saved!');			
		}else{
			return redirect()->guest('auth/register');			
		}

	}
}

?>