<?php namespace App\Http\Controllers\Api;

use App\Content;
use App\Http\Middleware\ApiResponse;
use App\Http\Middleware\Status;
use App\Http\Requests\Request;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;
use App\Region;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ApiContentsController extends ApiController
{

	public function __construct()
	{
        $this->middleware('auth.admin');
		parent::__construct('App\Content');
	}

	public function anySave($_id = null)
	{
		$this->apiResponse = new ApiResponse();
		$data = Input::all();
        if (\Session::has('my.locale')) {
            $local = \Session::get('my.locale');
        } else {
            $local = 'es';
        }
        if (Input::get('type') == 'name') {
            $data['name'] = trim(Input::get('text'));
        } else {
            $data['content'] = trim(Input::get('text'));
        }
        $page = Page::find($data['page_id']);
        $contents = $page->contents()->where('code', Input::get('code'))->where('lang', $local)->orderBy('id', 'desc')->first();
        if(count($contents)>0)
        {
            $data['id']=$contents->id;
        }
		if(isset($data['id']))
		{
			$entrie = Content::find($data['id']);
			$entrie->fill($data);
			$entrie->save();
		}else{
			if(isset($data['page_id']))
			{
				$p = Page::find($data['page_id']);
				$entrie = $p->contents()->save(Content::create($data));
			}
		}

		$this->apiResponse->setData($entrie);

		return response()->json($this->apiResponse);
	}
    public function anySavemap()
    {
        $this->apiResponse = new ApiResponse();

        if(Input::has('pageId'))
        {
            $lng=Input::get('lng');
            $lat=Input::get('lat');
            $zoom=Input::get('zoom');
            $data = [];
            $data['code']='lat';
            $data['name']='latitud';
            $data['content']=$lat;
            $data['page_id']=Input::get('pageId');
            $data2['page_id']=Input::get('pageId');
            $data2['code']='lng';
            $data2['name']='longitud';
            $data2['content']=$lng;
            $data3['page_id']=Input::get('pageId');
            $data3['code']='zoom';
            $data3['name']='zoom';
            $data3['content']=$zoom;
            $id=Input::get('pageId');
            $page=Page::find($id);
           $page->load('ubications');
            if(count($page->ubications)>0)
            {

                $entrie = Content::find($page->ubications[0]->id);
                $entrie2 = Content::find($page->ubications[1]->id);
                $entrie2->fill($data2);
                $entrie2->save();
                    $entrie->fill($data);
                    $entrie->save();
                if(count($page->ubications)>2)
                {
                    $entrie3 = Content::find($page->ubications[2]->id);
                    $entrie3->fill($data3);
                    $entrie3->save();
                }
                else
                {
                    $p = Page::find($data['page_id']);
                    $entrie3 = $p->contents()->save(Content::create($data3));
                }

            }
            else{
                if(isset($data['page_id']))
                {
                    $p = Page::find($data['page_id']);
                    $entrie = $p->contents()->save(Content::create($data));
                    $entrie2 = $p->contents()->save(Content::create($data2));
                    $entrie3 = $p->contents()->save(Content::create($data3));
                }
            }


            $this->apiResponse->setData($entrie);

            return response()->json($this->apiResponse);
        }

    }
}
