<?php namespace App\Http\Controllers\Api;

use App\Http\Middleware\ApiResponse;
use App\Http\Middleware\Status;
use App\Http\Requests\Request;
use App\Multimedia;
use App\Page;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use PhpParser\Node\Expr\AssignOp\Mul;
use Facebook\FacebookSession;
use Facebook\FacebookRequest;
use Facebook\FacebookRedirectLoginHelper;

class ApiFacebookController extends ApiController
{
    public function __construct()
    {
        parent::__construct('App\Multimedia');

    }

    public function anyImages()
    {
        $this->apiResponse = new ApiResponse();
        $multimedia=Multimedia::where('type','facebook')->get();
        $this->apiResponse->data=$multimedia;

        return response()->json($this->apiResponse);
    }

    public function anyImageurl()
    {
        $this->apiResponse = new ApiResponse();
        //$url='https://www.facebook.com/KalimbaOficial/photos/a.724807074236844.1073741825.130187067032184/1025818724135676/?type=3&theater';
        //$url='https://www.facebook.com/NASA/photos/a.67899501771.69169.54971236771/10153624731361772/?type=3&theater';
        if(Input::has('url'))
        {
            $url=Input::get('url');
            $url_image = str_replace("https://www.facebook.com/", "", $url);
            $rpta=explode("/", $url_image);
            $id_image= $rpta[3];
            if(is_numeric($id_image))
            {
                $url = 'https://graph.facebook.com/'.$id_image.'/picture';
                $rray=get_headers($url);
                $hd = $rray[6];
                $path_fb=(substr($hd,strpos($hd,'http')));
                $multimedia= new Multimedia();
                $path_fb=str_replace("s720x720", "s320x320", $path_fb);
                $multimedia->source=$path_fb;
                $multimedia->type='facebook';
                $multimedia->save();
            }
            else{
                $this->apiResponse->status->code="220";
                $this->apiResponse->status->description="No se encontro ninguna imagen en la URL,es posible que no tenga el formato adecuado";
            }
        }
        else{
            $this->apiResponse->status->code="220";
            $this->apiResponse->status->description="No se ingreso ninguna URL o no es valida";
        }
        return response()->json($this->apiResponse);
    }

    //BUSCA LAS IMAGES DE UN ALBUM DE FACEBOOK
    public function anyAlbums()
    {
        $this->apiResponse = new ApiResponse();
        //Maximo de fotos
        $max_photos=2;

        //Album ID
        $album_id='904549079594126';

        FacebookSession::setDefaultApplication('1566621556960269', '406734c6915019cf9f66a1e66c9d3df9');
        //$session = new FacebookSession('1566621556960269|XVJwLY7FcW9d-wLdfU58zP_pwBo');
        $session = FacebookSession::newAppSession();
        try {
            $session->validate();
            $request = new FacebookRequest(
                $session,
                'GET',
                '/'.$album_id.'/photos'
            );
            $response = $request->execute();
            $data=json_decode($response->getRawResponse());
            $count_data=count($data->data);
            $count_data--;
            $keys=array();

            $count=0;
            //Funcion random de indices
            while(!array_key_exists($max_photos,$keys))
            {
                $rand=rand(0,$count_data);
                if(!(in_array(strval($rand),$keys))){
                    //return var_dump($rand);
                    $keys[$count]=strval($rand);
                    $count++;
                }
            }
            $photos=array();
            //return var_dump($data->data[0]);
            for($x=0;$x<count($keys);$x++)
            {
                $photos[$x]=$data->data[$keys[$x]];
            }
            $this->apiResponse->data=$photos;
        } catch (FacebookRequestException $ex) {
            $this->apiResponse->status->code="220";
            $this->apiResponse->status->description=$ex->getMessage();
        } catch (\Exception $ex) {
            $this->apiResponse->status->code="220";
            $this->apiResponse->status->description=$ex->getMessage();
        }
        return ($this->apiResponse);

    }
}