<?php namespace App\Http\Controllers;

use App\Banner;

use App\Http\Middleware\ApiResponse;
use App\Http\Middleware\Status;
use App\Http\Requests\Request;
use App\Multimedia;
use App\Page;
use Illuminate\Support\Facades\Request as RequestFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Gregwar\Image\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use App\Content;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.admin');
    }



    public function myLocale()
    {
        if (\Session::has('my.locale')) {
            $local = \Session::get('my.locale');
        } else {
            $local = 'es';
        }
        return $local;
    }
    public function saveImageCrop()
    {
        $data = [];
        $id = Input::get('image_id');


        if (!empty($id)) {
            $m = Multimedia::find($id);

            if (!empty($m->source_crop) && $m->source_crop!=$m->source) {
                if(file_exists($m->source_crop))
                    unlink($m->source_crop);
            }

            $img = Image::open($m->source);
            $path = 'uploads/cache/';
            $filename = date('dm') . '_' . uniqid() .'.' . $img->guessType();
            $source = $path . $filename;

            $x = Input::get('x1');
            $y = Input::get('y1');
            $w = Input::get('x2') - Input::get('x1');
            $h = Input::get('y2') - Input::get('y1');

            $img->crop( $x, $y, $w, $h);
            $img->cropResize(Input::get('max-height'));
            $img->save($source, $img->guessType(), 100);
            $m->source_crop = $source;
            $m->save();

            /*if (Input::has('file')) {

                $file = Input::get('file');
                list($type, $file) = explode(';', $file);
                list(, $file) = explode(',', $file);
                $file = base64_decode($file);

                $path = 'uploads/';
                $filename = date('dm') . '_' . uniqid() . '.jpg';
                $source = $path . $filename;
                file_put_contents($source, $file);

                if (!empty($m->source_crop)) {
                    if(file_exists($m->source_crop))
                        unlink($m->source_crop);
                }
                $img = Image::make($source);
//				$img = \Intervention\Image\Image::make($source);
                $img->resize(Input::get('max-height'), null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save(null, 100);
                $m->source_crop = $source;
                $m->save();
            }*/
        }

        return $data;
    }

    public function saveContent(Request $request)
    {
        if($request->has('id'))
        {
            $content = Content::find($request->get('id'));
        }
    }

    public function contentEditable()
    {
        $local = $this->myLocale();
        $data = [];

        if (Input::get('page') == 'sitio') {
            $array = ['hecho_en', 'sitio-compras', 'sitio-comer', 'sitio-hospedaje-especiales', 'sitio-hospedaje-casa', 'sitio-hospedaje-hoteles', 'sitio-hospedaje-hostales', 'sitio-hospedaje-mochileros', 'sitio', 'fiesta', 'fiesta-festival', 'fiesta-evento'];
            $destiny = Page::where('code', Input::get('codedestiny'))->whereIn('type', $array)->first();
        } else {
            $destiny = Page::where('code', Input::get('codedestiny'))->where('type', Input::get('page'))->first();
        }
        $contents = $destiny->contents()->where('code', Input::get('code'))->where('lang', $local)->get();

        $data['name'] = 'nuevo';
        $data['content'] = 'nuevo';
        $data['lang'] = $local;
        $data['code'] = Input::get('code');
        if (count($contents) == 0) {

            if (Input::get('type') == 'name') {
                $data['name'] = trim(Input::get('content'));
            } else {
                $data['content'] = trim(Input::get('content'));
            }
            if (isset($destiny->id)) {
                $p = Page::find($destiny->id);
                $entrie = $p->contents()->save(Content::create($data));
            }
        } else {
            foreach ($contents as $content) {
                if ($content->lang == $local) {
                    if (Input::get('type') == 'name') {
                        $data['name'] = trim(Input::get('content'));
                        $data['content'] = $content->content;
                    } else {
                        $data['name'] = $content->name;
                        $data['content'] = trim(Input::get('content'));
                    }
                    $entrie = Content::find($content->id);
                    $entrie->fill($data);
                    $entrie->save();
                }
            }
        }

        return $contents;
    }

    public function contentEditablePageAttach()
    {

        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            $page_destiny = Page::where('code', Input::get('page_code'))->first();
            $child = Page::where('code', Input::get('child_code'))->first();
            if (count($page_destiny) > 0 && count($child) > 0) {
                $page = Page::find($page_destiny->id);
                $page->pages()->attach($child->id);

            }
        }
        return;
    }

    public function contentEditablePage()
    {
        $data = [];

        $caracteres = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $numerodeletras = 6;
        $cadena = "";
        for ($i = 0; $i < $numerodeletras; $i++) {
            $cadena .= substr($caracteres, rand(0, strlen($caracteres)), 1);
        }

        $data['name'] = Input::get('name');

        $type=Input::get('type');

        if(!empty($type))
        {
            $data['code'] = $cadena . '-' . Input::get('type') . '-' . str_replace(' ', '', Input::get('name'));
        }

        if (Input::has('description')) {
            $data['description'] = Input::get('description');
        } else {
            $data['description'] = '';
        }
        if (Input::has('postalcode')) {
            $data['postalcode'] = Input::get('postalcode');
        } else {
            $data['postalcode'] = '';
        }
        if (Input::has('id')) {
            $data['id'] = Input::get('id');
        }

        $data['type'] = Input::get('type');
        $name = Input::get('name');
        if (!empty($name)) {
            $local = $this->myLocale();


            if (Input::has('codedestiny')) {
                $destiny = Page::where('code', Input::get('codedestiny'))->where('type', Input::get('typefather'))->first();
                $data['page_id'] = $destiny->id;
            }

            //

            //$contents=$destiny->contents()->where('code',Input::get('code'))->where('lang',$local)->get();

            if (isset($data['id'])) {
                $entry = Page::find($data['id']);
                $entry->fill($data);
                $entry->save();
            } else {
                if (isset($data['page_id'])) {
                    $entry = Page::find($data['page_id']);
                    $data['parent_id'] = $data['page_id'];
                    $entry->childs()->save(Page::create($data));
                } else {
                    $entry = new Page();
                    $entry->fill($data);
                    $entry->save();
                }
            }
        }
        return $data;
    }

    public function contentDeletePage()
    {
        $id = '';
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            if (Input::has('id')) {
                $id = Input::get('id');
                $page = Page::find($id);
                $page->delete();
                $children_page=Page::where('parent_id',$id)->delete();
            }
        }
        return $id;
    }

    public function deleteAttachPage()
    {
        $r = new ApiResponse();
        $delete = false;
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            if (Input::has('child_id') && Input::has('page_code')) {
                $page = Page::where('code', Input::get('page_code'))->first();
                $page_list = \DB::table('page_page')->where('page_id', $page->id)->where('child_id', Input::get('child_id'))->get();
                if (count($page_list) > 0) {
                    $delete = \DB::table('page_page')->where('page_id', $page->id)->where('child_id', Input::get('child_id'))->delete();
                } else {
                    $r->status->code = "220";
                    $r->status->description = "El registro no existe";
                }
            }
            if ($delete != true) {
                $r->status->code = "220";
                $r->status->description = "Ocurrio un error al intentar eliminar el registro";
            }
        }
        return response()->json($r);
    }

    public function multimediaEdit()
    {

        $data = [];
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            /*  if (Input::has('file')) {
                $file= $_POST['file'];
                list($type, $file) = explode(';', $file);
                list(, $file)      = explode(',', $file);
                $file = base64_decode($file);
              //  $f = finfo_open();
              //  $mime_type = finfo_buffer($f, $file, FILEINFO_MIME_TYPE);

                $path = 'uploads/';
                $filename = date('dm') . '_' . uniqid() . '.' . $ext;

                file_put_contents($path.$filename, $file);
                $source = $path . $filename;
                $size = getimagesize($source);
                if (count($size) != 0) {
                    $data['width'] = $size[0];
                    $data['height'] = $size[1];
                }
            }*/
            if (Input::hasFile('file')) {

                $ext = Input::file('file')->getClientOriginalExtension();
                $path = 'uploads/';
                $path_original='originales/';
                $filename = date('dm') . '_' . uniqid() . '.' . $ext;
                $file = RequestFile::file('file');
                Storage::disk('local')->put("originales/$filename", File::get($file));
                if(Input::file('file')->move($path, $filename))
                {
                    $source = $path . $filename;
                    $img = Image::open($path.$filename);
                    $img->cropResize(2000);
                    $img->save($source, $img->guessType(), 100);
                    $size = getimagesize($source);
                    if (count($size) != 0) {
                        $data['width'] = $size[0];
                        $data['height'] = $size[1];
                    }

                }


            }
            if (Input::get('type') == "sitio") {
                $array = ['hecho_en', 'sitio-compras', 'sitio-comer', 'sitio-hospedaje-especiales', 'sitio-hospedaje-casa', 'sitio-hospedaje-hoteles', 'sitio-hospedaje-hostales', 'sitio-hospedaje-mochileros', 'sitio', 'fiesta', 'fiesta-festival', 'fiesta-evento'];
                $destiny = Page::where('code', Input::get('code'))->whereIn('type', $array)->first();
            } else {
                $destiny = Page::where('code', Input::get('code'))->where('type', Input::get('type'))->first();
            }


            $id = '';
            switch (Input::get('codechild')) {
                case 'cover':
                    $img = $destiny->getCoverData();
                    if (count($img) > 0) {
                        $id = $img->id;
                    }
                    break;
                case 'slider1':

                    break;
                case 'img':

                    break;
                default:
                    $img = $destiny->getFrontData(Input::get('codechild'));
                    if (count($img) > 0) {
                        $id = $img->id;
                    }
                    break;
            }

            $data['name'] = 'nuevo';
            $data['description'] = 'nuevo';
            $data['code'] = Input::get('codechild');
            $data['offsetx']=0;
            $data['offsety']=0;
            if (!empty($id)) {
                $m = Multimedia::find($id);

                $m->fill($data);
                if (isset($source)) {
                    if (!empty($m->source)) {
                        if(file_exists($m->source))
                        {
                            unlink($m->source);
                            $path_imgori=explode('/',$m->source);
                            if(Storage::disk('local')->exists("originales/$path_imgori[1]"))
                            {
                                Storage::delete("originales/$path_imgori[1]");
                            }
                        }

                    }
                    $m->source = $source;
                    $m->source_crop = $source;
                }
                $m->save();
            } else {
                if (isset($source)) {
                    $data['source'] = $source;
                    $data['source_crop'] = $source;
                }
                if (count($destiny) > 0) {
                    $data['page_id'] = $destiny->id;
                    $p = Page::find($data['page_id']);
                    $m = $p->multimedia()->save(Multimedia::create($data));
                }
            }
        }
        return $data;
    }

    public function contentEditableMultimedia()
    {


        $data = [];
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            if (Input::hasFile('file')) {

                $ext = Input::file('file')->getClientOriginalExtension();
                $path = 'uploads/';
                $filename = date('dm') . '_' . uniqid() . '.' . $ext;
                $f = Input::file('file')->move($path, $filename);
                $source = $path . $filename;
                $size = getimagesize($source);
                if (count($size) != 0) {
                    $data['width'] = $size[0];
                    $data['height'] = $size[1];
                }
            }

            $id = '';
            if (Input::has('id')) {
                $id = Input::get('id');
            }
            $data['name'] = Input::get('name');
            $data['description'] = Input::get('description');
            $data['code'] = Input::get('code');
            if (!empty($id)) {
                $m = Multimedia::find($id);
                $m->fill($data);
                if (isset($source)){
                    if (!empty($m->source)) {
                        if(file_exists($m->source))
                            unlink($m->source);
                    }
                    $m->source = $source;
                }
                $m->save();
            } else {
                if (isset($source)) $data['source'] = $source;
                if (isset($data['page_id'])) {
                    $p = Page::find($data['page_id']);
                    $m = $p->multimedia()->save(Multimedia::create($data));
                }
            }
        }
        return $data;
        //return response()->json($this->apiResponse);
    }

    public function saveMapPoint()
    {
        $data = [];
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            if (Input::has('pageId')) {
                $lng = Input::get('lng');
                $lat = Input::get('lat');
                $zoom = Input::get('zoom');

                $data['code'] = 'lat';
                $data['name'] = 'latitud';
                $data['content'] = $lat;
                $data['page_id'] = Input::get('pageId');
                $data2['page_id'] = Input::get('pageId');
                $data2['code'] = 'lng';
                $data2['name'] = 'longitud';
                $data2['content'] = $lng;
                $data3['page_id'] = Input::get('pageId');
                $data3['code'] = 'zoom';
                $data3['name'] = 'zoom';
                $data3['content'] = $zoom;
                $id = Input::get('pageId');
                $page = Page::find($id);
                $page->load('ubications');
                if (count($page->ubications) > 0) {

                    $entrie = Content::find($page->ubications[0]->id);
                    $entrie2 = Content::find($page->ubications[1]->id);
                    $entrie2->fill($data2);
                    $entrie2->save();
                    $entrie->fill($data);
                    $entrie->save();
                    if (count($page->ubications) > 2) {
                        $entrie3 = Content::find($page->ubications[2]->id);
                        $entrie3->fill($data3);
                        $entrie3->save();
                    } else {
                        $p = Page::find($data['page_id']);
                        $entrie3 = $p->contents()->save(Content::create($data3));
                    }

                } else {
                    if (isset($data['page_id'])) {
                        $p = Page::find($data['page_id']);
                        $entrie = $p->contents()->save(Content::create($data));
                        $entrie2 = $p->contents()->save(Content::create($data2));
                        $entrie3 = $p->contents()->save(Content::create($data3));
                    }
                }
            }
        }
        return $data2;
    }

    public function changePosition()
    {
        $data = [];
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            if (Input::get('type') == "sitio") {
                $array = ['hecho_en', 'sitio-compras', 'sitio-comer', 'sitio-hospedaje-especiales', 'sitio-hospedaje-casa', 'sitio-hospedaje-hoteles', 'sitio-hospedaje-hostales', 'sitio-hospedaje-mochileros', 'sitio', 'fiesta', 'fiesta-festival', 'fiesta-evento'];
                $destiny = Page::where('code', Input::get('code'))->whereIn('type', $array)->first();
            } else {
                $destiny = Page::where('code', Input::get('code'))->where('type', Input::get('type'))->first();
            }

            $code = Input::get('codechild');
            $id = '';
            switch ($code) {
                case 'cover':
                    $img = $destiny->getCoverData();
                    if (count($img) > 0) {
                        $id = $img->id;
                    }
                    break;
                case 'slider1':
                    $id=Input::get('sliderid');
                    break;
                default:
                    $img = $destiny->getFrontData($code);
                    if (count($img) > 0) {
                        $id = $img->id;
                    }
                    break;
            }

            if (!empty($id)) {
                if (Input::has('file')) {
                    $file= $_POST['file'];
                    list($type, $file) = explode(';', $file);
                    list(, $file)      = explode(',', $file);
                    $file = base64_decode($file);
                    $m = Multimedia::find($id);
                    if (!empty($m->source)) {
                        if(file_exists($m->source))
                            unlink($m->source);
                    }
                    $filename =  $m->source;
                    file_put_contents($filename, $file);

                    $size = getimagesize($filename);
                    if (count($size) != 0) {
                        $data['width'] = $size[0];
                        $data['height'] = $size[1];
                    }
                    $m->width=$data['width'];
                    $m->height=$data['height'];
                    $m->source=$filename;
                    $m->save();
                }
            }
        }
        return $data;
    }
//funcion nueva para guardar imagen cortada

    public function saveBanner()
    {
        $data = [];
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            if (Input::has('file')) {
                $file= $_POST['file'];
                list($type, $file) = explode(';', $file);
                list(, $file)      = explode(',', $file);
                $file = base64_decode($file);
                $f = finfo_open();
                $mime_type = finfo_buffer($f, $file, FILEINFO_MIME_TYPE);
                $ext = $mime_type;
                $path = 'uploads/';
                $filename = date('dm') . '_' . uniqid() . '.' . 'jpg';

                file_put_contents($path.$filename, $file);
                $source = $path . $filename;
                $size = getimagesize($source);
                if (count($size) != 0) {
                    $data['width'] = $size[0];
                    $data['height'] = $size[1];
                }
            }
            if (Input::hasFile('file')) {

                $ext = Input::file('file')->getClientOriginalExtension();
                $path = 'uploads/';
                $filename = date('dm') . '_' . uniqid() . '.' . $ext;
                $file = Request::file('file');
                Storage::disk('local')->put("originales/$filename", File::get($file));
                if(Input::file('file')->move($path, $filename))
                {
                    $source = $path . $filename;
                    $img = Image::open($path.$filename);
                    $img->cropResize(2000);
                    $img->save($source, $img->guessType(), 100);
                    $size = getimagesize($source);
                    if (count($size) != 0) {
                        $data['width'] = $size[0];
                        $data['height'] = $size[1];
                    }

                }


            }
            if (Input::get('type') == "sitio") {
                $array = ['hecho_en', 'sitio-compras', 'sitio-comer', 'sitio-hospedaje-especiales', 'sitio-hospedaje-casa', 'sitio-hospedaje-hoteles', 'sitio-hospedaje-hostales', 'sitio-hospedaje-mochileros', 'sitio', 'fiesta', 'fiesta-festival', 'fiesta-evento'];
                $destiny = Page::where('code', Input::get('code'))->whereIn('type', $array)->first();
            } else {
                $destiny = Page::where('code', Input::get('code'))->where('type', Input::get('type'))->first();
            }
            $id = '';
            if(Input::has('id'))
            {
                $id=Input::get('id');
            }
            $data['name'] = 'nuevo';
            $data['description'] = 'nuevo';
            if (!empty($id)) {
                $m = Multimedia::find($id);
                if(isset($source)){

                    if (!empty($m->source)) {
                        if(file_exists($m->source))
                        {
                            unlink($m->source);
                            $path_imgori=explode('/',$m->source);

                            if(Storage::disk('local')->exists("originales/$path_imgori[1]"))
                            {
                                Storage::delete("originales/$path_imgori[1]");
                            }
                        }
                    }
                    $m->source=$source;
                    $m->source_crop=$source;
                    $m->save();
                }
            } else{
                if (count($destiny) > 0) {
                    $data['page_id'] = $destiny->id;
                    $page=Page::find($data['page_id']);
                    if(count($page)!=0)
                    {
                        if(isset($source)){
                            $data['source'] = $source;
                            $data['source_crop'] = $source;}
                        $banner= new Banner();
                        $banner->fill($data);
                        $banner->save();
                        if(isset($banner))
                        {
                            $page->banners()->attach($banner->id);
                            $b = Banner::find($banner->id);
                            $m = $b->multimedia()->save(Multimedia::create($data));
                        }
                    }
                }
            }
        }
        return $data;
    }
    public function saveOrderList()
    {
        $list='';
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            $list = Input::get('list');
            $count = 0;

            foreach ($list as $item) {
                $page=Page::find($item[0]['id_child']);
                if($page->parent_id==$item[0]['id'])
                {
                    $page->weight=$count;
                    $page->save();
                }
                DB::table('page_page')
                    ->where('page_id', $item[0]['id'])->where('child_id', $item[0]['id_child'])
                    ->update(['weight' => $count]);
                $count++;
            }
        }
        return $list;
    }
    public function imgSliderDelete()
    {
        $id='';
        if (Auth::check() && in_array('admin', Auth::user()->roles[0]->toArray())) {
            if(Input::has('id'))
            {
                $id=Input::get('id');
                Multimedia::destroy($id);

            }
        }
        return $id;
    }
    //plugins
    public function addChildCode()
    {
        $x=4;
        while($x<5)
        {
            $pages='';
            switch($x)
            {
                case 1:
                    $pages=Page::orderBy('id','DESC')->where('type','destino')->get();
                    break;
                case 2:
                    $pages=Page::orderBy('id','DESC')->where('type','subdestino')->get();
                    break;
                case 3:
                    $pages=Page::orderBy('id','DESC')->where('type','hecho_engrupo')->get();
                    break;
                case 4:
                    $pages=Page::orderBy('id','DESC')->where('type','category')->get();
                    break;
            }
            foreach($pages as $page)
            {
                $childs=  $page->childs()->where('type', 'category')->get();
                foreach($childs as $child)
                {
                    $child->parent_id=$page->id;
                    $child->save();
                }
            }
            $x++;
        }
        return $pages;
    }
    public function pathImg()
    {
        $multimedias=Multimedia::all();
        foreach($multimedias as $multimedia)
        {
            $source=$multimedia->source;
            $multimedia->source=str_replace('img','uploads',$source);
            $multimedia->save();
        }
    }
}
