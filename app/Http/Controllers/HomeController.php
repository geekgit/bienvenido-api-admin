<?php namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth.admin');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function getIndex()
	{
		return view('home');
	}

	public function getDashboard()
	{
		return view('ng-views.dashboard');
	}

	public function getPages()
	{
		return view('ng-views.pages');
	}

	public function getContent()
	{
		return view('ng-views.contents');
	}

	public function getBanners()
	{
		return view('ng-views.banners');
	}

	public function getSecurity()
	{
		return view('ng-views.security');
	}

	public function getConfig()
	{
		return view('ng-views.dashboard');
	}

	public function getImagesfb()
	{
		return view('ng-views.image_facebook');
	}

}
