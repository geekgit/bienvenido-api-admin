<?php namespace App\Http\Controllers\Docs;

use App\Http\Controllers\Controller;

class DocsHomeController extends Controller
{

	public function getIndex()
	{
		return view('docs');
	}
}