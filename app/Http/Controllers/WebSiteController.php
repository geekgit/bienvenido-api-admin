<?php namespace App\Http\Controllers;

use App\Http\Middleware\WebSiteUtils;
use App\Multimedia;
use App\Favorite;
use App\Page;
use App\User;
use Illuminate\Support\Facades\Mail;
use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Gregwar\Image\Image;
use Illuminate\Support\Facades\Session;


class WebSiteController extends Controller
{


	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are \Authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	public function __construct()
	{
		//$this->middleware('guest');
	}

	public function anyIdioma()
	{
		$_idioma = Input::get('lenguaje');
		switch ($_idioma) {
			case 'en':
				Session::put('my.locale', "en");
				break;
			default:
				Session::put('my.locale', "es");
				break;
		}
		return $_idioma;
	}
  /*  public function getSendMail()
    {
        $emails=['c.rodriguez.villavicencio@gmail.com','gersonm17@gmail.com'];
        Mail::send('emails.mailin', ['data' => $emails], function ($message) use ($emails) {
            $message->subject('Soluciones Audiovisuales Brandea');
            $message->from('audiovisual@brandea.pe', 'BRANDEA');
            $message->replyTo('ventas@brandea.pe', 'BRANDEA');
            $message->to('brandea@geekadvice.pe');
            $swiftMessage = $message->getSwiftMessage();
            $headers = $swiftMessage->getHeaders();
            $headers->addTextHeader('X-Mailgun-Deliver-By', 'Tue, 14 Jun 2016 07:00:00 -0500');
        });
    }*/
	public function myLocale()
	{
		if (Session::has('my.locale')) {
			$local = Session::get('my.locale');
		} else {
			$local = 'es';
		}
		return $local;
	}
	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */


	// Funciones para procesar contenidos
	public function convertArrayContents($array)
	{
		$sortArray = [];
		for ($i = 0; $i < count($array); $i++) {
			$sortArray[$array[$i]->code] = $array[$i];
		}
		return $sortArray;
	}

	public function mainRouter($_destino, $_subdestino = null, $_sitio = null)
	{

		echo 'Destino: ' . $_destino . '<br>';
		echo 'Sub Destino: ' . $_subdestino . '<br>';
		echo 'Sitio: ' . $_sitio . '<br>';
		Lang::setLocale('es');
		echo Lang::getLocale();
	}

	public function convertArrayMultimedias($array)
	{
		$sortArray = [];
		for ($i = 0; $i < count($array); $i++) {
			if (!isset($sortArray[$array[$i]->code])) {
				$sortArray[$array[$i]->code] = [];
			}
			array_push($sortArray[$array[$i]->code], $array[$i]);
		}
		return $sortArray;
	}


	// Vista de Pais
	public function getIndex()
	{
		$local = $this->myLocale();

		$page = Page::where('code', 'home')->with('contents', 'ubications', 'multimedia')->first();

		if (!$page) return view('errors.404');

		$aven = Page::where('code', 'aventura')->first();

		$arraysites = ['fiesta', 'fiesta-festival', 'fiesta-evento'];

		$contents = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());

		$hechoen = $page->childs()->where('type', 'hecho_enperu')->get();
		$fiesta = $page->pages()->whereIn('type', $arraysites)->get();
		$comments = Comment::where('page_id', $page->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$infomultimedia = $this->convertArrayContents($page->multimedia);
		$regiones = Page::where('type', 'region')->get();
		if($aven)
			$aventura = $aven->childs()->where('type', 'category')->get();
		else
			$aventura = [];
		$ubication = $page->ubications()->get();
		$selectfiestas = Page::whereIn('type', $arraysites)->whereNotIn('id', $fiesta->lists('id'))->get();
		//Get phrases
		$phrases = $page->childs()->where('type', 'frasedestacada')->get();
		$phrase = null;
		if (count($phrases) > 0) {
			$phrase = ['page' => $phrases[0]];
			$phrase['contents'] = $this->convertArrayContents($phrases[0]->contents()->where('lang', '=', $local)->get());
		}
		$imperdibles = $page->pages()->where('type', 'destino')->get();

		$data = [
			'phrase' => $phrase,
			'page' => $page,
			'contents' => $contents,
			'slider1' => WebSiteUtils::getMultimediasByCode("slider1"),
//            'slider2' => WebSiteUtils::getMultimediasByCode("slider2"),
			'imperdibles' => $imperdibles,
			'hechoen' => $hechoen,
			'fiestas' => $fiesta,
			'comments' => $comments,
			'infomultimedia' => $infomultimedia,
			'regiones' => $regiones,
			'aventuras' => $aventura,
			'ubication' => $ubication,
			'selectfiestas' => $selectfiestas,
			'availableDestinies' => Page::where('type', 'destino')->whereNotIn('id', $imperdibles->lists('id'))->get()
		];
		return view('website.index', $data);
	}

	// Vista de region
	public function getRegion($regionCode)
	{
		$local = $this->myLocale();
		$region = Page::where('code', $regionCode)->first();
		$array = ['destino', 'subdestino'];
		if ($region != null) {
			$infocontent = $this->convertArrayContents($region->contents()->where('lang', '=', $local)->get());
			$infomultimedia = $this->convertArrayContents($region->multimedia);
			$places = $region->pages()->whereIn('type', $array)->get();
			$fiestas = $region->pages()->where('type', 'fiesta')->get();
			$selectfiestas = Page::Where('type', 'fiesta')->whereNotIn('id', $fiestas->lists('id'))->get();
			$selectdestinys = Page::whereIn('type', $array)->whereNotIn('id', $places->lists('id'))->get();
		} else {
			$infocontent = [];
			$places = [];
			$fiestas = [];
			$infomultimedia = [];
			$selectdestinys = [];
			$selectfiestas = [];
		}
		$data = [
			"selectdestinys" => $selectdestinys,
			"selectfiestas" => $selectfiestas,
			"region" => $region,
			"contents" => $infocontent,
			"places" => $places,
            "page"=>$region,
			"Multimediaplaces" => [],
			'multimedia' => $infomultimedia,
			'ubication' => $region->childsubication()->whereIn('type', $array)->get(),
			'position' => $region->ubications()->get(),
			"fiestas" => $fiestas
		];
		return view('website/region', $data);
	}

	// Vista de destino

	public function getDestino($destinyCode)
	{
		$local = $this->myLocale();
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->with('banners.multimedia')->first();

		if (!$destiny) return view('errors.404');


		$multimediaplaces = [];
		$array = ['lat', 'lng'];
		$arraysites = ['fiesta', 'fiesta-festival', 'fiesta-evento'];

//		$places = $destiny->childs()->where('type', 'subdestino')->get();
//		$imperdibles = $destiny->childs()->where('type', 'imperdible')->get();
//		$hechoen = $destiny->childs()->where('type', 'hecho_engrupo')->get();
		$frasesdestacada = $destiny->childs()->where('type', 'frasedestacada')->get();
		$fiestas = [];
		/*foreach ($places as $plac) {
			array_push($fiestas, $plac->childs()->whereIn('type', $arraysites)->get());
		}*/

		$infocontent = $this->convertArrayContents($destiny->contents()->where('lang', '=', $local)->get());
		$countfrase = 0;
		$frasecontent = [];
		foreach ($frasesdestacada as $frasedestacada) {
			$frasecontent[$countfrase] = $this->convertArrayContents($frasedestacada->contents()->where('lang', '=', $local)->orderBy('id', 'asc')->get());
			$countfrase++;
		}

		$infomultimedia = $this->convertArrayContents($destiny->multimedia);
		$comments = Comment::where('page_id', $destiny->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$ubication = $destiny->childsubication()->where('type', 'subdestino')->get();
		$position = $destiny->ubications()->get();
		$infoitinerarios = $destiny->childs()->where('type', '=', 'itinerario')->orderByRaw("RAND()")->limit(3)->get();


		$data = [

			'itinerarios' => $infoitinerarios,
			'imperdibles' => [],
			'page' => $destiny,
			'places' => [],
			'hechoen' => [],
			'fiestas' => $fiestas,
			'contents' => $infocontent,
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'ubication' => $ubication,
			'frasedestacada' => $frasesdestacada,
			'frasecontent' => $frasecontent,
			'position' => $position,
			'isAdmin' => false,
			'favorite' => WebSiteUtils::getfavorite($destiny->id)
		];

		if(Auth::check())
			$data['isAdmin'] = Auth::user()->hasRole('admin');


		return view('website/destino', $data);
	}

	public function getComoLlegar($destinyCode)
	{
		$local = $this->myLocale();
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->with('banners')->first();
		foreach ($destiny->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}
		$infomultimedia = $this->convertArrayContents($destiny->multimedia);
		$infocontent = $this->convertArrayContents($destiny->contents()->where('lang', '=', $local)->get());
		$comments = Comment::where('page_id', $destiny->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'destiny' => $destiny,
            'page'=>$destiny,
			'places' => $destiny->childs()->where('type', 'subdestino')->get(),
			'comollegar_tierra' => $destiny->childs()->where('type', 'como_llegar-tierra-bus')->get(),
			'comollegar_tierraauto' => $destiny->childs()->where('type', 'como_llegar-tierra-auto')->get(),
			'comollegar_aire' => $destiny->childs()->where('type', 'como_llegar-aire')->get(),
			'multimedia' => $infomultimedia,
			'contents' => $infocontent
		];
		return view('website/comollegar', $data);
	}

	public function comollegar_desde()
	{
		$destiny = Page::where('code', Input::get('codedestiny'))->where('type', 'destino')->first();
		$comollegar = $destiny->childs()->where('type', Input::get('type'))->where('code', Input::get('code'))->first();
		$escalas = $comollegar->childs()->where('type', 'escala_punto')->get();
		foreach ($escalas as $escala) {
			$escala->infocontent = $this->convertArrayContents($escala->contents()->get());
			$escala->imgescala = $escala->getescala();
		}
		$infocontent = $this->convertArrayContents($comollegar->contents()->where('code', 'como-llegar')->get());
		$comollegar->contents = $infocontent;
		$comollegar->escalas = $escalas;
		$img = $comollegar->getescala();
		$comollegar->escala = $img;
		return $comollegar;
	}

	public function getAventura($destinyCode)
	{
		$local = $this->myLocale();
		$pageaventura = Page::where('code', 'aventura')->first();
		$categorias = $pageaventura->childs()->where('type', 'category')->get();
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->first();
		$destinys = $destiny->childs()->where('type', 'subdestino')->get();
		$count = 0;
		$sitios_id = [];
		foreach ($destinys as $dest) {
			$sitios = $dest->childs()->where('type', 'sitio')->get();
			foreach ($sitios as $sitio) {
				$sitios_id[$count] = $sitio->id;
				$count++;
			}
		}
		$count = 0;
		$categories = [];
		foreach ($categorias as $category) {
			$category->load(['childs' => function ($query) use ($sitios_id) {
				$query->whereIn('id', $sitios_id)->get();
			}])->get();
			if (count($category->childs) != 0) {
				$categories[$count] = $category;
				$count++;
			}

		}
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->with('banners')->first();

		foreach ($destiny->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}
		$frasesdestacada = $destiny->childs()->where('type', 'frasedestacada')->get();
		$countfrase = 0;
		$frasecontent = [];
		foreach ($frasesdestacada as $frasedestacada) {
			$frasecontent[$countfrase] = $this->convertArrayContents($frasedestacada->contents()->where('lang', '=', $local)->get());
			$countfrase++;
		}

		//$contents = WebSiteUtils::getContentsByType('destinies', $destiny->id)->get();
		$comments = Comment::where('page_id', $destiny->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'frasedestacada' => $frasesdestacada,
			'frasecontent' => $frasecontent,
			'destiny' => $destiny,
            'page'=>$destiny,
			'content' => $this->convertArrayContents($destiny->contents),
			'multimedias' => $this->convertArrayContents($destiny->multimedia),
			'comments' => $comments,

			'categories' => $categories
		];

		return view('website/aventura', $data);
	}

	public function getNaturaleza($destinyCode)
	{
		$local = $this->myLocale();
		$pagenaturaleza = Page::where('code', 'naturaleza')->first();
		$categorias = $pagenaturaleza->childs()->where('type', 'category')->get();
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->first();
		$destinys = $destiny->childs()->where('type', 'subdestino')->get();
		$count = 0;
		$sitios_id = [];
		foreach ($destinys as $dest) {
			$sitios = $dest->childs()->where('type', 'sitio')->get();
			foreach ($sitios as $sitio) {
				$sitios_id[$count] = $sitio->id;
				$count++;
			}
		}
		$count = 0;
		$categories = [];
		foreach ($categorias as $category) {
			$category->load(['childs' => function ($query) use ($sitios_id) {
				$query->whereIn('id', $sitios_id)->get();
			}])->get();
			if (count($category->childs) != 0) {
				$categories[$count] = $category;
				$count++;
			}

		}
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->with('banners')->first();

		foreach ($destiny->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}
		$frasesdestacada = $destiny->childs()->where('type', 'frasedestacada')->get();
		$countfrase = 0;
		$frasecontent = [];
		foreach ($frasesdestacada as $frasedestacada) {
			$frasecontent[$countfrase] = $this->convertArrayContents($frasedestacada->contents()->where('lang', '=', $local)->get());
			$countfrase++;
		}
		$contents = WebSiteUtils::getContentsByType('destinies', $destiny->id)->get();
		$comments = Comment::where('page_id', $destiny->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'frasedestacada' => $frasesdestacada,
			'frasecontent' => $frasecontent,
			'destiny' => $destiny,
            'page'=>$destiny,
			'content' => $this->convertArrayContents($destiny->contents),
			'multimedias' => $this->convertArrayContents($destiny->multimedia),
			'comments' => $comments,
			'categories' => $categories
		];
		return view('website/naturaleza', $data);
	}

	public function getCultura($destinyCode)
	{
		$local = $this->myLocale();
		$pagecultura = Page::where('code', 'cultura')->first();
		$categorias = $pagecultura->childs()->where('type', 'category')->get();
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->first();
		$destinys = $destiny->childs()->where('type', 'subdestino')->get();
		$count = 0;
		$sitios_id = [];
		foreach ($destinys as $dest) {
			$sitios = $dest->childs()->where('type', 'sitio')->get();
			foreach ($sitios as $sitio) {
				$sitios_id[$count] = $sitio->id;
				$count++;
			}
		}
		$count = 0;
		$categories = [];
		foreach ($categorias as $category) {
			$category->load(['childs' => function ($query) use ($sitios_id) {
				$query->whereIn('id', $sitios_id)->get();
			}])->get();
			if (count($category->childs) != 0) {
				$categories[$count] = $category;
				$count++;
			}

		}
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->with('banners')->first();

		foreach ($destiny->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}

		$frasesdestacada = $destiny->childs()->where('type', 'frasedestacada')->get();
		$countfrase = 0;
		$frasecontent = [];
		foreach ($frasesdestacada as $frasedestacada) {
			$frasecontent[$countfrase] = $this->convertArrayContents($frasedestacada->contents()->where('lang', '=', $local)->get());
			$countfrase++;
		}

		//$contents = WebSiteUtils::getContentsByType('destinies', $destiny->id)->get();
		$comments = Comment::where('page_id', $destiny->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'frasedestacada' => $frasesdestacada,
			'frasecontent' => $frasecontent,
			'destiny' => $destiny,
            'page'=>$destiny,
			'content' => $this->convertArrayContents($destiny->contents),
			'multimedias' => $this->convertArrayContents($destiny->multimedia),
			'comments' => $comments,
			'categories' => $categories,
		];
		return view('website/cultura', $data);
	}


	public function getDescanso($destinyCode)
	{
		$local = $this->myLocale();
		$pagedescanso = Page::where('code', 'descanso')->first();
		$categorias = $pagedescanso->childs()->where('type', 'category')->get();
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->first();
		$destinys = $destiny->childs()->where('type', 'subdestino')->get();
		$count = 0;
		$sitios_id = [];
		foreach ($destinys as $dest) {
			$sitios = $dest->childs()->where('type', 'sitio')->get();
			foreach ($sitios as $sitio) {
				$sitios_id[$count] = $sitio->id;
				$count++;
			}
		}
		$count = 0;
		$categories = [];
		foreach ($categorias as $category) {
			$category->load(['childs' => function ($query) use ($sitios_id) {
				$query->whereIn('id', $sitios_id)->get();
			}])->get();
			if (count($category->childs) != 0) {
				$categories[$count] = $category;
				$count++;
			}

		}
		$destiny = Page::where('code', $destinyCode)->where('type', 'destino')->with('banners')->first();

		foreach ($destiny->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}

		$frasesdestacada = $destiny->childs()->where('type', 'frasedestacada')->get();
		$countfrase = 0;
		$frasecontent = [];
		foreach ($frasesdestacada as $frasedestacada) {
			$frasecontent[$countfrase] = $this->convertArrayContents($frasedestacada->contents()->where('lang', '=', $local)->get());
			$countfrase++;
		}

		//$contents = WebSiteUtils::getContentsByType('destinies', $destiny->id)->get();
		$comments = Comment::where('page_id', $destiny->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'frasedestacada' => $frasesdestacada,
			'frasecontent' => $frasecontent,
			'destiny' => $destiny,
            'page'=>$destiny,
			'content' => $this->convertArrayContents($destiny->contents),
			'multimedias' => $this->convertArrayContents($destiny->multimedia),
			'comments' => $comments,
			'categories' => $categories
		];
		return view('website/descansoarequipa', $data);
	}


	// Vistas de Sub Destino
	public function getSubDestino($siteCode){
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'subdestino')->first();

		if (!$place) return view('errors.404');

		// $father = DB::table('page_page')->join('pages', 'page_page.page_id', '=', 'pages.id')->where('page_page.child_id', '=', $place->id)->first();
	//	$father = Page::where('id', $place->parent_id)->where('type', 'destino')->first();

		$contents = Page::where('code', $siteCode)->where('type', 'subdestino')->with('contents', 'multimedia')->first();
		$places = $place->childs()->where('type', 'sitio')->get();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$frasesdestacada = $place->childs()->where('type', 'frasedestacada')->get();
		$countfrase = 0;
		$frasecontent = [];
		foreach ($frasesdestacada as $frasedestacada) {
			$frasecontent[$countfrase] = $this->convertArrayContents($frasedestacada->contents()->where('lang', '=', $local)->get());
			$countfrase++;
		}
		$data = [
			'frasedestacada' => $frasesdestacada,
			'frasecontent' => $frasecontent,
            'page'=>$place,
            'page'=>$place,
			'place' => $place,
			'contents' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'places' => $places,
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'ubication' => $place->childsubication()->where('type', 'sitio')->get(),
			'position' => $place->ubications()->get(),
			'isAdmin' => false,
			'favorite' => WebSiteUtils::getfavorite($place->id)
		];
		if(Auth::check())
			$data['isAdmin'] = Auth::user()->hasRole('admin');
		return view('website/lugar', $data);
	}

	public function getSubDestinoComer($siteCode)
	{
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'subdestino')->with('banners')->first();
		foreach ($place->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		};
		$contents = Page::where('code', $siteCode)->where('type', 'subdestino')->with('contents', 'multimedia')->first();

		$places = $place->childs()->where('type', 'sitio-comer')->get();
		$places2 = $place->childs()->where('type', 'sitio-experto')->get();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'place' => $place,
            'page'=>$place,
			'content' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'places' => $places,
			'places2' => $places2,
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'ubication' => $place->childsubication()->where('type', 'sitio-comer')->get(),
			'position' => $place->ubications()->get()
		];
		return view('website/centrocomer', $data);
	}

	public function getSubDestinoHospedaje($siteCode)
	{
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'subdestino')->with('banners')->first();
		foreach ($place->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}
		$contents = Page::where('code', $siteCode)->where('type', 'subdestino')->with('contents', 'multimedia')->first();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();

		$data = [
			'place' => $place,
            'page'=>$place,
			'content' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'multimedia' => $infomultimedia,
			'hospedajeEspeciales' => $place->childs()->where('type', 'sitio-hospedaje-especiales')->get(),
			'hospedajeHoteles' => $place->childs()->where('type', 'sitio-hospedaje-hoteles')->get(),
			'hospedajeHostales' => $place->childs()->where('type', 'sitio-hospedaje-hostales')->get(),
			'hospedajeCasa' => $place->childs()->where('type', 'sitio-hospedaje-casa')->get(),
			'hospedajeMochileros' => $place->childs()->where('type', 'sitio-hospedaje-mochileros')->get(),
			'comments' => $comments,
			'position' => $place->ubications()->get()
		];
		return view('website/centrohospedaje', $data);
	}

	public function getSubDestinoComprar($siteCode)
	{
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'subdestino')->with('banners')->first();
		foreach ($place->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}
		$contents = Page::where('code', $siteCode)->where('type', 'subdestino')->with('contents', 'multimedia')->first();
		$places = $place->childs()->where('type', 'sitio-compras')->get();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();

		$data = [
			'place' => $place,
            'page'=>$place,
			'content' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'places' => $places,
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'ubication' => $place->childsubication()->where('type', 'sitio-compras')->get(),
			'position' => $place->ubications()->get()
		];

		return view('website/dondecomprar', $data);
	}

	public function getSubDestinoFiesta($siteCode)
	{
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'subdestino')->with('banners')->first();
		foreach ($place->banners as $valdestiny) {
			$valdestiny->load('multimedia');
		}
		//$father = DB::table('page_page')->join('pages', 'page_page.page_id', '=', 'pages.id')->where('page_page.child_id', '=', $place->id)->first();

		$contents = Page::where('code', $siteCode)->where('type', 'subdestino')->with('contents', 'multimedia')->first();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();

		$data = [
			'place' => $place,
            'page'=>$place,
			'contents' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'fiesta' => $place->childs()->where('type', 'fiesta')->get(),
			'festival' => $place->childs()->where('type', 'fiesta-festival')->get(),
			'convencion' => $place->childs()->where('type', 'fiesta-evento')->get()
			//'father' => $father
		];

		return view('website/fiesta', $data);
	}

	// Fin de vista de Sub Destino

	public function getParallax($siteCode)
	{
		$local = $this->myLocale();
		$array = ['hecho_en', 'sitio-compras', 'sitio-comer', 'sitio-hospedaje-especiales', 'sitio-hospedaje-casa', 'sitio-hospedaje-hoteles', 'sitio-hospedaje-hostales', 'sitio-hospedaje-mochileros', 'sitio', 'fiesta', 'fiesta-festival', 'fiesta-evento'];
		$place = Page::where('code', $siteCode)->whereIn('type', $array)->first();
		$contents = Page::where('code', $siteCode)->whereIn('type', $array)->with('contents', 'multimedia')->first();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'place' => $place,
            'page'=>$place,
			'contents' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'parallax' => $contents->multimedia()->where('code', 'img')->get(),
			'multimedia' => $infomultimedia,
			'comments' => $comments
		];
		return view('website/parallax', $data);
	}

	public function getHechoen($siteCode = null)
	{
		$local = $this->myLocale();
		$array = ['hecho_enperu', 'hecho_engrupo'];
		$page = Page::where('code', $siteCode)->whereIn('type', $array)->first();
		if (!isset($page)) {
			return view('errors/404');
		}
		$content = Page::where('code', $siteCode)->whereIn('type', $array)->first();
        if($page->type=='hecho_engrupo')
        {
            $childs = $page->childs()->where('type', 'hecho_en')->get();
        }
        else
        {
            $childs = $page->pages()->where('type', 'hecho_en')->get();
        }

		$infomultimedia = $this->convertArrayContents($page->multimedia);
		$selecthechoen = Page::where('type', 'hecho_en')->whereNotIn('id', $childs->lists('id'))->get();
		$positions = $page->ubications()->get();

		$infocontent = null;
		if ($content != null) {
			$infocontent = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());
		}

		$data = [
			'position' => $positions,
			'contents' => $infocontent,
			'multimedia' => $infomultimedia,
			'page' => $page,
			'childs' => $childs,
			'selecthechoen' => $selecthechoen
		];

		return view('website/hechoen', $data);
	}

	public function getParallaxhechoen($siteCode)
	{
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'hecho_en')->first();
		$father[1] = DB::table('page_page')->join('pages', 'page_page.page_id', '=', 'pages.id')->where('page_page.child_id', '=', $place->id)->first();
		$contents = Page::where('code', $siteCode)->where('type', 'hecho_en')->with('contents', 'multimedia')->first();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'place' => $place,
            'page'=>$place,
			'contents' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'parallax' => $contents->multimedia()->where('code', 'img')->get(),
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'father' => $father
		];
		return view('website/parallax', $data);
	}

	public function getParallaxfestival($siteCode)
	{
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'fiesta-festival')->orWhere('type', 'fiesta')->first();
		$father[1] = DB::table('page_page')->join('pages', 'page_page.page_id', '=', 'pages.id')->where('page_page.child_id', '=', $place->id)->first();

		$contents = Page::where('code', $siteCode)->where('type', 'fiesta-festival')->orWhere('type', 'fiesta')->with('contents', 'multimedia')->first();

		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'place' => $place,
            'page'=>$place,
			'contents' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'parallax' => $contents->multimedia()->where('code', 'img')->get(),
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'father' => $father
		];
		return view('website/parallax', $data);
	}

	public function getParallaxevento($siteCode)
	{
		$local = $this->myLocale();
		$place = Page::where('code', $siteCode)->where('type', 'fiesta-evento')->first();
		$father[1] = DB::table('page_page')->join('pages', 'page_page.page_id', '=', 'pages.id')->where('page_page.child_id', '=', $place->id)->first();
		$contents = Page::where('code', $siteCode)->where('type', 'fiesta-evento')->with('contents', 'multimedia')->first();
		$infomultimedia = $this->convertArrayContents($place->multimedia);
		$comments = Comment::where('page_id', $place->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		$data = [
			'place' => $place,
            'page'=>$place,
			'contents' => $this->convertArrayContents($contents->contents()->where('lang', '=', $local)->get()),
			'parallax' => $contents->multimedia()->where('code', 'img')->get(),
			'multimedia' => $infomultimedia,
			'comments' => $comments,
			'father' => $father
		];
		return view('website/parallax', $data);
	}

	// Categorias
	public function getCategoria(Request $r, $categoryCode)
	{
		//TODO: El locale se debe setear en un middleware considerando el idioma del usuario si es que está logueado
		App::setLocale($r->session()->get('app.locale', 'es'));

		$page = Page::where('code', $categoryCode)->with('contents', 'multimedia', 'childs.contents')->first();
		$isAdmin = Auth::check()?Auth::user()->hasRole('admin'):false;

		if(!$page) return view('errors.404', ['isAdmin' => $isAdmin]);

		$categories = $page->childs;
        $selectsites=Page::where('type', 'sitio')->get();
		foreach($categories as $cat)
		{
			$cat->contents = WebSiteUtils::orderArrayByCode($cat->contents);
		}

		$data = [
			'page' => $page,
			'c' => WebSiteUtils::orderArrayByCode($page->contents),
			'm' => WebSiteUtils::orderArrayByCode($page->multimedia),
            'categories'=> $categories,
            'selectsites'=>$selectsites,
			'isAdmin' => $isAdmin
		];

		return view('website.category', $data);
	}

	public function getCategoriax($categoryCode)
	{
		$local = $this->myLocale();
		if ($categoryCode == 'aventura') {
			$page = Page::where('code', 'aventura')->first();
			$content = $page->with('contents', 'multimedia')->first();

			if ($content != null) {
				$infocontent = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());
				$infomultimedia = $this->convertArrayContents($page->multimedia);
				$categorias = $page->childs()->where('type', 'category')->get();

			} else {
				$infocontent = [];
				$infomultimedia = [];
				$categorias = [];

			}
			$data = [
				'page' => $page,
				'content' => $infocontent,
				'infomultimedia' => $infomultimedia,
				'categorias' => $categorias

			];

			return view('website/homeaventura', $data);

		} else if ($categoryCode == 'naturaleza') {
			$page = Page::where('code', 'naturaleza')->first();
			$content = $page->with('contents', 'multimedia')->first();

			if ($content != null) {
				$infocontent = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());
				$infomultimedia = $this->convertArrayContents($page->multimedia);
				$categorias = $page->childs()->where('type', 'category')->get();

			} else {
				$infocontent = [];
				$infomultimedia = [];
				$categorias = [];

			}
			$data = [
				'page' => $page,
				'content' => $infocontent,
				'infomultimedia' => $infomultimedia,
				'categorias' => $categorias,


			];
			return view('website/homenaturaleza', $data);
		}
		if ($categoryCode == 'cultura') {
			$page = Page::where('code', 'cultura')->first();
			$content = $page->with('contents', 'multimedia')->first();

			if ($content != null) {
				$infocontent = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());
				$infomultimedia = $this->convertArrayContents($page->multimedia);
				$categorias = $page->childs()->where('type', 'category')->get();
			} else {
				$infocontent = [];
				$infomultimedia = [];
				$categorias = [];

			}
			$data = [
				'page' => $page,
				'content' => $infocontent,
				'infomultimedia' => $infomultimedia,
				'categorias' => $categorias,


			];
			return view('website/homecultura', $data);
		}

		if ($categoryCode == 'descanso') {

			$page = Page::where('code', 'descanso')->first();
			$content = $page->with('contents', 'multimedia')->first();

			if ($content != null) {
				$infocontent = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());
				$infomultimedia = $this->convertArrayContents($page->multimedia);
				$categorias = $page->childs()->where('type', 'category')->get();

			} else {
				$infocontent = [];
				$infomultimedia = [];
				$categorias = [];

			}
			$data = [
				'page' => $page,
				'content' => $infocontent,
				'infomultimedia' => $infomultimedia,
				'categorias' => $categorias,


			];

			return view('website/homedescanso', $data);
		}

	}

	// Vistas de informacion general

	public function getSobrePeru()
	{
		// $countryCode = 'sobre-peru';
		// $country = WebSiteUtils::getPage($countryCode);
		// if ($country != null) {
		// } else {
		// 	$contentinfo = [];
		// }

		$lang = $this->myLocale();

		$page = Page::where('code', 'sobre-peru')->with('contents', 'multimedia')->first();
		$contents = $this->convertArrayContents($page->contents()->where('lang', '=', $lang)->get());
		$multimediainfo = $this->convertArrayContents($page->multimedia);
        $sobreperu_items = $page->childs()->where('type', 'sobreperu_item')->get();
        foreach($sobreperu_items as $item)
        {
            $item->contents=$this->convertArrayContents($item->contents()->where('lang', '=', $lang)->get());
        }
		$data = [
			'page' => $page,
			'content' => $contents,
            'sobreperu_items'=>$sobreperu_items,
			'multimediainfo' => $multimediainfo,
			'isAdmin' => false
		];

		if(Auth::check())
			$data['isAdmin'] = Auth::user()->hasRole('admin');

		return view('website/sobreperu', $data);
	}

	public function getBancoFotos()
	{
		return view('website/bancofotos');
	}

	public function getQuienesSomos()
	{
		$local = $this->myLocale();
		$page = Page::where('code', 'quienes-somos')->first();

		if (!$page) return view('errors.404');

		$content = $page->with('contents', 'multimedia')->first();

		if ($content != null) {
			$infocontent = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());
			$infomultimedia = $this->convertArrayContents($page->multimedia);
			//$categorias = $page->childs()->where('type','category')->get();
		} else {
			$infocontent = [];
			$infomultimedia = [];
			//$categorias = [];
		}
		$data = [
            'page'=>$page,
			'content' => $infocontent,
			'multimedia' => $infomultimedia,
			'isAdmin' => \Auth::check()?\Auth::user()->hasRole('admin'):false
		];
		return view('website/quienessomos', $data);
	}

	public function getRevistas()
	{
		return view('website/revistas');
	}

	public function getFormas()
	{
		return view('website/formas');
	}

	public function saveAvatar()
	{
		if (Input::hasFile('file')) {
			$ext = Input::file('file')->getClientOriginalExtension();
			$path = 'uploads/';
			$filename = date('dm') . '_' . uniqid() . '.' . $ext;
			$f = Input::file('file')->move($path, $filename);
			$source = $path . $filename;
			$size = getimagesize($source);
			if (count($size) != 0) {
				$data['width'] = $size[0];
				$data['height'] = $size[1];
			}
            $img = Image::open($source);
            $img->cropResize(500);
            $img->save($source, $img->guessType(), 100);
		}
		if (isset(Auth::user()->multimedia[0])) {
			$m = Multimedia::find(Auth::user()->multimedia[0]->id);
			$m->type = 'image';
			$m->code = 'avatar';
			if (isset($source)) {
				if (!empty($m->source)) {
					if (file_exists($m->source))
						unlink($m->source);
				}
				$m->source = $source;
			}
			$m->save();
		} else {
			if (isset($source)) $data['source'] = $source;
			$data['page_id'] = Auth::user()->id;
			$p = User::find($data['page_id']);
			$m = $p->multimedia()->save(Multimedia::create($data));
		}
	}

	public function getPerfil()
	{
		if (Auth::check()) {
			$destinies = Page::where('type', 'destino')->get();
			$grav_url = WebSiteUtils::getAvatar(Auth::user()->id);
			$cantidad = 0;
			$cantitad_user = 0;
			$id = Auth::user()->id;
			$favorites = Favorite::where('user_id', $id)->get();
			$comments_id = Comment::distinct()->select('page_id')->where('user_id', $id)->get();
			$favorites_page = Page::whereIn('id', $favorites->lists('page_id'))->get();
			foreach ($destinies as $destiny) {
				$cantidad = $cantidad + $destiny->childs()->where('type', 'subdestino')->count();
				$cantitad_user = $cantitad_user + $destiny->childs()->where('type', 'subdestino')->whereIn('id', $comments_id->lists('page_id'))->count();
			}
			$porcentaje = round(($cantitad_user * 100) / $cantidad);
			return view('website/usuario', ['conoce' => $porcentaje, 'favorites' => $favorites_page, 'profile_img' => $grav_url]);
		} else {
			return redirect('/');
		}
	}

	public function getItinerario($destinycode, $itinerarioCode)
	{

		$local = $this->myLocale();
		$destiny = Page::where('code', $destinycode)->where('type', 'destino')->first();
		$itinerario = Page::where('code', $itinerarioCode)->where('type', 'itinerario')->first();

		$imperdibles = $destiny->childs()->where('type', 'imperdible')->get();
		$infoitinerarios = $destiny->childs()->where('type', '=', 'itinerario')->orderByRaw("RAND()")->limit(3)->get();
		$frasesdestacada = $itinerario->childs()->where('type', 'frasedestacada')->get();
		$selectsites = Page::Where('type', 'sitio')->get();


		if ($itinerario != null) {

			$sites = $itinerario->Childs()->where('type', 'sitio')->get();
			$days = $itinerario->Childs()->where('type', 'itinerario_day')->get();
			foreach ($days as $day) {
				$day->activitys = $day->Childs()->where('type', 'activity_day')->get();
			}
			$infocontent = $this->convertArrayContents($itinerario->contents()->where('lang', '=', $local)->get());
			$infomultimedia = $this->convertArrayContents($itinerario->multimedia);
			$sites->load(['contents' => function ($query) use ($local) {
				$query->where('lang', $local)->where('code', 'text-itinerario');
			}]);
			$sites->load(['multimedia' => function ($query) {
				$query->where('code', 'cover');
			}]);

		} else {
			$infoitinerarios = [];
			$imperdibles = [];
			$sites = [];
			$infocontent = [];
			$infomultimedia = [];
			$frasesdestacada = [];
			$selectsites = [];
		}

		$data = [
            'page'=>$destiny,
			'destinycode' => $destinycode,
			'itinerarios' => $infoitinerarios,
			'imperdibles' => $imperdibles,
			'destiny' => $destiny,
			'frasedestacada' => $frasesdestacada,
			'itinerario' => $itinerario,
			'places' => $sites,
			'selectsites' => $selectsites,
			'content' => $infocontent,
			'multimedia' => $infomultimedia,
			'dias' => $days,
		];

		return view('website/itinerario', $data);
	}

	public function getSieteformas()
	{

		$local = $this->myLocale();
		$page = Page::where('code', 'siete-formas')->where('type','')->first();
		$content = null;
		if (isset($page)) {
			$content = $page->with('contents', 'multimedia')->first();
            $comments = Comment::where('page_id', $page->id)->where('visible',1)->orderBy('created_at', 'desc')->get();
		}
		if ($content != null) {
			$infocontent = $this->convertArrayContents($page->contents()->where('lang', '=', $local)->get());
			$infomultimedia = $this->convertArrayContents($page->multimedia);
			$childs = $page->childs()->where('type', 'siete_formas')->get();
            foreach($childs as $child)
            {
                $child->content = $this->convertArrayContents($child->contents()->where('lang', '=', $local)->get());
            }
		} else {
			$infocontent = [];
			$infomultimedia = [];
			$childs = [];
            $comments=[];
		}

		$data = [
			'content' => $infocontent,
			'multimedia' => $infomultimedia,
            'comments'=>$comments,
			'page' => $page,
			'childs' => $childs
		];
		return view('website/sieteformas', $data);
	}

	public function getBusqueda($search = null)
	{
		$search = trim($search);
		$keywords = preg_split('/[\ \n\,]+/', $search);
		$result = Page::select('name', 'code', 'type', 'description', 'parent_id')->whereNotIn('type', ['category', 'frasedestacada'])
			->whereNested(function ($query) use ($keywords) {
				foreach ($keywords as $keyword) {
					$query->where("name", "LIKE","%$keyword%")
                        ->orWhere("type", "LIKE", "%$keyword%");
				}
			})->get()->toArray();

		return view('website/search', ['results' => $result, 'search' => $search]);


	}

	public function saveFavorite()
	{
		$actual = 2;
		if (Auth::check() && Input::has('page_id')) {
			$id = Auth::user()->id;
			$favorite = Favorite::where('page_id', Input::get('page_id'))->where('user_id', $id)->count();
			if ($favorite > 0) {
				$favorite = Favorite::where('page_id', Input::get('page_id'))->where('user_id', $id)->delete();
				$actual = 0;
			} else {
				$favorite = new Favorite();
				$favorite->user_id = $id;
				$favorite->page_id = Input::get('page_id');
				$actual = 1;
				$favorite->save();
			}
		}
		return $actual;
	}
    public function anyCategoryList()
    {
        $category=Page::find(Input::get('category_id'));
        $places = $category->pages()->where('type', 'sitio')->get();
        $position=$category->ubications()->get();
        $lat=  (count($position)>0)?$position[0]->content:'-12.046623';
        $lon=  (count($position)>1)?$position[1]->content:'-77.042828';
        $zoom=  (count($position)>2)?$position[2]->content:'8';
        foreach ($places as $place) {
            $place->cover=$place->getImageCropImg('cover');
            $place->codeheader=$category->code;
            $place->category_lat=$lat;
            $place->category_lon=$lon;
            $place->category_zoom=$zoom;
            (count($place->ubications)>0)?$place->lat=$place->ubications[0]->content:$place->lat='-12.601475166388834';
            (count($place->ubications)>0)?$place->lon=$place->ubications[1]->content:$place->lon='-75.11077880859375';
        }
        return $places;
    }
}
